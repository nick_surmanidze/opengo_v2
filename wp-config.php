<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'opengo');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'root');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'root');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^1=XUGdAyG<]y:r8)(&TKG|4?_sNY{GlvMFwJ`G#Y|:5RBQYg+_/CV;)WH,DE#BR');
define('SECURE_AUTH_KEY',  'LR;|,<H*bE01PZn-,,5a.&x;cSihZZ?W9(xf)6#&To-K9+RVJp2A-zg}<[*G-3.d');
define('LOGGED_IN_KEY',    'i!uBJ&de.|ZE7yn+| erJWw>5oCHooc>6|-BM8l3^mR9X U2`|7{-!3#>UtYn;Yi');
define('NONCE_KEY',        'mwP>{-m$4g>D*nUki0V_W&X./hzIyr!onGgdp+a(%ekJbPYM-=-Y21X29%|m`C|X');
define('AUTH_SALT',        'T!-^P_wx7WZa tl9F|+H%ze)!KMIzoAD8V-2k`@hgU@7t7.-P|?tp{LUGN<)Im1r');
define('SECURE_AUTH_SALT', 'oL#w.f$W;T|_E K-+^3oe3u8nLWGX:XDwp[=*G[)%{<5)GhSuHT-)q#Vxm+qtH!_');
define('LOGGED_IN_SALT',   'hyOk~lzMP@1m>~B/i4VF|]:NAiN8[KN*rcA IFe/2V&xRw+qYCk,_tS82cw7z3BM');
define('NONCE_SALT',       'l;7R%a=)adoe?Nt7|qW?MNt-OI]Z<(IXs:S^Rtoz>5o]k=%pd^PXfygj-0%bCf`_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'og_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');


/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */
if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wpcore');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '');
}
if (!defined('WP_CONTENT_DIR')) {
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/app');
}
if (!defined('WP_CONTENT_URL')) {
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/app');
}


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
