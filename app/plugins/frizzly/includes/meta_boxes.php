<?php
function frizzly_post_settings_get_default_meta(){
	$defaults = array(
		'ignore' => array()
	);

	$activateable_modules_list = Frizzly()->get_module_manager()->get_activateable_modules_list();
	foreach($activateable_modules_list as $module_tag => $module_name){
		$defaults['ignore'][$module_tag] = '0';
	}

	return $defaults;
}


function frizzly_post_settings_add_meta_box() {

	$screens = array( 'post', 'page' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'frizzly_post_settings',
			__( 'Frizzly Settings', 'frizzly' ),
			'frizzly_post_settings_meta_box_callback',
			$screen,
			'side'
		);
	}
}
add_action( 'add_meta_boxes', 'frizzly_post_settings_add_meta_box' );

/**
 * @param int $post_id Post ID.
 */
function frizzly_post_settings_get_meta( $post_id ){
	$defaults = frizzly_post_settings_get_default_meta();
	$meta = get_post_meta( $post_id, '_frizzly_settings', true);
	return empty($meta) ? $defaults : Frizzly_Utils::array_merge_map_recursive($defaults, $meta);
}

/**
 * @param WP_Post $post The object for the current post/page.
 */
function frizzly_post_settings_meta_box_callback( $post ) {
	$meta = frizzly_post_settings_get_meta($post->ID);
	$activeatable_modules = Frizzly()->get_module_manager()->get_activateable_modules_list();
	include_once( Frizzly_Constants::get_plugin_dir() . 'includes/post_settings_meta_view.php');
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function frizzly_post_settings_save_meta( $post_id ) {

	// Check if our nonce is set.
	if ( !isset( $_POST['frizzly_settings_meta_box_nonce'] )
			|| !wp_verify_nonce( $_POST['frizzly_settings_meta_box_nonce'], 'frizzly_settings_meta_box' )
			|| ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			|| ! current_user_can( 'edit_post', $post_id )
	)
		return;

	if ( !isset( $_POST['frizzly_settings'] ) ) {
		delete_post_meta( $post_id, '_frizzly_settings' );
	} else {
		$settings = wp_parse_args($_POST['frizzly_settings']);
		update_post_meta( $post_id, '_frizzly_settings', $settings );
	}

}
add_action( 'save_post', 'frizzly_post_settings_save_meta' );
