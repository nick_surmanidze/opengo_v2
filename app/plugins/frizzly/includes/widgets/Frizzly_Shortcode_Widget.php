<?php

class Frizzly_Shortcode_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'frizzly',
			__( 'Frizzly Shortcode', 'frizzly' ),
			array(
				'classname' => 'frizzly_shortcode_widget-class',
				'description' => __( 'Share icons.', 'frizzly' )
			)
		);
	}

	/**
	 * @param array args The array of form elements
	 * @param array instance The current instance of the widget
	 */
	public function widget( $args, $instance ) {

		extract( $args, EXTR_SKIP );
		$defaults = $this->get_defaults();
		$instance = wp_parse_args( (array) $instance, $defaults);
		$shortcode_id = $instance['shortcode_id'];

		$widget_string = $before_widget;
		$widget_string .= $before_title . apply_filters('the_title', $instance['title'] ) . $after_title;
		$widget_string .= do_shortcode("[frizzly_shortcode id='$shortcode_id' widget='true']");
		$widget_string .= $after_widget;

		print $widget_string;
	}

	public function form( $instance ) {
		$defaults = $this->get_defaults();
		$instance = wp_parse_args( (array) $instance, $defaults);
		$available_shortcodes = frizzly_shortcode_get_list();

		include( Frizzly_Constants::get_plugin_dir() . 'includes/widgets/views/shortcode_admin_view.php');
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['shortcode_id'] = $new_instance['shortcode_id'];
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}

	private function get_defaults(){
		$defaults = array(
			'shortcode_id' => '-1',
			'title' =>  __('Share this article', 'frizzly')
		);
		return $defaults;
	}

} // end class
add_action( 'widgets_init', create_function( '', 'register_widget("Frizzly_Shortcode_Widget");' ) );
