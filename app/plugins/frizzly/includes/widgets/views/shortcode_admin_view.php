<p>
	<?php _e('Title', 'frizzly'); ?>: <input class="widefat" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>">
</p>
<p>
<?php if( count($available_shortcodes) == 0) { ?>
		<?php _e('There are no shortcodes created yet. Go <a href="edit.php?post_type=frizzly_shortcode">create one</a> and come back here.', 'frizzly'); ?>
<?php }  else { ?>
	<?php _e('Shortcode', 'frizzly'); ?>:
	<select name="<?php echo $this->get_field_name('shortcode_id'); ?>" class="widefat">
	<?php foreach($available_shortcodes as $id => $name) { ?>
		<option value="<?php echo $id; ?>" <?php selected($id, $instance['shortcode_id']); ?>><?php echo $name; ?></option>
	<?php } ?>
	</select>
<?php } ?>
</p>