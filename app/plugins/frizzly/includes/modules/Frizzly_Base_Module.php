<?php

interface iFrizzly_Base_Module {
	public function get_module_tag();
	public function get_module_name();
	public function get_default_settings();
	public function get_registered_settings();
	public function get_boolean_settings_ids();
	public function get_registered_settings_header();
	public function content_filter( $content );
	public function get_type_for_setting( $setting_name );
	public function is_active();
	public function copy_settings_to_js();
}

interface iFrizzly_Activateable_Module {
	public function use_for_current_request();
}

abstract class Frizzly_Base_Module implements iFrizzly_Base_Module {

	public function get_module_settings(){
		global $frizzly_options;
		return $frizzly_options[$this->get_module_tag()];
	}

	public function get_boolean_settings_ids() {
		$settings_with_types = $this->get_settings_with_types();
		return array_keys($settings_with_types, 'checkbox');
	}

	public function is_active(){
		return true;
	}

	public function content_filter( $content ) {
		return $content;
	}

	public function get_type_for_setting( $setting_name ){
		$settings_with_types = $this->get_settings_with_types();
		if (isset( $settings_with_types[ $setting_name]))
			return $settings_with_types[ $setting_name];
		return false;
	}

	public function copy_settings_to_js() {
		return true;
	}

	private function get_settings_with_types(){

		$settings = array();

		foreach ( $this->get_registered_settings() as $key => $value ) {

			switch( $value['type']){
				case 'header':
					break;
				case 'multiple_checkboxes':
					foreach(array_keys( $value['options']) as $checkbox_key)
						$settings[$checkbox_key] = 'checkbox';
					break;
				default:
					$settings[ $key ] = $value['type'];
			}
		}

		return $settings;
	}
}

abstract class Frizzly_Activateable_Module extends Frizzly_Base_Module implements iFrizzly_Activateable_Module {

	public function use_for_current_request(){
		global $post;

		if (false == parent::is_active())
			return false;

		$current_post_type = get_post_type( $post->ID );
		$ignored_post_types = apply_filters( 'frizzly_' . $this->get_module_tag() . '_ignored_post_types', array() );
		if ( in_array( $current_post_type, $ignored_post_types ) ) {
			return false;
		}

		$meta = frizzly_post_settings_get_meta($post->ID);
		return  '0' == $meta['ignore'][$this->get_module_tag()];
	}
}

