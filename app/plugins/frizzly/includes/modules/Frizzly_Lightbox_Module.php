<?php

class Frizzly_Lightbox_Module extends Frizzly_Activateable_Module {

	/* Public functions */
	public function get_module_tag() {
		return 'lightbox';
	}

	public function get_module_name() {
		return __('Lightbox Module', 'frizzly');
	}

	public function get_default_settings() {
		$settings = array(
			'descriptionSource' => array( 'titleAttribute', 'altAttribute' )
		);
		return $settings;
	}

	public function get_registered_settings() {
		$registered_settings = array(
			'descriptionSource' => array(
				'id'      => 'descriptionSource',
				'name'    => __( 'Description Source', 'frizzly' ),
				'type'    => 'sorted_list',
				'desc'    => __( 'Image description is shown in the lightbox window. Drag and drop buttons in order to choose which source should be prioritized.', 'frizzly' ),
				'options' => $this->get_description_source_options()
			)
		);

		return $registered_settings;
	}

	public function get_registered_settings_header() {
		return __("Lightbox", 'frizzly');
	}

	public function content_filter( $content ) {
		global $post;
		// universal IMG-Tag pattern matches everything between "<img" and the closing "(/)>"
		// will be used to match all IMG-Tags in Content.
		$imgPattern = "/<img([^\>]*?)>/i";
		if (preg_match_all($imgPattern, $content, $imgTags)) {
			foreach ($imgTags[0] as $imgTag) {
				// only work on imgTags that do not already contain the string "colorbox-"
				if (!preg_match('/frizzly-group-/i', $imgTag)) {
					if (!preg_match('/class=/i', $imgTag)) {
						// imgTag does not contain class-attribute
						$pattern = $imgPattern;
						$replacement = '<img class="frizzly-group-' . $post->ID . '" $1>';
					}	else {
						// imgTag already contains class-attribute
						$pattern = "/<img(.*?)class=('|\")([A-Za-z0-9 \/_\.\~\:-]*?)('|\")([^\>]*?)>/i";
						$replacement = '<img$1class=$2$3 frizzly-group-' . $post->ID . '$4$5>';
					}
					$replacedImgTag = preg_replace($pattern, $replacement, $imgTag);
					$content = str_replace($imgTag, $replacedImgTag, $content);
				}
			}
		}
		return $content;
	}

	public function is_active() {
		global $frizzly_options;
		return $frizzly_options['main']['moduleLightboxActive'] == '1';
	}

	/* Private functions */

	function get_description_source_options() {

		$available_options = array(
			'titleAttribute'   => __( 'Image Title', 'frizzly' ),
			'altAttribute' => __( 'Image Alt Attribute', 'frizzly' )
		);

		return $available_options;
	}
}

 