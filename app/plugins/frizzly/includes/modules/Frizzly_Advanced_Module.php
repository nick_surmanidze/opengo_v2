<?php

class Frizzly_Advanced_Module extends Frizzly_Base_Module {

	public function get_module_tag() {
		return 'advanced';
	}

	public function get_module_name() {
		return __('Advanced Module', 'frizzly');
	}

	public function get_default_settings() {
		$settings = array(
			'additionalJS' => '',
			'additionalCSS' => ''
		);
		return $settings;
	}

	public function get_registered_settings() {
		$registered_settings = array(
			'additionalJS'         => array(
				'id'   => 'additionalJS',
				'name' => __( 'Additional JavaScript Settings', 'frizzly' ),
				'desc' => __( 'For list of options you can use go to the <a href="http://mrsztuczkens.me/frizzly-customization/" target="_blank">Frizzly customization page</a>.', 'frizzly' ),
				'type' => 'textarea',
				'rows' => 10
			),
			'additionalCSS' => array(
			'id'   => 'additionalCSS',
			'name' => __( 'Additional CSS', 'frizzly' ),
			'desc' => __( 'Here you can add additional style changes for the plugin. For some examples go to the <a href="http://mrsztuczkens.me/frizzly-customization/" target="_blank">Frizzly customization page</a>.', 'frizzly' ),
			'type' => 'textarea',
			'rows' => 10
		)
		);

		return $registered_settings;
	}

	public function get_registered_settings_header() {
		return __("Advanced", 'frizzly' );
	}

	public function copy_settings_to_js(){
		return false;
	}
}