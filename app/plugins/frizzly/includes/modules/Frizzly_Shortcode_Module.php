<?php

class Frizzly_Shortcode_Module extends Frizzly_Activateable_Module {

	public function get_module_tag() {
		return 'shortcode';
	}

	public function get_module_name(){
		return __('Shortcode Module', 'frizzly');
	}

	public function get_default_settings() {
		return array(
			'beforeContent' => '0',
			'beforeContentShortcodeId' => '',
			'afterContent' => '0',
			'afterContentShortcodeId' => ''
		);
	}

	public function get_registered_settings() {
		$registered_settings = array(
			'before_content_section' => array(
				'id'   => 'before_content_section',
				'name' => '<h3 class="frizzly-title">' . __( 'Before Post Content', 'frizzy' ) . '</h3>',
				'desc' => '',
				'type' => 'header'
			),
			'beforeContent' => array(
				'id'   => 'beforeContent',
				'name' => __( 'Render?', 'frizzly' ),
				'desc' => __( 'If checked, the chosen shortcode will be rendered.', 'frizzly' ),
				'type' => 'checkbox'
			),
			'beforeContentShortcodeId'  => array(
				'id'      => 'beforeContentShortcodeId',
				'name'    => __( 'Shortcode', 'frizzly' ),
				'desc'    => __( 'Choose the shortcode you want to use before post content. Create new shortcodes <a href="edit.php?post_type=frizzly_shortcode">here</a>. Learn how shortcodes work <a target="_blank" href="http://mrsztuczkens.me/frizzly-working-with-shortcodes/">here</a>.', 'frizzly' ),
				'type'    => 'select',
				'options' => frizzly_shortcode_get_list()
			),
			'after_content_section' => array(
				'id'   => 'after_content_section',
				'name' => '<h3 class="frizzly-title">' . __( 'After Post Content', 'frizzy' ) . '</h3>',
				'desc' => '',
				'type' => 'header'
			),
			'afterContent'=> array(
				'id'   => 'afterContent',
				'name' => __( 'Render?', 'frizzly' ),
				'desc' => __( 'If checked, the chosen shortcode will be rendered.', 'frizzly' ),
				'type' => 'checkbox'
			),
			'afterContentShortcodeId' => array(
				'id'      => 'afterContentShortcodeId',
				'name'    => __( 'Shortcode', 'frizzly' ),
				'desc'    => __( 'Choose the shortcode you want to use after post content. Create new shortcodes <a href="edit.php?post_type=frizzly_shortcode">here</a>. Learn how shortcodes work <a target="_blank" href="http://mrsztuczkens.me/frizzly-working-with-shortcodes/">here</a>.', 'frizzly' ),
				'type'    => 'select',
				'options' => frizzly_shortcode_get_list()
			)
		);
		return $registered_settings;
	}

	public function get_registered_settings_header() {
		return __('Shortcode', 'frizzly');
	}

	public function is_active(){
		global $frizzly_options;
		return $frizzly_options['main']['moduleShortcodeActive'] == '1';
	}

	public function content_filter( $content ){
		if( ! is_main_query() )
			return $content;

		$before_post = $after_post = '';
		$module_settings = $this->get_module_settings();
		if ('1' == $module_settings['beforeContent']){
			$before_shortcode_id = $module_settings['beforeContentShortcodeId'];
			$before_post = do_shortcode("[frizzly_shortcode id='$before_shortcode_id']");
		}
		if ('1' == $module_settings['afterContent']){
			$after_shortcode_id = $module_settings['afterContentShortcodeId'];
			$after_post = do_shortcode("[frizzly_shortcode id='$after_shortcode_id']");
		}
		return $before_post . $content . $after_post;
	}
}