<?php

class Frizzly_Hover_Module extends Frizzly_Activateable_Module {

	public function get_module_tag() {
		return 'hover';
	}

	public function get_module_name() {
		return __('Hover Module', 'frizzly');
	}

	public function get_default_settings() {
		$settings = array(
			'imageSelector'      => '.frizzly-hover-container img',
			'minImageHeight'     => 100,
			'minImageWidth'      => 100,
			'hoverPanelPosition' => 'middle-middle',
			'buttonSet'          => 'default',
			'theme' 			=> 'default48',
			'orientation'       => 'horizontal',
			'showOnHome'         => '1',
			'showOnSingle'      => '1',
			'showOnPage'         => '1',
			'showOnBlog'         => '1',
			'showOnLightbox' => '1'
		);
		return $settings;
	}

	public function get_registered_settings() {
		$registered_settings = array(
			'selection_settings'    => array(
				'id'   => 'selection_settings',
				'name' => '<h3 class="frizzly-title">' . __( 'Selection Settings', 'frizzy' ) . '</h3>',
				'desc' => '',
				'type' => 'header'
			),
			'imageSelector'         => array(
				'id'   => 'imageSelector',
				'name' => __( 'Image selector', 'frizzly' ),
				'desc' => __( 'jQuery selector for all the images that should have the buttons on hover. Set the value to <b>.frizzly-hover-container img</b> if you want the buttons to appear only on images in the content or to <b>img</b> to appear on all images on site (including sidebar, header and footer). If you are familiar with jQuery, feel free to use your own selector.', 'frizzly' ),
				'type' => 'text'
			),
			'minImageHeight' => array(
				'id'   => 'minImageHeight',
				'name' => __( 'Minimum image height', 'frizzly' ),
				'desc' => __( 'Images with height lower than this value won\'t show buttons on hover.', 'frizzly' ),
				'type' => 'number',
				'max'  => 9999,
				'size' => 'small'
			),
			'minImageWidth' => array(
				'id'   => 'minImageWidth',
				'name' => __( 'Minimum image width', 'frizzly' ),
				'desc' => __( 'Images with width lower than this value won\'t show buttons on hover.', 'frizzly' ),
				'type' => 'number',
				'max'  => 9999,
				'size' => 'small'
			),
			'other_section' => array(
				'id'   => 'other_section',
				'name' => '<h3 class="frizzly-title">' . __( 'Other Settings', 'frizzy' ) . '</h3>',
				'desc' => '',
				'type' => 'header'
			),
			'hoverPanelPosition'    => array(
				'id'      => 'hoverPanelPosition',
				'name'    => __( 'Position', 'frizzy' ),
				'desc'    => __( 'Choose where on image should the buttons appear.', 'frizzly' ),
				'type'    => 'select',
				'options' => array(
					'top-left'      => __( 'Top left', 'frizzly' ),
					'top-middle'    => __( 'Top middle', 'frizzly' ),
					'top-right'     => __( 'Top right', 'frizzly' ),
					'middle-left'   => __( 'Middle left', 'frizzly' ),
					'middle-middle' => __( 'Middle', 'frizzly' ),
					'middle-right'  => __( 'Middle right', 'frizzly' ),
					'bottom-left'   => __( 'Bottom left', 'frizzly' ),
					'bottom-middle' => __( 'Bottom middle', 'frizzly' ),
					'bottom-right'  => __( 'Bottom right', 'frizzly' )
				)
			),
			'buttonSet'  => array(
				'id'      => 'buttonSet',
				'name'    => __( 'Button Set', 'frizzly' ),
				'desc'    => __( 'Choose the button set you want to use on hover. Create new button sets <a href="edit.php?post_type=frizzly_button_set">here</a>. Learn how button sets work <a target="_blank" href="http://mrsztuczkens.me/frizzly-working-with-button-sets/">here</a>.', 'frizzly' ),
				'type'    => 'select',
				'options' => frizzly_button_set_get_list()
			),
			'theme'  => array(
				'id'      => 'theme',
				'name'    => __( 'Theme', 'frizzly' ),
				'desc'    => __( 'Choose the theme you want to use on hover. Create new themes <a href="edit.php?post_type=frizzly_theme">here</a>. Learn how themes work <a target="_blank" href="http://mrsztuczkens.me/frizzly-working-with-themes/">here</a>.', 'frizzly' ),
				'type'    => 'select',
				'options' => frizzly_theme_get_list()
			),
			'orientation' => array(
				'id'      => 'orientation',
				'name'    => __( 'Orientation', 'frizzly' ),
				'type'    => 'select',
				'desc'    => __( 'Choose on which pages hover buttons should appear.', 'frizzly' ),
				'options' => array(
					'horizontal'   => __( 'Horizontal', 'frizzly' ),
					'vertical' => __( 'Vertical', 'frizzly' )
				)
			),
			'hoverShowGroupSetting' => array(
				'id'      => 'hoverShowGroupSetting',
				'name'    => __( 'Show settings', 'frizzly' ),
				'type'    => 'multiple_checkboxes',
				'desc'    => __( 'Choose on which pages hover buttons should appear.', 'frizzly' ),
				'options' => array(
					'showOnHome'   => __( 'Show on home page', 'frizzly' ),
					'showOnSingle' => __( 'Show on single post', 'frizzly' ),
					'showOnPage'   => __( 'Show on single page', 'frizzly' ),
					'showOnBlog'   => __( 'Show on blog page', 'frizzly' )
				)
			),
			'showOnLightbox' => array(
				'id'   => 'showOnLightbox',
				'name' => __( 'Show buttons on lightbox', 'frizzly' ),
				'desc' => __( 'If checked and lightbox module is activated then the user will see share buttons when hovering over the image.', 'frizzly' ),
				'type' => 'checkbox'
			),
		);

		return $registered_settings;
	}

	public function get_registered_settings_header() {
		return __("Hover", 'frizzly' );
	}

	public function content_filter( $content ){

		$attributes = ' data-frizzlyPostContainer=""';
		$attributes.= ' data-frizzlyPostUrl="' . get_permalink() . '"';
		$attributes.= ' data-frizzlyPostTitle="' . wp_strip_all_tags( get_the_title(), true ) . '"';
		$attributes.= ' data-frizzlyHoverContainer=""';

		$post_container = '<input type="hidden" value=""' . $attributes . '>';

		return $post_container . $content;
	}

	public function is_active() {
		global $frizzly_options;
		return $frizzly_options['main']['moduleHoverActive'] == '1';
	}

	public function use_for_current_request() {
		global $frizzly_options;

		$base_value = parent::use_for_current_request();
		if (false == $base_value)
			return false;

		$result = true;

		if ( is_front_page() )
			$result = $frizzly_options[ $this->get_module_tag() ]['showOnHome'] == '1';
		else if ( is_single() )
			$result = $frizzly_options[ $this->get_module_tag() ]['showOnSingle'] == '1';
		else if ( is_page() )
			$result = $frizzly_options[ $this->get_module_tag() ]['showOnPage'] == '1';
		else if ( $this->is_this_blog_page() )
			$result = $frizzly_options[ $this->get_module_tag() ]['showOnBlog'] == '1';
		return $result;
	}

	/* function copied from https://gist.github.com/wesbos/1189639 */
	private function is_this_blog_page() {
		global $post;

		$post_type = get_post_type( $post );

		return ( ( is_home() || is_archive() || is_single() ) && ( $post_type == 'post' )	);
	}
}