<?php

class Frizzly_Main_Module extends Frizzly_Base_Module {

	public function get_module_tag(){
		return 'main';
	}

	public function get_module_name(){
		return __('Main', 'frizzly');
	}

	public function get_default_settings() {
		$settings = array(
			'moduleHoverActive' => '1',
			'moduleLightboxActive' => '0',
			'moduleShortcodeActive' => '1'
		);
		return $settings;
	}

	public function get_registered_settings() {
		$registered_settings = array(
			'active_modules' => array(
				'id'      => 'active_modules',
				'name'    => __( 'Active Modules', 'frizzy' ),
				'desc'    => __( 'Choose which modules should be active.', 'frizzly' ),
				'type'    => 'multiple_checkboxes',
				'options' => $this->get_active_modules_options()
			)
		);

		return $registered_settings;
	}

	public function get_registered_settings_header() {
		return __("General", 'frizzly');
	}

	public function content_filter( $content ){
		return $content;
	}

	private function get_active_modules_options(){
		$result = array();
		$activateable_modules_list = Frizzly()->get_module_manager()->get_activateable_modules_list();
		foreach($activateable_modules_list as $module_tag => $module_name){
			$result['module' . ucfirst($module_tag) . 'Active'] = $module_name;
		}
		return $result;
	}
}