<?php

class Frizzly_Button_Settings_Module extends Frizzly_Base_Module {

	public function get_module_tag(){
		return 'buttonSettings';
	}

	public function get_module_name(){
		return __('Button Settings', 'frizzly');
	}

	public function get_default_settings() {
		$settings = array(
			'pinterestImageDescription'  => array('titleAttribute', 'altAttribute', 'postTitle', 'mediaLibraryDescription'),
			'generalUseFullImages' => '0',
			'generalDownloadImageDescription' => '0',
			'twitterHandle' => '',
		);
		return $settings;
	}

	public function get_registered_settings() {
		$registered_settings = array(
			'generalButtonSettingsHeader'    => array(
				'id'   => 'generalButtonSettingsHeader',
				'name' => '<h3 class="frizzly-title">' . __( 'General', 'frizzy' ) . '</h3>',
				'desc' => '',
				'type' => 'header'
			),
			'generalUseFullImages' => array(
				'id'      => 'generalUseFullImages',
				'name'    => __( 'Use full-sized images if possible', 'frizzy' ),
				'desc'    => __( 'If checked, the plugin will check if an image is linked to its full version and if it is, it will use the full-sized version instead.', 'frizzly' ),
				'type'    => 'checkbox',
			),
			'generalDownloadImageDescription' => array(
				'id'      => 'generalDownloadImageDescription',
				'name'    => __( 'Download image description and caption from Media Library', 'frizzy' ),
				'desc'    => __( 'If checked, the plugin will download the image description and caption for each image (if possible) from Media Library. You need to check this setting if you want to use the <b>Image Description (from Media Library)</b> or <b>Image Caption</b> setting for Pinterest image description below. Please note that it is a time-consuming operation and should be avoided on sites with lots of images.', 'frizzly' ),
				'type'    => 'checkbox',
			),
			'pinterestButtonSettingsHeader'    => array(
				'id'   => 'pinterestButtonSettingsHeader',
				'name' => '<h3 class="frizzly-title">' . __( 'Pinterest', 'frizzy' ) . '</h3>',
				'desc' => '',
				'type' => 'header'
			),
			'pinterestImageDescription' => array(
				'id'      => 'pinterestImageDescription',
				'name'    => __( 'Image description', 'frizzy' ),
				'desc'    => __( 'The source of pre-populated Pin message. Drag and drop buttons in order to choose which source should be prioritized. Please note that if you want to use the <b>Image Description (from Media Library)</b> option, you need to check the <b>Download image description from Media Library</b> setting above.', 'frizzly' ),
				'type'    => 'sorted_list',
				'options' => array(
					'titleAttribute'     => __( 'Image Title Attribute', 'frizzly' ),
					'altAttribute' => __( 'Image Alt Attribute', 'frizzly' ),
					'postTitle' => __('Post Title', 'frizzly'),
					'mediaLibraryDescription' => __('Image Description (from Media Library)'),
					'mediaLibraryCaption' => __( 'Image Caption (from Media Library)' )
				)
			),
			'twitterButtonSettingsHeader'    => array(
				'id'   => 'twitterButtonSettingsHeader',
				'name' => '<h3 class="frizzly-title">' . __( 'Twitter', 'frizzy' ) . '</h3>',
				'desc' => '',
				'type' => 'header'
			),
			'twitterHandle' => array(
				'id' => 'twitterHandle',
				'name' => __('Handle', 'frizzly'),
				'desc' => __('Your Twitter handle. If set, it will be associated with the Tweet (appended to the end with "via @author").', 'frizzly'),
				'type' => 'text'
			)
		);

		return $registered_settings;
	}

	public function get_registered_settings_header() {
		return __("Button settings", 'frizzly');
	}

	public function content_filter( $content ){
		global $frizzly_options;

		if ('1' == $frizzly_options['buttonSettings']['generalDownloadImageDescription']) {
			$content = Frizzly_Utils::add_description_attribute_to_images( $content );
		}

		return $content;
	}
}

 