<?php

function frizzly_content_filter( $content ) {

	if ( is_feed() || false == frizzly_plugin_active_for_current_request() )
		return $content;

	$active_modules = Frizzly()->get_module_manager()->get_active_modules();
	foreach($active_modules as $module_name => $module) {
		$content = $module->content_filter( $content );
	}

	return $content;
}

function frizzly_the_content_filter( $content ){
	return frizzly_content_filter( $content );
}

function frizzly_the_excerpt_filter( $content ){
	return frizzly_content_filter( $content );
}

add_filter('the_content', 'frizzly_the_content_filter');
add_filter('the_excerpt', 'frizzly_the_excerpt_filter');