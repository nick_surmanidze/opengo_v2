<?php wp_nonce_field( 'frizzly_settings_meta_box', 'frizzly_settings_meta_box_nonce' ); ?>

<p><?php _e('Modules that should be ignored on the current post (works only when viewing this single post/page)', 'frizzly'); ?></p>
<?php foreach($activeatable_modules as $module_tag => $module_name) { ?>
	<label><input type="checkbox" value="1" name="frizzly_settings[ignore][<?php echo $module_tag; ?>]" <?php checked('1', $meta['ignore'][$module_tag]); ?>><?php echo $module_name;?></label><br/>
<?php } ?>