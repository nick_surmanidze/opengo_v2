<?php wp_nonce_field( 'frizzly_shortcode_meta_box', 'frizzly_shortcode_meta_box_nonce' ); ?>
<p class="description">
	<?php _e('Frizzly shortcode consists of a chosen theme and a chosen button set. Whenever you will use a certain shortcode you created, the chosen theme and button set will be used to render share buttons.', 'frizzly'); ?>
</p>
<p class="description">
	<?php _e('Create new button sets <a href="edit.php?post_type=frizzly_button_set">here</a>. Learn how button sets work <a target="_blank" href="http://mrsztuczkens.me/frizzly-working-with-button-sets/">here</a>.', 'frizzly'); ?>
</p>
<p class="description">
	<?php _e('Create new themes <a href="edit.php?post_type=frizzly_theme">here</a>. Learn how themes work <a target="_blank" href="http://mrsztuczkens.me/frizzly-working-with-themes/">here</a>.', 'frizzly'); ?>
</p>
<p>
	<?php _e('Button set', 'frizzly'); ?>:
	<select name="frizzly_shortcode[button_set]">
		<?php foreach($button_sets as $button_set_id => $button_set_name): ?>
				<option value="<?php echo $button_set_id; ?>" <?php selected($db_value['button_set'], $button_set_id); ?>><?php echo $button_set_name; ?></option>
		<?php endforeach; ?>
	</select>
</p>
<p>
<?php _e('Theme', 'frizzly'); ?>:
	<select name="frizzly_shortcode[theme]">
		<?php foreach($themes as $theme_id => $theme_name): ?>
			<option value="<?php echo $theme_id; ?>" <?php selected($db_value['theme'], $theme_id); ?>><?php echo $theme_name; ?></option>
		<?php endforeach; ?>
	</select>
</p>
<p>
	<?php _e('Alignment', 'frizzly'); ?>:
	<select name="frizzly_shortcode[align]">
		<?php foreach($alignment_options as $name => $text): ?>
			<option value="<?php echo $name; ?>" <?php selected($db_value['align'], $name); ?>><?php echo $text; ?></option>
		<?php endforeach; ?>
	</select>
</p>