<?php wp_nonce_field( 'frizzly_theme_meta_box', 'frizzly_theme_meta_box_nonce' ); ?>

<h4><?php _e('Common settings', 'frizzly'); ?></h4>
<p class="description">
	<?php _e('To create a custom theme, first you need to set the width and height of images you will use.', 'frizzly'); ?>
</p>
<p><label><?php _e('Width', 'frizzly'); ?>: <input type="number" min="0" step="1"  name="frizzly_theme[width]" value="<?php echo $meta['width']; ?>"> px</label></p>
<p><label><?php _e('Height', 'frizzly'); ?>: <input type="number" min="0" step="1" name="frizzly_theme[height]" value="<?php echo $meta['height']; ?>"> px</label></p>
<h4><?php _e('Images', 'frizzly');?></h4>
<p class="description">
	<?php _e('Below you need to put URL addresses of the images, Please note that <b>all images must be of the same width and height as declared above</b>. If you would not specify a URL for an image, the default image will be used if needed. That will often cause issues with how the icons are being rendered.', 'frizzly'); ?>
</p>
<?php foreach($buttons as $button_tag => $button_name) { ?>
	<p>
	<label><?php echo $button_name; ?>: <input type="text" name="frizzly_theme[buttons][<?php echo $button_tag; ?>]" frizzly-button="<?php echo $button_tag; ?>" value="<?php echo $meta['buttons'][$button_tag]; ?>"></label>
	<a class="button" href='#' frizzly-button-type="imageurl" frizzly-button="<?php echo $button_tag; ?>" style="display: none;"><?php _e('Find image', 'frizzly'); ?></a>
	</p>
<?php } ?>