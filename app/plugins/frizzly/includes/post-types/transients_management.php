<?php

function frizzly_custom_post_types_get_array() {
	return array( 'frizzly_button_set', 'frizzly_theme', 'frizzly_shortcode' );
}

/**
 * @param int $post_id The ID of the post being saved.
 */
function frizzly_custom_post_types_save_post( $post_id ) {

	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ! current_user_can( 'edit_post', $post_id ) )
		return;
	$custom_post_types = frizzly_custom_post_types_get_array();
	$post = get_post( $post_id );
	if (in_array( $post->post_type, $custom_post_types))
		frizzly_custom_post_typees_delete_transients( $post->post_type );
}
add_action( 'save_post', 'frizzly_custom_post_types_save_post' );

/**
 * @param $new_status string New status
 * @param $old_status string Old status
 * @param $post WP_POST Changed post
 */
function frizzly_custom_post_types_transition_post_status( $new_status, $old_status, $post ){
	$post_types = frizzly_custom_post_types_get_array();
	if ( $old_status !== $new_status && ( $old_status === 'publish' || $new_status == 'publish')){
		if (in_array($post->post_type, $post_types))
			frizzly_custom_post_typees_delete_transients( $post->post_type );
	}
}

add_action( 'transition_post_status', 'frizzly_custom_post_types_transition_post_status', 10, 3 );


function frizzly_custom_post_typees_delete_transients( $post_type ){
	delete_transient($post_type);
	delete_transient($post_type . '_meta');
}
 