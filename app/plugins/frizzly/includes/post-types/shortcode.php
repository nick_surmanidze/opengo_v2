<?php

/*
 * CREATE CUSTOM POST TYPE
 */

function frizzly_shortcode_post_type() {

	$labels = array(
		'name'                => _x( 'Shortcode', 'Post Type General Name', 'frizzly' ),
		'singular_name'       => _x( 'Shortcode', 'Post Type Singular Name', 'frizzly' ),
		'menu_name'           => __( 'Shortcode', 'frizzly' ),
		'parent_item_colon'   => __( 'Parent Item:', 'frizzly' ),
		'all_items'           => __( 'Shortcodes', 'frizzly' ),
		'view_item'           => __( 'View Item', 'frizzly' ),
		'add_new_item'        => __( 'Add New Shortcode', 'frizzly' ),
		'add_new'             => __( 'Add New Shortcode', 'frizzly' ),
		'edit_item'           => __( 'Edit Shortcode', 'frizzly' ),
		'update_item'         => __( 'Update Item', 'frizzly' ),
		'search_items'        => __( 'Search Item', 'frizzly' ),
		'not_found'           => __( 'Not found', 'frizzly' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'frizzly' ),
	);
	$args = array(
		'label'               => __( 'frizzly_shortcode', 'frizzly' ),
		'description'         => __( 'Shortcode', 'frizzly' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => 'frizzly-settings',
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'rewrite'             => false,
		'capability_type'     => 'post',
	);
	register_post_type( 'frizzly_shortcode', $args );

}

add_action( 'init', 'frizzly_shortcode_post_type', 0 );


function frizzly_shortcode_get_shortcode_text_for_post($post_id){
	return "[frizzly_shortcode id='$post_id']";
}

/*
 * CUSTOM META BOX FOR POST TYPE
 */

function frizzly_shortcode_add_meta_box() {

	add_meta_box(
		'frizzly_shortcode_info',
		__( 'Shortcode Info', 'frizzly' ),
		'frizzly_shortcode_info_meta_callback',
		'frizzly_shortcode'
	);

	add_meta_box(
		'frizzly_shortcode_settings',
		__( 'Shortcode Settings', 'frizzly' ),
		'frizzly_shortcode_meta_callback',
		'frizzly_shortcode'
	);
}
add_action( 'add_meta_boxes', 'frizzly_shortcode_add_meta_box' );


function frizzly_shortcode_get_default_meta(){
	$defaults =  array(
		'theme' => 'default',
		'button_set' => 'default',
		'align' => 'left'
	);
	return $defaults;
}

/**
 * @param int $post_id Post Id we want to get meta for.
 */
function frizzly_shortcode_get_meta($post_id){
	$db_value = get_post_meta( $post_id, '_frizzly_shortcode', true );
	return empty($db_value) ? frizzly_shortcode_get_default_meta() : Frizzly_Utils::array_merge_map_recursive(frizzly_shortcode_get_default_meta(), $db_value);
}


/**
 * Prints the box content.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function frizzly_shortcode_meta_callback( $post ) {
	$button_sets = frizzly_button_set_get_list();
	$themes = frizzly_theme_get_list();
	$alignment_options = array(
		'left' => __('Left', 'frizzly'),
		'center' => __('Center', 'frizzly'),
		'right' => __('Right', 'frizzly')
	);
	$db_value = frizzly_shortcode_get_meta( $post->ID );
	include_once( Frizzly_Constants::get_plugin_dir() . 'includes/post-types/views/shortcode_meta_view.php');
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function frizzly_shortcode_save_meta( $post_id ) {

	// Check if our nonce is set.
	if ( !isset( $_POST['frizzly_shortcode_meta_box_nonce'] )
			|| !wp_verify_nonce( $_POST['frizzly_shortcode_meta_box_nonce'], 'frizzly_shortcode_meta_box' )
			|| ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			|| ! current_user_can( 'edit_post', $post_id )
	)
		return;
	/* OK, it's safe for us to save the data now. */

	$settings = wp_parse_args($_POST['frizzly_shortcode']);
	update_post_meta( $post_id, '_frizzly_shortcode', $settings );
}
add_action( 'save_post', 'frizzly_shortcode_save_meta' );

/**
 * @param WP_Post $post The object for the current post/page.
 */
function frizzly_shortcode_info_meta_callback( $post ) {

	if ('publish' == get_post_status( $post->ID)){
		_e('Copy the following shortcode to use it on the website.', 'frizzly');
		echo '<p><input type="text" onfocus="this.select();" readonly="readonly" value="'
				. frizzly_shortcode_get_shortcode_text_for_post($post->ID)
				. '" class="wp-ui-text-highlight code" style="display:block; width: 100%;"/></p>';
	} else{
		_e('Please publish the post to make the shortcode active.', 'frizzly');
	}
}

/*
 * GETTING SHORTCODES FROM DB
 */
function frizzly_shortcode_get_list() {

	$select = array();

	$shortcodes = frizzly_shortcode_get_query();

	foreach ( $shortcodes as $shortcode )
		$select[ $shortcode->ID ] = $shortcode->post_title;

	return $select;
}

function frizzly_shortcode_get_query() {
	$shortcodes_transient = get_transient( 'frizzly_shortcode' );
	if ( false !== $shortcodes_transient )
		return $shortcodes_transient;

	$args = array(
		'posts_per_page' => 0,
		'post_type' => 'frizzly_shortcode',
	);

	$shortcodes = get_posts( $args );
	set_transient( 'frizzly_shortcode', $shortcodes, 60*60*24*7 );
	return $shortcodes;
}

/*
 * SHORTCODES ADDITIONAL COLUMNS
 */

function frizzly_shortcode_columns_head( $defaults ) {

	$columns = array();
	foreach( $defaults as $key => $name){
		$columns[ $key ] = $name;
		if ( 'title' == $key )
			$columns['shortcode'] = __('Shortcode', 'frizzly');
	}

	return $columns;
}

function frizzly_shortcode_columns_values($column_name, $post_ID) {

	if ($column_name == 'shortcode') {
		echo '<input type="text" onfocus="this.select();" readonly="readonly" value="'
			. frizzly_shortcode_get_shortcode_text_for_post($post_ID)
			. '" class="shortcode-in-list-table wp-ui-text-highlight code" style="display:block;width:100%;"/>';
	}
}

add_filter('manage_frizzly_shortcode_posts_columns', 'frizzly_shortcode_columns_head');
add_action('manage_frizzly_shortcode_posts_custom_column', 'frizzly_shortcode_columns_values', 10, 2);


//SHORTCODE HANDLING

// [frizzly_shortcode id="124"]
function frizzly_shortcode_handle( $atts ) {
	global $post;
	global $wp_query;
	global $wp;

	$a = shortcode_atts( array( 'id' => '-1', 'widget' => false ), $atts );
	$is_widget = $a['widget'];
	$meta = frizzly_shortcode_get_meta( $a['id']);
	$button_set = $meta['button_set'];
	$theme = $meta['theme'];
	$align = $meta['align'];

	if ($is_widget && false == is_singular()){

		$url =  Frizzly_Utils::get_current_URL();
		$post_title = get_bloginfo('name');
		foreach($wp_query->posts as $current_post) {
			$image_data = Frizzly_Utils::get_image_attributes_from_post( $current_post->post_content );
			if (false == empty($image_data['src']))
				break;
		}

	} else {

		$url = get_permalink();
		if (false == $url)
			$url = home_url();
		$post_title = wp_strip_all_tags( get_the_title(), true );
		if (empty($post_title))
			$post_title = get_bloginfo('name');
		$image_data = Frizzly_Utils::get_image_attributes_from_post($post->post_content);
	}

	$img_src = $image_data['src'];
	$img_alt = $image_data['alt'];
	$img_title = $image_data['title'];
	$img_description = $image_data['description'];

	$rand = uniqid();
	$js_renderer  = '<script type="text/javascript">';
	$js_renderer .= 'if (window.frizzly !== undefined) {';
	$js_renderer .= 'window.frizzly.call("shortcode", "render", "' . $rand . '");';
	$js_renderer .= '}';
	$js_renderer .= '</script>';

	$shortcode = "<div ";
		$shortcode .= "class='frizzly-shortcode-container frizzly-shortcode-$rand' ";
		$shortcode .= "data-frizzlyPostTitle='$post_title'";
		$shortcode .= "data-frizzlyButtonSet='$button_set'";
		$shortcode .= "data-frizzlyTheme='$theme'";
		$shortcode .= "data-frizzlyPostUrl='$url'";
		$shortcode .= "data-frizzlyImageUrl='$img_src'";
		$shortcode .= "data-frizzlyImageAlt='$img_alt'";
		$shortcode .= "data-frizzlyImageTitle='$img_title'";
		$shortcode .= "data-frizzlyImageDescription='$img_description'";
		$shortcode .= "data-frizzlyAlign='$align'";
	$shortcode .= "></div>";
	return $shortcode . $js_renderer;
}
add_shortcode( 'frizzly_shortcode', 'frizzly_shortcode_handle' );