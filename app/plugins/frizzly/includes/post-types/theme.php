<?php

/*
 * CREATE CUSTOM POST TYPE
 */

function frizzly_theme_post_type() {

	$labels = array(
		'name'                => _x( 'Themes', 'Post Type General Name', 'frizzly' ),
		'singular_name'       => _x( 'Theme', 'Post Type Singular Name', 'frizzly' ),
		'menu_name'           => __( 'Theme', 'frizzly' ),
		'parent_item_colon'   => __( 'Parent Item:', 'frizzly' ),
		'all_items'           => __( 'Themes', 'frizzly' ),
		'view_item'           => __( 'View Item', 'frizzly' ),
		'add_new_item'        => __( 'Add New Theme', 'frizzly' ),
		'add_new'             => __( 'Add New Theme', 'frizzly' ),
		'edit_item'           => __( 'Edit Theme', 'frizzly' ),
		'update_item'         => __( 'Update Item', 'frizzly' ),
		'search_items'        => __( 'Search Item', 'frizzly' ),
		'not_found'           => __( 'Not found', 'frizzly' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'frizzly' ),
	);
	$args = array(
		'label'               => __( 'frizzly_theme', 'frizzly' ),
		'description'         => __( 'Theme desc', 'frizzly' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => 'frizzly-settings',
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'rewrite'             => false,
		'capability_type'     => 'post',
	);
	register_post_type( 'frizzly_theme', $args );

}

add_action( 'init', 'frizzly_theme_post_type', 0 );

/*
 * CUSTOM META BOX FOR POST TYPE
 */

function frizzly_theme_add_meta_box() {

	add_meta_box(
		'frizzly_theme_settings',
		__( 'Theme settings', 'frizzly' ),
		'frizzly_theme_meta_callback',
		'frizzly_theme'
	);
}
add_action( 'add_meta_boxes', 'frizzly_theme_add_meta_box' );

function frizzly_theme_get_default_meta(){
	$defaults =  array(
		'width' => '',
		'height' => '',
		'buttons' => array()
	);

	$buttons = frizzly_get_available_buttons();
	foreach($buttons as $button_tag => $button_name){
		$defaults['buttons'][$button_tag] = '';
	}

	return $defaults;
}

/**
 * Prints the box content.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function frizzly_theme_meta_callback( $post ) {
	$buttons = frizzly_get_available_buttons();

	$defaults = frizzly_theme_get_default_meta();
	$meta = get_post_meta( $post->ID, '_frizzly_theme', true);
	$meta = '' === $meta ? $defaults : array_merge($defaults, $meta);
	include_once( Frizzly_Constants::get_plugin_dir() . 'includes/post-types/views/theme_meta_view.php');
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function frizzly_theme_save_meta( $post_id ) {

	// Check if our nonce is set.
	if ( !isset( $_POST['frizzly_theme_meta_box_nonce'] )
			|| !wp_verify_nonce( $_POST['frizzly_theme_meta_box_nonce'], 'frizzly_theme_meta_box' )
			|| ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			|| ! current_user_can( 'edit_post', $post_id )
			|| !isset( $_POST['frizzly_theme'])
	)
		return;

	$theme_settings = wp_parse_args($_POST['frizzly_theme']);

	update_post_meta( $post_id, '_frizzly_theme', $theme_settings );
}
add_action( 'save_post', 'frizzly_theme_save_meta' );

/* Themes list for dropdowns */
function frizzly_theme_get_list(){
	$select = array();
	$select['default48'] = __('Default theme', 'frizzly');

	$themes = frizzly_theme_get_query();
	foreach($themes as $theme)
		$select[$theme->ID] = $theme->post_title;

	return $select;
}

function frizzly_theme_get_for_js(){
	$themes_meta_transient = get_transient( 'frizzly_theme_meta' );
	if (false !== $themes_meta_transient )
		return $themes_meta_transient;

	$themes = frizzly_theme_get_query();

	$themes_meta = array();
	$available_buttons = frizzly_get_available_buttons();

	foreach($themes as $theme){
		$meta = get_post_meta($theme->ID, '_frizzly_theme', true);
		$themes_meta[$theme->ID] = array();
		$themes_meta[$theme->ID]['buttons'] = array();

		foreach($available_buttons as $button_tag => $button_name){
			$themes_meta[$theme->ID]['buttons'][$button_tag] = array('height' => (int)$meta['height'], 'width' => (int)$meta['width']);
		}
	}

	set_transient( 'frizzly_theme_meta', $themes_meta, 60*60*24*7 );
	return $themes_meta;
}

function frizzly_theme_get_query(){
	$themes_transient = get_transient( 'frizzly_theme' );
	if (false !== $themes_transient)
		return $themes_transient;

	$args = array(
		'posts_per_page' => 0,
		'post_type' => 'frizzly_theme',
	);

	$themes = get_posts( $args );
	set_transient( 'frizzly_theme', $themes, 60*60*24*7 );

	return $themes;
}
