<?php wp_nonce_field( 'frizzly_button_set_meta_box', 'frizzly_button_set_meta_box_nonce' ); ?>
<p><?php _e('To use a button drag it to the Selected Buttons area. If you no longer want to use it, drag it back to the Available Buttons area. Buttons will be shown in the order you have set in the Selected Buttons area.', 'frizzly'); ?></p>
<input type="hidden" id="frizzly_button_set_settings" name="frizzly_button_set_settings" value="<?php echo $selected_buttons_input_value; ?>" >
<div class="frizzly-container">
	<div class="frizzly-button-container">
		<h4><?php _e('Selected Buttons', 'frizzly'); ?></h4>
		<ul id="selected_buttons" class="frizzly-button-list">
			<?php foreach($selected_buttons as $button_key => $button_name) { ?>
				<li class="button" data-value="<?php echo $button_key; ?>"><?php echo $button_name; ?></li>
			<?php } ?>
		</ul>
	</div>
	<div class="frizzly-button-container">
		<h4><?php _e('Available Buttons', 'frizzly'); ?></h4>
		<ul id="available_buttons" class="frizzly-button-list">
			<?php foreach($available_buttons as $button_key => $button_name) { ?>
				<li class="button" data-value="<?php echo $button_key; ?>"><?php echo $button_name; ?></li>
			<?php } ?>
		</ul>
	</div>
</div>