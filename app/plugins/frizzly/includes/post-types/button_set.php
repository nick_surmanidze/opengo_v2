<?php

/*
 * CREATE CUSTOM POST TYPE
 */

function frizzly_button_set_post_type() {

	$labels = array(
		'name'                => _x( 'Button Sets', 'Post Type General Name', 'frizzly' ),
		'singular_name'       => _x( 'Button Set', 'Post Type Singular Name', 'frizzly' ),
		'menu_name'           => __( 'Button Set', 'frizzly' ),
		'parent_item_colon'   => __( 'Parent Item:', 'frizzly' ),
		'all_items'           => __( 'Button Sets', 'frizzly' ),
		'view_item'           => __( 'View Item', 'frizzly' ),
		'add_new_item'        => __( 'Add New Button Set', 'frizzly' ),
		'add_new'             => __( 'Add New Button Set', 'frizzly' ),
		'edit_item'           => __( 'Edit Button Set', 'frizzly' ),
		'update_item'         => __( 'Update Item', 'frizzly' ),
		'search_items'        => __( 'Search Item', 'frizzly' ),
		'not_found'           => __( 'Not found', 'frizzly' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'frizzly' ),
	);
	$args = array(
		'label'               => __( 'frizzly_button_set', 'frizzly' ),
		'description'         => __( 'Button Set desc', 'frizzly' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => 'frizzly-settings',
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'rewrite'             => false,
		'capability_type'     => 'post',
	);
	register_post_type( 'frizzly_button_set', $args );

}

add_action( 'init', 'frizzly_button_set_post_type', 0 );


/*
 * CUSTOM META BOX FOR POST TYPE
 */

function frizzly_button_set_add_meta_box() {

	add_meta_box(
		'frizzly_button_set_buttons',
		__( 'Buttons Settings', 'frizzly' ),
		'frizzly_button_set_meta_callback',
		'frizzly_button_set'
	);
}
add_action( 'add_meta_boxes', 'frizzly_button_set_add_meta_box' );

/**
 * Prints the box content.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function frizzly_button_set_meta_callback( $post ) {
	$available_buttons = frizzly_get_available_buttons();

	$db_value = get_post_meta( $post->ID, '_frizzly_buttons', true );
	if ('' == $db_value)
		$db_value = array();

	$selected_buttons_input_value = join(',', $db_value);

	$selected_buttons = array();
	foreach($db_value as $key) {
		$selected_buttons[$key] = $available_buttons[ $key ];
	}
	$available_buttons = array_diff_key( $available_buttons, $selected_buttons);

	include_once( Frizzly_Constants::get_plugin_dir() . 'includes/post-types/views/button_set_meta_view.php');
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function frizzly_button_set_save_meta( $post_id ) {

	// Check if our nonce is set.
	if ( !isset( $_POST['frizzly_button_set_meta_box_nonce'] )
			|| !wp_verify_nonce( $_POST['frizzly_button_set_meta_box_nonce'], 'frizzly_button_set_meta_box' )
			|| ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			|| ! current_user_can( 'edit_post', $post_id )
	)
		return;
	/* OK, it's safe for us to save the data now. */

	// Make sure that it is set.
	if ( ! isset( $_POST['frizzly_button_set_settings'] ) ) {
		return;
	}

	$button_settings =  $_POST['frizzly_button_set_settings'];
	$db_value = explode(',', $button_settings);
	update_post_meta( $post_id, '_frizzly_buttons', $db_value );
}
add_action( 'save_post', 'frizzly_button_set_save_meta' );

/*
 * GETTING BUTTON SETS FROM DB
 */
function frizzly_button_set_get_list() {
	//add the default button set
	$select = array();
	$select['default'] = __('Default button set (Pinterest, Facebook, Twitter)', 'frizzly');

	//add any custom button sets user created
	$button_sets = frizzly_button_set_get_query();

	foreach ( $button_sets as $button_set )
		$select[ $button_set->ID ] = $button_set->post_title;

	return $select;
}

function frizzly_button_set_get_for_js() {
	//check for transient and if it's there, return it
	$button_sets_meta_transient = get_transient('frizzly_button_set_meta');
	if (false !== $button_sets_meta_transient)
		return $button_sets_meta_transient;

	$button_sets = frizzly_button_set_get_query();

	$button_sets_meta = array();

	foreach($button_sets as $button_set){
		$meta = get_post_meta($button_set->ID, '_frizzly_buttons', true);
		if ( '' != $meta ){
			$button_sets_meta[ (string)$button_set->ID ] = $meta;
		}
	}
	//save the transient for a week
	set_transient('frizzly_button_set_meta', $button_sets_meta, 60*60*24*7 );

	return $button_sets_meta;
}

function frizzly_button_set_get_query() {
	//if the transient exists, return it
	$button_sets_transient = get_transient('frizzly_button_set');
	if ( false !== $button_sets_transient )
		return $button_sets_transient;

	$args = array(
		'posts_per_page' => 0,
		'post_type' => 'frizzly_button_set',
	);

	$button_sets = get_posts( $args );
	//save the transient for a week and return value
	set_transient('frizzly_button_set', $button_sets, 60*60*24*7);
	return $button_sets;
}

/*
 * BUTTON SETS ADDITIONAL COLUMNS
 */

function frizzly_button_set_columns_head( $defaults ) {

	$columns = array();
	foreach( $defaults as $key => $name){
		$columns[ $key ] = $name;
		if ( 'title' == $key )
			$columns['selected_buttons'] = __('Selected buttons', 'frizzly');
	}

	return $columns;
}

function frizzly_button_set_columns_values($column_name, $post_ID) {

	if ($column_name == 'selected_buttons') {
		$button_keys = get_post_meta($post_ID, '_frizzly_buttons', true);
		if ('' ==  $button_keys) {
			_e('No buttons selected', 'frizzly');
		} else {
			$output = '';
			$availableButtons = frizzly_get_available_buttons();
			for($i = 0; $i < count($button_keys); $i++) {
				$output .= $availableButtons[ $button_keys[ $i ] ];
				if ( $i + 1 != count($button_keys))
					$output .= ', ';
			}
			echo $output;
		}
	}
}

add_filter('manage_frizzly_button_set_posts_columns', 'frizzly_button_set_columns_head');
add_action('manage_frizzly_button_set_posts_custom_column', 'frizzly_button_set_columns_values', 10, 2);