<?php

/* Returns available buttons as an array */
function frizzly_get_available_buttons() {
	$buttons = array(
		'pinterest' => __('Pinterest', 'frizzly'),
		'facebook' => __('Facebook', 'frizzly'),
		'twitter' => __('Twitter', 'frizzly')
	);

	return $buttons;
}

/* True if plugin should be added to the current post/page */
function frizzly_plugin_active_for_current_request() {

	$result = false;

	//loop through active modules
	$activateable_modules = Frizzly()->get_module_manager()->get_activateable_modules();

	foreach($activateable_modules as $module_name => $module){
			$result = $result || $module->use_for_current_request();
	}

	return apply_filters( 'frizzly_plugin_active_for_current_request', $result );
}
 