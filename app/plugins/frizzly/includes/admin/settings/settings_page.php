<?php

/**
 * Admin Options Page
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function frizzly_add_options_link() {
	$screen_id = add_menu_page( __('Frizzly Settings', 'frizzly' ), __( 'Frizzly', 'frizzly' ), 'manage_options', 'frizzly-settings', 'frizzly_options_page' );
	Frizzly_Constants::set_admin_screen_id( $screen_id );
}
add_action( 'admin_menu', 'frizzly_add_options_link', 9 );

function frizzly_add_settings_link( $links ) {
	$settings_link = '<a href="admin.php?page=frizzly-settings">Settings</a>';
	array_push( $links, $settings_link );
	return $links;
}
add_filter( "plugin_action_links_" . plugin_basename( Frizzly_Constants::get_root_file() ), 'frizzly_add_settings_link' );

function frizzly_options_page() {
	$tabs = Frizzly()->get_module_manager()->get_registered_settings_headers();
	$tabs_keys = array_keys($tabs);
	$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : $tabs_keys[0];

	ob_start(); ?>
		<?php //var_dump(get_transient('frizzly_test')); ?>
	<div class="wrap">
		<h2><?php _e( 'Frizzly Settings', 'frizzly' ); ?></h2>
		<div id="icon-plugins" class="icon32"></div>
		<h2 class="nav-tab-wrapper">
			<?php foreach($tabs as $tab => $tab_description) { ?>
			<a href="<?php echo add_query_arg('tab', $tab, remove_query_arg('settings-updated')); ?>" class="nav-tab <?php echo $active_tab == $tab ? 'nav-tab-active' : ''; ?>"><?php echo $tab_description; ?></a>
			<?php } ?>
		</h2>

		<p>
			<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VJ5EXW2FH5DJU" class="button" target="_blank" rel="nofollow"><b><?php _e( 'Donate', 'frizzly' ); ?></b></a>
			<a href="https://wordpress.org/support/plugin/frizzly" class="button" target="_blank" rel="nofollow"><b><?php _e( 'Support forum', 'frizzly' ); ?></b></a>
			<a href="http://mrsztuczkens.me/frizzly-customization/" class="button" target="_blank" rel="nofollow"><b><?php _e( 'Customization', 'frizzly' ); ?></b></a>
		</p>

		<div id="tab_container">
			<form method="post" action="options.php">
				<?php
				settings_fields( 'frizzly_settings');
				do_settings_sections( 'frizzly_settings_' . $active_tab );
				submit_button();
				?>

			</form>
		</div><!-- #tab_container-->
	</div><!-- .wrap -->
	<?php
	echo ob_get_clean();
}