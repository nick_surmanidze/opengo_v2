<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

function frizzly_get_settings() {

	$settings = get_option( 'frizzly_settings' );
	//$settings = false;
	$default_settings = Frizzly()->get_module_manager()->get_default_settings();

	if (false == $settings)
		$settings = $default_settings;
	else {
		foreach($default_settings as $module_name => $module_settings) {

			$settings[ $module_name ] = array_key_exists( $module_name, $settings)
						? array_intersect_key($settings[ $module_name ], $module_settings) + $module_settings
						: $module_settings;
		}
	}

	return $settings;
}

function frizzly_register_settings() {

	if ( false == get_option( 'frizzly_settings' ) ) {
		add_option( 'frizzly_settings' );
	}

	foreach(Frizzly()->get_module_manager()->get_registered_settings() as $module => $settings) {

		add_settings_section(
			'frizzly_settings_' . $module,
			__return_null(),
			'__return_false',
			'frizzly_settings_' . $module
		);

		foreach ( $settings as $option ) {

			$name = isset( $option['name'] ) ? $option['name'] : '';

			add_settings_field(
				'frizzly_settings[' . $module . '][' . $option['id'] . ']',
				$name,
				function_exists( 'frizzly_' . $option['type'] . '_callback' ) ? 'frizzly_' . $option['type'] . '_callback' : 'frizzly_missing_callback',
				'frizzly_settings_' . $module,
				'frizzly_settings_' . $module,
				array(
					'id'      => $option['id'],
					'module'  => $module,
					'desc'    => ! empty( $option['desc'] ) ? $option['desc'] : '',
					'name'    => isset( $option['name'] ) ? $option['name'] : null,
					'section' => $module,
					'size'    => isset( $option['size'] ) ? $option['size'] : null,
					'options' => isset( $option['options'] ) ? $option['options'] : '',
					'min'     => isset( $option['min'] ) ? $option['min'] : '',
					'max'     => isset( $option['max'] ) ? $option['max'] : '',
					'step'     => isset( $option['step'] ) ? $option['step'] : '',
					'rows' => isset( $option['rows'] ) ? $option['rows'] : null
				)
			);
		}

	}

	// Creates our settings in the options table
	register_setting( 'frizzly_settings', 'frizzly_settings', 'frizzly_settings_sanitize' );

}
add_action('admin_init', 'frizzly_register_settings');

function frizzly_settings_sanitize( $input = array() ) {
	if ( empty( $_POST['_wp_http_referer'] ) ) { return $input;	}

	global $frizzly_options;
	$settings = Frizzly()->get_module_manager()->get_registered_settings();

	//in case we have only checkboxes in settings and all of them are unchecked
	if( 0 == count($input)){
		parse_str( $_POST['_wp_http_referer'], $referrer );
		$settings_keys = array_keys($settings);
		$mod_name      = isset( $referrer['tab'] ) ? $referrer['tab'] : $settings_keys[0];
		$input[$mod_name] = array();
	}

	foreach($input as $module_name => $module_input) {

		$current_module = Frizzly()->get_module_manager()->get_module( $module_name );

		// Loop through each setting being saved and pass it through a sanitization filter
		foreach ( $module_input as $key => $value ) {

			// Get the setting type (checkbox, select, etc)
			$type = $current_module->get_type_for_setting( $key);

			if ( $type )
				$input[$module_name][$key] = apply_filters( 'frizzly_settings_sanitize_' . $type, $input[$module_name][$key], $module_name, $key );
			$input[$module_name][$key] = apply_filters( 'frizzly_settings_sanitize_global', $input[$module_name][$key], $module_name, $key );
		}

		//set all checkboxes to 0 instead of no value
		foreach($current_module->get_boolean_settings_ids() as $checkbox_key) {
			if ( empty( $input[$module_name][$checkbox_key] )  ) {
				$input[$module_name][$checkbox_key] = '0';
			}
		}
	}

	// Merge our new settings with the existing
	$output = array_merge( $frizzly_options, $input );
	return $output;
}

/* Sanitization filters */

/* Sorted lists are stored as single string with a comma as a separator, we need to return them as arrays */
function frizzly_sanitize_sorted_list( $value, $module_name, $key) {
	return explode(',', $value);
}

add_filter('frizzly_settings_sanitize_sorted_list', 'frizzly_sanitize_sorted_list', 1, 3);

/* If checkbox gets sanitized, its value has to be 1 */
function frizzly_sanitize_checkbox( $value, $module_name, $key) {
	return 1;
}
add_filter('frizzly_settings_sanitize_checkbox', 'frizzly_sanitize_checkbox', 10, 3);

function frizzly_sanitize_text( $value, $module_name, $key) {
	return sanitize_text_field($value);
}

add_filter('frizzly_settings_sanitize_text', 'frizzly_sanitize_text', 10, 3);

function frizzly_sanitize_number( $value, $module_name, $key) {
	if (!is_numeric( $value )){
		$module = Frizzly()->get_module_manager()->get_module( $module_name );
		$defaults = $module->get_default_settings();
		$value = $defaults[ $key ];
	}
	return $value;
}
add_filter('frizzly_settings_sanitize_number', 'frizzly_sanitize_number', 10, 3);


/* Value of select field needs to be one from a existing options */
function frizzly_sanitize_select( $value, $module_name, $key) {
	$module = Frizzly()->get_module_manager()->get_module( $module_name );
	$settings = $module->get_registered_settings();
	if ( isset( $settings[ $key ] ) ){
		$options = $settings[ $key ]['options'];
		$options_keys = array_keys( $options );

		//if the value doesn't exist in options, we change it to the default one
		if (!in_array($value, $options_keys)){
			$defaults = $module->get_default_settings();
			$value = $defaults[ $key ];
		}
	}
	return $value;
}
add_filter('frizzly_settings_sanitize_select', 'frizzly_sanitize_select', 10, 3);
