<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

function frizzly_setting_description( $description ) {
	return '<p class="description">'  . $description . '</p>';
}

function frizzly_get_setting_name( $args ) {
	return 'frizzly_settings[' . $args['module'] . '][' . $args['id'] . ']';
}

function frizzly_get_setting_value( $args ) {
	global $frizzly_options;
	$value = isset( $frizzly_options[$args['module']][ $args['id'] ] ) ? $frizzly_options[$args['module']][ $args['id'] ] : '';
	return $value;
}

/* SETTINGS CALLBACKS FOR COMMON TYPES */

function frizzly_missing_callback($args) {
	printf( __( 'The callback function used for the <strong>%s</strong> setting is missing.', 'frizzly' ), $args['id'] );
}

function frizzly_header_callback( $args ) {}

function frizzly_text_callback($args) {
	$value = frizzly_get_setting_value( $args );
	$name = frizzly_get_setting_name( $args );

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<input type="text" class="' . $size . '-text" id="' . $name . '" name="' . $name . '" value="' . esc_attr( stripslashes( $value ) ) . '"/>';
	$html .= frizzly_setting_description( $args['desc']);

	echo $html;
}

function frizzly_textarea_callback( $args ) {
	$value = frizzly_get_setting_value( $args );
	$name = frizzly_get_setting_name( $args );

	$rows = ( isset( $args['rows'] ) && ! is_null( $args['rows'] ) ) ? $args['rows'] : 5;
	$html = '<textarea rows="' .  $rows . '" class="widefat" id="' . $name . '" name="' . $name . '" >' . esc_attr( stripslashes( $value ) ) . '</textarea>';
	$html .= frizzly_setting_description( $args['desc']);

	echo $html;
}

function frizzly_number_callback($args) {

	$value = frizzly_get_setting_value( $args );
	$name = frizzly_get_setting_name( $args );

	$min = isset( $args['min'] ) ? $args['min'] : 0;
	$max = isset( $args['max'] ) ? $args['max'] : 10;
	$step = isset( $args['step'] ) ? $args['step'] : 1;
	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';

	$html = '<input type="number" min="' . $min . '" step="' . $step . '" max="' . $max . '" class="' . $size . '-text" id="' . $name .'" name="' . $name . '" value="' . esc_attr( stripslashes( $value ) ) . '"/>';
	$html .= frizzly_setting_description( $args['desc']);

	echo $html;
}

function frizzly_checkbox_callback( $args ) {

	$value = frizzly_get_setting_value( $args );
	$name = frizzly_get_setting_name( $args );

	$checked = checked(1, $value, false);
	$html = '<input type="checkbox" id="' . $name . '" name="' . $name . '" value="1" ' . $checked . '/>';
	$html .= frizzly_setting_description( $args['desc']);

	echo $html;
}

function frizzly_multiple_checkboxes_callback( $args ) {

	$html = '';

	foreach( $args['options'] as $key => $description ) {
		$local_args = array('module' => $args['module'], 'id' => $key );
		$value = frizzly_get_setting_value( $local_args );
		$name = frizzly_get_setting_name( $local_args );

		$checked = checked(1, $value, false);
		$html .= '<input type="checkbox" id="' . $name . '" name="' . $name . '" value="1" ' . $checked . '/>';
		$html .= '<label for="' . $name . '">' . $description . '</label><br/>';
	}

	$html .= frizzly_setting_description( $args['desc']);
	echo $html;
}

function frizzly_select_callback($args) {
	$value = frizzly_get_setting_value( $args );
	$name = frizzly_get_setting_name( $args );

	$html = '<select id="'. $name . '" name="' . $name . '"/>';

	foreach ( $args['options'] as $option => $name ) {
		$html .= '<option value="' . $option . '" ' . selected( $option, $value, false ) . '>' . $name . '</option>';
	}

	$html .= '</select>';
	$html .= frizzly_setting_description( $args['desc']);

	echo $html;
}

function frizzly_sorted_list_callback( $args ) {
	$options = $args['options'];
	$value = frizzly_get_setting_value( $args );

	//if there are any new options, we need to add them at the end of current options before moving forward
	foreach($options as $option_tag => $option_name){
		if (false == in_array($option_tag, $value)){
			$value[] = $option_tag;
		}
	}

	$display_value = join(',', $value);
	$name = frizzly_get_setting_name( $args );

	$sorted = array();
	foreach($value as $desc_source_name){
		$sorted[ $desc_source_name ] = $options[ $desc_source_name ];
	}

	$html = '<div class="frizzly-button-container">';
	$html .= '<ul class="frizzly-button-list" data-input-id="' . $args['id'] . '">';

	foreach($sorted as $option_name => $option_value)
		$html .= '<li class="button" data-value="' . $option_name . '">' . $option_value . '</li>';

	$html .= '</ul>';
	$html .= '</div>';
	$html .= '<input type="hidden" name="' . $name  . '" id="' . $args['id'] . '" value="' . $display_value . '">';
	$html .= '<div style="clear:both;"></div>';
	$html .= frizzly_setting_description( $args['desc']);

	echo $html;
}

