<?php


add_action( 'admin_notices', 'frizzly_show_admin_notice' );

function frizzly_show_admin_notice() {
	global $hook_suffix;
	if ( Frizzly_Constants::get_admin_screen_id() == $hook_suffix ) {
		include_once( 'views/admin_notice.php');
	}
}

 