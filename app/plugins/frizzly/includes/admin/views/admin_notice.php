<div class="updated">
	<p><?php _e('Help shape the future of Frizzly by taking the survey.', 'frizzly'); ?><a style="font-weight: bold; margin-left: 25px;" class="button button-primary" href="https://www.surveymonkey.com/r/FWS35NW" target="_blank"><?php _e('Take the Survey', 'frizzly'); ?></a></p>
</div>