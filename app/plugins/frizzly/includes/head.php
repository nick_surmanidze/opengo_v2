<?php

function frizzly_print_themes_css(){
	global $frizzly_options;

	$themes = frizzly_theme_get_query();
	$output_css = '<style type="text/css">';
	foreach($themes as $theme){
		$meta = get_post_meta($theme->ID, '_frizzly_theme', true);
		$theme_name = 'frizzly-' . $theme->ID;

		$output_css .= ".frizzly-container.$theme_name div {";
				$output_css .= "width:{$meta['width']}px;";
				$output_css .= "height:{$meta['height']}px;";
				$output_css .= 'float: left;';
		$output_css .= '}';

		foreach($meta['buttons'] as $button_name => $button_url) {
			if (false == empty($button_url)){
				$output_css .= ".frizzly-container.$theme_name div.frizzly-icon-$button_name { background-image: url('$button_url'); }";
			}
		}
	}
	$output_css .= $frizzly_options['advanced']['additionalCSS'];
	$output_css.= '</style>';
	echo $output_css;
	?>
	<?php
}
add_action('wp_head', 'frizzly_print_themes_css');