<?php

/* FRONT END  */

function frizzly_load_scripts_styles() {
	global $frizzly_options;

	if (false == frizzly_plugin_active_for_current_request())
		return;

	wp_enqueue_style( 'frizzly-styles', Frizzly_Constants::get_plugin_url() . '/assets/css/styles.css', array(), Frizzly_Constants::get_version() );
	wp_enqueue_script( 'frizzly-main', Frizzly_Constants::get_plugin_url() . '/assets/js/frizzly.min.js', array( 'jquery' ), Frizzly_Constants::get_version(), false );

	$options_copy = $frizzly_options;
	//update module info for current request
	$active_modules = array();
	foreach(Frizzly()->get_module_manager()->get_modules() as $module_name => $module){
		$use_module = $module->is_active();
		if ($use_module && $module instanceof iFrizzly_Activateable_Module){
			$use_module = $module->use_for_current_request();
		}
		if ($use_module)
			$active_modules[] = $module_name;
	}

	$options_copy['main']['activeModules'] = $active_modules;
	//hide settings for modules that don't need to copy them
	foreach(Frizzly()->get_module_manager()->get_modules() as $module_name => $module){
		if ($module->copy_settings_to_js() == false)
			$options_copy[$module_name] = null;
	}

	$js_settings = array(
		'modules' => $options_copy,
		'buttonSets' => frizzly_button_set_get_for_js(),
		'themes' => frizzly_theme_get_for_js()
	);
	wp_localize_script( 'frizzly-main', 'FrizzlySettings', $js_settings);

	if ('1' == $frizzly_options['main']['moduleLightboxActive']) {
		wp_enqueue_style( 'jquery-colorbox', Frizzly_Constants::get_plugin_url() . '/assets/css/colorbox-default.css', array(), Frizzly_Constants::get_version() );
		wp_enqueue_script( 'jquery-colorbox', Frizzly_Constants::get_plugin_url() . '/assets/js/jquery.colorbox.min.js', array( 'jquery', 'frizzly-main' ), Frizzly_Constants::get_version(), true );
	}
}
add_action( 'wp_enqueue_scripts', 'frizzly_load_scripts_styles' );

/* BACK END */

function frizzly_load_admin_scripts( $hook ) {
	global $post;

	$current_screen = get_current_screen();

	$admin_screen = '';
	if ( ($hook == 'post-new.php' || $hook == 'post.php') && 'frizzly_button_set' === $post->post_type )
		$admin_screen = 'ButtonSetScreen';
	else if ( ($hook == 'post-new.php' || $hook == 'post.php') && 'frizzly_theme' === $post->post_type )
			$admin_screen = 'ThemeScreen';
	else if ( Frizzly_Constants::get_admin_screen_id() == $current_screen->id )
		$admin_screen = 'SettingsScreen';

	if ( '' != $admin_screen ) {
		if ($admin_screen == 'ThemeScreen' && function_exists( "wp_enqueue_media")){
				wp_enqueue_media();
		}
		wp_enqueue_style('frizzly-admin', Frizzly_Constants::get_plugin_url() . '/assets/css/frizzly-admin.css', array(), Frizzly_Constants::get_version());
		wp_enqueue_script('frizzly-admin', Frizzly_Constants::get_plugin_url() . '/assets/js/frizzly-admin.min.js', array('jquery', 'jquery-ui-core', 'jquery-ui-sortable', 'jquery-ui-draggable'));
		wp_localize_script('frizzly-admin', 'FrizzlySettings', array('screen' => $admin_screen));
	}
}
add_action( 'admin_enqueue_scripts', 'frizzly_load_admin_scripts' );


function frizzly_print_advanced_scripts() {
	global $frizzly_options;

	if ( '' === $frizzly_options['advanced']['additionalJS'])
		return;

	if ( wp_script_is( 'frizzly-main', 'done' ) ) {
	?>
		<script type="text/javascript">
			<?php echo $frizzly_options['advanced']['additionalJS']; ?>
		</script>
	<?php
	}
}
add_action( 'wp_footer', 'frizzly_print_advanced_scripts', 11 );