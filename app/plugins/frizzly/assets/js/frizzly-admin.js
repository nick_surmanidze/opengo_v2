(function($) {

	var readyFunctions = {
		'ButtonSetScreen' : function() {
			$( ".frizzly-button-list" ).sortable({
				connectWith: ".frizzly-button-list"
			}).disableSelection();

			$( "#selected_buttons" ).on( "sortupdate", function( event, ui ) {
				$('#frizzly_button_set_settings').val( $('#selected_buttons').sortable( 'toArray', { attribute: 'data-value'}) );
			} );
		},
		'SettingsScreen' : function() {
			$( ".frizzly-button-list" ).each( function() {
				var $this = $(this);
				var id = '#' + $this.data('input-id');
				$this.sortable();
				$this.on( 'sortupdate', function( event, ui) {
					$(id).val( $this.sortable( 'toArray', { attribute: 'data-value' }));
				});
			});
		}
	};

	$(function() {
		if (FrizzlySettings === undefined) return;

		readyFunctions[ FrizzlySettings.screen ].call(null);
	});
})(jQuery);
