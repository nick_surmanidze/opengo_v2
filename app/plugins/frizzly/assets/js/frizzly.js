(function($) {

	if (!String.format) {
		String.format = function(format) {
			var args = Array.prototype.slice.call(arguments, 1);
			return format.replace(/{(\d+)}/g, function(match, number) {
				return typeof args[number] != 'undefined'
						? args[number]
						: match
						;
			});
		};
	}

	var jQueryExtensions = {
		transferDataAttributes: function( $source, $destination ) {
			var attributes = $source.data();
			for(var key in attributes){
				if (attributes.hasOwnProperty(key))
					$destination.attr('data-' + key, attributes[key]);
			}
		}
	};

	function BaseElement() {
		this.$element = null;
	}

	BaseElement.prototype.addAttr = function (attrName, attrValue) {
		this.$element.attr(attrName, attrValue);
	};

	BaseElement.prototype.addClass = function( className ) {
		this.$element.addClass(className);
	};

	window.frizzly = (function () {

		/* Internal classes */

		/* Icon class - represents a single icon */
		function Icon(width, height, type){
			BaseElement.call(this);
			this.width = width;
			this.height = height;
			this.$element = $('<a/>');
			this.addAttr(frizzly.attr.type, type);
			this.addAttr('href', '#');
			this.$iconElement = $('<div/>').addClass( frizzly.cssClass.iconPrefix + type);
			this.$iconElement.html( $('<div/>').addClass( frizzly.cssClass.prefix + 'inner') );
		}

		Icon.prototype = new BaseElement();

		Icon.prototype.createElement = function() {
			return this.$element.html( this.$iconElement );
		};

		/* Container class - holds icons */
		function BaseContainer(type) {
			this.icons = [];
			this.width = 0;
			this.height = 0;
			this.$element = $('<div/>');

			switch (type)	{
				case 'vertical':
					this.updateContainerSize = this.updateSizeVertical;
					break;
				case 'horizontal':
					this.updateContainerSize = this.updateSizeHorizontal;
					break;
			}

			this.addClass( frizzly.cssClass.container );
		}

		BaseContainer.prototype = new BaseElement();

		BaseContainer.prototype.updateSizeHorizontal = function (width, height) {
			this.width += width;
			this.height = height > this.height ? height : this.height;
		};

		BaseContainer.prototype.updateSizeVertical = function (width, height) {
			this.height += height;
			this.width = width > this.width ? width : this.width;
		};

		BaseContainer.prototype.createContainer = function() {
			var $elements = [];

			for(var i = 0; i < this.icons.length; i++)
				$elements.push(this.icons[i].createElement());

			return this.$element
					.css('min-height', this.height + 'px')
					.css('min-width', this.width + 'px')
					.html( $elements );
		};

		BaseContainer.prototype.addIcon = function( icon ) {
			this.icons.push( icon );
			this.updateContainerSize(icon.width, icon.height);
			return this;
		};


		function ClickHandlerArg() {
			this.url = '';
			this.imageUrl = '';
			this.description = '';
		}

		var _functions = {
			openWindow: function(url, name, width, height) {
				var topOffset = Math.round(screen.height/2 - height/ 2),
						leftOffset = Math.round(screen.width/2 - width/2);
				window.open(url, name,
						String.format('width={0},height={1},status=0,toolbar=0,menubar=0,location=1,scrollbars=1,top={2},left={3}', width, height, topOffset, leftOffset ));

			}
		}

		var frizzly = {};

		/* Container for all attributes */
		frizzly.attr = {};
		frizzly.attr.prefix = 'data-frizzly';
		frizzly.attr.type  = frizzly.attr.prefix + 'Type';
		frizzly.attr.postTitle  = frizzly.attr.prefix + 'PostTitle';
		frizzly.attr.postUrl  = frizzly.attr.prefix + 'PostUrl';

		/* Containers for CSS classes */
		frizzly.cssClass = {};
		frizzly.cssClass.prefix = 'frizzly-';
		frizzly.cssClass.container = frizzly.cssClass.prefix + 'container';
		frizzly.cssClass.iconPrefix = frizzly.cssClass.prefix + 'icon-';

		frizzly.buttonTypes = {
			pinterestShare: 'pinterest',
			twitterShare: 'twitter',
			facebookShare: 'facebook'
		}

		frizzly.clickHandlers = {};
		frizzly.clickHandlers[frizzly.buttonTypes.pinterestShare] = function( clickHandlerArg ) {

			var url = String.format('http://pinterest.com/pin/create/bookmarklet/?is_video=false&url={0}&media={1}&description={2}',
					encodeURIComponent( clickHandlerArg.url ),
					encodeURIComponent( clickHandlerArg.imageUrl ),
					encodeURIComponent( clickHandlerArg.description )
			);
			_functions.openWindow(url, 'Pinterest', 632, 453);
			return false;
		}

		frizzly.clickHandlers[frizzly.buttonTypes.twitterShare] = function( clickHandlerArg ) {
			/* https://dev.twitter.com/docs/intents */
			var via = '';
			if (frizzly.buttonSettings.settings.twitterHandle)
				via = String.format('&via={0}', frizzly.buttonSettings.settings.twitterHandle);
			var url = String.format('https://twitter.com/intent/tweet?text={0}&url={1}{2}',
					encodeURIComponent( clickHandlerArg.postTitle ),
					encodeURIComponent( clickHandlerArg.url ),
					via
			);
			_functions.openWindow(url, 'Twitter', 550, 470);
			return false;
		}

		frizzly.clickHandlers[frizzly.buttonTypes.facebookShare] = function( clickHandlerArg ){
			/* https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.addthis.com%2F%23.U7WMEWqqez4.facebook */
			var url = String.format('https://www.facebook.com/sharer/sharer.php?u={0}&display=popup',
					encodeURIComponent( clickHandlerArg.url )
			);

			_functions.openWindow(url, 'Facebook', 550, 420);
			return false;
		}

		frizzly.debug = function(args) {
			if ( 1 || frizzly.main.settings.debug ) {
				console.log('Frizzly.log');
				console.log(args);
			}
		};

		frizzly.init = function() {
			//remove console.log errors
			var console = (window.console = window.console || {});
			if (!console['log']) console['log'] = function() {};
			//check if in debug mode
		  frizzly.main.settings.debug = window.location.search.indexOf('frizzlydebug') != -1;
		}

		/* Container for themes */
		frizzly.themes = {};

		frizzly.getTheme = function( themeName ){
			if (frizzly.themes.hasOwnProperty( themeName ) )
				return frizzly.themes[ themeName ];
			return frizzly.themes[ 'default48' ];
		};

		frizzly.addTheme = function( themeName, theme) {
			frizzly.themes[ themeName ] = theme;
		}

		var default48Theme = { buttons: {} };
		default48Theme.buttons[frizzly.buttonTypes.pinterestShare] = { height: 48, width: 48 };
		default48Theme.buttons[frizzly.buttonTypes.twitterShare] = { height: 48, width: 48 };
		default48Theme.buttons[frizzly.buttonTypes.facebookShare] = { height: 48, width: 48 };
		frizzly.addTheme( 'default48', default48Theme );

		/* Container for button sets */
		frizzly.buttonSets = {};
		frizzly.getButtonSet = function( buttonSetName ) {
			if (frizzly.buttonSets.hasOwnProperty( buttonSetName ) )
				return frizzly.buttonSets[ buttonSetName ];
			return frizzly.buttonSets[ 'default' ];
		}

		frizzly.addButtonSet = function( name, buttonSet ) {
			frizzly.buttonSets[ name ] = buttonSet;
		}

		frizzly.addButtonSet( 'default', [ frizzly.buttonTypes.pinterestShare, frizzly.buttonTypes.facebookShare, frizzly.buttonTypes.twitterShare]);

		/* Module names */
		frizzly.moduleNames = {
			main: 'main',
			hover: 'hover',
			lightbox: 'lightbox',
			buttonSettings: 'buttonSettings'
		};

		/* Container for modules */
		frizzly.module = {};

		/* ======================================================================================== */
		/* MAIN MODULE */
		/*==========================================================================================*/

		frizzly.main = frizzly.module[frizzly.moduleNames.main] = (function (){
			var _settings = {
				debug: 1
			};

			var module = {};
			module.settings = _settings;
			module.setSettings = function( settings) {
				_settings = $.extend( _settings, settings );
			};

			return module;
		})();

		/* ======================================================================================== */
		/* END MAIN MODULE */
		/*==========================================================================================*/

		/* ======================================================================================== */
		/* BUTTON SETTINGS MODULE */
		/*==========================================================================================*/

		frizzly.buttonSettings = frizzly.module[frizzly.moduleNames.buttonSettings] = (function (){
			var _settings = {
				'pinterestImageDescription': ['titleAttribute', 'altAttribute'],
				'twitterHandle': ''
			};

			var module = {};
			module.settings = _settings;
			module.setSettings = function( settings) {
				_settings = $.extend( _settings, settings );
			};

			return module;
		})();

		/* ======================================================================================== */
		/* END BUTTON SETTINGS MODULE */
		/*==========================================================================================*/

		/* ======================================================================================== */
		/* HOVER MODULE */
		/*==========================================================================================*/
		frizzly.module[frizzly.moduleNames.hover] = (function (){

			/* Private vars */
			var imageIndex = 0;
			var attr = {
				ignore: frizzly.attr.prefix + 'Ignore',
				imageDescription: frizzly.attr.prefix + 'ImageDescription',
				index : frizzly.attr.prefix + 'Index',
				postContainer: frizzly.attr.prefix + 'HoverContainer',
				timeoutId: frizzly.attr.prefix + 'TimeoutId',
				timeoutId2: frizzly.attr.prefix + 'TimeoutId2'
			};
			var classes = {
				visible: 'visible',
				overlay: frizzly.cssClass.prefix + 'hover-overlay',
				container: frizzly.cssClass.prefix + 'hover-container'
			};
			var _settings = {
				theme: 'default48',
				buttonSet: 'default',
				orientation: 'horizontal',
				hoverPanelPosition: 'middle-middle',
				imageSelector: String.format('.[0] img', classes.container),
				minImageHeight: 100,
				minImageWidth: 100,
				descriptionSource: ['titleAttribute', 'altAttribute'],
				disabledClasses: '.wp-smiley',
				showOnLightbox: '1',
				enabledClasses: '*',
				parentContainerSelector: '',
				parentContainerLevel: 2
			}

			/* Private classes */
			/* IconContainer - holds icons */
			function HoverContainer(type, index) {
				BaseContainer.call(this, type);
				this.addClass(classes.overlay);
				this.addAttr(module.attr.index, index);
			}

			HoverContainer.prototype = new BaseContainer();

			/* Private functions */
			function getNextImageIndex(){	return ++imageIndex;}

			function validateImage( $image ) {
				return $image[0].clientHeight >= _settings.minImageHeight
						&& $image[0].clientWidth >= _settings.minImageWidth
						&& $image.not( _settings.disabledClasses).length > 0
						&& $image.filter( _settings.enabledClasses).length > 0;
			}

			function getDescriptionValue( $image, settingName ) {
				switch( settingName ) {
					case 'titleAttribute':
						return $image.attr('title');
					case 'altAttribute':
						return $image.attr('alt');
					default:
						return '';
				}
			}

			function getDescription( $image ) {
				var result = '',
						descriptionSource = frizzly.buttonSettings.settings.pinterestImageDescription;

				for(var i = 0; i < descriptionSource.length && !result; i++) {
					result = getDescriptionValue( $image, descriptionSource[i] );
				}
				return result;
			}

			function getAttrFromContainerOrDefault( $image, attrName, defaultVal ) {
				var $parent = $image.parents( String.format('[{0}]', attr.postContainer));
				return $parent.attr( attrName ) || defaultVal;
			}

			function getUrl( $image ){
				return getAttrFromContainerOrDefault( $image, frizzly.attr.postUrl, document.URL );
			}

			function getPostTitle( $image ) {
				return getAttrFromContainerOrDefault( $image, frizzly.attr.postTitle, document.title );
			}

			function getContainerOffset(	imageSize, containerSize) {
				var top = 0,
						left = 0;
				var getVerticalMiddle = function() { return Math.round(imageSize.height/2 - containerSize.height/2);},
						getVerticalBottom = function() { return Math.round(imageSize.height - containerSize.height);},
						getHorizontalMiddle = function() { return Math.round(imageSize.width/2 - containerSize.width/2)},
						getHorizontalRight = function() { return Math.round(imageSize.width - containerSize.width)};

				switch ( _settings.hoverPanelPosition ){
					case 'top-left':
						top = 0;
						left = 0;
						break;
					case 'top-middle':
						top = 0;
						left =  getHorizontalMiddle();
						break;
					case 'top-right':
						top = 0;
						left = getHorizontalRight();
						break;
					case 'middle-left':
						top = getVerticalMiddle();
						left = 0;
						break;
					case 'middle-middle':
						top = getVerticalMiddle();
						left = getHorizontalMiddle();
						break;
					case 'middle-right':
						top = getVerticalMiddle();
						left = getHorizontalRight();
						break;
					case 'bottom-left':
						top = getVerticalBottom();
						left = 0;
						break;
					case 'bottom-middle':
						top = getVerticalBottom();
						left = getHorizontalMiddle();
						break;
					case 'bottom-right':
						top = getVerticalBottom();
						left = getHorizontalRight();
						break;
				}
				return { top: top, left: left };
			}

			function getContainerSelector(index) {
				if (index !== undefined)
					return String.format('div.{0}[{1}="{2}"]', classes.overlay, attr.index, index);
				else
					return String.format('div.{0}', classes.overlay);
			}

			function getImageSelector(){
				var selectors = [];

				if (_settings['showOnLightbox'] == '1')
					selectors.push( 'img.cboxPhoto' );

				if (_settings.imageSelector != '')
					selectors.push( String.format('{0}:not([{1}])', _settings.imageSelector, attr.ignore) );

				return selectors.join(',');
			}

			function getImageSource( $imageElement ) {
				var tagName = $imageElement.prop('tagName')
				switch (tagName.toLowerCase()){
					case 'div':
						return $imageElement.css('background-image').replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
					case 'img':
						return $imageElement.attr('src');
					default:
						return '';
				}
			}

			function getIconContainer(index) {
				var themeName = _settings.theme;
				var buttonSet = frizzly.getButtonSet( _settings.buttonSet );
				var currentTheme = frizzly.getTheme( themeName );
				var container = new HoverContainer(_settings.orientation, index);
				container.addClass( themeName );

				for(var i = 0; i < buttonSet.length; i++) {
					container.addIcon(new Icon(currentTheme.buttons[ buttonSet[i] ].width, currentTheme.buttons[ buttonSet[i] ].height, buttonSet[i]) );
				}
				return container;
			}

			function onHover() {
				var $image = $(this);

				if ( !$image.attr(attr.index) && validateImage( $image ) ) {
					$image.attr(attr.index, getNextImageIndex());
				}
				var index = $image.attr(attr.index);

				if (!index) {
					$image.attr( attr.ignore, '' );
					return;
				}

				var $container = $( getContainerSelector(index) );

				if ( $container.length == 0 ) {
					//no container - we have to create it
					var container = getIconContainer( index );
					var $containerElement = container.createContainer();

					var containerOffset = getContainerOffset(
							{ height: $image[0].clientHeight, width: $image[0].clientWidth },
							{ height: container.height, width: container.width }
					);
					var imageOffset = $image.offset();
					var finalOffset = {
						top: imageOffset.top + containerOffset.top,
						left: imageOffset.left + containerOffset.left
					};

					$image.after($containerElement);
					$containerElement.offset(finalOffset).addClass( classes.visible );
				} else {
					//container exists, we need to cancel its hiding
					cancelHide( $container );
				}
			}

			/* Hides the icon container */
			function asyncHide( $container ) {
				var timeoutId = setTimeout(function(){
					$container.removeClass( classes.visible );
					$container.attr(attr.timeoutId2, setTimeout(function() { $container.remove();	}, 600));
				}, 100 );
				$container.attr(attr.timeoutId, timeoutId);
			}

			/* Cancel hiding the overlay */
			function cancelHide( $container ) {
				clearTimeout( $container.attr(attr.timeoutId2) );
				clearTimeout( $container.attr(attr.timeoutId) );
				$container.addClass( classes.visible );
			}

			/* Handle clicking on a link */
			function onClick( ) {
				var $link = $(this);

				var type = $link.attr(frizzly.attr.type);
				if ( frizzly.clickHandlers[ type ] === undefined)
					return false;

				var index = $link.parent( String.format("div.{0}", frizzly.cssClass.container)).attr(attr.index);
				var $image = $( String.format('[{0}="{1}"]', attr.index, index));

				var clickHandlerArg = new ClickHandlerArg();
				clickHandlerArg.url = getUrl( $image );
				clickHandlerArg.imageUrl = getImageSource( $image );
				clickHandlerArg.description = getDescription( $image );
				clickHandlerArg.postTitle = getPostTitle( $image );
				frizzly.clickHandlers[ type ].call($link, clickHandlerArg);
				return false;
			}

			function createPostContainer() {
				var $this = $(this);
				//empty jQuery element
				var $parent = $();
				//find the post container
				if( _settings.parentContainerSelector )
					$parent = $this.parents( _settings.parentContainerSelector).first();
				if ( $parent.length == 0 ) {
					$parent = $this;
					for(var i = 0; i < _settings.parentContainerLevel; i++)
						$parent = $parent.parent();
				}
				//transfer data attributes
				if ($parent.length > 0){
					jQueryExtensions.transferDataAttributes($this, $parent);
					$parent.addClass(classes.container);
				}
			}

			/* Public stuff */
			var module = {};

			module.attr = attr;

			module.onReady = function() {
				$( String.format('input[{0}]', attr.postContainer )). each( createPostContainer );

				$( document ).delegate( getImageSelector(), 'mouseenter', onHover);

				$( document ).delegate( getImageSelector(), 'mouseleave', function() {
					var index = $(this).attr(attr.index);
					var $container = $( getContainerSelector(index) );
					asyncHide( $container );
				});

				$( document).delegate( String.format("div.{0} a", classes.overlay), 'click', onClick);

				$( document ).delegate( getContainerSelector(), 'mouseenter', function() {
					cancelHide( $( this ) );
				});

				$( document ).delegate( getContainerSelector(), 'mouseleave', function() {
					asyncHide( $( this ) );
				});
			};

			module.onResize = function() {
				$( String.format('[{0}]', module.attr.ignore)).each( function() {	$(this).removeAttr( module.attr.ignore );	});
				$( String.format('[{0}]', module.attr.index)).each( function() {	$(this).removeAttr( module.attr.index );	});
			}

			module.setSettings = function( settings) {
				_settings = $.extend( _settings, settings );
			};

			return module;
		})();

		/* ======================================================================================== */
		/* END HOVER MODULE */
		/*==========================================================================================*/

		/* ======================================================================================== */
		/* LIGHTBOX MODULE */
		/*==========================================================================================*/

		frizzly.module[frizzly.moduleNames.lightbox] = (function (){
			/* Private variables */
			var _settings = {
				pluginExists: $.fn.colorbox != 'undefined',
				descriptionSource: [ 'titleAttribute', 'altAttribute']
			};

			var _classes = {
				groupPrefix : frizzly.cssClass.prefix + 'group-'
			}

			/* Private functions */
			function getDescriptionValue( $image, settingName ) {
				switch( settingName ) {
					case 'titleAttribute':
						return $image.attr('title');
					case 'altAttribute':
						return $image.attr('alt');
					default:
						return '';
				}
			}

			function getDescription( $image ) {
				var result = '';
				for(var i = 0; i < _settings.descriptionSource.length && !result; i++) {
					result = getDescriptionValue( $image, _settings.descriptionSource[i] );
				}
				return result;
			}

			var module = {};

			module.settings = _settings;
			module.setSettings = function( settings) {
				_settings = $.extend( _settings, settings );
			};

			module.onReady = function() {
				if (!_settings.pluginExists) return;

				$( String.format('a:has(img[class*={0}])', _classes.groupPrefix)).each(function(){
					var $img = $(this).find('img');
					var rel = $img.attr('class').match( String.format("{0}[0-9]+", _classes.groupPrefix));
					var title = getDescription( $img );
					$(this).colorbox({
						rel: rel[0],
						title: title,
						maxWidth: '95%',
						maxHeight: '95%'
					});

				});
			};

			return module;
		})();

		/* ======================================================================================== */
		/* END LIGHTBOX MODULE */
		/*==========================================================================================*/

		frizzly.setSettings = function( settings ) {
			frizzly.debug(settings);

			/* Add all button sets */
			for(var buttonSetName in settings.buttonSets) {
				if ( settings.buttonSets.hasOwnProperty( buttonSetName ) )
					frizzly.addButtonSet( buttonSetName, settings.buttonSets[ buttonSetName ] );
			}

			/* Add all themes */
			for(var themeName in settings.themes) {
				if ( settings.themes.hasOwnProperty( themeName ) )
					frizzly.addTheme( settings.themes[ themeName ] );
			}

			/* Distribute module settings across modules */
			for(var moduleName in settings.modules){
				if ( settings.modules.hasOwnProperty( moduleName ) && frizzly.module.hasOwnProperty( moduleName ) )
					frizzly.module[ moduleName ].setSettings( settings.modules[ moduleName ] );
			}
		}

		frizzly.triggerActiveModules = function( functionName ) {
			for(var moduleName in frizzly.module) {
				if (frizzly.module.hasOwnProperty(moduleName)){
					var moduleNameWithUpperCase = moduleName.charAt(0).toUpperCase() + moduleName.slice(1);
					var settingName = 'module' + moduleNameWithUpperCase + 'Active';
					if (frizzly.module.hasOwnProperty( moduleName )
							&& typeof frizzly.module[moduleName][functionName] === 'function'
							&& frizzly.main.settings[ settingName ] == '1')	{
							frizzly.module[moduleName][functionName]();
					}
				}
			}
		};

		frizzly.onReady = function() {
			frizzly.debug('onReady');
			frizzly.triggerActiveModules('onReady');
		}

		frizzly.onResize = function() {
			frizzly.debug('onResize');
			frizzly.triggerActiveModules('onResize');
		}

		return frizzly;
	}());


	$(function() {

		frizzly.init();
		frizzly.setSettings( FrizzlySettings );
		frizzly.onReady();

		$(window).resize( frizzly.onResize );
	});

})(jQuery);