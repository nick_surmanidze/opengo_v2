<?php
// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

//delete settings
delete_option('frizzly_settings');

//delete custom post types
$custom_post_types = array('frizzly_button_set', 'frizzly_theme', 'frizzly_shortcode');

foreach($custom_post_types as $custom_post_type){
	$posts = get_posts( array( 'post_type' => $custom_post_type) );
	foreach( $button_sets as $post ) {
		wp_delete_post( $post->ID, true);
	}
}

//delete post meta
delete_post_meta_by_key( '_frizzly_settings' );