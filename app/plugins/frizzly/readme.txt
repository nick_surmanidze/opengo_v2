=== Frizzly ===
Contributors: mrsztuczkens
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VJ5EXW2FH5DJU
Tags: Facebook, twitter, pinterest, share, share button, bookmark, Share, share this, sharing, social, socialize, pin it, button, image, images, pinit, social media, hover, photo, photos, lightbox, colorbox
Requires at least: 3.5
Tested up to: 4.0
Stable tag: 0.34
License: GPLv2 and (components under MIT License)
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Frizzly allows you to add share buttons where you need them: over images, in post content or widget areas.

== Description ==
If you use many different plugins to add social media share buttons to your website, most likely each plugin has its own images and your website looks inconsistent. Frizzly is here to fix that. Frizzly allows you to share your posts on three social networks: Facebook, Twitter and Pinterest. You can easily choose on which pages the share buttons should show up and where they should be placed.

Features:

* showing share icons when users hover over images (especially useful for blogs about cooking and fashion where people add a ton of great photos)
* show share buttons only on images of your choice by using classes or image size constraints
* lightbox to show your images in full size (images shown in lightbox also have share icons!)
* shortcodes - you can easily add share buttons before or after your posts or in any widget area your theme offers (think sidebar, footer, etc.)
* themes - you can use your own share icons customized to fit your brand
* showing only share buttons you need
* don't show share buttons on certain pages (e.g. your home page)


As the tool to create the lightbox Frizzly uses [ColorBox](http://www.jacklmoore.com/colorbox). ColorBox was created by Jack Moore and is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php). ColorBox remains unchanged - Frizzly only uses what it already provides.

To learn more about the plugin and read tutorials on how to use it, visit [mrsztuczkens.me](http://www.mrsztuczkens.me/).

== Frequently Asked Questions ==

= Does the plugin allow to use my own share icons? =
Yes, it does! Feel free to create and use your own icons using the themes feature.

= Where do I report bugs, improvements and suggestions? =
Please report them in the plugin's [support forum on WordPress.org](https://wordpress.org/support/plugin/frizzly).

== Screenshots ==
1. Share buttons on hover.
2. Lightbox with share buttons.
3. Settings

== Changelog ==

= 0.34 =
* Released 2014-11-22
* New: added custom CSS field in advanced options
* Enhancement: the plugin now uses transients instead of constantly downloading all button sets, themes and shortcodes from database
* New: added support for image caption for Pin description
* New: possibility to style hovered image

= 0.33 =
* Released 2014-11-11
* New: added horizonstal orientation to hover module

= 0.32 =
* Released 2014-11-06
* Fix: changed get_the_permalink -> get_permalink
* New: added filter to ignore post types

= 0.31 =
* Released 2014-10-23
* New feature: Pinterest description can be taken from image description from Media Library
* Fix: fixed issue with too long image descriptions in lightbox
* Fix: disabled the plugin for feeds

= 0.30 =
* Released 2014-10-21
* New feature: advanced settings that allow the user to change some settings that are not available in the settings panel
* Few small fixes and changes

= 0.22 =
* Release 2014-10-15
* Fix: Pin description now works properly when pinning from lightbox

= 0.21 =
* Release 2014-10-10
* Fix: Themes with icons in sizes other 48x48 now should work properly
* New feature: use full-sized images if they are linked to the image

= 0.20 =
* Release 2014-10-07
* Added shortcode support

= 0.12 =
* Release 2014-09-14
* Added theme support

= 0.11 =
* Release 2014-08-10
* Code changes for PHP 5.3 compatibility

= 0.1 =
* Release 2014-08-03
* First version of the plugin

== Upgrade Notice ==

= 0.34 =
* Few minor features and enhancements

= 0.33 =
* New minor feature

= 0.32 =
* A minor fix and one new minor feature

= 0.31 =
* Few minor fixes and one new minor feature

= 0.30 =
* Added advanced settings feature plus a few small fixes

= 0.22 =
* Fix: Pin description now works properly when pinning from lightbox

= 0.21 =
* Themes fix plus one new feature

= 0.20 =
* Added shortcode support

= 0.12 =
* Added theme support

= 0.11 =
* Code changes for PHP 5.3 compatibility