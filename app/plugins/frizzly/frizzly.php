<?php
/**
 * @wordpress-plugin
 * Plugin Name:       Frizzly
 * Plugin URI:        http://www.mrsztuczkens.me
 * Description:       Great-looking social icons all over your website.
 * Version:           0.34
 * Author:            Marcin Skrzypiec
 * Author URI:        http://www.mrsztuczkens.me
 * Text Domain:       frizzly
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) exit;

class Frizzly_Constants {

	public static function get_version(){
		$versionConstant = '0.34';
		return $versionConstant;
	}

	private static $root_file = null;
	public static function get_root_file() {
		if ( Frizzly_Constants::$root_file == null )
			Frizzly_Constants::$root_file = __FILE__;
		return Frizzly_Constants::$root_file;
	}

	private static $plugin_url = null;
	public static function get_plugin_url() {
		if ( Frizzly_Constants::$plugin_url == null )
			Frizzly_Constants::$plugin_url = plugin_dir_url( Frizzly_Constants::get_root_file() );
		return Frizzly_Constants::$plugin_url;
	}

	private static $plugin_dir = null;
	public static function get_plugin_dir() {
		if ( Frizzly_Constants::$plugin_dir == null )
			Frizzly_Constants::$plugin_dir = plugin_dir_path( Frizzly_Constants::get_root_file() );
		return Frizzly_Constants::$plugin_dir;
	}

	private static $admin_screen_id = null;
	public static function get_admin_screen_id() {
		return Frizzly_Constants::$admin_screen_id;
	}

	public static function set_admin_screen_id( $screen_id ) {
		Frizzly_Constants::$admin_screen_id = $screen_id;
	}
}

if ( ! class_exists( 'Frizzly' ) ) :

	final class Frizzly {
		/** Singleton *************************************************************/

		private static $instance;

		private $module_manager;

		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Frizzly ) ) {
				self::$instance = new Frizzly();
				self::$instance->includes();
				self::$instance->load_textdomain();
			}
			return self::$instance;
		}

		private function includes() {
			global $frizzly_options;

			//load utils
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/Frizzly_Utils.php');

			//load modules
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/modules/Frizzly_Base_Module.php');
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/modules/Frizzly_Main_Module.php');
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/modules/Frizzly_Hover_Module.php');
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/modules/Frizzly_Lightbox_Module.php');
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/modules/Frizzly_Button_Settings_Module.php');
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/modules/Frizzly_Shortcode_Module.php');
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/modules/Frizzly_Advanced_Module.php');
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/modules/Frizzly_Module_Manager.php');

			$modules = array(
				new Frizzly_Main_Module(),
				new Frizzly_Button_Settings_Module(),
				new Frizzly_Hover_Module(),
				new Frizzly_Lightbox_Module(),
				new Frizzly_Shortcode_Module(),
				new Frizzly_Advanced_Module()
			);
			$this->module_manager = new Frizzly_Module_Manager();
			foreach($modules as $module) {
				$this->module_manager->add_module( $module);
			}

			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/admin/settings/register_settings.php');
			$frizzly_options = frizzly_get_settings();

			//load custom post types and related
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/post-types/button_set.php' );
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/post-types/theme.php' );
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/post-types/shortcode.php' );
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/post-types/transients_management.php' );
			//load widgets
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/widgets/Frizzly_Shortcode_Widget.php' );
			//load everything else
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/misc_functions.php' );
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/head.php' );
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/scripts.php' );
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/content.php' );
			require_once( Frizzly_Constants::get_plugin_dir() . 'includes/meta_boxes.php' );

			if ( is_admin() ) {
				require_once( Frizzly_Constants::get_plugin_dir() . 'includes/admin/settings/settings_page.php');
				require_once( Frizzly_Constants::get_plugin_dir() . 'includes/admin/settings/settings_callbacks.php');
				require_once( Frizzly_Constants::get_plugin_dir() . 'includes/admin/admin_notice.php');
			} else {

			}
		}

		public function load_textdomain() {
			$lang_dir = dirname( plugin_basename( Frizzly_Constants::get_root_file() ) ) . '/languages/';

			// Traditional WordPress plugin locale filter
			$locale        = apply_filters( 'plugin_locale',  get_locale(), 'frizzly' );
			$mofile        = sprintf( '%1$s-%2$s.mo', 'frizzly', $locale );

			// Setup paths to current locale file
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/frizzly/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/frizzly folder
				load_textdomain( 'frizzly', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/frizzly/languages/ folder
				load_textdomain( 'frizzly', $mofile_local );
			} else {
				// Load the default language files
				load_plugin_textdomain( 'frizzly', false, $lang_dir );
			}
		}

		public function get_module_manager() {
			return $this->module_manager;
		}
	}

endif; // End if class_exists check


function Frizzly() {
	return Frizzly::instance();
}

// Get Frizzly Running
Frizzly();