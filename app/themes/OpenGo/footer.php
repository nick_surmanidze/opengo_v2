<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package OpenGo
 */

?>

	</div><!-- #content -->


<?php if(!(is_page( 'Sign-in' ) || is_page('public-preview') || is_page('talent-cloud'))) { ?>


	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info  footer-menu-wrapper">

			<div class="middle-section">

				<div class="footer-left-menu-wrapper"><?php wp_nav_menu( array( 'theme_location' => 'footer-left' ) ); ?></div>
				<?php if(!nsauth_logged_in()) { ?>
				<div class="footer-right-menu-wrapper"><?php wp_nav_menu( array( 'theme_location' => 'footer-right' ) ); ?></div>
				<?php } ?>
			</div>

			<div class="copyright">
				Copyright © 2016 <a href="http://opengoglobal.com">OpenGo</a>. All Rights Reserved.
			</div>

		</div><!-- .site-info -->
	</footer><!-- #colophon -->

<?php } ?>


<?php

// if we are on public preview page then let's credit..

 if(is_page('public-preview') || is_page('talent-cloud')) { ?>


	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info  footer-menu-wrapper">
			<div class="copyright" style="border-top:none;">
				Powered by <a href="http://opengoglobal.com">OpenGo</a>.
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

<?php } ?>


</div><!-- #page -->
<?php if($_SERVER['HTTP_HOST'] == 'opengo.dev') { ?>


<?php

echo "<pre>";
print_r($_SESSION);
echo "</pre>";
?>


<script type='text/javascript' id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.2.9.11.js'><\/script>".replace("HOST", location.hostname));
//]]></script>
<?php } ?>

<?php wp_footer(); ?>

</body>
</html>
