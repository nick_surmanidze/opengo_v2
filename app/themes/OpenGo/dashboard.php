<?php
/**
 * Template Name: Dashboard
 * Custom template.
 */
get_header();



// check if user matches the job he registered for

$profile = opengo::get_profile();

if(opengo::completed_current_job($profile)) {

if(isset($_SESSION['gts']['signup-for-job']) && $_SESSION['gts']['signup-for-job'] > 0) {
	global $api;

$target_match_id = $_SESSION['gts']['signup-for-job'];
$match_arr = opengo::job_matches_id_array();
if(in_array($target_match_id, $match_arr)) {
} else {


		$job = $api->sendRequest(array(
			'action'             => 'read',
			'controller'         => 'job',
			'mark_read'          => false, // or jobs
			'id'                 => $target_match_id,
			'read_by_profile_id' =>  0 // profile id goes here
		));


	if(strlen($job->job->job_title) > 0) {
		$target_job_title = $job->job->job_title;
	} else {
		$target_job_title = '***';
	}


	@$api->sendRequest(array(
		'action'     => 'create',
		'controller' => 'email',
		'type'       => 'opengo_rejected_notification',
		'name'       => stripslashes(sanitize_text_field($_SESSION['nsauth']['user_display_name'])),
		'email'      => $_SESSION['nsauth']['user_email'],
		'job_title'  => $target_job_title
  ));

}

unset($_SESSION['gts']['signup-for-job']);
}


}





	$ideal = array(
		'function'      => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_function),
		'seniority'     => opengo::check_selected($profile->ideal_job->ideal_job_seniority),
		'industry'      => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_industry),
		'location'      => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_location),
		'company_type'  => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_company_type),
		'contract_type' => opengo::check_selected($profile->ideal_job->ideal_job_contract_type),
		'salary'        => opengo::check_selected($profile->ideal_job->ideal_job_basic_salary),
		'currency'      => opengo::check_selected($profile->ideal_job->ideal_job_basic_salary_currency)
		);


		$personal = array(
			'name'    => opengo::check_selected($profile->first_name),
			'surname' => opengo::check_selected($profile->last_name),
			'email'   => opengo::check_selected($profile->email_address),
			'phone'   => opengo::check_selected($profile->phone_number),
			'country' => opengo::check_selected($profile->country),
			'summary' => stripslashes(opengo::check_selected($profile->personal->personality))
		);



	if (opengo::completed_current_job($profile)) {
		$current = array(
			'organisation_name' => opengo::check_selected($profile->current_job->current_job_organisation_name),
			'industry'          => opengo::check_selected($profile->current_job->current_job_industry),
			'company_type'      => opengo::check_selected($profile->current_job->current_job_company_type),
			'job_function'      => opengo::check_selected($profile->current_job->current_job_function),
			'seniority'         => opengo::check_selected($profile->current_job->current_job_seniority),
			'year_started'      => opengo::check_selected($profile->current_job->current_job_year_started),
			'contract'          => opengo::check_selected($profile->current_job->current_job_type_of_contract),
			'notice_period'     => opengo::check_selected($profile->current_job->current_job_notice_period),
			'salary'            => opengo::check_selected($profile->current_job->current_job_basic_salary),
			'currency'          => opengo::check_selected($profile->current_job->current_job_basic_salary_currency),
			'responsibilities'  => opengo::check_selected($profile->current_job->current_job_main_responsibilities)
		);
	}


	if (opengo::completed_experience_one($profile)) {
		$experience_1 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_1->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_1->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_1->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_1->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_1->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_1->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_1->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_1->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_1->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_two($profile)) {
		$experience_2 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_2->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_2->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_2->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_2->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_2->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_2->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_2->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_2->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_2->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_three($profile)) {
		$experience_3 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_3->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_3->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_3->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_3->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_3->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_3->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_3->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_3->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_3->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_four($profile)) {
		$experience_4 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_4->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_4->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_4->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_4->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_4->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_4->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_4->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_4->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_4->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_five($profile)) {
		$experience_5 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_5->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_5->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_5->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_5->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_5->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_5->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_5->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_5->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_5->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_six($profile)) {
		$experience_6 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_6->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_6->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_6->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_6->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_6->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_6->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_6->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_6->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_6->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_seven($profile)) {
		$experience_7 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_7->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_7->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_7->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_7->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_7->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_7->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_7->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_7->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_7->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_education_one($profile)) {
		$education_1 = array(
			'university'    => stripslashes(opengo::check_selected($profile->education_1->university)),
			'course_name'   => stripslashes(opengo::check_selected($profile->education_1->course_name)),
			'qualification' => opengo::check_selected($profile->education_1->qualification_type),
			'grade'         => opengo::check_selected($profile->education_1->grade_attained),
			'year'          => opengo::check_selected($profile->education_1->year_attained)
		);
	}

	if (opengo::completed_education_two($profile)) {
		$education_2 = array(
			'university'    => stripslashes(opengo::check_selected($profile->education_2->university)),
			'course_name'   => stripslashes(opengo::check_selected($profile->education_2->course_name)),
			'qualification' => opengo::check_selected($profile->education_2->qualification_type),
			'grade'         => opengo::check_selected($profile->education_2->grade_attained),
			'year'          => opengo::check_selected($profile->education_2->year_attained)
		);
	}

	if (opengo::completed_education_three($profile)) {
		$education_3 = array(
			'university'    => stripslashes(opengo::check_selected($profile->education_3->university)),
			'course_name'   => stripslashes(opengo::check_selected($profile->education_3->course_name)),
			'qualification' => opengo::check_selected($profile->education_3->qualification_type),
			'grade'         => opengo::check_selected($profile->education_3->grade_attained),
			'year'          => opengo::check_selected($profile->education_3->year_attained)
		);
	}

	if (opengo::completed_skills($profile)) {
		$skill = array(
				'one'   => opengo::check_selected($profile->skills->specialist_skill_1),
				'two'   => opengo::check_selected($profile->skills->specialist_skill_2),
				'three' => opengo::check_selected($profile->skills->specialist_skill_3)
		);
	}

$complete_label = '<span class="label label-success">complete</span>';
$incomplete_label = '<span class="label label-warning">incomplete</span>';
$percentage = 0;


if(opengo::completed_personal($profile) == true) {
	$completed_personal_label = $complete_label;
	$percentage = $percentage + 15;
} else {
	$completed_personal_label = $incomplete_label;
}


if(opengo::completed_ideal_job($profile) == true) {
	$completed_ideal_job_label = $complete_label;
	$percentage = $percentage + 20;
} else {
	$completed_ideal_job_label = $incomplete_label;
}


if(opengo::completed_current_job($profile) == true) {
	$completed_current_job_label = $complete_label;
	$percentage = $percentage + 15;
} else {
	$completed_current_job_label = $incomplete_label;
}


if(opengo::completed_experience_one($profile) == true) {
	$completed_experience_label = $complete_label;
	$percentage = $percentage + 15;
} else {
	$completed_experience_label = $incomplete_label;
}


if(opengo::completed_education_one($profile) == true) {
	$completed_education_label = $complete_label;
	$percentage = $percentage + 20;
} else {
	$completed_education_label = $incomplete_label;
}

if(opengo::completed_skills($profile) == true) {
	$completed_skills_label = $complete_label;
	$percentage = $percentage + 15;
} else {
	$completed_skills_label = $incomplete_label;
}


$profile_progress_label = "Basic";
if($percentage > 20) {
	$profile_progress_label = "Weak";
}
if($percentage > 40) {
	$profile_progress_label = "Weak";
}
if($percentage > 60) {
	$profile_progress_label = "Average";
}
if($percentage > 80) {
	$profile_progress_label = "Excellent";
}
if($percentage > 90) {
	$profile_progress_label = "Ultimate";
}



?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">

			<div class="blue-part"></div>
			<div class="content-part">

				<?php while ( have_posts() ) : the_post(); ?>
					<div class="middle-section">
						<section class="page-general page-dashboard">



							<div class="dashboard-main-area-wrapper">
								<div class="content">

									<!-- Profile Completion Mobile Widget - Start -->
									<div class="widget profile-completion" id="mobile-profile-completion-widget">
										<div class="title">
											<h4>Profile Completion</h4>
										</div>
										<div class="content">
										<div class="bg-info og-info"><?php the_field('hint_profile_completion_widget', 'option'); ?></div>
													<h6><?php echo $profile_progress_label; ?></h6>
													<div class="progress">
													  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentage; ?>%">
													    <span><?php echo $percentage; ?>%</span>
													  </div>
													</div>
										</div>
									</div>
									<!-- Profile Completion Mobile Widget - End -->

								<!-- Ideal Job - start -->
									<div class="title"><h1>Ideal Job</h1><a  href="<?php get_home_url(); ?>/edit-ideal-job" class="btn btn-link edit-btn"><i class="fa fa-pencil"></i>edit</a>
									</div>
									<section class="section row ideal-job">


										<div class="col-md-6">

											<div class="content-snippet col-md-12">
												<b class="ideal-label">Seniority</b><br>

												<?php if (isset($ideal['seniority']) && strlen($ideal['seniority']) > 0) { ?>

													<span class="label label-info"><?php echo  $ideal['seniority']; ?></span>

												<?php } else { ?>

													<span class="label label-info">Nothing Selected</span>

												<?php } ?>

											</div>

											<div class="content-snippet col-md-12">
												<b class="ideal-label">Contract Type</b><br>
												<?php if (isset($ideal['contract_type']) && strlen($ideal['contract_type']) > 0) { ?>

													<span class="label label-info"><?php echo  $ideal['contract_type']; ?></span>

												<?php } else { ?>

													<span class="label label-info">Nothing Selected</span>

												<?php } ?>
											</div>

											<div class="content-snippet col-md-12">
												<b class="ideal-label">Basic Salary</b><br>

												<?php if (isset($ideal['salary']) && strlen($ideal['salary']) > 0) { ?>

													<span class="label label-info"><?php echo  $ideal['currency'] . " " . $ideal['salary']; ?></span>

												<?php } else { ?>

													<span class="label label-info">Nothing Selected</span>

												<?php } ?>

											</div>

											<div class="content-snippet col-md-12">
												<b class="ideal-label">Company Type</b><br>

									<?php if (isset($ideal['company_type']) && count($ideal['company_type']) > 0) {
													$nothing_selected = true;
													foreach($ideal['company_type'] as $item) {
														if(opengo::check_selected($item)) {
															echo "<span class='label label-info'>" . $item . "</span>";
															$nothing_selected = false;
														}
													}
													if($nothing_selected == true) {
														echo '<span class="label label-info">Nothing Selected</span>';
													}
												}
											else
												{ ?>
													<span class="label label-info">Nothing Selected</span>
									<?php } ?>

											</div>

										</div>

  									<div class="col-md-6">

											<div class="content-snippet col-md-12">
												<b class="ideal-label">Job Function</b><br>

									<?php if (isset($ideal['function']) && count($ideal['function']) > 0) {
													$nothing_selected = true;
													foreach($ideal['function'] as $item) {
														if(opengo::check_selected($item)) {
															echo "<span class='label label-info'>" . $item . "</span>";
															$nothing_selected = false;
														}
													}

													if($nothing_selected == true) {
														echo '<span class="label label-info">Nothing Selected</span>';
													}
												}
											else
												{ ?>
													<span class="label label-info">Nothing Selected</span>
									<?php } ?>
											</div>

											<div class="content-snippet col-md-12">
												<b class="ideal-label">Job Industry</b><br>
									<?php if (isset($ideal['industry']) && count($ideal['industry']) > 0) {
													$nothing_selected = true;
													foreach($ideal['industry'] as $item) {
														if(opengo::check_selected($item)) {
															echo "<span class='label label-info'>" . $item . "</span>";
															$nothing_selected = false;
														}
													}
													if($nothing_selected == true) {
														echo '<span class="label label-info">Nothing Selected</span>';
													}
												}
											else
												{ ?>
													<span class="label label-info">Nothing Selected</span>
									<?php } ?>
											</div>

											<div class="content-snippet col-md-12">
												<b class="ideal-label">Job Location</b><br>
									<?php if (isset($ideal['location']) && count($ideal['location']) > 0) {
													$nothing_selected = true;
													foreach($ideal['location'] as $item) {
														if(opengo::check_selected($item)) {
															echo "<span class='label label-info'>" . $item . "</span>";
															$nothing_selected = false;
														}
													}
													if($nothing_selected == true) {
														echo '<span class="label label-info">Nothing Selected</span>';
													}
												}
											else
												{ ?>
													<span class="label label-info">Nothing Selected</span>
									<?php } ?>
											</div>

  									</div>

									</section>
								<!-- Ideal Job - END -->

								<!-- Cloud CV - start -->
									<div class="title"><h1>Your Profile</h1></div>
									<section class="section row talent-url-wrapper">
									<div class='talent-url-text'>This is a url of your OpenGo profile. It shows how it is presented to recruiters.</div>

									<a class='talent-url' href="<?php get_home_url(); ?><?php echo opengo::get_my_cv_url(); ?>" target='_blank'><?php echo get_home_url(); ?><?php echo opengo::get_my_cv_url(); ?></a>

									</section>
								<!-- Cloud CV - end -->

								<!-- Profile section - start -->
									<div class="title"><h1>Edit Your Profile</h1><a href="<?php get_home_url(); ?>/edit-personal-details" class="btn btn-link edit-btn"><i class="fa fa-pencil"></i>edit</a>
									</div>
								<!-- Personal Details - start -->
									<section class="section profile row">

									<?php if(strlen($personal['name']) > 0  && strlen($personal['surname']) > 0) { ?>

										<h3 class="name"><?php echo $personal['name'] . ' ' . $personal['surname']; ?></h3>
										<div class="contacts-wrapper">
										<?php if(isset($personal['email']) && strlen($personal['email']) > 0) { ?>
												<span class="mail"><i class="fa fa-envelope-o"></i><?php echo $personal['email']; ?></span>
										<?php } ?>
										<?php if(isset($personal['phone']) && strlen($personal['phone']) > 0) { ?>
											<span class="phone"><i class="fa fa-phone"></i><?php echo $personal['phone']; ?></span>
										<?php } ?>
										</div>

										<?php } else { ?>


									<div class="add-new-item personal-details">
										<div class="text">You do not have any personal details completed. Please add one by clicking the button below.</div>
										<a href="<?php echo home_url(); ?>/edit-personal-details/" class="btn btn-success">Add Personal Details</a>
									</div>


									<?php	} ?>
								<!-- Personal Details - End -->

								<!-- Summary - start -->
										<?php if(isset($personal['summary']) && strlen(strip_tags($personal['summary'])) > 0) { ?>
										<div class="subtitle col-md-12">
											<h2>Summary</h2>

										</div>
										<div class="summary col-md-12">
											<?php echo $personal['summary']; ?>
										</div>

										<?php } else { ?>

										<?php } ?>
								<!-- Summary - end -->

								<?php if(opengo::completed_current_job($profile) || opengo::completed_experience_one($profile)) { ?>

										<div class="subtitle col-md-12">
											<h2>Career History and Key Achievements</h2>
											<a href="<?php get_home_url(); ?>/edit-current-job" class="btn btn-link edit-btn"><i class="fa fa-pencil"></i>edit</a>
										</div>


										<div class="career timeline-wrapper col-md-12">

										<?php if(!opengo::completed_current_job($profile)) { ?>


									<div class="add-new-item inner">
										<div class="text">You did not indicate your current or most recent job. Please add one by clicking the button below.</div>
										<a href="<?php echo home_url(); ?>/edit-current-job/" class="btn btn-success">Add Current Job</a>
									</div>


									<?php } else {
											?>

											<div class="item">
												<div class="timeline <?php if(!opengo::completed_experience_one($profile)) { echo 'remove-line'; } ?>">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $current['seniority']; ?>, </span>
															    <span><?php echo $current['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $current['organisation_name']; ?>, </span>
															    <span><?php echo $current['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $current['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $current['year_started']; ?> </span><span> to </span>
															    <span>date. </span><br>

															    <?php if(strlen(trim(strip_tags($current['responsibilities']))) > 0 ) { ?>
																			<span><pre><?php echo $current['responsibilities']; ?></pre></span>
															    <?php } ?>

															</small>

														</div>
													</div>
												</div>
											</div>

											<?php } ?>

									<?php if(!opengo::completed_experience_one($profile)) { ?>


									<div class="add-new-item inner">
										<div class="text">You do not have any previous job experience entries. Please add one by clicking the button below.</div>
										<a href="<?php echo home_url(); ?>/edit-previous-experience/" class="btn btn-success">Add Experience</a>
									</div>


									<?php } else {?>
											<div class="item">
												<div class="timeline <?php if(!opengo::completed_current_job($profile) && !opengo::completed_experience_two($profile)) { echo 'remove-line'; } elseif(!opengo::completed_current_job($profile) && opengo::completed_experience_two($profile)) { echo 'line-down';} elseif(!opengo::completed_experience_two($profile)) { echo 'line-up'; } ?>">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_1['seniority']; ?>, </span>
															    <span><?php echo $experience_1['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_1['organisation_name']; ?>, </span>
															    <span><?php echo $experience_1['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_1['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_1['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_1['year_ended']; ?>. </span><br>

																	<?php if(strlen(trim(strip_tags($experience_1['responsibilities']))) > 0 ) { ?>
																			<span><pre><?php echo $experience_1['responsibilities']; ?></pre></span>
															    <?php } ?>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_experience_two($profile)) { ?>
											<div class="item">
												<div class="timeline <?php if(!opengo::completed_experience_three($profile)) { echo 'line-up'; } ?>">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_2['seniority']; ?>, </span>
															    <span><?php echo $experience_2['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_2['organisation_name']; ?>, </span>
															    <span><?php echo $experience_2['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_2['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_2['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_2['year_ended']; ?>. </span><br>
																	<?php if(strlen(trim(strip_tags($experience_2['responsibilities']))) > 0 ) { ?>
																			<span><pre><?php echo $experience_2['responsibilities']; ?></pre></span>
															    <?php } ?>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_experience_three($profile)) { ?>
											<div class="item">
												<div class="timeline <?php if(!opengo::completed_experience_four($profile)) { echo 'line-up'; } ?>">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_3['seniority']; ?>, </span>
															    <span><?php echo $experience_3['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_3['organisation_name']; ?>, </span>
															    <span><?php echo $experience_3['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_3['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_3['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_3['year_ended']; ?>. </span><br>
																	<?php if(strlen(trim(strip_tags($experience_3['responsibilities']))) > 0 ) { ?>
																			<span><pre><?php echo $experience_3['responsibilities']; ?></pre></span>
															    <?php } ?>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_experience_four($profile)) { ?>
											<div class="item">
												<div class="timeline <?php if(!opengo::completed_experience_five($profile)) { echo 'line-up'; } ?>">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_4['seniority']; ?>, </span>
															    <span><?php echo $experience_4['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_4['organisation_name']; ?>, </span>
															    <span><?php echo $experience_4['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_4['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_4['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_4['year_ended']; ?>. </span><br>
																	<?php if(strlen(trim(strip_tags($experience_4['responsibilities']))) > 0 ) { ?>
																			<span><pre><?php echo $experience_4['responsibilities']; ?></pre></span>
															    <?php } ?>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_experience_five($profile)) { ?>
											<div class="item">
												<div class="timeline <?php if(!opengo::completed_experience_six($profile)) { echo 'line-up'; } ?>">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_5['seniority']; ?>, </span>
															    <span><?php echo $experience_5['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_5['organisation_name']; ?>, </span>
															    <span><?php echo $experience_5['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_5['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_5['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_5['year_ended']; ?>. </span><br>
																	<?php if(strlen(trim(strip_tags($experience_5['responsibilities']))) > 0 ) { ?>
																			<span><pre><?php echo $experience_5['responsibilities']; ?></pre></span>
															    <?php } ?>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>

											<?php if(opengo::completed_experience_six($profile)) { ?>
											<div class="item">
												<div class="timeline <?php if(!opengo::completed_experience_seven($profile)) { echo 'line-up'; } ?>">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_6['seniority']; ?>, </span>
															    <span><?php echo $experience_6['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_6['organisation_name']; ?>, </span>
															    <span><?php echo $experience_6['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_6['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_6['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_6['year_ended']; ?>. </span><br>
																	<?php if(strlen(trim(strip_tags($experience_6['responsibilities']))) > 0 ) { ?>
																			<span><pre><?php echo $experience_6['responsibilities']; ?></pre></span>
															    <?php } ?>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>

											<?php if(opengo::completed_experience_seven($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_7['seniority']; ?>, </span>
															    <span><?php echo $experience_7['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_7['organisation_name']; ?>, </span>
															    <span><?php echo $experience_7['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_7['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_7['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_7['year_ended']; ?>. </span><br>
																	<?php if(strlen(trim(strip_tags($experience_7['responsibilities']))) > 0 ) { ?>
																			<span><pre><?php echo $experience_7['responsibilities']; ?></pre></span>
															    <?php } ?>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php }  else { ?>


									<div class="add-new-item inner">
										<div class="text">You can add more previous experience entries.</div>
										<a href="<?php echo home_url(); ?>/edit-previous-experience/" class="btn btn-success">Add Experience</a>
									</div>


									<?php	} ?>

										</div>
									<?php } else { ?>

									<div class="add-new-item">
										<div class="text">You did not indicate your current or most recent job. Please add one by clicking the button below.</div>
										<a href="<?php echo home_url(); ?>/edit-current-job/" class="btn btn-success">Add Current Job</a>
									</div>

									<div class="add-new-item">
										<div class="text">You do not have any previous job experience entries. Please add one by clicking the button below.</div>
										<a href="<?php echo home_url(); ?>/edit-previous-experience/" class="btn btn-success">Add Experience</a>
									</div>


									<?php	} ?>




								<?php if(opengo::completed_education_one($profile) || opengo::completed_skills($profile)) {

									//if any of them is complete than show the section, otherwise show two messages

									?>
										<div class="subtitle col-md-12">
											<h2>Education and Skills</h2>
											<a href="<?php get_home_url(); ?>/edit-education" class="btn btn-link edit-btn"><i class="fa fa-pencil"></i>edit</a>
										</div>

										<?php // education  start ?>
										<?php if(opengo::completed_education_one($profile)) {
											// if education is complete
											?>
										<div class="education timeline-wrapper col-md-12">

											<?php if(opengo::completed_education_one($profile)) { ?>
											<div class="item">
												<div class="timeline <?php if(!opengo::completed_education_two($profile)) { echo 'line-up'; } ?>">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">
															<h5 class="list-group-item-heading">
															    <span><?php echo $education_1['university'] ?></span>
															</h5>
															<span><?php echo $education_1['course_name'] ?></span>
															<span>(<?php echo $education_1['qualification'] ?>)</span><br>
															<span>Grade Attained: <?php echo $education_1['grade'] ?></span><br>
															<small>
															    <span>Awarded in <?php echo $education_1['year'] ?></span>
															</small>
														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_education_two($profile)) { ?>
											<div class="item">
												<div class="timeline <?php if(!opengo::completed_education_three($profile)) { echo 'line-up'; } ?>">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">
															<h5 class="list-group-item-heading">
															    <span><?php echo $education_2['university'] ?></span>
															</h5>
															<span><?php echo $education_2['course_name'] ?></span>
															<span>(<?php echo $education_2['qualification'] ?>)</span><br>
															<span>Grade Attained: <?php echo $education_2['grade'] ?></span><br>
															<small>
															    <span>Awarded in <?php echo $education_2['year'] ?></span>
															</small>
														</div>
													</div>
												</div>
											</div>
											<?php } ?>

											<?php if(opengo::completed_education_three($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">
															<h5 class="list-group-item-heading">
															    <span><?php echo $education_3['university'] ?></span>
															</h5>
															<span><?php echo $education_3['course_name'] ?></span>
															<span>(<?php echo $education_3['qualification'] ?>)</span><br>
															<span>Grade Attained: <?php echo $education_3['grade'] ?></span><br>
															<small>
															    <span>Awarded in <?php echo $education_3['year'] ?></span>
															</small>
														</div>
													</div>
												</div>
											</div>
											<?php }  else { ?>

											<div class="add-new-item inner">
												<div class="text">You can add more education entries.</div>
												<a href="<?php echo home_url(); ?>/edit-education" class="btn btn-success">Add Education</a>
											</div>

											<?php	} ?>

										</div>
									<?php } else { ?>


									<div class="add-new-item">
										<div class="text">You do not have any education entries. Please add one by clicking the button below.</div>
										<a href="<?php echo home_url(); ?>/edit-education" class="btn btn-success">Add Education</a>
									</div>

									<?php	} ?>
									<?php // education  end ?>

									<?php // skills start ?>


									<?php if(opengo::completed_skills($profile)) { ?>
										<div class="subtitle col-md-12">
											<h2>Specialist and Technical Skills</h2>
											<a href="<?php get_home_url(); ?>/edit-skills" class="btn btn-link edit-btn"><i class="fa fa-pencil"></i>edit</a>
										</div>


										<div class="skills col-md-12">
											<ul class="list-group">
											<?php

											if(isset($skill['one']) && strlen($skill['one']) > 0) {
												echo "<li class='list-group-item'>".$skill['one']."</li>";
											}
											if(isset($skill['two']) && strlen($skill['two']) > 0) {
												echo "<li class='list-group-item'>".$skill['two']."</li>";
											}
											if(isset($skill['three']) && strlen($skill['three']) > 0) {
												echo "<li class='list-group-item'>".$skill['three']."</li>";
											} ?>

											</ul>

											<?php if(!isset($skill['three']) || !strlen($skill['three']) > 0) { ?>

											<div class="add-new-item inner">
												<div class="text">You can add more specialist skills.</div>
												<a href="<?php echo home_url(); ?>/edit-skills/" class="btn btn-success">Add Skills</a>
											</div>

											<?php } ?>

										</div>

 									<?php } else { ?>

									<div class="add-new-item">
										<div class="text">You did not indicate your specilist skills. Please add them by clicking the button below.</div>
										<a href="<?php echo home_url(); ?>/edit-skills/" class="btn btn-success">Add Skills</a>
									</div>


 										<?php } ?>
									<?php } else { ?>


									<div class="add-new-item">
										<div class="text">You do not have any education entries. Please add one by clicking the button below.</div>
										<a href="<?php echo home_url(); ?>/edit-education" class="btn btn-success">Add Education</a>
									</div>

									<div class="add-new-item">
										<div class="text">You did not indicate your specilist skills. Please add them by clicking the button below.</div>
										<a href="<?php echo home_url(); ?>/edit-skills/" class="btn btn-success">Add Skills</a>
									</div>


									<?php	} ?>




									</section>
								<!-- Profile section - end -->

								</div>
							</div>

							<div class="dashboard-widget-area-wrapper">
								<div class="widgets">




									<div class="widget profile-completion" id="desktop-widget">
										<div class="title">
											<h4>Profile Completion</h4>
										</div>
										<div class="content">
										<div class="bg-info og-info"><?php the_field('hint_profile_completion_widget', 'option'); ?></div>
													<h6><?php echo $profile_progress_label; ?></h6>
													<div class="progress">
													  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentage; ?>%">
													    <span><?php echo $percentage; ?>%</span>
													  </div>
													</div>


										</div>

									</div>


									<div class="widget messages">
										<div class="title">
											<h4>Messages</h4>


										<?php $new = opengo::get_number_of_new_messages();
										if($new > 0) { ?>
											<span class="label label-danger"><?php echo $new; ?> new</span>
										<?php } ?>


										</div>
										<div class="content">
											<div class="list-group">

											<?php
											$user_id = opengo::user_logged_in();
											$output_messages = opengo::get_messages(0, $user_id, '', '', 1, 4, 0);

											$msg_array = (array) $output_messages->messages;

											if(count($msg_array) > 0) {


												foreach($msg_array as $message) { ?>

											<a href="<?php get_home_url(); ?>/conversation?from-user=<?php echo $message->from_user_id; ?>&amp;to-user=<?php echo $message->to_user_id; ?>&amp;subject=<?php echo $message->message_subject; ?>" class="list-group-item"><i class="fa fa-envelope-o"></i><?php echo ogTruncate($message->sent_from, 20); ?></a>

										<?php }	} else { ?>

											<span class="no-items">You have no messages.</span>

											<?php } ?>
											</div>
										</div>
										<div class="footer">
										<?php if(count($msg_array) > 0) { ?>
												<a href="/inbox" class="btn btn-opengo-blue btn-xs">view all messages</a>
										<?php	} ?>

										</div>
									</div>


									<div class="widget jobs">
										<div class="title">
											<h4>Job Matches</h4>
											<?php $new = opengo::get_number_of_new_jobs();
											if($new > 0) { ?>
												<span class="label label-danger"><?php echo $new; ?> new</span>
											<?php } ?>
										</div>
										<div class="content">
											<div class="list-group">
											<?php $output_jobs = opengo::find_job_matches(4, 0);

											if(count((array) $output_jobs->jobs) > 0) {

												foreach ((array) $output_jobs->jobs as $job) { ?>
													<a href="/search?id=<?php echo $job->id; ?>" class="list-group-item"><i class="fa fa-briefcase"></i><?php echo ogTruncate($job->job_title, 20); ?></a>
											<?php } } else { ?>

												<span class="no-items">You have no matches.</span>

											<?php } ?>
											</div>
										</div>
										<div class="footer">
										<?php if(count((array) $output_jobs->jobs) > 0) {  ?>
											<a href="/jobs" class="btn btn-opengo-blue btn-xs">view all matches</a>
										<?php } ?>
										</div>
									</div>


									<?php if(strlen(sanitize_text_field(get_field('dashboard_ad_title', 'option'))) > 0) { ?>

									<div class="widget ad">
										<div class="title">
											<h4><?php the_field('dashboard_ad_title', 'option'); ?></h4>
										</div>
										<div class="content">
											<a href="<?php the_field('dashboard_ad_url', 'option'); ?>"><img src="<?php the_field('dashboard_ad_image', 'option'); ?>" alt="ad" class="img-rounded"></a>
										</div>
									</div>

									<?php } ?>


								</div>
							</div>

						</section><!-- .page-general-->
					</div>
				<?php endwhile; // End of the loop. ?>


			</div>


		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
