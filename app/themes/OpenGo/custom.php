<?php
/**
 * Template Name:custom
 * Custom template.
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<section class="page-general">
					<header class="page-header">
						<h1 class="page-title">Page title</h1>
					</header><!-- .page-header -->

					<div class="page-content">

						Custom Page!

					</div><!-- .page-content -->
				</section><!-- .page-general-->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
