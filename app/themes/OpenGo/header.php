<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package OpenGo
 */


?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon-16x16.png" sizes="16x16" />
<script type='text/javascript'>var ogAjax = "<?php echo admin_url('admin-ajax.php');?>"</script>


<?php

if(is_page('talent-cloud')) {

		// get user id by slug

		$slug = get_query_var('profile_slug');
		$profile = opengo::get_profile_by_slug($slug);

		//print_r($profile['user_id']);

		global $api;
		$get_avatar = $api->sendRequest(array(
	      'action'       => 'read',
	      'controller'   => 'meta',
	      'data_type'    => 'user',
	      'data_id'      => $profile->user_id,
	      'meta_key'     => 'avatar'
	  ));

		$avatar_array = unserialize($get_avatar);
		if(isset($avatar_array['url'])) {
			$current_avatar = $avatar_array['url'];
		} else {
			$current_avatar = home_url() . "/app/themes/OpenGo/images/defaultavatar.jpg";
		}


	?>

	<meta property="og:title" content="Check out my career profile"/>
	<meta property="og:url" content="<?php echo home_url(); echo $_SERVER['REQUEST_URI']; ?>" />
	<meta property="og:image" content="<?php echo $current_avatar; ?>"/>
	<meta property="og:site_name" content="OpenGo Global"/>
	<meta property="og:description" content="Check out my career profile that I created using OpenGo:  <?php echo home_url(); echo $_SERVER['REQUEST_URI']; ?>"/>

<?php } ?>




<?php if($_SERVER['HTTP_HOST'] != 'opengo.dev') { ?>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '945342705495492');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=945342705495492&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61069280-1', 'auto');
  ga('send', 'pageview');

</script>

<?php } ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

<?php if(!(is_page( 'Sign-in' ) || is_page('public-preview') || is_page('talent-cloud'))) { ?>

	<header id="masthead" class="site-header" role="banner">

		<div class="site-branding middle-section">
			<a href="/" class="logo-wrapper"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="" class="logo" alt="OpenGo"></a>

<?php if(nsauth_logged_in()) { ?>

			<nav id="site-navigation" class="main-navigation" role="navigation">

			<?php if(opengo::has_profile()) { ?>

<!--

					<a title="Jobs" href="<?php echo get_home_url();?>/jobs" class="nav-item jobs"><i class="fa fa-briefcase"></i>
					<?php $new = opengo::get_number_of_new_jobs();
					if($new > 0) { ?>
						<span class="badge header-badge"><?php echo $new; ?></span>
					<?php } else { ?>
						<span class="badge header-badge grey">0</span>
					<?php } ?>


					</a>


					<a title="Messages" href="<?php echo get_home_url();?>/inbox" class="nav-item messages"><i class="fa fa-envelope"></i>

					<?php $new = opengo::get_number_of_new_messages();
					if($new > 0) { ?>
						<span class="badge header-badge"><?php echo $new; ?></span>
					<?php } else { ?>
						<span class="badge header-badge grey">0</span>
					<?php } ?>

					</a>
-->





			<?php } ?>

					<nav class=" settings-dropdown-wrapper">
						<a title="Settings" class="nav-item nav-dropdown dropdown-toggle settings-dropdown"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i></a>
	          <ul class="dropdown-menu">
	            <li><a href="<?php echo get_home_url(); ?>/settings"><i class="fa fa-cog"></i>Settings</a></li>
	            <li role="separator" class="divider"></li>
	            <li><a href="<?php echo do_shortcode('[nsauth-logout-url]'); ?>"><i class="fa fa-sign-out"></i>Logout</a></li>
	          </ul>
					</nav>

				<a class="nav-item nav-dropdown  mobile-navigation dropdown-toggle"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="hi-username">Hi, <?php echo stripslashes(sanitize_text_field($_SESSION['nsauth']['user_display_name'])); ?></span><span class="label-menu">menu</span><i class="fa fa-bars"></i></a>
		          <ul class="dropdown-menu">
		            <li><a class="" href="<?php echo get_home_url(); ?>/blog"><i class="fa fa-angle-right"></i>Blog</a></li>
					      <li><a class=""  href="<?php echo get_home_url(); ?>/products"><i class="fa fa-angle-right"></i>Products</a></li>
		          <?php if(opengo::has_profile()) { ?>
		            <li><a href="<?php echo get_home_url(); ?>/dashboard"><i class="fa fa-tachometer"></i>Dashboard</a></li>
		            <li><a href="<?php echo get_home_url(); ?>/settings"><i class="fa fa-cog"></i>Settings</a></li>
		            <li role="separator" class="divider"></li>
		           <?php } ?>
		            <li><a href="<?php echo do_shortcode('[nsauth-logout-url]'); ?>"><i class="fa fa-sign-out"></i>Logout</a></li>
		          </ul>
			</nav><!-- #site-navigation -->


			<nav class="inline-navigation pull-right">
				<ul class="navigation">
					<li class="nav-item"><a class="desktop-item" href="<?php echo get_home_url(); ?>/blog">Blog</a></li>
					<li class="nav-item"><a class="desktop-item"  href="<?php echo get_home_url(); ?>/products">Products</a></li>
					<li class="nav-item"><a class="desktop-item"  href="<?php echo get_home_url(); ?>/dashboard">Dashboard</a></li>
				</ul>
			</nav>

<?php } else { ?>

			<nav id="site-navigation" class="main-navigation mobile-navigation" role="navigation">
				<a class="nav-item nav-dropdown"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="hi-username">menu</span><span class="label-menu">menu</span><i class="fa fa-bars"></i></a>
          <ul class="dropdown-menu">
					   <li><a class="" href="<?php echo get_home_url(); ?>/blog"><i class="fa fa-angle-right"></i>Blog</a></li>
					   <li><a class=""  href="<?php echo get_home_url(); ?>/products"><i class="fa fa-angle-right"></i>Products</a></li>
					   <li role="separator" class="divider"></li>
					   <li><a class=""  href="<?php echo get_home_url(); ?>/sign-in"><i class="fa fa-sign-in"></i>Sign in</a></li>
					   <li role="separator" class="divider"></li>
					   <li><a class="opengo-nav-btn" href="<?php echo get_home_url(); ?>/career-acceleration"><i class="fa fa-paper-plane"></i>Start here</a></li>
          </ul>
			</nav><!-- #site-navigation -->

			<nav class="inline-navigation pull-right">
				<ul class="navigation">
					<li class="nav-item"><a class="desktop-item" href="<?php echo get_home_url(); ?>/blog">Blog</a></li>
					<li class="nav-item"><a class="desktop-item"  href="<?php echo get_home_url(); ?>/products">Products</a></li>
					<li class="nav-item"><a class="desktop-item"  href="<?php echo get_home_url(); ?>/sign-in">Sign in</a></li>
					<li class="nav-item"><a class="opengo-nav-btn" href="<?php echo get_home_url(); ?>/career-acceleration">Start here</a></li>
				</ul>
			</nav>




<?php } ?>

		</div><!-- .site-branding -->

	</header><!-- #masthead -->

<?php } ?>



	<div id="content" class="site-content">
