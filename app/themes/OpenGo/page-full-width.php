<?php
/**
 * Template Name: Full Width Template
 * Custom template.
 */

get_header(); ?>

	<div id="primary" class="content-area full-width-page">
		<main id="main" class="site-main clearfix" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
						<section class="page-general  clearfix">
							<article class="page-content-wrapper full-width-page-content-wrapper">
							 	<?php the_content(); ?>
							</article>
						</section><!-- .page-general-->
				<?php endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
