<?php
// This is an admin page..


// Add Menu Pages and submenu pages.

function setup_og_mailchimp_admin_menus() {


  	add_menu_page('Mailchimp', //$page_title is the title of the page to be added
			'Mailchimp', // $menu_title is the title shown in the menu (often a shorter version of $page_title
			'manage_options', //$capability is the minimum capability required from a user in order to have access to this menu.
			'og-mailchimp-options', // $menu_slug is a unique identifier for the menu being created
			'og_mailchimp_settings'); //$function is the name of a function that is called to handle (and render) this menu page

}

// This tells WordPress to call the function named "setup_nsauth_admin_menus"
// when it's time to create the menu pages.
add_action("admin_menu", "setup_og_mailchimp_admin_menus");


//Add function generating output of the page.

function og_mailchimp_settings() {

	// Check that the user is allowed to update options
	if (!current_user_can('manage_options')) {
	    wp_die('You do not have sufficient permissions to access this page.');
	}

$og_mailchimp_api_key = get_option("og_mailchimp_api_key");
$og_mailchimp_list_id_just_registered = get_option("og_mailchimp_list_id_just_registered");

?>
<div class="wrap">
    <?php screen_icon('themes'); ?> <h2>Opengo Auth</h2>

    <form method="POST" action="">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="og_mailchimp_api_key">
                        Mailchimp API KEY:
                    </label>
                </th>

                <td>
                    <input type="text" name="og_mailchimp_api_key" value="<?php echo $og_mailchimp_api_key;?>" size="100" />
                </td>

            </tr>

            <tr valign="top">
                <th scope="row">
                    <label for="og_mailchimp_list_id_just_registered">
                        Mailchimp List ID (to subscribe just registered users):
                    </label>
                </th>

                <td>
                    <input type="text" name="og_mailchimp_list_id_just_registered" value="<?php echo $og_mailchimp_list_id_just_registered;?>" size="100" />
                </td>

            </tr>

        </table>


        <input type="hidden" name="og_mailchimp_update_settings" value="Y" />
        <input type="submit" name="submit" id="submit" class="button button-primary" value="save"/>
    </form>


</div>


<?php
}

// Fetching $_POST request with data and updating settings

if (isset($_POST["og_mailchimp_update_settings"])) {

  $og_mailchimp_api_key = esc_attr($_POST["og_mailchimp_api_key"]);
	update_option("og_mailchimp_api_key", $og_mailchimp_api_key);


	$og_mailchimp_list_id_just_registered = esc_attr($_POST["og_mailchimp_list_id_just_registered"]);
	update_option("og_mailchimp_list_id_just_registered", $og_mailchimp_list_id_just_registered);


}
