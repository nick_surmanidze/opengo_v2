<?php
/**
 * Template Name: Search (single job)
 * Custom template.
 */
get_header();

// if user matches the job
$is_match = false;
$valid_job_id = false;


if(isset($_GET['id'])) {

	if(opengo::user_logged_in()) {
	$match_array = opengo::job_matches_id_array();

	if(in_array($_GET['id'], $match_array)) {
		$is_match = true;
	} else {
		$is_match = false;
	}

	} else {
		$is_match = false;
	}


	global $api;
	$profile_id = opengo::get_profile_id_from_session();

	if($profile_id > 0) {

	$output = $api->sendRequest(array(
		'action'             => 'read',
		'controller'         => 'job',
		'mark_read'          => true, // or jobs
		'id'                 => $_GET['id'],
		'read_by_profile_id' =>  $profile_id // profile id goes here
		));

	} else {

	$output = $api->sendRequest(array(
		'action'             => 'read',
		'controller'         => 'job',
		'mark_read'          => false, // or jobs
		'id'                 => $_GET['id'],
		'read_by_profile_id' =>  0 // profile id goes here
		));

	}


	if($output->job->id > 0) {
		$valid_job_id = true;
	}


}


$applied = false;
if(($output->job->application[0]->result == 'pass') || ($output->job->application[0]->result == 'fail'))  {
	$applied = true;
}
// get single job by id including tests

// user user is logged in than show below
if(opengo::user_logged_in() != false) {


?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general single-job clearfix">

						<div class="page-title-wrapper">
							<h1><?php if($is_match == true) { echo "Job Match Found"; } else { echo "OpenGo Job"; } ?></h1>
						</div>

						<article class="page-content-wrapper">

						<?php if($valid_job_id == true) {
							// Then we can show this job
							?>




								<div class="job-match" id="job-match" data-job-id="<?php echo $_GET['id']; ?>" data-profile-id="<?php echo $profile_id; ?>">
									<h2><?php echo $output->job->job_title; ?></h2>
									<h4><?php echo $output->job->company; ?></h4>
									<pre class="no-formatting-pre"><?php echo stripslashes($output->job->job_description); ?></pre>




								<?php if(opengo::user_logged_in() != false && $is_match == true) { ?>




									<?php if(count($output->job->og_job_test_requirements) > 0) { ?>

									<?php if($applied == true) { ?>

										<div class="already-applied">You have already registered interest for this job.</div>

									<?php	} else { ?>

										<div class="capability-requirements">
											<div class="bg-info og-info">For registering interest for this job, you need to answer few questions regarding your capabilities in certain areas. Please pick your level in the drop-down list on the right.</div>

											<?php foreach($output->job->og_job_test_requirements as $capability) { ?>
												<div class="capability-line">
													<div class="capability-question" data-test-id="<?php echo $capability[2]; ?>"><?php echo $capability[0]; ?></div>
													<select class="multiselect-single" style="visibility:hidden;"  title="Level Required">
		                        <option value="">Competency Level</option>
		                        <option value="4">Expert</option>
		                        <option value="3">Proficient</option>
		                        <option value="2">Competent</option>
		                        <option value="1">Beginner</option>
		                      </select>
												</div>
												<div class="error-message">Please Indicate Your Level.</div>
											<?php } // endforeach?>
										</div>
									<?php } ?>
									<?php } //endif ?>



								<?php } //endif ?>

									<div class="button-wrapper">

										<?php if(opengo::user_logged_in()) { ?>

										<?php if($applied == false) { ?>
											<button class="btn btn-success" id="register-interest">Register Interest</button>
										<?php	} ?>


										<?php	} else { ?>


											<a href="<?php echo home_url(); ?>/?signup-for-job=<?php echo $_GET['id']; ?>" class="btn btn-success">Register and Apply</a>


										<?php	} ?>
										<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"></span>
									</div>

								</div>




						<?php } else {
							// If the job is not a match
							?>

							<article class="page-content-wrapper error-wrapper">
								<h1>404</h1>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/404.jpg" alt="404">
								<h3>Seems like this job does not exist any more.</h3>
							</article>

						<?php } ?>


						</article>




	          <!-- Modal Begin -->
	          <div class="modal fade interest_registered"  tabindex="-1" role="dialog" aria-labelledby="interest_registered">
	            <div class="modal-dialog " role="document">
	              <div class="modal-content">
	                <div class="modal-header">
	                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                  <h4 class="modal-title" id="interest_registered">Interest registered</h4>
	                </div>
	                <div class="modal-body">
	                  Your interest was registered. Recruiter will receive a notification and will contact you.
	                </div>
	                <div class="modal-footer">
	                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
	              </div><!-- /.modal-content -->
	            </div><!-- /.modal-dialog -->
	          </div><!-- /.modal -->
	          <!-- Modal END -->




					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php } else {
// if user is not logged in that show full with template and output job title and description from a shortcode.






?>

	<div id="primary" class="content-area full-width-page">
		<main id="main" class="site-main clearfix" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
						<section class="page-general  clearfix">
							<article class="page-content-wrapper full-width-page-content-wrapper">
							 	<?php the_content(); ?>
							</article>
						</section><!-- .page-general-->
				<?php endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php } ?>

<?php get_footer(); ?>
