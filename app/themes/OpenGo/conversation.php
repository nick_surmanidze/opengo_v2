<?php
/**
 * Template Name: Conversation
 * Custom template.
 */
get_header();

// getting current user id
$user_id = opengo::user_logged_in();

// getting id of the other user
if(isset($_GET['from-user']) && $_GET['from-user'] > 0) {
  if($user_id == $_GET['from-user']) {
    $from = $_GET['to-user'];
  } else {
    $from = $_GET['from-user'];
  }

}

// getting subject
if(isset($_GET['subject'])) {
	$subject = $_GET['subject'];
}
// if subject and other user id are retrieved
if($from && $subject) {
	// get the conversation via api request (see og-functions.php)
	$conversation = opengo::get_conversation($user_id, $from, $subject, 10, 0);
} else {
	$conversation = null;
}

$total = 0;
if($conversation->total > 0 ) {
$total = $conversation->total;
}
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
		<script>
		var recipient = <?php echo $from; ?>;
		var subject = '<?php echo $subject; ?>';
		var sender_name = "<?php echo $_SESSION['nsauth']['user_display_name']; ?>";</script>

			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general inbox conversation-page clearfix">

						<div class="page-title-wrapper">
							<h1>Conversation</h1>
						</div>

						<article class="page-content-wrapper">

							<div class="inbox-wrapper">
								<div class="inbox-nav">
									<div class="list-group">
									  <a href="<?php get_home_url();?>/inbox?show=inbox" class="list-group-item <?php if(is_current_item('show', 'inbox') || !isset($_GET['show'])) {echo "active"; }?>"><i class="fa fa-inbox"></i>Inbox

										<?php $new = opengo::get_number_of_new_messages();
										if($new > 0) { ?>
											<span class="badge header-badge"><?php echo $new; ?></span>
										<?php } else { ?>
											<span class="badge header-badge grey">0</span>
										<?php } ?>


									  </a>
									  <a href="<?php get_home_url();?>/inbox?show=sent" class="list-group-item <?php is_current_item('show', 'sent'); ?>"><i class="fa fa-paper-plane"></i>Sent</a>
									  <a href="<?php get_home_url();?>/inbox?show=unread" class="list-group-item <?php is_current_item('show', 'unread'); ?>"><i class="fa fa-exclamation"></i>Unread
											<?php $new = opengo::get_number_of_new_messages();
											if($new > 0) { ?>
												<span class="badge header-badge"><?php echo $new; ?></span>
											<?php } else { ?>
												<span class="badge header-badge grey">0</span>
											<?php } ?>
									  </a>
									</div>


								</div>
								<div class="inbox-content conversation-wrapper">

                <!-- Start Conversation -->
                  <div class="conversation">
										<?php
										if(count( (array) $conversation->messages) < $total) { ?>
                    <div class="load-more">
                      <span id="load-more-messages">Load Older Messages</span>
                    </div>

										<?php } ?>


                    <div class="timeline-wrapper  col-md-12">

											<?php if(count( (array) $conversation->messages) > 0 ) {


												foreach ((array) $conversation->messages as $message) {

													if ($message->from_user_id == $user_id) {
														// if current user id is "from_user_id" then this is a sent message ?>

			                      <div class="item out">
			                        <div class="timeline">
			                          <div class="circle"></div>
			                          <div class="line"></div>
			                        </div>
			                        <div class="item-content">
			                          <div class="panel panel-default">
			                            <div class="panel-body">
			                             <b><?php echo $message->sent_from; ?> ( <?php echo $message->message_sent_date; ?> )  </b><br><?php echo $message->message_body; ?>
			                            </div>
			                          </div>
			                        </div>
			                      </div>


													<?php } else { ?>


			                      <div class="item in">
			                        <div class="timeline">
			                          <div class="circle"></div>
			                          <div class="line"></div>
			                        </div>
			                        <div class="item-content">
			                          <div class="panel panel-default">
			                            <div class="panel-body">
			                            <b><?php echo $message->sent_from; ?> ( <?php echo $message->message_sent_date; ?> )  </b><br><?php echo $message->message_body; ?>
			                            </div>
			                          </div>
			                        </div>
			                      </div>

													<?php }

												}

											} else { ?>

												<div class="no-messages">There are no messages so far.</div>

											<?php } ?>

                    </div>


                  </div>

                  <div class="editor">
                    <textarea name="msg" id="msg-text" cols="30" rows="4"></textarea>
                    <span class="chars-counter">chars left <span class="chars-num">1500</span></span>
                    <span id="send-msg-btn" class="btn btn-info btn-sm"><i class="fa fa-paper-plane-o"></i> send message</span>
                  </div>

                <!-- End Conversation -->





								</div>
							</div>


						</article>

						<script>

						var maxchars = 1500;

						jQuery('#msg-text').keyup(function () {
						    var tlength = jQuery(this).val().length;
						    jQuery(this).val(jQuery(this).val().substring(0, maxchars));
						    var tlength = jQuery(this).val().length;
						    remain = maxchars - parseInt(tlength);
						    jQuery('.chars-num').text(remain);
						});

						function scroll_to_bottom() {

							var d = jQuery('.conversation');
						d.scrollTop(d.prop("scrollHeight"));
						}

						function escapeHtml(text) {
						    'use strict';
						    return text.replace(/[\"&<>]/g, function (a) {
						        return { '"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;' }[a];
						    });

						}

						function nl2br (str, is_xhtml) {
						    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
						    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
						}
						jQuery(document).ready(function() {
						  scroll_to_bottom();
						});

						jQuery("#send-msg-btn").click(function() {

							if(escapeHtml(jQuery('#msg-text').val()).length > 0) {
								jQuery('.timeline-wrapper').append("<div class='item out'><div class='timeline'><div class='circle'></div><div class='line'></div></div><div class='item-content'><div class='panel panel-default'><div class='panel-body'><b>"+sender_name + "  (sending...)</b><br>"+ nl2br (escapeHtml(jQuery('#msg-text').val())) +"</div></div></div></div>");
								var message = nl2br (escapeHtml(jQuery('#msg-text').val()));

								jQuery('#msg-text').val("");
								jQuery('.chars-num').text("1500");
								scroll_to_bottom();

								// make ajax request
								var data = {};

								data.recipient = recipient;
								data.subject = subject;
								data.body = message;

								jQuery.ajax({
								    data: ({
								    	action : 'og_send_message',
								    	params: data,
								    	}),
									    type: 'POST',
									    async: true,
									    url: ogAjax,
									  	})
								// On done return response
								.done(function( msg ) {
											console.log(msg);
											var number_of_items = jQuery('.timeline-wrapper .item').length;

											var sdata = {};

											sdata.recipient = recipient;
											sdata.subject = subject;
											sdata.limit = number_of_items;
											sdata.offset = 0;


											jQuery.ajax({
												    data: ({
												    	action : 'og_get_conversation',
												    	params: sdata,
												    	}),
													    type: 'POST',
													    async: true,
													    url: ogAjax,
													  	})
												// On done return response
												.done(function( msg ) {
													var output = JSON.parse(msg);

															console.log(msg);

															jQuery('.timeline-wrapper').html(output.data);

												});

								});


							}



						});




							jQuery("#load-more-messages").click(function() {

											var sdata = {};

											sdata.recipient = recipient;
											sdata.subject = subject;
											sdata.limit = (jQuery('.timeline-wrapper .item').length) + 10;
											sdata.offset = 0;

											jQuery.ajax({
												    data: ({
												    	action : 'og_get_conversation',
												    	params: sdata,
												    	}),
													    type: 'POST',
													    async: true,
													    url: ogAjax,
													  	})
												// On done return response
												.done(function( msg ) {
													var output = JSON.parse(msg);

															console.log(msg);

															jQuery('.timeline-wrapper').html(output.data);

															if(output.total > jQuery('.timeline-wrapper .item').length) {
																jQuery("#load-more-messages").show();
															} else {
																jQuery("#load-more-messages").hide();
															}

												});



							});


						</script>

					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
