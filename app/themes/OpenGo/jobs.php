<?php
/**
 * Template Name: Job Matches
 * Custom template.
 */
get_header();


// search for jobs

$output = opengo::find_job_matches(10, 0);
$total = $output->total;
$jobs = $output->jobs;

?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">

			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general job-matches clearfix">

						<div class="page-title-wrapper">
							<h1>Job Matches</h1>
						</div>

						<article class="page-content-wrapper">
							<?php if($output->total) { ?>
								<div class="totals">We found <?php echo $output->total; ?> job match(es) for you.</div>
							<?php } ?>


<!--  							<pre>

							<?php //print_r($output); ?>
							</pre> -->
							<div class="list-wrapper">
								<div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
								<div class="list-group">

									<?php if($output->total > 0) {
									foreach($output->jobs as $job) { ?>
								  <a href="/search?id=<?php echo $job->id; ?>" class="list-group-item job-item">
								  <?php if($job->read == 'new') { ?>
										<span class="label label-danger new">NEW</span>
								  <?php } ?>
								    <h4 class="list-group-item-heading"><?php echo $job->job_title;?></h4>
								    <h5 class="list-group-item-heading"><?php echo $job->company; ?></h5>
								  </a>


									<?php }

								} else { ?>

										<div class="nothing-found">Sorry, no matching jobs found.</div>

								<?php	} ?>


								</div>
							</div>

                <div class="load-more-btn-wrapper">
                  <span class='counter'><span class='loaded'><?php echo count($jobs); ?></span> out of <span class='total'><?php echo $total; ?></span></span>
                  <?php if(count($jobs) < $total) { ?>
                      <button class="btn btn-sm btn-success" id="load-more-button">Load More ...</button>
                  <?php } ?>

                </div>



						</article>

					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
