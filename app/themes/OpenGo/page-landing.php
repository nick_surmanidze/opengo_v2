<?php
/**
 * Template Name: Landing
 * Custom template.
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">

			<div class="blue-part"></div>
			<div class="content-part">

				<?php while ( have_posts() ) : the_post(); ?>
					<div class="middle-section">
						<section class="page-general  clearfix">

							<article class="page-content-wrapper">
							 	<?php the_content(); ?>


							</article>

						</section><!-- .page-general-->
					</div>
				<?php endwhile; // End of the loop. ?>


			</div>


		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
