<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package OpenGo
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general  clearfix">
						<article class="page-content-wrapper error-wrapper">
							<h1>404</h1>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/404.jpg" alt="404">
							<h3>Seems like the page you've requested does not exist</h3>
						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
