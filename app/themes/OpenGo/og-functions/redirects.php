<?php


add_action('template_redirect', 'og_redirects');

function og_redirects(){
### being inside a session is necessary for logging in and out.
@session_start();

   // array with restricted pages
	$restricted_if_not_logged_in = array(
		'dashboard',
		'settings',
		'add-current-job',
		'add-ideal-job',
		'edit-current-job',
		'edit-education',
		'edit-ideal-job',
		'edit-personal-details',
		'edit-previous-experience',
		'edit-skills',
		'inbox',
		'jobs'
		);


		$restricted_if_has_profile = array(
		'add-ideal-job',
		);

		$restricted_if_has_no_profile = array(
		'dashboard',
		'settings',
		'edit-current-job',
		'edit-education',
		'edit-ideal-job',
		'edit-personal-details',
		'edit-previous-experience',
		'edit-skills',
		'inbox',
		'jobs'
		);




// if registered from the ad, then pre-populate ideal job

if(is_page('add-ideal-job') && isset($_SESSION['gts']['signup-for-job']) ) {



	$job_id = $_SESSION['gts']['signup-for-job'];

	//unset($_SESSION['gts']['signup-for-job']);

	$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);


	// get job parameters

	$job_results = $apicaller->sendRequest(array(
		'action'             => 'read',
		'controller'         => 'job',
		'mark_read'          => false,
		'id'                 => $job_id,
		'read_by_profile_id' => 0
   ));

	if($job_results->job->id > 0) {

		$_POST['params']['ideal_job_seniority']    =  $job_results->job->og_job_query_seniority[0];
		$_POST['params']['ideal_job_contract']     =  $job_results->job->og_job_query_contract[0];
		$_POST['params']['ideal_job_salary']       =  $job_results->job->og_job_query_pay[0];
		$_POST['params']['ideal_job_currency']     =  $job_results->job->og_job_query_currency[0];
		$_POST['params']['ideal_job_company_type'] =  $job_results->job->og_job_query_company_type;
		$_POST['params']['ideal_job_function']     =  $job_results->job->og_job_query_function;
		$_POST['params']['ideal_job_industry']     =  $job_results->job->og_job_query_industry;
		$_POST['params']['ideal_job_location']     =  $job_results->job->og_job_query_location;


		// $_POST['params']['ideal_job_seniority']    = array($job->og_job_query_seniority[0]);
		// $_POST['params']['ideal_job_contract']     = array($job->og_job_query_contract[0]);
		// $_POST['params']['ideal_job_salary']       = array($job->og_job_query_pay[0]);
		// $_POST['params']['ideal_job_currency']     = array($job->og_job_query_currency[0]);


		// $_POST['params']['ideal_job_company_type'] = array();
		// if(isset($job->og_job_query_company_type[0])) {
		// 	$_POST['params']['ideal_job_company_type'][0] = $job->og_job_query_company_type[0];
		// }
		// if(isset($job->og_job_query_company_type[1])) {
		// 	$_POST['params']['ideal_job_company_type'][1] = $job->og_job_query_company_type[1];
		// }
		// if(isset($job->og_job_query_company_type[2])) {
		// 	$_POST['params']['ideal_job_company_type'][2] = $job->og_job_query_company_type[2];
		// }



		// $_POST['params']['ideal_job_function']     = array('one', 'two', 'three');
		// $_POST['params']['ideal_job_industry']     = array('one', 'two', 'three');
		// $_POST['params']['ideal_job_location']     = array('one', 'two', 'three');

		$result = $apicaller->sendRequest(array(
			'action'                            => 'create',
			'controller'                        => 'profile',
			'id'                                => '',
			'user_id'                           => opengo::user_logged_in(),
			'first_name'                        => '',
			'last_name'                         => '',
			'email_address'                     => $_SESSION['nsauth']['user_email'],
		  'phone_number'                      => '',
			'country'                           => '',
			'date_created'                      => 'today',

			'current_job'                       => array(
				'current_job_organisation_name'     => '',
				'current_job_industry'              => '',
				'current_job_company_type'          => '',
				'current_job_function'              => '',
				'current_job_seniority'             => '',
				'current_job_year_started'          => '',
				'current_job_type_of_contract'      => '',
				'current_job_notice_period'         => '',
				'current_job_basic_salary'          => '',
				'current_job_basic_salary_currency' => '',
				'current_job_main_responsibilities' => ''
				),

			'education_1'                       => array(
				'university'                        => '',
				'course_name'                       => '',
				'qualification_type'                => '',
				'grade_attained'                    => '',
				'year_attained'                     => ''
				),

			'education_2'                       => array(
				'university'                        => '',
				'course_name'                       => '',
				'qualification_type'                => '',
				'grade_attained'                    => '',
				'year_attained'                     => ''
				),

			'education_3'                       => array(
				'university'                        => '',
				'course_name'                       => '',
				'qualification_type'                => '',
				'grade_attained'                    => '',
				'year_attained'                     => ''
				),

			'experience_1'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_2'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_3'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_4'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),


			'experience_5'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_6'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_7'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'ideal_job'                         => array(
				'ideal_job_seniority'               => $_POST['params']['ideal_job_seniority'],
				'ideal_job_contract_type'           => $_POST['params']['ideal_job_contract'],
				'ideal_job_basic_salary'            => $_POST['params']['ideal_job_salary'],
				'ideal_job_basic_salary_currency'   => $_POST['params']['ideal_job_currency'],
				'og_pr_ideal_job_company_type'      => array(

					'og_pr_ideal_job_company_type_1'    => $_POST['params']['ideal_job_company_type'][0],
					'og_pr_ideal_job_company_type_2'    => $_POST['params']['ideal_job_company_type'][1],
					'og_pr_ideal_job_company_type_3'    => $_POST['params']['ideal_job_company_type'][2]
					),

					'og_pr_ideal_job_function'          => array(
					'og_pr_ideal_job_function_1'        => $_POST['params']['ideal_job_function'][0],
					'og_pr_ideal_job_function_2'        => $_POST['params']['ideal_job_function'][1],
					'og_pr_ideal_job_function_3'        => $_POST['params']['ideal_job_function'][2]
					),

					'og_pr_ideal_job_industry'          => array(
					'og_pr_ideal_job_industry_1'        => $_POST['params']['ideal_job_industry'][0],
					'og_pr_ideal_job_industry_2'        => $_POST['params']['ideal_job_industry'][1],
					'og_pr_ideal_job_industry_3'        => $_POST['params']['ideal_job_industry'][2],
					),

					'og_pr_ideal_job_location'          => array(
					'og_pr_ideal_job_location_1'        => $_POST['params']['ideal_job_location'][0],
					'og_pr_ideal_job_location_2'        => $_POST['params']['ideal_job_location'][1],
					'og_pr_ideal_job_location_3'        => $_POST['params']['ideal_job_location'][2],
					),

				),

			'personal'                          => array(
				'personality'                       => ''
				),

			'skills'                            => array(
				'specialist_skill_1'                => '',
				'specialist_skill_2'                => '',
				'specialist_skill_3'                => '',
				),

		));


		if($result->id) {
			// we have just created the profile and need to update session
			$_SESSION['nsauth']['user_profile'] = $result;
		}
}
	wp_redirect(home_url() . '/add-current-job');
}




if(is_page('talent-cloud')) {
	if(strlen(get_query_var('profile_slug')) !== false) {

	} else {
		wp_redirect(home_url());
	}
}

	// if not logged in redirect to home page
	if(opengo::user_logged_in() == false) {
		foreach ($restricted_if_not_logged_in as $restricted_page) {
			if(is_page($restricted_page)) {
				wp_redirect(home_url());
			}
		}
	}

	// if logged in
	if(opengo::user_logged_in() != false) {

		if(is_front_page()) {
			wp_redirect(home_url() . '/dashboard');
		}


		// if user did not indicate current job than redirect him to add-current-job
		$profile = opengo::get_profile();

		if(opengo::has_profile() != false && !opengo::completed_current_job($profile)) {

			foreach ($restricted_if_has_no_profile as $restricted_page) {
				if(is_page($restricted_page)) {
					wp_redirect(home_url() . '/add-current-job');
				}
			}
		}

		// restricted pages [if has profile]
		if(opengo::has_profile() != false) {

			foreach ($restricted_if_has_profile as $restricted_page) {
				if(is_page($restricted_page)) {
					wp_redirect(home_url() . '/dashboard');
				}
			}

		}

		// restricted pages [if has no profile]
		if(opengo::has_profile() == false) {

			foreach ($restricted_if_has_no_profile as $restricted_page) {
				if(is_page($restricted_page)) {
					wp_redirect(home_url() . '/add-ideal-job');
				}
			}

		}


		// IF USER IS ON 'ADD-CURRENT-JOB' PAGE THEN WE NEED TO CHECK IF PROFILE EXISTS. (IF IT WAS PREVIOUSLY CREATED)



	}






}




?>
