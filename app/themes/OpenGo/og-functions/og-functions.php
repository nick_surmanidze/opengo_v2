<?php


////////////////////////////////////////////////////////////
// Working with values
	@session_start();

/**
 * getting the value of current profile field
 */

function get_current_profile_value($value) {
	if (isset($_SESSION['my_profile'])) {
		// profile is in the session
		return $_SESSION['my_profile'][$value];
	} else {
		return "Nothing Selected";
	}
}


function ogTruncate($string, $limit, $pad="...")
{
  // return with no change if string is shorter than $limit
  if(strlen($string) <= $limit)

  	{
  		return $string;
		} else {
			$string = substr($string, 0, $limit) . $pad;
			return $string;
		}


}



function get_currency_sign($currency = '') {

	$currency = strtolower($currency);
	if($currency == 'usd') {
		return '$';
	} elseif ($currency == 'eur') {
		return '€';
	} elseif ($currency == 'gbp') {
		return '£';
	} else {
		return '';
	}
}



/**
 * Function is creting <option> 's
 * First argument is the value of textarea, for example textarea from ACF (get_field() function).
 * Second paramenter is optional. It indicated current value and adds "active" mark.
 * Third parameter indicates if current value us an array of current values.
 */
function list_options_from_textarea($textarea_text, $current, $is_array = false, $nothing_selected = true, $prepand = false) {

	$list_array = explode("\n", $textarea_text);
	$output = null;

	if ($prepand != false) {
		array_unshift ($list_array, $prepand);
	}

	foreach($list_array as $item) {

		$item = sanitize_text_field($item);

		$is_selected = '';

		if($is_array) {
			if(in_array($item, (array) $current)) {
				$is_selected = "selected='selected'";
			}
		} else {
			if ($current == $item) {
				$is_selected = "selected='selected'";
			}
		}

		$output .= "<option value='".esc_html(stripslashes($item))."' ".$is_selected.">".esc_html(stripslashes($item))."</option>";

	}


	if ($nothing_selected) {
		$output = "<option value=''>None selected</option>" . $output;
	}

	return $output;
}


function list_options_from_textarea_currency($textarea_text, $current, $is_array = false, $nothing_selected = true, $prepand = false) {

	$list_array = explode("\n", $textarea_text);
	$output = null;

	if ($prepand != false) {
		array_unshift ($list_array, $prepand);
	}

	foreach($list_array as $item) {

		$item = sanitize_text_field($item);

		$is_selected = '';

		if($is_array) {
			if(in_array($item, (array) $current)) {
				$is_selected = "selected='selected'";
			}
		} else {
			if ($current == $item) {
				$is_selected = "selected='selected'";
			}
		}

		$output .= "<option value='".esc_html(stripslashes($item))."' ".$is_selected.">".get_currency_sign(esc_html(stripslashes($item)))."</option>";

	}


	if ($nothing_selected) {
		$output = "<option value=''>None selected</option>" . $output;
	}

	return $output;
}


class opengo {

// checking values before outputting to the view
public static function check_selected($value) {
	if ($value == 'Nothing Selected') {
		return '';
	} else {
		return $value;
	}
}


// checking values before sending via api request
public static function check_isset($value) {

	if(isset($value)) {
		return $value;
	} else {
		return '';
	}

}

/**
 * Checks if the user is logged in. If yes -> returns user id
 */
public static function user_logged_in() {

	if(isset($_SESSION['nsauth'])) {
		return $_SESSION['nsauth']['user_id'];
	} else {
		return false;
	}

}

/**
 * if user has profile then return id, else -> false
 */
public static function has_profile() {

	if(isset($_SESSION['nsauth'])) {
		if(isset($_SESSION['nsauth']['user_profile']) && $_SESSION['nsauth']['user_profile'] !== 'none') {

			return $_SESSION['nsauth']['user_profile']->id;

		} else {
			return false;
		}

	} else {
		return false;
	}
}

/**
 * returns profie array/object mix
 */

public static function get_profile() {

	if(self::user_logged_in() !== false && self::has_profile() !== false) {

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
			'action'     => 'read',
			'controller' => 'profile',
			'id'         => self::has_profile(),
		));

		return $result;

	} else {

		return false;

	}

}


/**
 * returns profie array/object mix
 */

public static function get_profile_by_id($id) {

	if(isset($id)) {
$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
		$result = $apicaller->sendRequest(array(
			'action'     => 'read',
			'controller' => 'profile',
			'id'         => $id,
		));

		return $result;

	} else {

		return false;

	}

}


public static function get_my_cv_url()
{
	$page = '/talent-cloud/';
	$slug = $_SESSION['nsauth']['user_profile']->slug;
	$url = $page . $slug;
	return $url;
}

public static function get_profile_by_slug($slug)
{
	$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
	$result = $apicaller->sendRequest(array(
		'action'     => 'read',
		'controller' => 'profile',
		'find_by_slug' => $slug,
	));
	return $result;
}


public static function wrap($value = '', $before = '', $after = '') {
	return $value;
}


public static function remove_value_from_array($array, $value_to_remove)
{
	$array = (array) $array;
	foreach ($array as $key => $value) {
		if ($value == $value_to_remove) {
			unset($array[$key]);
		}
	}

	return $array;
}

public static function generate_cover_letter($line_break = '%0D%0A', $profile = '', $html = true)
{


	if($profile == '') {
		$profile = opengo::get_profile();
	}

	// current
	$current_company_name = false;
	$current_seniority = false;
	$current_job_function = false;

	if(opengo::check_selected($profile->current_job->current_job_organisation_name)) {
			$current_company_name = opengo::check_selected($profile->current_job->current_job_organisation_name);
	}

	if(opengo::check_selected($profile->current_job->current_job_seniority)) {
			$current_seniority = opengo::check_selected($profile->current_job->current_job_seniority);
	}

	if(opengo::check_selected($profile->current_job->current_job_function)) {
			$current_job_function = opengo::check_selected($profile->current_job->current_job_function);
	}

	$ideal_company_type = false;
	$ideal_job_function = false;
	$ideal_job_location = false;
	$ideal_currency = false;
	$ideal_salary = false;

	if(opengo::check_selected($profile->ideal_job->og_pr_ideal_job_company_type)) {
			$ideal_company_type = opengo::check_selected($profile->ideal_job->og_pr_ideal_job_company_type);
			$ideal_company_type = opengo::remove_value_from_array($ideal_company_type, "Nothing Selected");

			// $last  = array_slice($ideal_company_type, -1);
			// $first = join(', ', array_slice($array, 0, -1));
			// $both  = array_filter(array_merge(array($first), $last), 'strlen');
			// $ideal_company_type = join(' or ', $both);
	}

	if(opengo::check_selected($profile->ideal_job->og_pr_ideal_job_function)) {
			$ideal_job_function = opengo::check_selected($profile->ideal_job->og_pr_ideal_job_function);
			$ideal_job_function = opengo::remove_value_from_array($ideal_job_function, "Nothing Selected");
	}

	if(opengo::check_selected($profile->ideal_job->og_pr_ideal_job_location)) {
			$ideal_job_location = opengo::check_selected($profile->ideal_job->og_pr_ideal_job_location);
			$ideal_job_location = opengo::remove_value_from_array($ideal_job_location, "Nothing Selected");
	}

	if(opengo::check_selected($profile->ideal_job->ideal_job_basic_salary_currency)) {
			$ideal_currency = opengo::check_selected($profile->ideal_job->ideal_job_basic_salary_currency);
			$ideal_currency = opengo::remove_value_from_array($ideal_currency, "Nothing Selected");
	}

	if(opengo::check_selected($profile->ideal_job->ideal_job_basic_salary)) {
			$ideal_salary = opengo::check_selected($profile->ideal_job->ideal_job_basic_salary);
			$ideal_salary = opengo::remove_value_from_array($ideal_salary, "Nothing Selected");
	}


	$email = false;
	$phone = false;

	if(opengo::check_selected($profile->phone_number)) {
		$phone = $profile->phone_number;
	}

	if(htmlspecialchars(stripslashes(opengo::check_selected($profile->email_address)))) {
		$email = htmlspecialchars(stripslashes(opengo::check_selected($profile->email_address)));
	}


$intro = '';
$current = '';
$ideal = '';
$contact = '';



if($html == true) {


	$intro = "<p>Please find my career profile attached.".$line_break."".$line_break."
		I have provided a summary of my work experience, education, and key skills. It is designed to cover all of your important questions about me without taking much time. You can read this in 60 seconds.</p>";


if($current_company_name && $current_seniority && $current_job_function){
	$current = "<p>You will see that I am currently working at " . self::wrap($current_company_name) . " as a " . self::wrap($current_seniority) . " in " . self::wrap($current_job_function) . ". I have enjoyed this position but am ready for my next move.".$line_break."</p>";
}

if ($ideal_company_type && $ideal_job_function && $ideal_job_location && $ideal_currency && $ideal_salary) {
	$ideal = "<p>Ideally, I am seeking to join a " .
	self::wrap(implode(" or ", (array) $ideal_company_type))
	 . " in " .
	self::wrap(implode(" or ", (array) $ideal_job_function))
	 . ". I can work in " .
	self::wrap(implode(" or ", (array) $ideal_job_location))
	 . " and am seeking a salary of around " .
	 get_currency_sign($ideal_currency[0])
	  . " " .
	 self::wrap(implode(" or ", (array) $ideal_salary))
	  . " plus benefits.</p>".$line_break."";
}

if ($email && $phone) {
	$contact = "<p>I do hope my career profile is of interest to you. I can be contacted on <span class='email-span'>" . self::wrap($email) . "</span> or <span class='phone-span'>" . self::wrap($phone) . "</span>.</p>";
}



} else {

	$intro = "Please find my career profile attached.".$line_break."".$line_break."
		I have provided a summary of my work experience, education, and key skills. It is designed to cover all of your important questions about me without taking much time. You can read this in 60 seconds.";


if($current_company_name && $current_seniority && $current_job_function){
	$current = " ".$line_break."
		You will see that I am currently working at " . self::wrap($current_company_name) . " as a " . self::wrap($current_seniority) . " in " . self::wrap($current_job_function) . ". I have enjoyed this position but am ready for my next move.".$line_break."";
}

if ($ideal_company_type && $ideal_job_function && $ideal_job_location && $ideal_currency && $ideal_salary) {
	$ideal = "Ideally, I am seeking to join a " . self::wrap(implode(", ", (array) $ideal_company_type)) . " in " . self::wrap(implode(", ", (array) $ideal_job_function)) . ". I can work in " . self::wrap(implode(", ", (array) $ideal_job_location)) . " and am seeking a salary of around " . get_currency_sign($ideal_currency[0]) . " " . self::wrap(implode(", ", (array) $ideal_salary)) . " plus benefits.".$line_break."";
}

if ($email && $phone) {
	$contact = "I do hope my career profile is of interest to you. I can be contacted on " . self::wrap($email) . " or " . self::wrap($phone) . ".";
}


}





	$cover_letter = $intro . $current . $ideal . $contact;


	return $cover_letter;
}


public function generate_email_sharing_body()
{
	$name = '--';

	if(isset($_SESSION['nsauth']['user_display_name'])) {
		$name = $_SESSION['nsauth']['user_display_name'];
	}

	$body = "Please find attached a link to my career profile: " . home_url() . self::get_my_cv_url() . "%0D%0A %0D%0A";

	$body .= self::generate_cover_letter('%0D%0A','',false);

	$body .= "%0D%0A %0D%0A %0D%0A";
	$body .= "I do hope it is of interest to you and look forward to speaking to you soon. %0D%0A %0D%0A";
	$body .= "Kind regards, %0D%0A " . $name;

	return $body;
}



public static function get_profile_id_from_session() {

if(isset($_SESSION['nsauth']['user_profile'])) {
	return $_SESSION['nsauth']['user_profile']->id;
} else {
	return 0;
}

}


public static function get_number_of_new_jobs() {

	if(isset($_SESSION['nsauth']['user_profile']) && $_SESSION['nsauth']['user_profile'] > 0) {
		$output = opengo::find_job_matches(10, 0);
		$new = $output->total_new;
		return $new;
	} else {
		return 0;
	}

	return 0;

}



public static function get_number_of_new_messages() {

	if(isset($_SESSION['nsauth']['user_id']) && $_SESSION['nsauth']['user_id'] > 0) {
		$user_id = $_SESSION['nsauth']['user_id'];

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
		$result = $apicaller->sendRequest(array(
			'action'         => 'read',
			'controller'     => 'message',
			'from_user'      => 0,
			'to_user'        => $user_id,
			'qstring'        => '',
			'subject'        => '',
			'unique_subject' => false,
			'limit'          => 0,
			'offset'         => 0,
			'multi'          => true,
			'read'           => 0, // this is for new messages or 1 for all messages
		));

		$new = $result->total_unread;
		return $new;
	} else {
		return 0;
	}

	return 0;


}

/**
 * Returns messages
 */

public static function get_messages($from, $to, $query, $subject, $read, $limit, $offset) {
		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
		$result = $apicaller->sendRequest(array(
			'action'         => 'read',
			'controller'     => 'message',
			'from_user'      => $from,
			'to_user'        => $to,
			'qstring'        => $query,
			'subject'        => $subject,
			'unique_subject' => true,
			'limit'          => $limit,
			'offset'         => $offset,
			'multi'          => true,
			'read'           => $read, // this is for new messages or 1 for all messages
		));
	return $result;
}



/**
 * Returns conversation
 */

public static function get_conversation($to, $from, $subject, $limit, $offset) {
		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
		$result = $apicaller->sendRequest(array(

			'action'         => 'read',
			'controller'     => 'message',
			'from_user'      => $from,
			'to_user'        => $to,
			'qstring'        => '',
			'subject'        => $subject,
			'unique_subject' => false,

			'sort_by'        => 'date', // subject, talent, date
			'order'          => 'DESC', // ASC, DESC
			'section'        => 'inbox', // inbox or sent. Needed to determine sorting by talent. If we are in sent then we are sorting by sent to user name

			'load' => 'conversation',

			'limit'          => $limit,
			'offset'         => $offset,
			'multi'          => true,
			'read'           => 1, // this is for new messages or 1 for all messages
			));
	return $result;
}



/**
 * Tracking profile completion
 */

public static function check($value) {

	if(is_object($value)) {

	$value_arr = (array) $value;

	$result = false;

		foreach($value_arr as $array_item) {
			if($array_item != 'Nothing Selected') {
				$result = true;
			}
			return $result;
		}

	} else {
			if($value !== 'Nothing Selected') {
					return true;
			} else {
				return false;
			}
	}

}

public static function completed_personal($profile = 'undefined') {

	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(self::check($profile->first_name) && self::check($profile->last_name) && self::check($profile->email_address) ) {

			return true;
		} else {

			return false;
		}

	}

}

public static function completed_ideal_job($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile != false) {

		// checking for required fields.
		if(  self::check($profile->ideal_job->ideal_job_seniority)
			&& self::check($profile->ideal_job->ideal_job_contract_type)
			&& self::check($profile->ideal_job->ideal_job_basic_salary)
			&& self::check($profile->ideal_job->ideal_job_basic_salary_currency)
			&& self::check($profile->ideal_job->og_pr_ideal_job_company_type)
			&& self::check($profile->ideal_job->og_pr_ideal_job_function)
			&& self::check($profile->ideal_job->og_pr_ideal_job_industry)
			&& self::check($profile->ideal_job->og_pr_ideal_job_location)
			) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_current_job($profile = 'undefined') {

	if($profile == 'undefined') {
		$profile = self::get_profile();
	}


	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->current_job->current_job_organisation_name)
			&& self::check($profile->current_job->current_job_industry)
			&& self::check($profile->current_job->current_job_company_type)
			&& self::check($profile->current_job->current_job_function)
			&& self::check($profile->current_job->current_job_seniority)
			&& self::check($profile->current_job->current_job_year_started)) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_experience_one($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_1->previous_exp_organisation_name)
			&& self::check($profile->experience_1->previous_exp_industry)
			&& self::check($profile->experience_1->previous_exp_company_type)
			&& self::check($profile->experience_1->previous_exp_job_function)
			&& self::check($profile->experience_1->previous_exp_seniority)
			&& self::check($profile->experience_1->previous_exp_year_started)
			&& self::check($profile->experience_1->previous_exp_year_ended) ) {

			return true;

		} else {

			return false;
		}

	}
}


public static function completed_experience_two($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_2->previous_exp_organisation_name)
			&& self::check($profile->experience_2->previous_exp_industry)
			&& self::check($profile->experience_2->previous_exp_company_type)
			&& self::check($profile->experience_2->previous_exp_job_function)
			&& self::check($profile->experience_2->previous_exp_seniority)
			&& self::check($profile->experience_2->previous_exp_year_started)
			&& self::check($profile->experience_2->previous_exp_year_ended) ) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_experience_three($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_3->previous_exp_organisation_name)
			&& self::check($profile->experience_3->previous_exp_industry)
			&& self::check($profile->experience_3->previous_exp_company_type)
			&& self::check($profile->experience_3->previous_exp_job_function)
			&& self::check($profile->experience_3->previous_exp_seniority)
			&& self::check($profile->experience_3->previous_exp_year_started)
			&& self::check($profile->experience_3->previous_exp_year_ended) ) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_experience_four($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_4->previous_exp_organisation_name)
			&& self::check($profile->experience_4->previous_exp_industry)
			&& self::check($profile->experience_4->previous_exp_company_type)
			&& self::check($profile->experience_4->previous_exp_job_function)
			&& self::check($profile->experience_4->previous_exp_seniority)
			&& self::check($profile->experience_4->previous_exp_year_started)
			&& self::check($profile->experience_4->previous_exp_year_ended) ) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_experience_five($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_5->previous_exp_organisation_name)
			&& self::check($profile->experience_5->previous_exp_industry)
			&& self::check($profile->experience_5->previous_exp_company_type)
			&& self::check($profile->experience_5->previous_exp_job_function)
			&& self::check($profile->experience_5->previous_exp_seniority)
			&& self::check($profile->experience_5->previous_exp_year_started)
			&& self::check($profile->experience_5->previous_exp_year_ended) ) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_experience_six($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_6->previous_exp_organisation_name)
			&& self::check($profile->experience_6->previous_exp_industry)
			&& self::check($profile->experience_6->previous_exp_company_type)
			&& self::check($profile->experience_6->previous_exp_job_function)
			&& self::check($profile->experience_6->previous_exp_seniority)
			&& self::check($profile->experience_6->previous_exp_year_started)
			&& self::check($profile->experience_6->previous_exp_year_ended) ) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_experience_seven($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->experience_7->previous_exp_organisation_name)
			&& self::check($profile->experience_7->previous_exp_industry)
			&& self::check($profile->experience_7->previous_exp_company_type)
			&& self::check($profile->experience_7->previous_exp_job_function)
			&& self::check($profile->experience_7->previous_exp_seniority)
			&& self::check($profile->experience_7->previous_exp_year_started)
			&& self::check($profile->experience_7->previous_exp_year_ended) ) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_education_one($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->education_1->university)
			&& self::check($profile->education_1->course_name)
			&& self::check($profile->education_1->qualification_type)
			&& self::check($profile->education_1->grade_attained)
			&& self::check($profile->education_1->year_attained) ) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_education_two($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->education_2->university)
			&& self::check($profile->education_2->course_name)
			&& self::check($profile->education_2->qualification_type)
			&& self::check($profile->education_2->grade_attained)
			&& self::check($profile->education_2->year_attained) ) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_education_three($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}

	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->education_3->university)
			&& self::check($profile->education_3->course_name)
			&& self::check($profile->education_3->qualification_type)
			&& self::check($profile->education_3->grade_attained)
			&& self::check($profile->education_3->year_attained) ) {

			return true;

		} else {

			return false;
		}

	}
}

public static function completed_skills($profile = 'undefined') {
	if($profile == 'undefined') {
		$profile = self::get_profile();
	}
	if($profile !== false) {

		// checking for required fields.
		if(  self::check($profile->skills->specialist_skill_1)
			|| self::check($profile->skills->specialist_skill_2)
			|| self::check($profile->skills->specialist_skill_3) ) {

			return true;

		} else {

			return false;
		}

	}
}


public static function find_job_matches($limit, $offset) {

global $api;


$og_job_query_company_type         = array();
$og_job_query_contract             = array();
$og_job_query_course_name          = array();
$og_job_query_current_contract     = array();
$og_job_query_current_pay          = array();
$og_job_query_function             = array();
$og_job_query_grade_attained       = array();
$og_job_query_industry             = array();
$og_job_query_industry_experience  = array();
$og_job_query_location             = array();
$og_job_query_pay                  = array();
$og_job_query_qualification_type   = array();
$og_job_query_role_experience      = array();
$og_job_query_seniority            = array();
$og_job_query_seniority_experience = array();
$og_job_query_currency             = array();

$profile = opengo::get_profile();


	// Company type - ideal company type
	foreach((array) $profile->ideal_job->og_pr_ideal_job_company_type as $item) {
		$item = (string) $item;
		array_push($og_job_query_company_type, $item);
	}


	// ideal contract
	foreach((array) $profile->ideal_job->ideal_job_contract_type as $item) {
		$item = (string) $item;
		array_push($og_job_query_contract, $item);
	}

	// course name
	array_push($og_job_query_course_name, $profile->education_1->course_name);
	array_push($og_job_query_course_name, $profile->education_2->course_name);
	array_push($og_job_query_course_name, $profile->education_3->course_name);

	array_push($og_job_query_current_contract, $profile->current_job->current_job_type_of_contract);

	array_push($og_job_query_current_pay, $profile->current_job->current_job_basic_salary);


	foreach((array) $profile->ideal_job->og_pr_ideal_job_function as $item) {
		$item = (string) $item;
		array_push($og_job_query_function, $item);
	}


	array_push($og_job_query_grade_attained, $profile->education_1->grade_attained);
	array_push($og_job_query_grade_attained, $profile->education_2->grade_attained);
	array_push($og_job_query_grade_attained, $profile->education_3->grade_attained);

	foreach((array) $profile->ideal_job->og_pr_ideal_job_industry as $item) {
		$item = (string) $item;
		array_push($og_job_query_industry, $item);
	}

	array_push($og_job_query_industry_experience, $profile->current_job->current_job_industry);
	array_push($og_job_query_industry_experience, $profile->experience_1->previous_exp_industry);
	array_push($og_job_query_industry_experience, $profile->experience_2->previous_exp_industry);
	array_push($og_job_query_industry_experience, $profile->experience_3->previous_exp_industry);
	array_push($og_job_query_industry_experience, $profile->experience_4->previous_exp_industry);
	array_push($og_job_query_industry_experience, $profile->experience_5->previous_exp_industry);
	array_push($og_job_query_industry_experience, $profile->experience_6->previous_exp_industry);
	array_push($og_job_query_industry_experience, $profile->experience_7->previous_exp_industry);

	foreach((array) $profile->ideal_job->og_pr_ideal_job_location as $item) {
		$item = (string) $item;
		array_push($og_job_query_location, $item);
	}

	foreach((array) $profile->ideal_job->ideal_job_basic_salary as $item) {
		$item = (string) $item;
		array_push($og_job_query_pay, $item);
	}

	array_push($og_job_query_qualification_type, $profile->education_1->qualification_type);
	array_push($og_job_query_qualification_type, $profile->education_2->qualification_type);
	array_push($og_job_query_qualification_type, $profile->education_3->qualification_type);

	array_push($og_job_query_role_experience, $profile->current_job->current_job_function);
	array_push($og_job_query_role_experience, $profile->experience_1->previous_exp_job_function);
	array_push($og_job_query_role_experience, $profile->experience_2->previous_exp_job_function);
	array_push($og_job_query_role_experience, $profile->experience_3->previous_exp_job_function);
	array_push($og_job_query_role_experience, $profile->experience_4->previous_exp_job_function);
	array_push($og_job_query_role_experience, $profile->experience_5->previous_exp_job_function);
	array_push($og_job_query_role_experience, $profile->experience_6->previous_exp_job_function);
	array_push($og_job_query_role_experience, $profile->experience_7->previous_exp_job_function);

	foreach((array) $profile->ideal_job->ideal_job_seniority as $item) {
		$item = (string) $item;
		array_push($og_job_query_seniority, $item);
	}

	array_push($og_job_query_seniority_experience, $profile->current_job->current_job_seniority);
	array_push($og_job_query_seniority_experience, $profile->experience_1->previous_exp_seniority);
	array_push($og_job_query_seniority_experience, $profile->experience_2->previous_exp_seniority);
	array_push($og_job_query_seniority_experience, $profile->experience_3->previous_exp_seniority);
	array_push($og_job_query_seniority_experience, $profile->experience_4->previous_exp_seniority);
	array_push($og_job_query_seniority_experience, $profile->experience_5->previous_exp_seniority);
	array_push($og_job_query_seniority_experience, $profile->experience_6->previous_exp_seniority);
	array_push($og_job_query_seniority_experience, $profile->experience_7->previous_exp_seniority);

	array_push($og_job_query_currency, $profile->ideal_job->ideal_job_basic_salary_currency);
	array_push($og_job_query_currency, $profile->current_job->current_job_basic_salary_currency);


$output = $api->sendRequest(array(
	'action'                            => 'read',
	'controller'                        => 'search',
	'search_for'                        => 'jobs', // or jobs
	'limit'                             => $limit,
	'offset'                            => $offset,
	'og_job_query_company_experience'   => array(''), //*
	'og_job_query_company_type'         => $og_job_query_company_type,
	'og_job_query_contract'             => $og_job_query_contract,
	'og_job_query_course_name'          => $og_job_query_course_name,
	'og_job_query_current_contract'     => $og_job_query_current_contract,
	'og_job_query_current_pay'          => $og_job_query_current_pay,
	'og_job_query_function'             => $og_job_query_function,
	'og_job_query_grade_attained'       => $og_job_query_grade_attained,
	'og_job_query_industry'             => $og_job_query_industry,
	'og_job_query_industry_experience'  => $og_job_query_industry_experience,
	'og_job_query_institution'          => array(''),
	'og_job_query_location'             => $og_job_query_location,
	'og_job_query_pay'                  => $og_job_query_pay,
	'og_job_query_qualification_type'   => $og_job_query_qualification_type,
	'og_job_query_role_experience'      => $og_job_query_role_experience,
	'og_job_query_seniority'            => $og_job_query_seniority,
	'og_job_query_seniority_experience' => $og_job_query_seniority_experience,
	'og_job_query_currency'             => $og_job_query_currency,
	'profile_id' => $profile->id,
	'status' => 'live',
	));

 return $output;

}


public static function job_matches_id_array() {

	$id_array =  array();

	$output = self::find_job_matches(100000, 0);

	foreach((array)$output->jobs as $job) {

		array_push($id_array, $job->id);
	}

	return $id_array;

}


} // end of opengo class