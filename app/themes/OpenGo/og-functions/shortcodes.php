<?php
//registering shortcodes for job (search) page


// function show_job_company() {
// 	if(isset($_GET['id'])) {
// 		$if_not_found = '---';
// 		global $api;
// 	$output = $api->sendRequest(array(
// 		'action'             => 'read',
// 		'controller'         => 'job',
// 		'mark_read'          => false, // or jobs
// 		'id'                 => $_GET['id'],
// 		'read_by_profile_id' =>  0 // profile id goes here
// 		));

// 	if($output->job->id > 0) {
// 		return $output->job->company;
// 	} else {
// 		return $if_not_found;
// 	}
// 	} else {
// 		return $if_not_found;
// 	}
// }


function show_job_company() {

	global $output;
	$if_not_found = 'Not Found';

	if($output->job->id > 0) {

		return $output->job->company;

	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_company', 'show_job_company' );






// function show_job_title() {
// 	if(isset($_GET['id'])) {
// 		$if_not_found = 'Not Found';
// 		global $api;
// 	$output = $api->sendRequest(array(
// 		'action'             => 'read',
// 		'controller'         => 'job',
// 		'mark_read'          => false, // or jobs
// 		'id'                 => $_GET['id'],
// 		'read_by_profile_id' =>  0 // profile id goes here
// 		));

// 	if($output->job->id > 0) {
// 		return $output->job->job_title;
// 	} else {
// 		return $if_not_found;
// 	}
// 	} else {
// 		return $if_not_found;
// 	}
// }

function show_job_title() {
	global $output;
	$if_not_found = 'Not Found';

	if($output->job->id > 0) {

		return $output->job->job_title;

	} else {
		return $if_not_found;
	}
}

add_shortcode( 'show_job_title', 'show_job_title' );


// function show_job_description() {
// 	if(isset($_GET['id'])) {
// 		$if_not_found = 'Not Found';
// 		global $api;
// 	$output = $api->sendRequest(array(
// 		'action'             => 'read',
// 		'controller'         => 'job',
// 		'mark_read'          => false, // or jobs
// 		'id'                 => $_GET['id'],
// 		'read_by_profile_id' =>  0 // profile id goes here
// 		));

// 	if($output->job->id > 0) {
// 		return stripslashes($output->job->job_description);
// 	} else {
// 		return $if_not_found;
// 	}
// 	} else {
// 		return $if_not_found;
// 	}
// }



function show_job_description() {
	global $output;
	$if_not_found = 'Not Found';

	if($output->job->id > 0) {
		return stripslashes($output->job->job_description);
	} else {
		return $if_not_found;
	}
}

add_shortcode( 'show_job_description', 'show_job_description' );


// function show_job_reg_button() {
// 	if(isset($_GET['id'])) {
// 		$if_not_found = 'Not Found';
// 		global $api;
// 	$output = $api->sendRequest(array(
// 		'action'             => 'read',
// 		'controller'         => 'job',
// 		'mark_read'          => false, // or jobs
// 		'id'                 => $_GET['id'],
// 		'read_by_profile_id' =>  0 // profile id goes here
// 		));

// 	if($output->job->id > 0) {
// 		return '<a href="'.home_url().'/?signup-for-job='.$output->job->id.'" class="btn btn-success">Register and Apply</a>';
// 	} else {
// 		return $if_not_found;
// 	}
// 	} else {
// 		return $if_not_found;
// 	}
// }


function show_job_reg_button() {
	global $output;
	$if_not_found = 'Not Found';
	if($output->job->id > 0) {
		return '<a href="'.home_url().'/?signup-for-job='.$output->job->id.'" class="btn btn-success">Register and Apply</a>';
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_reg_button', 'show_job_reg_button' );





// shortcodes for other job fields
#############################################################################

# [show_job_industry_1 separator=" or " if_nothing=" Any "]
function show_job_industry_1($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_industry_1' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_industry) > 0) {

			$result = '';

			foreach($output->job->og_job_query_industry as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_industry_1', 'show_job_industry_1' );

#############################################################################

# [show_job_seniority_1 separator=" or " if_nothing=" Any "]
function show_job_seniority_1($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_seniority_1' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_seniority) > 0) {

			$result = '';

			foreach($output->job->og_job_query_seniority as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_seniority_1', 'show_job_seniority_1' );


#############################################################################

# [show_job_function_1 separator=" or " if_nothing=" Any "]
function show_job_function_1($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_function_1' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_function) > 0) {

			$result = '';

			foreach($output->job->og_job_query_function as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_function_1', 'show_job_function_1' );



#############################################################################

# [show_job_location_1 separator=" or " if_nothing=" Any "]
function show_job_location_1($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_location_1' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_location) > 0) {

			$result = '';

			foreach($output->job->og_job_query_location as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_location_1', 'show_job_location_1' );


#############################################################################

# [show_job_pay_1 separator=" or " if_nothing=" Any "]
function show_job_pay_1($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_pay_1' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_pay) > 0) {

			$result = '';

			foreach($output->job->og_job_query_pay as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_pay_1', 'show_job_pay_1' );

#############################################################################

# [show_job_currency_1 separator=" or " if_nothing=" Any "]
function show_job_currency_1($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_currency_1' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_currency) > 0) {

			$result = '';

			foreach($output->job->og_job_query_currency as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_currency_1', 'show_job_currency_1' );


#############################################################################

# [show_job_company_type_1 separator=" or " if_nothing=" Any "]
function show_job_company_type_1($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_company_type_1' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_company_type) > 0) {

			$result = '';

			foreach($output->job->og_job_query_company_type as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_company_type_1', 'show_job_company_type_1' );


#############################################################################

# [show_job_contract_1 separator=" or " if_nothing=" Any "]
function show_job_contract_1($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_contract_1' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_contract) > 0) {

			$result = '';

			foreach($output->job->og_job_query_contract as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_contract_1', 'show_job_contract_1' );


#############################################################################


# [show_job_industry_experience_2 separator=" or " if_nothing=" Any "]
function show_job_industry_experience_2($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_industry_experience_2' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_industry_experience) > 0) {

			$result = '';

			foreach($output->job->og_job_query_industry_experience as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_industry_experience_2', 'show_job_industry_experience_2' );


#############################################################################


# [show_job_role_experience_2 separator=" or " if_nothing=" Any "]
function show_job_role_experience_2($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_role_experience_2' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_role_experience) > 0) {

			$result = '';

			foreach($output->job->og_job_query_role_experience as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));



			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}


		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_role_experience_2', 'show_job_role_experience_2' );

#############################################################################


# [show_job_seniority_experience_2 separator=" or " if_nothing=" Any "]
function show_job_seniority_experience_2($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_seniority_experience_2' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_seniority_experience) > 0) {

			$result = '';

			foreach($output->job->og_job_query_seniority_experience as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));

			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_seniority_experience_2', 'show_job_seniority_experience_2' );

#############################################################################

# [show_job_current_pay_2 separator=" or " if_nothing=" Any "]
function show_job_current_pay_2($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_current_pay_2' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_current_pay) > 0) {

			$result = '';

			foreach($output->job->og_job_query_current_pay as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_current_pay_2', 'show_job_current_pay_2' );

#############################################################################

# [show_job_current_contract_2 separator=" or " if_nothing=" Any "]
function show_job_current_contract_2($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_current_contract_2' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_current_contract) > 0) {

			$result = '';

			foreach($output->job->og_job_query_current_contract as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_current_contract_2', 'show_job_current_contract_2' );

#############################################################################

# [show_job_qualification_type_3 separator=" or " if_nothing=" Any "]
function show_job_qualification_type_3($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_qualification_type_3' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_qualification_type) > 0) {

			$result = '';

			foreach($output->job->og_job_query_qualification_type as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_qualification_type_3', 'show_job_qualification_type_3' );

#############################################################################

# [show_job_course_name_3 separator=" or " if_nothing=" Any "]
function show_job_course_name_3($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_course_name_3' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_course_name) > 0) {

			$result = '';

			foreach($output->job->og_job_query_course_name as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_course_name_3', 'show_job_course_name_3' );


#############################################################################

# [show_job_grade_attained_3 separator=" or " if_nothing=" Any "]
function show_job_grade_attained_3($atts) {
	global $output;

	// default shortcode attributes
	$atts = shortcode_atts(
			array(
				'if_nothing' => ' Any ',
				'separator' => ', ',
			), $atts, 'show_job_grade_attained_3' );

	$if_not_found = $atts['if_nothing'];
	$separator = $atts['separator'];

	if($output->job->id > 0) {

		if(count($output->job->og_job_query_grade_attained) > 0) {

			$result = '';

			foreach($output->job->og_job_query_grade_attained as $item) {
				if($item != 'Nothing Selected') {
					$result .= $item . $separator;
				}
			}

			$result = substr($result, 0, -(strlen($separator)));
			if(strlen($result) > 0) {
				return $result;
			} else {
				return $if_not_found;
			}

		} else {
			return $if_not_found;
		}
	} else {
		return $if_not_found;
	}
}


add_shortcode( 'show_job_grade_attained_3', 'show_job_grade_attained_3' );
