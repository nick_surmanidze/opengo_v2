<?php
/**
 * Project related ajax functions
 */


// ogajax  -  Add Ideal Job fucntion
add_action('wp_ajax_og_add_ideal_job', 'og_add_ideal_job');
add_action('wp_ajax_nopriv_og_add_ideal_job', 'og_add_ideal_job');

function og_add_ideal_job() {

	// starting session to access session variables
 	@session_start();

	if(isset($_POST['params'])) {

		// here all the magic will happen

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
			'action'                            => 'create',
			'controller'                        => 'profile',
			'id'                                => '',
			'user_id'                           => opengo::user_logged_in(),
			'first_name'                        => '',
			'last_name'                         => '',
			'email_address'                     => $_SESSION['nsauth']['user_email'],
		  'phone_number'                      => '',
			'country'                           => '',
			'date_created'                      => 'today',

			'current_job'                       => array(
				'current_job_organisation_name'     => '',
				'current_job_industry'              => '',
				'current_job_company_type'          => '',
				'current_job_function'              => '',
				'current_job_seniority'             => '',
				'current_job_year_started'          => '',
				'current_job_type_of_contract'      => '',
				'current_job_notice_period'         => '',
				'current_job_basic_salary'          => '',
				'current_job_basic_salary_currency' => '',
				'current_job_main_responsibilities' => ''
				),

			'education_1'                       => array(
				'university'                        => '',
				'course_name'                       => '',
				'qualification_type'                => '',
				'grade_attained'                    => '',
				'year_attained'                     => ''
				),

			'education_2'                       => array(
				'university'                        => '',
				'course_name'                       => '',
				'qualification_type'                => '',
				'grade_attained'                    => '',
				'year_attained'                     => ''
				),

			'education_3'                       => array(
				'university'                        => '',
				'course_name'                       => '',
				'qualification_type'                => '',
				'grade_attained'                    => '',
				'year_attained'                     => ''
				),

			'experience_1'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_2'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_3'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_4'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),


			'experience_5'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_6'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'experience_7'                      => array(
				'previous_exp_organisation_name'    => '',
				'previous_exp_industry'             => '',
				'previous_exp_company_type'         => '',
				'previous_exp_job_function'         => '',
				'previous_exp_seniority'            => '',
				'previous_exp_year_started'         => '',
				'previous_exp_year_ended'           => '',
				'previous_exp_type_of_contract'     => '',
				'previous_exp_main_responsibilities'=> ''
				),

			'ideal_job'                         => array(
				'ideal_job_seniority'               => $_POST['params']['ideal_job_seniority'],
				'ideal_job_contract_type'           => $_POST['params']['ideal_job_contract'],
				'ideal_job_basic_salary'            => $_POST['params']['ideal_job_salary'],
				'ideal_job_basic_salary_currency'   => $_POST['params']['ideal_job_currency'],
				'og_pr_ideal_job_company_type'      => array(

					'og_pr_ideal_job_company_type_1'    => $_POST['params']['ideal_job_company_type'][0],
					'og_pr_ideal_job_company_type_2'    => $_POST['params']['ideal_job_company_type'][1],
					'og_pr_ideal_job_company_type_3'    => $_POST['params']['ideal_job_company_type'][2]
					),

					'og_pr_ideal_job_function'          => array(
					'og_pr_ideal_job_function_1'        => $_POST['params']['ideal_job_function'][0],
					'og_pr_ideal_job_function_2'        => $_POST['params']['ideal_job_function'][1],
					'og_pr_ideal_job_function_3'        => $_POST['params']['ideal_job_function'][2]
					),

					'og_pr_ideal_job_industry'          => array(
					'og_pr_ideal_job_industry_1'        => $_POST['params']['ideal_job_industry'][0],
					'og_pr_ideal_job_industry_2'        => $_POST['params']['ideal_job_industry'][1],
					'og_pr_ideal_job_industry_3'        => $_POST['params']['ideal_job_industry'][2],
					),

					'og_pr_ideal_job_location'          => array(
					'og_pr_ideal_job_location_1'        => $_POST['params']['ideal_job_location'][0],
					'og_pr_ideal_job_location_2'        => $_POST['params']['ideal_job_location'][1],
					'og_pr_ideal_job_location_3'        => $_POST['params']['ideal_job_location'][2],
					),

				),

			'personal'                          => array(
				'personality'                       => ''
				),

			'skills'                            => array(
				'specialist_skill_1'                => '',
				'specialist_skill_2'                => '',
				'specialist_skill_3'                => '',
				),

		));


		if($result->id) {
			// we have just created the profile and need to update session
			$_SESSION['nsauth']['user_profile'] = $result;

			print_r('profile created');

		} else {

			print_r('failed');

		}



	} else {
		print_r("failed");
	}

	die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  Edit Ideal Job
add_action('wp_ajax_og_edit_ideal_job', 'og_edit_ideal_job');
add_action('wp_ajax_nopriv_og_edit_ideal_job', 'og_edit_ideal_job');

function og_edit_ideal_job() {

	// starting session to access session variables
 	@session_start();

	if(isset($_POST['params'])) {

		// here all the magic will happen

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
			'action'                            => 'update',
			'controller'                        => 'profile',
			'id'                                => $_SESSION['nsauth']['user_profile']->id,
			'ideal_job'                         => array(
				'ideal_job_seniority'               => $_POST['params']['ideal_job_seniority'],
				'ideal_job_contract_type'           => $_POST['params']['ideal_job_contract'],
				'ideal_job_basic_salary'            => $_POST['params']['ideal_job_salary'],
				'ideal_job_basic_salary_currency'   => $_POST['params']['ideal_job_currency'],
				'og_pr_ideal_job_company_type'      => array(

					'og_pr_ideal_job_company_type_1'    => opengo::check_isset($_POST['params']['ideal_job_company_type'][0]),
					'og_pr_ideal_job_company_type_2'    => opengo::check_isset($_POST['params']['ideal_job_company_type'][1]),
					'og_pr_ideal_job_company_type_3'    => opengo::check_isset($_POST['params']['ideal_job_company_type'][2])
					),

					'og_pr_ideal_job_function'          => array(
					'og_pr_ideal_job_function_1'        => opengo::check_isset($_POST['params']['ideal_job_function'][0]),
					'og_pr_ideal_job_function_2'        => opengo::check_isset($_POST['params']['ideal_job_function'][1]),
					'og_pr_ideal_job_function_3'        => opengo::check_isset($_POST['params']['ideal_job_function'][2])
					),

					'og_pr_ideal_job_industry'          => array(
					'og_pr_ideal_job_industry_1'        => opengo::check_isset($_POST['params']['ideal_job_industry'][0]),
					'og_pr_ideal_job_industry_2'        => opengo::check_isset($_POST['params']['ideal_job_industry'][1]),
					'og_pr_ideal_job_industry_3'        => opengo::check_isset($_POST['params']['ideal_job_industry'][2])
					),

					'og_pr_ideal_job_location'          => array(
					'og_pr_ideal_job_location_1'        => opengo::check_isset($_POST['params']['ideal_job_location'][0]),
					'og_pr_ideal_job_location_2'        => opengo::check_isset($_POST['params']['ideal_job_location'][1]),
					'og_pr_ideal_job_location_3'        => opengo::check_isset($_POST['params']['ideal_job_location'][2])
					),

				),
		));

		print_r($result);

		// end of the request
//print_r($_POST['params']);
	} else {
		print_r("failed");
	}

	die();
}


////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  Add Current Job fucntion
add_action('wp_ajax_og_add_current_job', 'og_add_current_job');
add_action('wp_ajax_nopriv_og_add_current_job', 'og_add_current_job');

function og_add_current_job() {

	// starting session to access session variables
 	@session_start();

	if(isset($_POST['params'])) {

		// here all the magic will happen

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
			'action'                            => 'update',
			'controller'                        => 'profile',
			'id'                                => $_SESSION['nsauth']['user_profile']->id,
			'current_job'                       => array(
				'current_job_organisation_name'     => $_POST['params']['organisation_name'],
				'current_job_industry'              => $_POST['params']['industry'],
				'current_job_company_type'          => $_POST['params']['company_type'],
				'current_job_function'              => $_POST['params']['job_function'],
				'current_job_seniority'             => $_POST['params']['seniority'],
				'current_job_year_started'          => $_POST['params']['year_started'],
				'current_job_type_of_contract'      => $_POST['params']['contract'],
				'current_job_notice_period'         => $_POST['params']['notice_period'],
				'current_job_basic_salary'          => $_POST['params']['current_salary'],
				'current_job_basic_salary_currency' => $_POST['params']['current_currency'],
				'current_job_main_responsibilities' => $_POST['params']['responsibilities']
				)
		));


		// WHEN ADDING A CURRENT JOB WE CAN ALSO ADD USER TO MAILCHIMP LIST
		$list_id = get_option("og_mailchimp_list_id_just_registered");
		subscribe_to_mailchimp($_SESSION['nsauth']['user_email'], $_SESSION['nsauth']['user_display_name'], $list_id);


		$profile = opengo::get_profile();

		$_SESSION['nsauth']['user_profile'] = $profile;


		print_r($result);

		// end of the request

	} else {
		print_r("failed");
	}

	die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  Edit Current Job fucntion
add_action('wp_ajax_og_edit_current_job', 'og_edit_current_job');
add_action('wp_ajax_nopriv_og_edit_current_job', 'og_edit_current_job');

function og_edit_current_job() {

	// starting session to access session variables
 	@session_start();

	if(isset($_POST['params'])) {

		// here all the magic will happen

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
			'action'                            => 'update',
			'controller'                        => 'profile',
			'id'                                => $_SESSION['nsauth']['user_profile']->id,
			'current_job'                       => array(
				'current_job_organisation_name'     => $_POST['params']['organisation_name'],
				'current_job_industry'              => $_POST['params']['industry'],
				'current_job_company_type'          => $_POST['params']['company_type'],
				'current_job_function'              => $_POST['params']['job_function'],
				'current_job_seniority'             => $_POST['params']['seniority'],
				'current_job_year_started'          => $_POST['params']['year_started'],
				'current_job_type_of_contract'      => $_POST['params']['contract'],
				'current_job_notice_period'         => $_POST['params']['notice_period'],
				'current_job_basic_salary'          => $_POST['params']['current_salary'],
				'current_job_basic_salary_currency' => $_POST['params']['current_currency'],
				'current_job_main_responsibilities' => $_POST['params']['responsibilities']
				)
		));


		print_r($result);




		// end of the request

	} else {
		print_r("failed");
	}

	die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  Edit Personal details
add_action('wp_ajax_og_edit_personal_details', 'og_edit_personal_details');
add_action('wp_ajax_nopriv_og_edit_personal_details', 'og_edit_personal_details');

function og_edit_personal_details() {

	// starting session to access session variables
 	@session_start();

	if(isset($_POST['data_name']) && isset($_POST['data_surname']) && isset($_POST['data_country'])) {



		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
				'action'        => 'update',
				'controller'    => 'profile',
				'id'            => $_SESSION['nsauth']['user_profile']->id,
				'first_name'    => $_POST['data_name'],
				'last_name'     => $_POST['data_surname'],
				'email_address' => $_POST['data_email'],
				'phone_number'  => $_POST['data_phone'],
				'country'       => $_POST['data_country'],
		));


		if(isset($_POST['data_remove_avatar']) && $_POST['data_remove_avatar'] == "true") {

			// remove avatar
				$get_avatar = $apicaller->sendRequest(array(
			      'action'       => 'read',
			      'controller'   => 'meta',
			      'data_type'    => 'user',
			      'data_id'      => opengo::user_logged_in(),
			      'meta_key'     => 'avatar'
			  ));


				$remove_avatar = $apicaller->sendRequest(array(
			      'action'       => 'delete',
			      'controller'   => 'meta',
			      'data_type'    => 'user',
			      'data_id'      => opengo::user_logged_in(),
			      'meta_key'     => 'avatar'
			  ));

				$avatar_array = unserialize($get_avatar);
				if(isset($avatar_array['path'])) {
					unlink($avatar_array['path']);
				}

		}

		if(isset($_FILES['file'])) {
			// process upload and get url










			function exifRotate( $file ){
				$file = wp_handle_upload($file);
				$file = exif($file);
			}

			function exif($file) {
			        //This line reads the EXIF data and passes it into an array
			        $exif = read_exif_data($file['file']);

			        //We're only interested in the orientation
			        $exif_orient = isset($exif['Orientation'])?$exif['Orientation']:0;
			        $rotateImage = 0;

			        //We convert the exif rotation to degrees for further use
			        if (6 == $exif_orient) {
			            $rotateImage = 90;
			            $imageOrientation = 1;
			        } elseif (3 == $exif_orient) {
			            $rotateImage = 180;
			            $imageOrientation = 1;
			        } elseif (8 == $exif_orient) {
			            $rotateImage = 270;
			            $imageOrientation = 1;
			        }

			        //if the image is rotated
			        if ($rotateImage) {

			            //WordPress 3.5+ have started using Imagick, if it is available since there is a noticeable difference in quality
			            //Why spoil beautiful images by rotating them with GD, if the user has Imagick

			            if (class_exists('Imagick')) {
			                $imagick = new Imagick();
			                $imagick->readImage($file['file']);
			                $imagick->rotateImage(new ImagickPixel(), $rotateImage);
			                $imagick->setImageOrientation($imageOrientation);
			                $imagick->writeImage($file['file']);
			                $imagick->clear();
			                $imagick->destroy();
			            } else {

			                //if no Imagick, fallback to GD
			                //GD needs negative degrees
			                $rotateImage = -$rotateImage;

			                switch ($file['type']) {
			                    case 'image/jpeg':
			                        $source = imagecreatefromjpeg($file['file']);
			                        $rotate = imagerotate($source, $rotateImage, 0);
			                        imagejpeg($rotate, $file['file']);
			                        break;
			                    case 'image/png':
			                        $source = imagecreatefrompng($file['file']);
			                        $rotate = imagerotate($source, $rotateImage, 0);
			                        imagepng($rotate, $file['file']);
			                        break;
			                    case 'image/gif':
			                        $source = imagecreatefromgif($file['file']);
			                        $rotate = imagerotate($source, $rotateImage, 0);
			                        imagegif($rotate, $file['file']);
			                        break;
			                    default:
			                        break;
			                }
			            }
			        }
			        // The image orientation is fixed, pass it back for further processing
			        return $file;
			    }









				// check if user had an avatar and delete it


				$get_avatar = $apicaller->sendRequest(array(
			      'action'       => 'read',
			      'controller'   => 'meta',
			      'data_type'    => 'user',
			      'data_id'      => opengo::user_logged_in(),
			      'meta_key'     => 'avatar'
			  ));

				$avatar_array = unserialize($get_avatar);
				if(isset($avatar_array['path'])) {
					unlink($avatar_array['path']);
				}


				$user_id  = $_SESSION['nsauth']['user_id'];

				function opengo_upload_dir($upload) {
					$upload['subdir']	= '/avatars'; // DIRECTORY TO UPLOAD ALL FILES
					$upload['path']		= $upload['basedir'] . $upload['subdir'];
					$upload['url']		= $upload['baseurl'] . $upload['subdir'];
					return $upload;
				}

				function opengo_rename( $file ){
					$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
				  $file['name'] = uniqid('avatar', true) . '.' . $ext;
				    return $file;
				}

				//add_filter('wp_handle_upload_prefilter', 'exifRotate' );
				add_filter('upload_dir', 'opengo_upload_dir');
				add_filter('wp_handle_upload_prefilter', 'opengo_rename' );


				$upload_file = wp_handle_upload($_FILES["file"], array('test_form' => FALSE));



				// redefine paths
	    	$uploads_dir_array = wp_upload_dir();
				$uploads_dir_path = $uploads_dir_array['basedir'] . '/avatars';




				// exif fix
				$image = exif($upload_file);

				// crop an image
				$image = wp_get_image_editor($upload_file['file']);




















				if ( ! is_wp_error( $image) ) {
						$image->set_quality( 100 );
				    $image->resize( 500, 500, true);
				    $image->save( $upload_file['file']);
				}


		    $file_url = $upload_file['url'];
				$file_path = $upload_file['file'];

				$avatar_out = array();
				$avatar_out['path'] = $file_path;
				$avatar_out['url'] = $file_url;


				// now update user meta




				remove_filter('upload_dir', 'opengo_upload_dir');
				remove_filter('wp_handle_upload_prefilter', 'opengo_rename' );
				//remove_filter('wp_handle_upload_prefilter', 'exifRotate' );


				$avatar_update = $apicaller->sendRequest(array(
		        'action'       => 'create',
		        'controller'   => 'meta',
		        'data_type'    => 'user',
		        'data_id'      => opengo::user_logged_in(),
		        'meta_key'     => 'avatar',
		        'meta_value'   => serialize($avatar_out),
		    ));




		}


		print_r($result);

		// end of the request
		//print_r($_POST['params']);
	} else {
		print_r("failed");
	}


	die();
}

////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  Edit Skills
add_action('wp_ajax_og_edit_skills', 'og_edit_skills');
add_action('wp_ajax_nopriv_og_edit_skills', 'og_edit_skills');

function og_edit_skills() {

	// starting session to access session variables
 	@session_start();

	if(isset($_POST['params'])) {

		// here all the magic will happen

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
				'action'        => 'update',
				'controller'    => 'profile',
				'id'            => $_SESSION['nsauth']['user_profile']->id,
				'skills'                            => array(
					'specialist_skill_1'                => $_POST['params']['skill_one'],
					'specialist_skill_2'                => $_POST['params']['skill_two'],
					'specialist_skill_3'                => $_POST['params']['skill_three']
					),
		));

		print_r($_POST['params']);

		// end of the request
//print_r($_POST['params']);
	} else {
		print_r("failed");
	}

	die();
}




////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  Edit Education
add_action('wp_ajax_og_edit_education', 'og_edit_education');
add_action('wp_ajax_nopriv_og_edit_education', 'og_edit_education');

function og_edit_education() {

	// starting session to access session variables
 	@session_start();

	if(isset($_POST['params'])) {

		// here all the magic will happen

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
			'action'                            => 'update',
			'controller'                        => 'profile',
			'id'                                => $_SESSION['nsauth']['user_profile']->id,

			'education_1'                       => array(
				'university'                        => opengo::check_isset($_POST['params'][0]['university']),
				'course_name'                       => opengo::check_isset($_POST['params'][0]['course_name']),
				'qualification_type'                => opengo::check_isset($_POST['params'][0]['qualification']),
				'grade_attained'                    => opengo::check_isset($_POST['params'][0]['grade']),
				'year_attained'                     => opengo::check_isset($_POST['params'][0]['year']),
				),

			'education_2'                       => array(
				'university'                        => opengo::check_isset($_POST['params'][1]['university']),
				'course_name'                       => opengo::check_isset($_POST['params'][1]['course_name']),
				'qualification_type'                => opengo::check_isset($_POST['params'][1]['qualification']),
				'grade_attained'                    => opengo::check_isset($_POST['params'][1]['grade']),
				'year_attained'                     => opengo::check_isset($_POST['params'][1]['year']),
				),

			'education_3'                       => array(
				'university'                        => opengo::check_isset($_POST['params'][2]['university']),
				'course_name'                       => opengo::check_isset($_POST['params'][2]['course_name']),
				'qualification_type'                => opengo::check_isset($_POST['params'][2]['qualification']),
				'grade_attained'                    => opengo::check_isset($_POST['params'][2]['grade']),
				'year_attained'                     => opengo::check_isset($_POST['params'][2]['year']),
				),
		));


		print_r($_POST['params']);

		// end of the request

	} else {
		print_r("failed");
	}

	die();
}






////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  Edit Education
add_action('wp_ajax_og_edit_experience', 'og_edit_experience');
add_action('wp_ajax_nopriv_og_edit_experience', 'og_edit_experience');

function og_edit_experience() {

	// starting session to access session variables
 	@session_start();

	if(isset($_POST['params'])) {

		// here all the magic will happen

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
			'action'                            => 'update',
			'controller'                        => 'profile',
			'id'                                => $_SESSION['nsauth']['user_profile']->id,
			'experience_1'                      => array(
				'previous_exp_organisation_name'    => opengo::check_isset($_POST['params'][0]['organisation_name']),
				'previous_exp_industry'             => opengo::check_isset($_POST['params'][0]['industry']),
				'previous_exp_company_type'         => opengo::check_isset($_POST['params'][0]['company_type']),
				'previous_exp_job_function'         => opengo::check_isset($_POST['params'][0]['job_function']),
				'previous_exp_seniority'            => opengo::check_isset($_POST['params'][0]['seniority']),
				'previous_exp_year_started'         => opengo::check_isset($_POST['params'][0]['year_started']),
				'previous_exp_year_ended'           => opengo::check_isset($_POST['params'][0]['year_ended']),
				'previous_exp_type_of_contract'     => opengo::check_isset($_POST['params'][0]['contract']),
				'previous_exp_main_responsibilities' => opengo::check_isset($_POST['params'][0]['responsibilities']),
				),

			'experience_2'                      => array(
				'previous_exp_organisation_name'    => opengo::check_isset($_POST['params'][1]['organisation_name']),
				'previous_exp_industry'             => opengo::check_isset($_POST['params'][1]['industry']),
				'previous_exp_company_type'         => opengo::check_isset($_POST['params'][1]['company_type']),
				'previous_exp_job_function'         => opengo::check_isset($_POST['params'][1]['job_function']),
				'previous_exp_seniority'            => opengo::check_isset($_POST['params'][1]['seniority']),
				'previous_exp_year_started'         => opengo::check_isset($_POST['params'][1]['year_started']),
				'previous_exp_year_ended'           => opengo::check_isset($_POST['params'][1]['year_ended']),
				'previous_exp_type_of_contract'     => opengo::check_isset($_POST['params'][1]['contract']),
				'previous_exp_main_responsibilities' => opengo::check_isset($_POST['params'][1]['responsibilities']),
				),

			'experience_3'                      => array(
				'previous_exp_organisation_name'    => opengo::check_isset($_POST['params'][2]['organisation_name']),
				'previous_exp_industry'             => opengo::check_isset($_POST['params'][2]['industry']),
				'previous_exp_company_type'         => opengo::check_isset($_POST['params'][2]['company_type']),
				'previous_exp_job_function'         => opengo::check_isset($_POST['params'][2]['job_function']),
				'previous_exp_seniority'            => opengo::check_isset($_POST['params'][2]['seniority']),
				'previous_exp_year_started'         => opengo::check_isset($_POST['params'][2]['year_started']),
				'previous_exp_year_ended'           => opengo::check_isset($_POST['params'][2]['year_ended']),
				'previous_exp_type_of_contract'     => opengo::check_isset($_POST['params'][2]['contract']),
				'previous_exp_main_responsibilities' => opengo::check_isset($_POST['params'][2]['responsibilities']),
				),

			'experience_4'                      => array(
				'previous_exp_organisation_name'    => opengo::check_isset($_POST['params'][3]['organisation_name']),
				'previous_exp_industry'             => opengo::check_isset($_POST['params'][3]['industry']),
				'previous_exp_company_type'         => opengo::check_isset($_POST['params'][3]['company_type']),
				'previous_exp_job_function'         => opengo::check_isset($_POST['params'][3]['job_function']),
				'previous_exp_seniority'            => opengo::check_isset($_POST['params'][3]['seniority']),
				'previous_exp_year_started'         => opengo::check_isset($_POST['params'][3]['year_started']),
				'previous_exp_year_ended'           => opengo::check_isset($_POST['params'][3]['year_ended']),
				'previous_exp_type_of_contract'     => opengo::check_isset($_POST['params'][3]['contract']),
				'previous_exp_main_responsibilities' => opengo::check_isset($_POST['params'][3]['responsibilities']),
				),


			'experience_5'                      => array(
				'previous_exp_organisation_name'    => opengo::check_isset($_POST['params'][4]['organisation_name']),
				'previous_exp_industry'             => opengo::check_isset($_POST['params'][4]['industry']),
				'previous_exp_company_type'         => opengo::check_isset($_POST['params'][4]['company_type']),
				'previous_exp_job_function'         => opengo::check_isset($_POST['params'][4]['job_function']),
				'previous_exp_seniority'            => opengo::check_isset($_POST['params'][4]['seniority']),
				'previous_exp_year_started'         => opengo::check_isset($_POST['params'][4]['year_started']),
				'previous_exp_year_ended'           => opengo::check_isset($_POST['params'][4]['year_ended']),
				'previous_exp_type_of_contract'     => opengo::check_isset($_POST['params'][4]['contract']),
				'previous_exp_main_responsibilities' => opengo::check_isset($_POST['params'][4]['responsibilities']),
				),

			'experience_6'                      => array(
				'previous_exp_organisation_name'    => opengo::check_isset($_POST['params'][5]['organisation_name']),
				'previous_exp_industry'             => opengo::check_isset($_POST['params'][5]['industry']),
				'previous_exp_company_type'         => opengo::check_isset($_POST['params'][5]['company_type']),
				'previous_exp_job_function'         => opengo::check_isset($_POST['params'][5]['job_function']),
				'previous_exp_seniority'            => opengo::check_isset($_POST['params'][5]['seniority']),
				'previous_exp_year_started'         => opengo::check_isset($_POST['params'][5]['year_started']),
				'previous_exp_year_ended'           => opengo::check_isset($_POST['params'][5]['year_ended']),
				'previous_exp_type_of_contract'     => opengo::check_isset($_POST['params'][5]['contract']),
				'previous_exp_main_responsibilities' => opengo::check_isset($_POST['params'][5]['responsibilities']),
				),

			'experience_7'                      => array(
				'previous_exp_organisation_name'    => opengo::check_isset($_POST['params'][6]['organisation_name']),
				'previous_exp_industry'             => opengo::check_isset($_POST['params'][6]['industry']),
				'previous_exp_company_type'         => opengo::check_isset($_POST['params'][6]['company_type']),
				'previous_exp_job_function'         => opengo::check_isset($_POST['params'][6]['job_function']),
				'previous_exp_seniority'            => opengo::check_isset($_POST['params'][6]['seniority']),
				'previous_exp_year_started'         => opengo::check_isset($_POST['params'][6]['year_started']),
				'previous_exp_year_ended'           => opengo::check_isset($_POST['params'][6]['year_ended']),
				'previous_exp_type_of_contract'     => opengo::check_isset($_POST['params'][6]['contract']),
				'previous_exp_main_responsibilities' => opengo::check_isset($_POST['params'][6]['responsibilities']),
				),
		));


		print_r(opengo::check_isset($_POST['params'][0]['responsibilities']));

		// end of the request

	} else {
		print_r("failed");
	}

	die();
}







////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  Edit Settings
add_action('wp_ajax_og_edit_settings', 'og_edit_settings');
add_action('wp_ajax_nopriv_og_edit_settings', 'og_edit_settings');

function og_edit_settings() {

	// starting session to access session variables
 	@session_start();

  $output = array();
		// here all the magic will happen
if(isset($_POST['params']['display_name'])) {

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
		'action'       => 'update',
		'controller'   => 'user',
		'id'           => opengo::user_logged_in(),
		'display_name' => $_POST['params']['display_name']
		));

		$_SESSION['nsauth']['user_display_name'] = $_POST['params']['display_name'];

		$email_frequency = $apicaller->sendRequest(array(
        'action'       => 'create',
        'controller'   => 'meta',
        'data_type'    => 'user',
        'data_id'      => opengo::user_logged_in(),
        'meta_key'     => 'talent_email_frequency',
        'meta_value'   => $_POST['params']['email_frequency'],
    ));



    if(isset($_POST['params']['slug']) && strlen($_POST['params']['slug']) > 0) {


      // We have user id and now need to get profile id

      $profile_id = opengo::has_profile();


      // pre-sanitize slug
      $slug = $_POST['params']['slug'];
      $slug = strtolower($slug);
  		$slug = preg_replace("/[^A-Za-z0-9_-]/", '', $slug);

      // update slug
      $update_slug = $apicaller->sendRequest(array(
      		'action'          => 'update',
      		'controller'      => 'profile',
          'id'              => $profile_id,
          'slug'            => $slug
      	));

      $updated_profile = $apicaller->sendRequest(array(
          'action'          => 'read',
          'controller'      => 'profile',
          'id'              => $profile_id,
      ));

        $_SESSION['nsauth']['user_profile']->slug = $updated_profile->slug;
        $output['new_slug'] = $updated_profile->slug;

    }



      $output['result'] = 'success'; //  success || failed
  		print_r(json_encode($output));


		} else {

      $output['result'] = 'failed'; //  success || failed
		  print_r(json_encode($output));

    }


		// end of the request


	die();
}




////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  Edit Settings
add_action('wp_ajax_og_delete_profile', 'og_delete_profile');
add_action('wp_ajax_nopriv_og_delete_profile', 'og_delete_profile');

function og_delete_profile() {

	// starting session to access session variables
 	@session_start();
		// here all the magic will happen
		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);
  // DELETE MESSAGES

		$apicaller->sendRequest(array(
		'action'       => 'delete',
		'controller'   => 'profile',
		'id'           => opengo::has_profile()
		));

	  $apicaller->sendRequest(array(
	    'action'       => 'delete',
	    'controller'   => 'message',
	    'delete_by_user_id' => opengo::has_profile()
	  ));

		if(isset($_SESSION['nsauth'])) {
		$_SESSION['nsauth'] = null;
			unset($_SESSION['nsauth']);
		}
		unset($_SESSION);
		unset($_COOKIE);
		// end of the request
	die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  load more jobs
add_action('wp_ajax_og_load_more_jobs', 'og_load_more_jobs');
add_action('wp_ajax_nopriv_og_load_more_jobs', 'og_load_more_jobs');

function og_load_more_jobs() {

	// starting session to access session variables
 	@session_start();
		// here all the magic will happen
 	if(isset($_POST['params']['limit'])) {

		$output = opengo::find_job_matches($_POST['params']['limit'], 0);

		$output_html = '';

		// now we nee to generate output html

		foreach($output->jobs as $job) {

	  $output_html .= '<a href="/search?id=' . $job->id . '" class="list-group-item job-item">';
	  if($job->read == 'new') {
			$output_html .= '<span class="label label-danger new">NEW</span>';
	  }
	    $output_html .= '<h4 class="list-group-item-heading">'.$job->job_title.'</h4>';
	    $output_html .= '<h5 class="list-group-item-heading">'.$job->company.'</h5></a>';
	   }

	  $response = array();
	  $response['data'] = $output_html;
	  $response['total'] = $output->total;

		print_r(json_encode($response));

 	} else {
 		return 'failed';
 		die();
 	}


		// end of the request
	die();
}




////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  load more jobs
add_action('wp_ajax_og_register_interest', 'og_register_interest');
add_action('wp_ajax_nopriv_og_register_interest', 'og_register_interest');

function og_register_interest() {

	// starting session to access session variables
 	@session_start();
		// here all the magic will happen
 	if(isset($_POST['params'])) {

		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
						'action'     => 'create',
						'controller' => 'interest',
						'profile_id' => $_POST['params']['profile_id'],
						'job_id'     => $_POST['params']['job_id'],
						'tests'      => $_POST['params']['tests']
		));

		print_r($result);


 	} else {
 		return 'failed';
 		die();
 	}
		// end of the request
	die();
}

////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  load more conversations
add_action('wp_ajax_og_load_more_conversations', 'og_load_more_conversations');
add_action('wp_ajax_nopriv_og_load_more_conversations', 'og_load_more_conversations');

function og_load_more_conversations() {

	// starting session to access session variables
 	@session_start();
 	$user_id = opengo::user_logged_in();
		// here all the magic will happen
 	if(isset($_POST['params']['limit']) && isset($_POST['params']['section'])) {

 		$section = $_POST['params']['section'];
 		$limit = $_POST['params']['limit'];


 		if($section == 'unread') {

			$output = opengo::get_messages(0, $user_id, '', '', 0, $limit, 0);

 		} elseif($section == 'sent') {

			$output = opengo::get_messages($user_id, 0, '', '', 1, $limit, 0);

 		} elseif($section == 'inbox') {

			$output = opengo::get_messages(0, $user_id, '', '', 1, $limit, 0);

 		}



		$output_html = '';

		// now we nee to generate output html

		if($section == 'sent') {

			foreach( (array) $output->messages as $message) {

			  $output_html .= '<a href="' . get_home_url() . '/conversation?from-user=' . $message->from_user_id . '&amp;to-user=' . $message->to_user_id . '&amp;subject=' .$message->message_subject .'" class="list-group-item inbox-item">';
				$output_html .= '<h4 class="list-group-item-heading">' . $message->sent_to . ' | ' . $message->message_subject . '</h4>';
				$output_html .= '<p class="list-group-item-text">' . $message->message_body . '</p>';
				$output_html .= '</a>';
		   }


		} else {

			foreach( (array) $output->messages as $message) {

			  $output_html .= '<a href="' . get_home_url() . '/conversation?from-user=' . $message->from_user_id . '&amp;to-user=' . $message->to_user_id . '&amp;subject=' .$message->message_subject .'" class="list-group-item inbox-item">';
				$output_html .= '<h4 class="list-group-item-heading">' . $message->sent_from . ' | ' . $message->message_subject . '</h4>';
				if($message->message_read == 0) {
				$output_html .= '<span class="label label-danger">New</span>';
				}
				$output_html .= '<p class="list-group-item-text">' . $message->message_body . '</p>';
				$output_html .= '</a>';
		   }


		}



	  $response = array();
	  $response['data'] = $output_html;
	  $response['total'] = $output->total;

		print_r(json_encode($response));

 	} else {
 		return 'failed';
 		die();
 	}


		// end of the request
	die();
}




////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  send message
add_action('wp_ajax_og_send_message', 'og_send_message');
add_action('wp_ajax_nopriv_og_send_message', 'og_send_message');

function og_send_message() {

	// starting session to access session variables
 	@session_start();


 	$from_id = opengo::user_logged_in();


 	if(isset($_POST['params']['recipient']) && isset($_POST['params']['subject'])) {




		$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

		$result = $apicaller->sendRequest(array(
				'action'          => 'create',
				'controller'      => 'message',
				'from_user_id'    => $from_id,
				'to_user_id'      => $_POST['params']['recipient'],
				'message_subject' => $_POST['params']['subject'],
				'message_body'    => $_POST['params']['body'],
				'message_read'    => '0', // initially message is unread

			));


		print_r($result);


 	} else {
 		return 'failed';
 		die();
 	}
		// end of the request
	die();
}



////////////////////////////////////////////////////////////////////////////////////////////////
// ogajax  -  get messages.
add_action('wp_ajax_og_get_conversation', 'og_get_conversation');
add_action('wp_ajax_nopriv_og_get_conversation', 'og_get_conversation');

function og_get_conversation() {

	// starting session to access session variables
 	@session_start();


 	$user_id = opengo::user_logged_in();


 	if(isset($_POST['params']['recipient']) && isset($_POST['params']['subject']) && isset($_POST['params']['limit']) && isset($_POST['params']['offset'])) {


 		$output = array();
 		$html = '';

		$conversation = opengo::get_conversation($user_id, $_POST['params']['recipient'], $_POST['params']['subject'], $_POST['params']['limit'], $_POST['params']['offset']);

		foreach((array) $conversation->messages as $message) {

			if ($message->from_user_id == $user_id) {

				$html .=	"<div class='item out'><div class='timeline'><div class='circle'></div><div class='line'></div></div><div class='item-content'><div class='panel panel-default'><div class='panel-body'>";
			  $html .=	"<b>" . $message->sent_from . " ( " . $message->message_sent_date . " )  </b><br>" . $message->message_body . "</div></div></div></div>";

			} else {

				$html .=	"<div class='item in'><div class='timeline'><div class='circle'></div><div class='line'></div></div><div class='item-content'><div class='panel panel-default'><div class='panel-body'>";
			  $html .=	"<b>" . $message->sent_from . " ( " . $message->message_sent_date . " )  </b><br>" . $message->message_body . "</div></div></div></div>";

			}


		}

		$output['data'] = $html;
		$output['total'] = $conversation->total;


		print_r(json_encode($output));


 	} else {
 		return 'failed';
 		die();
 	}
		// end of the request
	die();
}




