<?php
/**
 * Template Name:Login
 * Custom template.
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="home-wrapper" style="background: url(<?php the_field('home_background', 'option'); ?>) no-repeat center center; background-size: cover;">
					<div class="row">



						  <div class="col-md-6 home-we-connect">
							<div class="logo-wrapper">
								<a href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="OpenGo"></a>
							</div>
							<div class="hero-text-wrapper">
								<h1><?php the_field('home_main_text', 'option'); ?></h1>
								<p><?php the_field('home_subtitle_text', 'option'); ?></p><br>
								<a href="<?php the_field('home_button_url', 'option'); ?>" class="find-out-more btn btn-info"><?php the_field('home_button_text', 'option'); ?></a>
							</div>
						  </div>


						  <div class="col-md-6 home-social-connect">
						  	<?php echo do_shortcode('[nsauth-buttons]'); ?>
						  	<p><?php the_field('home_privacy_text', 'option'); ?></p>
								<div class="home-menu">
									<?php wp_nav_menu( array( 'theme_location' => 'home-footer' ) ); ?>
								</div>
						  </div>


					</div>
				</div>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
