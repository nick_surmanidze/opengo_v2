<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package OpenGo
 */

 /**
  * Run post view counter
  */
increase_post_views_counter();

get_header();
?>
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

    <?php if ( have_posts() ) : ?>

      <?php while ( have_posts() ) : the_post(); ?>


        <section class="white-section vertical-padding-40">
          <div class="container blog-container">

              <div class="blog-post">
                <div class="row">

                  <article class="col-md-12">

  	                  <div class="article-heading">
  	                    <h1 class="title"><?php the_title(); ?></h1>
  	                  </div>

  	                  <div class="article-featured-image">
  	                    <?php if ( has_post_thumbnail() ) : ?>

  											        <?php the_post_thumbnail(); ?>

  											<?php endif; ?>
  	                  </div>

                      <div class="article-brief row">

                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 article-summary">
                          <?php the_field('article_summary'); ?>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 author-n-sharing">
                          <div class="author">
                            By Ian Hallett
                          </div>
                          <div class="sharing">
                            <?php echo do_shortcode('[Sassy_Social_Share]'); ?>
                          </div>
                        </div>

                      </div>

                      <div class="article-content">
                        <?php the_content(); ?>
                      </div>

                  </article>
                </div>
               </div>




                  <?php
                  $tags = wp_get_post_tags($post->ID);
                  if ($tags) {
                  $tag_id_array = array();
                  foreach($tags as $tag) {
                    array_push($tag_id_array, $tag->term_id);
                  }
                  $args=array(
                  'tag__in' => $tag_id_array,
                  'post__not_in' => array($post->ID),
                  'posts_per_page'=>3,
                  'caller_get_posts'=>1
                  );
                  $my_query = new WP_Query($args);
                  if( $my_query->have_posts() ) { ?>
                  <div class="row">
                    <div class="col-md-12 related-posts ">
                      <h2>Related Posts</h2>
                    </div>
                  </div>
                  	<div class="execution-plans-wrapper row">
                  <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>


                  <div class="ex-plan">
                  	<div class="plan">

                  		<div class="plan-image" onclick="location.href='<?php the_permalink(); ?>'" style="background: #01a2be url(<?php echo the_post_thumbnail_url(); ?>) no-repeat top center; background-size: cover; cursor: pointer;">

                  		</div>
                  		<div class="aligner">
                  			<h3 class="plan-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                  			<div class="plan-description"><?php echo strip_tags(get_field('article_intro')); ?></div>
                  		</div>

                  		<div class="plan-footer">
                  			<a href="<?php the_permalink(); ?>" class="btn btn-success btn-block">Read More</a>
                  		</div>
                  	</div>
                  </div>








                  <?php endwhile; ?>
                  </div>
                  <?php }
                  wp_reset_query();
                  } ?>

               </div>



        </section>

      <?php endwhile; ?>

    <?php endif; ?>

    </main><!-- #main -->
  </div><!-- #primary -->
<?php get_footer(); ?>
