<?php
/**
 * OpenGo functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 */

// instantitate api

$api = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

if (!function_exists('opengo_setup')) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function opengo_setup()
{
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on OpenGo, use a find and replace
     * to change 'opengo' to the name of your theme in all the template files.
     */
    //load_theme_textdomain( 'opengo', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    //add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(array(
        'primary' => esc_html__('Primary Menu', 'opengo'),
    ));

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));
}
endif; // opengo_setup
add_action('after_setup_theme', 'opengo_setup');

/**
 * Enqueue scripts and styles.
 */
function opengo_scripts()
{
    $scripts_version = 2;

    wp_enqueue_style('opengo-style', get_stylesheet_uri());
    wp_enqueue_script('bootstrap', get_stylesheet_directory_uri().'/bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.min.js', array('jquery'), $scripts_version, true);
    wp_enqueue_script('html5shiv', get_stylesheet_directory_uri().'/bower_components/html5shiv/dist/html5shiv.min.js', array(), $scripts_version, false);
    wp_enqueue_script('modernizr', get_stylesheet_directory_uri().'/bower_components/modernizr/modernizr.js', array(), $scripts_version, false);

    //summernote  {#Load on demand#}
    wp_enqueue_style('summernote', get_stylesheet_directory_uri().'/bower_components/summernote/dist/summernote.css');
    wp_enqueue_script('summernote', get_stylesheet_directory_uri().'/bower_components/summernote/dist/summernote.js', array('jquery', 'bootstrap'), $scripts_version, true);

    //Angular js  {#Load on demand#}
    wp_enqueue_script('angular', get_stylesheet_directory_uri().'/bower_components/angular/angular.js', array('jquery'), $scripts_version, true);

    wp_enqueue_script('angular-auto-value', get_stylesheet_directory_uri().'/bower_components/angular/angular-auto-value.min.js', array('angular'), $scripts_version, true);

    // angular summernote
    wp_enqueue_script('angular-summernote', get_stylesheet_directory_uri().'/bower_components/angular-summernote/dist/angular-summernote.js', array('angular'), $scripts_version, true);

    // bootstrap multiselect  {#Load on demand#}
    wp_enqueue_style('bootstrap-multiselect', get_stylesheet_directory_uri().'/lib/bootstrap-multiselect/bootstrap-multiselect.css');
    wp_enqueue_script('bootstrap-multiselect', get_stylesheet_directory_uri().'/lib/bootstrap-multiselect/bootstrap-multiselect.js', array('jquery', 'bootstrap'), $scripts_version, true);

  // bootstrap select  {#for now used only for universities#}
  wp_enqueue_style('bootstrap-select', get_stylesheet_directory_uri().'/lib/bootstrap-select/css/bootstrap-select.min.css');
    wp_enqueue_script('bootstrap-select', get_stylesheet_directory_uri().'/lib/bootstrap-select/js/bootstrap-select.min.js', array('jquery', 'bootstrap'), $scripts_version, true);

    // jquery ui
    wp_enqueue_script('jquery-ui', get_stylesheet_directory_uri().'/bower_components/jquery-ui/jquery-ui.min.js', array('jquery'), $scripts_version, true);

    // DROPZONE
    wp_enqueue_script('exif', get_stylesheet_directory_uri().'/lib/dropzone/exif.js', array(), $scripts_version, true);
    wp_enqueue_script('dropzone', get_stylesheet_directory_uri().'/lib/dropzone/dropzone.js', array(), $scripts_version, true);




    wp_enqueue_script('opengo-main', get_stylesheet_directory_uri().'/js/main.js', array(), $scripts_version, true);




}

add_action('wp_enqueue_scripts', 'opengo_scripts');

// Hide admin bar
show_admin_bar(false);

// Limiting access to wo-admin to non admins
add_action('init', 'blockusers_init');
function blockusers_init()
{
    if (is_admin() && !current_user_can('administrator') &&
!(defined('DOING_AJAX') && DOING_AJAX)) {
        wp_redirect(home_url());
        exit;
    }
}

// Routing and redirections

// function custom_login(){
//  global $pagenow;
//  if( 'wp-login.php' == $pagenow ) {
//  	$site = get_home_url();
//   wp_redirect($site);
//   exit();
//  }
// }

// add_action('init','custom_login');

// nav menus

register_nav_menu('home-footer', 'Home Footer');

register_nav_menu('header', 'Header');

register_nav_menu('footer-left', 'Footer Left');

register_nav_menu('footer-right', 'Footer Right');

// Options Pages
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'OpenGo Settings',
        'menu_title' => 'Manage OpenGo',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false,
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Home page settings',
        'menu_title' => 'Home Page',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Dashboard settings',
        'menu_title' => 'Dashboard Settings',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Pre-defined list values',
        'menu_title' => 'Pre-defined lists',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Hints on editor pages',
        'menu_title' => 'Hints',
        'parent_slug' => 'theme-general-settings',
    ));
}

function is_current_item($query, $value)
{
    if (isset($_GET[$query]) && $_GET[$query] == $value) {
        echo 'active';
    }
}

function if_is_page_echo($page, $string)
{
    if (is_page($page)) {
        echo $string;
    }
}

function disable_wp_emojicons()
{

  // all actions related to emojis
  remove_action('admin_print_styles', 'print_emoji_styles');
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
  remove_filter('the_content_feed', 'wp_staticize_emoji');
  remove_filter('comment_text_rss', 'wp_staticize_emoji');

  // filter to remove TinyMCE emojis
  add_filter('tiny_mce_plugins', 'disable_emojicons_tinymce');
}
add_action('init', 'disable_wp_emojicons');

function disable_emojicons_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

add_action('init', 'myoverride', 100);
function myoverride()
{

	if(function_exists('visual_composer')) {
    remove_action('wp_head', array(visual_composer(), 'addMetaData'));
	}
}

remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version

// Include opengo specific libraries

// API caller class

//this class is already decrlared within a plugin - 'nsauth'

//require_once(get_stylesheet_directory() . '/og-functions/apicaller.class.php');

require_once get_stylesheet_directory().'/og-functions/og-functions.php';

require_once get_stylesheet_directory().'/og-functions/og-ajax.php';

require_once get_stylesheet_directory().'/og-functions/redirects.php';

require_once get_stylesheet_directory().'/og-functions/shortcodes.php';

require_once get_stylesheet_directory().'/mailchimp/mailchimp.php';

if (isset($_GET)) {
    @session_start();

    if (!isset($_SESSION['gts'])) {
        $_SESSION['gts'] = array();
    }

    $current_session_array = $_SESSION['gts'];
    $current_get_array = $_GET;

    $new_session_arr = array_merge($current_session_array, $current_get_array);

    $_SESSION['gts'] = $new_session_arr;
}

function increase_post_views_counter()
{
    global $post;
    update_post_meta($post->ID, 'post_view_count', get_post_meta($post->ID, 'post_view_count', true) + 1);
}


function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'';
      } else {
        $excerpt = implode(" ",$excerpt);
      }
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
    }

    function content($limit) {
      $content = explode(' ', get_the_content(), $limit);
      if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'';
      } else {
        $content = implode(" ",$content);
      }
      $content = preg_replace('/\[.+\]/','', $content);
      $content = apply_filters('the_content', $content);
      $content = str_replace(']]>', ']]&gt;', $content);
      return $content;
    }






    add_action( 'init', 'create_execution_plans' );
    function create_execution_plans() {
      register_post_type( 'execution_plan',
        array(
          'labels' => array(
            'name' => __( 'Execution Plans' ),
            'singular_name' => __( 'Execution Plan' )
          ),
          'public' => true,
          'has_archive' => true,
          'rewrite' => array('slug' => 'execution_plan'),
          'supports' => array( 'thumbnail', 'title', 'editor', 'custom-fields', 'page-attributes', 'author', 'revisions' )
        )
      );
    }



    // ogajax  -  load more blog posts to index.php template (blog archive page)
    add_action('wp_ajax_load_more_blog_posts', 'load_more_blog_posts');
    add_action('wp_ajax_nopriv_load_more_blog_posts', 'load_more_blog_posts');

    function load_more_blog_posts() {

      $output = array();
      $output['html'] = '';
      // if this variable gets 1 than we need to hide load more button
      $output['all'] = 0;

      $limit = 3;
      $offset = 0;
      if(isset($_POST['params']['offset'])) {
        $offset = (int) $_POST['params']['offset'];
      }

      if(isset($_POST['params']['tag'])) {

      	$args = array(
      		'tag__in' => array((int) $_POST['params']['tag']),
      	  'posts_per_page' => $limit,
      	  'offset' => $offset,
      	  'post_status' => 'publish',
      	);


      } else {

      	$args = array(
      	  'posts_per_page' => $limit,
      	  'offset' => $offset,
      	  'post_status' => 'publish',
      	);


      }



      $my_query = new WP_Query($args);


      $current_output = $limit + $offset;
	    if(isset($_POST['params']['tag'])) {
	      $total_published = get_tag($_POST['params']['tag'])->count;
	    } else {
	    	$total_published = wp_count_posts()->publish;
	    }


      if ($current_output >= $total_published) {
        $output['all'] = 1;
      }

      if( $my_query->have_posts() ) :
        while ($my_query->have_posts()) :
          $my_query->the_post();


      $output['html'] .= '<!-- post snippet - start -->';
    			$output['html'] .= '<section class="white-section blog-archive-section">';
    				$output['html'] .= '<div class="container blog-archive-container">';
    						$output['html'] .= '<div class="row blog-post">';
    								$output['html'] .= '<div class="post-snippet col-md-12">';
    									$output['html'] .= '<div class="post-tags">';
    										$output['html'] .= '<a href="' . get_permalink() . '">';

    										 $tag_array = array();
    										 $posttags = get_the_tags();
    										 if ($posttags) {
    											 foreach($posttags as $tag) {
    												 array_push($tag_array, $tag->name);
    											 }
    										 }
    										 $tags_out = implode(', ', $tag_array);

    										$output['html'] .= $tags_out . '</a>';
    									$output['html'] .= '</div>';
    									$output['html'] .= '<h3 class="post-title"><a href="' . get_permalink() . '" rel="bookmark">' . get_the_title() . '</a></h3>';
    									$output['html'] .= '<div class="row">';
    										$output['html'] .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 image-wrapper">';
    											$output['html'] .= '<a href="' . get_permalink() . '">';

    											if ( has_post_thumbnail() ) :
    												$output['html'] .= get_the_post_thumbnail();
    											endif;

    											$output['html'] .= '</a>';
    										$output['html'] .= '</div>';

    										$output['html'] .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 excerpt-wrapper">';
    											$output['html'] .= '<div class="excerpt"><p>' . strip_tags(get_field('article_intro')) . '  <a href="' . get_permalink() . '" rel="bookmark">read more...</a></p></div>';
    											$output['html'] .= '<div class="author">By Ian Hallett</div>';
    										$output['html'] .= '</div>';
    									$output['html'] .= '</div>';

    								$output['html'] .= '</div>';
    						 $output['html'] .= '</div>';

    				$output['html'] .= '</div>';
    			$output['html'] .= '</section>';
    			$output['html'] .= '<!-- post snippet - end -->';


        endwhile;
      endif;
      wp_reset_query();

      print_r(json_encode($output));
    	die();
    }




    // ogajax  -  load more blog posts to dashboard
    add_action('wp_ajax_load_more_blog_posts_dashboard', 'load_more_blog_posts_dashboard');
    add_action('wp_ajax_nopriv_load_more_blog_posts_dashboard', 'load_more_blog_posts_dashboard');

    function load_more_blog_posts_dashboard() {

      $output = array();
      $output['html'] = '';
      // if this variable gets 1 than we need to hide load more button
      $output['all'] = 0;

      $limit = 3;
      $offset = 0;
      if(isset($_POST['params']['offset'])) {
        $offset = (int) $_POST['params']['offset'];
      }

      $args = array(
        'posts_per_page' => $limit,
        'post_status' => 'publish',
        'offset' => $offset,
      );


      $my_query = new WP_Query($args);


      $current_output = $limit + $offset;
      $total_published = wp_count_posts()->publish;

      if ($current_output >= $total_published) {
        $output['all'] = 1;
      }

      if( $my_query->have_posts() ) :
        while ($my_query->have_posts()) :
          $my_query->the_post();


      $output['html'] .= '<!-- post snippet - start -->';
    			$output['html'] .= '<section class="white-section blog-archive-section">';
    				$output['html'] .= '<div class="container-fluid blog-archive-container">';
    						$output['html'] .= '<div class="row blog-post">';
    								$output['html'] .= '<div class="post-snippet col-md-12">';
    									$output['html'] .= '<div class="post-tags">';
    										$output['html'] .= '<a href="' . get_permalink() . '">';

    										 $tag_array = array();
    										 $posttags = get_the_tags();
    										 if ($posttags) {
    											 foreach($posttags as $tag) {
    												 array_push($tag_array, $tag->name);
    											 }
    										 }
    										 $tags_out = implode(', ', $tag_array);

    										$output['html'] .= $tags_out . '</a>';
    									$output['html'] .= '</div>';
    									$output['html'] .= '<h3 class="post-title"><a href="' . get_permalink() . '" rel="bookmark">' . get_the_title() . '</a></h3>';
    									$output['html'] .= '<div class="row">';
    										$output['html'] .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 image-wrapper">';
    											$output['html'] .= '<a href="' . get_permalink() . '">';

    											if ( has_post_thumbnail() ) :
    												$output['html'] .= get_the_post_thumbnail();
    											endif;

    											$output['html'] .= '</a>';
    										$output['html'] .= '</div>';

    										$output['html'] .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 excerpt-wrapper">';
    											$output['html'] .= '<div class="excerpt"><p>' . strip_tags(get_field('article_intro')) . '  <a href="' . get_permalink() . '" rel="bookmark">read more...</a></p></div>';
    											$output['html'] .= '<div class="author">By Ian Hallett</div>';
    										$output['html'] .= '</div>';
    									$output['html'] .= '</div>';

    								$output['html'] .= '</div>';
    						 $output['html'] .= '</div>';

    				$output['html'] .= '</div>';
    			$output['html'] .= '</section>';
    			$output['html'] .= '<!-- post snippet - end -->';


        endwhile;
      endif;
      wp_reset_query();

      print_r(json_encode($output));
    	die();
    }



// ogajax  -  load more blog posts to dashboard
add_action('wp_ajax_load_more_execution_plans_dashboard', 'load_more_execution_plans_dashboard');
add_action('wp_ajax_nopriv_load_more_execution_plans_dashboard', 'load_more_execution_plans_dashboard');

function load_more_execution_plans_dashboard() {

	$regular_plan_url = '/the-ultimate-career-shortcut';

  $output = array();
  $output['html'] = '';
  // if this variable gets 1 than we need to hide load more button
  $output['all'] = 0;

  $limit = 3;
  $offset = 0;
  if(isset($_POST['params']['offset'])) {
    $offset = (int) $_POST['params']['offset'];
  }

	$featured_plan_id = get_field('featured_execution_plan', 'option');


	if($featured_plan_id) {

	  $args = array(
	    'post_type' => 'execution_plan',
	    'post_status' => 'publish',
	    'posts_per_page' => $limit,
	    'offset' => ($offset - 1),
	    'post__not_in' => array($featured_plan_id)
	  );


	} else {

	  $args = array(
	    'post_type' => 'execution_plan',
	    'post_status' => 'publish',
	    'posts_per_page' => $limit,
	    'offset' => $offset,
	    'post__not_in' => array($featured_plan_id)
	  );

	}


  $my_query = new WP_Query($args);


  $current_output = $limit + $offset;
  $total_published = wp_count_posts('execution_plan')->publish;

  if ($current_output >= $total_published) {
    $output['all'] = 1;
  }

  if( $my_query->have_posts() ) :
    while ($my_query->have_posts()) :
      $my_query->the_post();


      $output['html'] .= '<div class="ex-plan">';
        $output['html'] .= '<div class="plan">';

          $output['html'] .= '<div class="plan-image" onclick="location.href=\'' . $regular_plan_url . '\'" style="background: #01a2be url(' . wp_get_attachment_image_src(get_post_thumbnail_id(),'medium', true)[0] . ') no-repeat center center; background-size: cover; cursor: pointer;">';
            $output['html'] .= '<span class="shadow-title">' . get_the_title() . '</span>';
          $output['html'] .= '</div>';
          $output['html'] .= '<div class="aligner">';
            $output['html'] .= '<h3 class="plan-title"><a href="' . $regular_plan_url . '">' . get_the_title() . '</a></h3>';

            $output['html'] .= '<div class="plan-description">' . strip_tags(get_field('article_intro')) . '</div>';
          $output['html'] .= '</div>';

          $output['html'] .= '<div class="plan-footer">';
            $output['html'] .= '<a href="' . $regular_plan_url . '" class="btn btn-opengo-red btn-block">Learn More</a>';
          $output['html'] .= '</div>';
        $output['html'] .= '</div>';
      $output['html'] .= '</div>';









    endwhile;
  endif;
  wp_reset_query();

  print_r(json_encode($output));
	die();
}



/// could cv reqrite rule

add_action( 'init', 'opengo_cv_rewrite_rule' );
function opengo_cv_rewrite_rule(){
    add_rewrite_rule(
        '^talent-cloud/([^/]*)/?', // talent-could/slug
        'index.php?pagename=talent-cloud&profile_slug=$matches[1]',
        'top' );
}

add_filter( 'query_vars', 'opengo_query_slug' );
function opengo_query_slug( $query_vars ){
    $query_vars[] = 'profile_slug';
    return $query_vars;
}


add_filter('init','flushRules');
function flushRules(){
  global $wp_rewrite;
  $wp_rewrite->flush_rules();
 }



 	remove_action('wp_head', 'rel_canonical');
 	// Remove Canonical Link Added By Yoast WordPress SEO Plugin
 	function at_remove_dup_canonical_link() {
 		return false;
 	}
 	add_filter( 'wpseo_canonical', 'at_remove_dup_canonical_link' );
