/*
 * Summernote PasteClean
 *
 * This is a plugin for Summernote (www.summernote.org) WYSIWYG editor.
 * It will clean up the content your editors may paste in for unknown sources
 * into your CMS. It strips Word special characters, style attributes, script
 * tags, and other annoyances so that the content can be clean HTML with
 * no unknown hitchhiking scripts or styles.
 *
 * @author Jason Byrne, FloSports <jason.byrne@flosports.tv>
 *
 */

(function (factory) {

  /* global define */
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else {
    // Browser globals: jQuery
    factory(window.jQuery);
  }
}(function ($) {

    var badTags = [
            'font',
            'style',
            'embed',
            'param',
            'script',
            'html',
            'body',
            'head',
            'meta',
            'title',
            'link',
            'iframe',
            'applet',
            'noframes',
            'noscript',
            'form',
            'input',
            'select',
            'option',
            'std',
            'xml:',
            'st1:',
            'o:',
            'w:',
            'v:'
        ],
        badAttributes = [
            'style',
            'start',
            'charset'
        ];

    $.summernote.addPlugin({
        name: 'pasteClean',
        init: function(layoutInfo) {

            var $note = layoutInfo.holder();

            $note.on('summernote.paste', function(e, evt) {
                evt.preventDefault();
                // Capture pasted data
                var text = evt.originalEvent.clipboardData.getData('text/plain'),
                    html = evt.originalEvent.clipboardData.getData('text/html');
                // Clean up html input
                if (html) {
                    // Regular expressions
                    var tagStripper         = new RegExp('<[ /]*(' + badTags.join('|') + ')[^>]*>', 'gi'),
                        attributeStripper   = new RegExp(' (' + badAttributes.join('|') + ')(="[^"]*"|=\'[^\']*\'|=[^ ]+)?', 'gi'),
                        commentStripper     = new RegExp('<!--(.*)-->', 'g');

                    // clean it up
                    html = html.toString()
                        // Remove comments
                        .replace(commentStripper, '')
                        // Remove unwanted tags
                        .replace(tagStripper, '')
                        // remove unwanted attributes
                        .replace(attributeStripper, ' ')
                        // remove Word classes
                        .replace(/( class=(")?Mso[a-zA-Z]+(")?)/g, ' ')
                        // remove whitespace (space and tabs) before tags
                        .replace(/[\t ]+\</g, "<")
                        // remove whitespace between tags
                        .replace(/\>[\t ]+\</g, "><")
                        // remove whitespace after tags
                        .replace(/\>[\t ]+$/g, ">")
                        // smart single quotes and apostrophe
                        .replace(/[\u2018\u2019\u201A]/g, "'")
                        // smart double quotes
                        .replace(/[\u201C\u201D\u201E]/g, '"')
                        // ellipsis
                        .replace(/\u2026/g, '...')
                        // dashes
                        .replace(/[\u2013\u2014]/g, '-');
                }
                // Do the paste
                var $dom = $('<div class="pasted"/>').html(html || text);
                $note.summernote('insertNode', $dom[0]);
                return false;
            });

        }
    });
}));


(function($) {


// pre init
Dropzone.autoDiscover = false;

var ogApp = {};


function isOnlyText(val) {
		if(val !== undefined) {
		var matches = val.match(/^[a-z\d\-_\s\.\,\'\`\"\(\)\/]+$/i);
		if (matches != null) {
		    return true;
		} else {
			return false;
		}
	}
}



function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
};


function isPhoneNumber(number) {
	var pattern = new RegExp(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
    return pattern.test(number);
};




  $(function() {
    $( "#sortable" ).sortable({cancel: '.note-editor, .form-control'});
    //$( "#sortable" ).disableSelection();
  });




	// do the same on resize

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Angular App ////


// Little jquery dirty checking
$('.list-group.full-width > .list-group-item > .rendered').click(function() {

	$('.list-group.full-width > .list-group-item').each(function() {

		if($(this).find('form.ng-dirty').length > 0) {
				$(this).find('.unsaved-changes').show();
		}

	});

});







//// Personal editor



if($('#editor-personal-details').length) {

// we are on personal details editor page

function validatePersonalData() {

	$('.og-error').hide();

	var isValid = true;

// VALIDATE NAME
	if($('#name').length) {

		if(
				(
					 $('#name').val().length > 0
					 &&
					 $('#name').val().length < 40
				)
				&&
				( isOnlyText($('#name').val()) )
			)

		{
			//console.log('name is valid');
		} else {
			//error
			isValid = false;
			$('#name').parent().parent().find('.og-error').show();
		}
	}

// VALIDATE SURNAME

	if($('#surname').length) {

		if(
				(
					 $('#surname').val().length > 0
					 &&
					 $('#surname').val().length < 40
				)
				&&
				( isOnlyText($('#surname').val()) )
			)

		{
			//console.log('surname is valid');
		} else {
			//error
			isValid = false;
			$('#surname').parent().parent().find('.og-error').show();
		}
	}

// VALIDATE Email

	if($('#email').length) {

		if(isValidEmailAddress($('#email').val()))
		{
			//console.log('email is valid');
		} else {
			//error
			isValid = false;
			$('#email').parent().parent().find('.og-error').show();
		}
	}


// VALIDATE Phone

	if($('#phone').length) {

		if(isPhoneNumber($('#phone').val()) || $('#phone').val() == '')
		{
			//console.log('phone is valid');
		} else {
			//error
			isValid = false;
			$('#phone').parent().parent().find('.og-error').show();
		}
	}


	return isValid;
}


///////////////////// AVATAR CROPPER ///////////////////////
///////////////////// AVATAR CROPPER ///////////////////////



		// file types
		var images = '.jpg,.jpeg,.png';
		var allowedFileTypes = images;

		if($("#avatar-dropzone").length) {
			console.log('dropzone detected!');
			// Create dropzone
			var avatarDrop = new Dropzone("#avatar-dropzone", {
				url: ogAjax, // output from scripts.php to header ..
				maxFilesize: 4,
				maxFiles: 1,
				paramName: "file",
				uploadMultiple:false,
				acceptedFiles: allowedFileTypes,
				parallelUploads: 1,
				autoProcessQueue:false,
				// previewsContainer: '.dropzone-previews',
				thumbnailWidth: 600,
    		thumbnailHeight: null,
   			addRemoveLinks:false,
   			clickable: ['#upload-new-avatar', '#avatar-dropzone'],
   			init: function() {
				      this.on("maxfilesexceeded", function(file) {
				            this.removeAllFiles();
				            this.addFile(file);
				      });
				}
			});

			Dropzone.options.avatarDrop = false;

			//handle file adding and preview
			avatarDrop.on("thumbnail", function(file, dataUrl) {
				if(file.accepted == true) {
					$('#dz-remove-file').show();
					$('.upload-error').hide();
					$('#avatar-dropzone img').attr('src', dataUrl);
					$('#avatar-dropzone').attr('data-remove-avatar', 'false');
				} else {
					$('.upload-error').show();
				}
				console.log(file);
			});


		}



		$('#dz-remove-file').click(function(){
			avatarDrop.removeAllFiles();
			$('#dz-remove-file').hide();
			$('#avatar-dropzone img').attr('src', defaultAvatar);
			$('#avatar-dropzone').attr('data-remove-avatar', 'true');
		});




			avatarDrop.on("sending", function(file, xhr, formData) {

				var data = {};

				data.name     = $('#name').val();
				data.surname  = $('#surname').val();
				data.email    = $('#email').val();
				data.phone    = $('#phone').val();
				data.country  = $('#country').val();

				// Will send the filesize along with the file as POST data.
				// this is basically the ajax function that will handle things on the back end. ajax.php file
				formData.append("action", "og_edit_personal_details");
				formData.append("data_name", data.name);
				formData.append("data_surname", data.surname);
				formData.append("data_email", data.email);
				formData.append("data_phone", data.phone);
				formData.append("data_country", data.country);
			});


			// progress bar shows loading progress
		    avatarDrop.on('uploadprogress', function (file, progress) {
		    	$(".item-preloader").show();
					$(".item-failed-msg").hide();
					$(".item-success-msg").hide();
		    });

		    // when upload finished and got response from the back end
		  	avatarDrop.on("success", function(a, msg) {

		  				$(".item-preloader").hide();
							console.log(msg);
							//console.log(a);
							if(msg == 'failed') {
								//show failed msg
								$(".item-failed-msg").show();
							} else {
								//show success msg
								location.reload();
								$(".item-success-msg").show();

							}

			});



// on button click validate data

$('#editor-personal-details').on('click', '#save-btn', function(event) {
	event.preventDefault();
	/* Act on the event */

	if(validatePersonalData()) {
		// then proces request


		if($('#avatar-dropzone').hasClass('dz-max-files-reached')) {
			//if dropzone has a file than send ajax request


			avatarDrop.processQueue();



		} else {

			// send regular ajax request
			var data = {};

				data.name     = $('#name').val();
				data.surname  = $('#surname').val();
				data.email    = $('#email').val();
				data.phone    = $('#phone').val();
				data.country  = $('#country').val();

				if($('#avatar-dropzone').attr('data-remove-avatar') == "true") {
					data.remove_avatar = true;
					//console.log( $('#avatar-dropzone').attr('data-remove-avatar') );
					//console.log( 'is true' );
				} else {
					data.remove_avatar = false;
					//console.log( $('#avatar-dropzone').attr('data-remove-avatar') );
					//console.log( 'is not true' );
				}

				// start of ajax wrapper
				$(".item-preloader").show();
				$(".item-failed-msg").hide();
				$(".item-success-msg").hide();
				$.ajax({
				    data: ({
				    	action : 'og_edit_personal_details',
				    	data_name: data.name,
				    	data_surname: data.surname,
				    	data_email: data.email,
				    	data_phone: data.phone,
				    	data_country: data.country,
				    	data_remove_avatar: data.remove_avatar,
				    	}),
					    type: 'POST',
					    async: true,
					    url: ogAjax,
					  	})
				// On done return response
				.done(function( msg ) {
				 		 $(".item-preloader").hide();
							console.log(msg);
							if(msg == 'failed') {
								//show failed msg
								$(".item-failed-msg").show();
							} else {
								//show success msg
								location.reload();
								$(".item-success-msg").show();

							}
				});
				// end of ajax wrapper


		}





	} else {
		// show errors
	}


});



}



$('#editor-personal-details #phone').keyup(function(event) {
	$(".phone-span").html($('#editor-personal-details #phone').val());
});

$('#editor-personal-details #email').keyup(function(event) {
	$(".email-span").html($('#editor-personal-details #email').val());
});


////////////////////////////////////////////////////////////////////////////////////////






























//// Ideal Job editor

var ogIdealJobApp = angular.module('ogIdealJobApp', ['auto-value']);

ogIdealJobApp.controller('idealJobController', function ($scope) {

	$scope.update = function() {
		// create object
		var data = {};

		data.ideal_job_function     = $scope.ideal_job_function;
		data.ideal_job_seniority    = $scope.ideal_job_seniority;
		data.ideal_job_industry     = $scope.ideal_job_industry;
		data.ideal_job_location     = $scope.ideal_job_location;
		data.ideal_job_company_type = $scope.ideal_job_company_type;
		data.ideal_job_contract     = $scope.ideal_job_contract;
		data.ideal_job_salary       = $scope.ideal_job_salary;
		data.ideal_job_currency     = $scope.ideal_job_currency;

		// start of ajax wrapper
		$(".item-preloader").show();
		$(".item-failed-msg").hide();
		$(".item-success-msg").hide();
		$.ajax({
		    data: ({
		    	action : 'og_edit_ideal_job',
		    	params: data
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
		 		 $(".item-preloader").hide();
					console.log(msg);
					if(msg == 'failed') {
						//show failed msg
						$(".item-failed-msg").show();
					} else {
						//show success msg
						location.reload();
						$(".item-success-msg").show();
					}
		});
		// end of ajax wrapper
	}

});



//// Ideal Job adder

var ogAddIdealJobApp = angular.module('ogAddIdealJobApp', ['auto-value']);

ogAddIdealJobApp.controller('addIdealJobController', function ($scope) {

	$scope.update = function() {
		// create object
		var data = {};

		data.ideal_job_function     = $scope.ideal_job_function;
		data.ideal_job_seniority    = $scope.ideal_job_seniority;
		data.ideal_job_industry     = $scope.ideal_job_industry;
		data.ideal_job_location     = $scope.ideal_job_location;
		data.ideal_job_company_type = $scope.ideal_job_company_type;
		data.ideal_job_contract     = $scope.ideal_job_contract;
		data.ideal_job_salary       = $scope.ideal_job_salary;
		data.ideal_job_currency     = $scope.ideal_job_currency;



		// start of ajax wrapper
		$(".item-preloader").show();
		$(".item-failed-msg").hide();
		$(".item-success-msg").hide();
		$.ajax({
		    data: ({
		    	action : 'og_add_ideal_job',
		    	params: data
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
		 		 $(".item-preloader").hide();
					console.log(msg);
					if(msg == 'failed') {
						//show failed msg
						$(".item-failed-msg").show();
					} else {
						//show success msg
						$(".item-success-msg").show();

						// redirect to add current job

						window.location.href = '/add-current-job/';

					}
		});
		// end of ajax wrapper



	}

});




//// Current Job editor

var ogCurrentJobApp = angular.module('ogCurrentJobApp', ['auto-value', 'summernote']);

ogCurrentJobApp.controller('currentJobController', function ($scope) {

$scope.valid = true;



  $scope.summernoteOptions = {
    height: 140,
    toolbar: [
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['view', ['fullscreen']],
        ]
  };


	$scope.is_alpha = function(val) {
			return isOnlyText(val);
	}

	$scope.update = function() {
		// create object
		var data = {};

		data.organisation_name = $scope.organisation_name;
		data.industry          = $scope.industry;
		data.company_type      = $scope.company_type;
		data.job_function      = $scope.job_function;
		data.seniority         = $scope.seniority;
		data.year_started      = $scope.year_started;
		data.contract          = $scope.contract;
		data.notice_period     = $scope.notice_period;
		data.current_salary    = $scope.current_salary;
		data.current_currency  = $scope.current_currency;
		data.responsibilities  = $scope.responsibilities;


		// start of ajax wrapper
		$(".item-preloader").show();
		$(".item-failed-msg").hide();
		$(".item-success-msg").hide();
		$.ajax({
		    data: ({
		    	action : 'og_edit_current_job',
		    	params: data
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
		 		 $(".item-preloader").hide();
					console.log(msg);
					if(msg == 'failed') {
						//show failed msg
						$(".item-failed-msg").show();
					} else {
						//show success msg
						location.reload();
						$(".item-success-msg").show();
					}
		});
		// end of ajax wrapper

	}

	$scope.validate = function() {

		$scope.valid = true;

		// check if required fileds are filled
		if ( !$scope.organisation_name
			|| !$scope.is_alpha($scope.organisation_name)
			|| !$scope.industry
			|| !$scope.company_type
			|| !$scope.job_function
			|| !$scope.seniority
			|| !$scope.year_started) {

			// if any of them is not filled then set to invalid
			$scope.valid = false;
		}


		if(!$scope.valid) {
			console.log("invalid. need to hilight errors.");
		} else {
			$scope.update();
		}
	}

});





//// Current Job Adder

var ogAddCurrentJobApp = angular.module('ogAddCurrentJobApp', ['auto-value', 'summernote']);

ogAddCurrentJobApp.controller('addCurrentJobController', function ($scope) {

  $scope.summernoteOptions = {
    height: 140,
    toolbar: [
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['view', ['fullscreen']],
        ]
  };

$scope.valid = true;

	$scope.is_alpha = function(val) {
			return isOnlyText(val);
	}

	$scope.update = function() {
		// create object
		var data = {};

		data.organisation_name = $scope.organisation_name;
		data.industry          = $scope.industry;
		data.company_type      = $scope.company_type;
		data.job_function      = $scope.job_function;
		data.seniority         = $scope.seniority;
		data.year_started      = $scope.year_started;
		data.contract          = $scope.contract;
		data.notice_period     = $scope.notice_period;
		data.current_salary    = $scope.current_salary;
		data.current_currency  = $scope.current_currency;
		data.responsibilities  = $scope.responsibilities;

		// start of ajax wrapper
		$(".item-preloader").show();
		$(".item-failed-msg").hide();
		$(".item-success-msg").hide();
		$.ajax({
		    data: ({
		    	action : 'og_add_current_job',
		    	params: data
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
		 		 $(".item-preloader").hide();
					console.log(msg);
					if(msg == 'failed') {
						//show failed msg
						$(".item-failed-msg").show();
					} else {
						//show success msg
						$(".item-success-msg").show();

						// show modal
						$("#profile-complete-modal").modal("show");

						var secondsLeft = 5;

						$("#profile-complete-modal").find('.countdown').html(secondsLeft);

						var interval = setInterval(function(){
							if(secondsLeft < 1) {
								clearInterval(interval);
								window.location.href = '/edit-personal-details';
							} else {
								secondsLeft = secondsLeft - 1;
								$("#profile-complete-modal").find('.countdown').html(secondsLeft);
							}

						}, 1000);




						//window.location.href = '/complete-profile/';
					}
		});
		// end of ajax wrapper

	}

	$scope.validate = function() {

		$scope.valid = true;

		// check if required fileds are filled
		if ( !$scope.organisation_name
			|| !$scope.is_alpha($scope.organisation_name)
			|| !$scope.industry
			|| !$scope.company_type
			|| !$scope.job_function
			|| !$scope.seniority
			|| !$scope.year_started) {

			// if any of them is not filled then set to invalid
			$scope.valid = false;
		}


		if(!$scope.valid) {
			console.log("invalid. need to hilight errors.");
		} else {
			$scope.update();
		}
	}

});






//// Experience editor
var ogExperienceApp = angular.module('ogExperienceApp', ['auto-value', 'summernote']);

ogExperienceApp.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});


ogExperienceApp.controller('experienceController', function ($scope, $timeout) {


  $scope.summernoteOptions = {
    height: 140,
    toolbar: [
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['view', ['fullscreen']],
        ]
  };

	$scope.validOne   = true;
	$scope.validTwo   = true;
	$scope.validThree = true;
	$scope.validFour  = true;
	$scope.validFive  = true;
	$scope.validSix   = true;
	$scope.validSeven = true;

	$scope.validNew   = true;



	$scope.is_alpha = function(val) {
			return isOnlyText(val);
	}

	$scope.deleteNode = function(n) {
			if(n === 1) {
				$('#experience-node-one').remove();
			} else if (n === 2) {

				$('#experience-node-two').remove();

			} else  if (n === 3) {

				$('#experience-node-three').remove();

			} else  if (n === 4) {

				$('#experience-node-four').remove();

			} else  if (n === 5) {

				$('#experience-node-five').remove();

			} else  if (n === 6) {

				$('#experience-node-six').remove();

			} else  if (n === 7) {

				$('#experience-node-seven').remove();

			}
	}


// Main array with data
$scope.dataArray = [];

//Init state
$scope.expandedOne   = false;
$scope.expandedTwo   = false;
$scope.expandedThree = false;
$scope.expandedFour  = false;
$scope.expandedFive  = false;
$scope.expandedSix   = false;
$scope.expandedSeven = false;
$scope.expandedNew   = true;

$scope.addNewActive = false;

$scope.getDataByID = function(id) {
	var data = [];
	if(id == 'experience-node-one') {
		data = $scope.updateOne();
	}
	if(id == 'experience-node-two') {
		data = $scope.updateTwo();
	}
	if(id == 'experience-node-three') {
		data = $scope.updateThree();
	}
	if(id == 'experience-node-four') {
		data = $scope.updateFour();
	}
	if(id == 'experience-node-five') {
		data = $scope.updateFive();
	}
	if(id == 'experience-node-six') {
		data = $scope.updateSix();
	}
	if(id == 'experience-node-seven') {
		data = $scope.updateSeven();
	}
	if(id == 'experience-node-new') {
		data = $scope.updateNew();
	}
	return data;
}




// Toggle state
$scope.toggleBoolOne = function() { if($scope.expandedOne == true) { $scope.expandedOne = false; } else { $scope.expandedOne = true; } }
$scope.toggleBoolTwo = function() { if($scope.expandedTwo == true) { $scope.expandedTwo = false; } else { $scope.expandedTwo = true; } }
$scope.toggleBoolThree = function() { if($scope.expandedThree == true) { $scope.expandedThree = false; } else { $scope.expandedThree = true; } }
$scope.toggleBoolFour = function() { if($scope.expandedFour == true) { $scope.expandedFour = false; } else { $scope.expandedFour = true; } }
$scope.toggleBoolFive = function() { if($scope.expandedFive == true) { $scope.expandedFive = false; } else { $scope.expandedFive = true; } }
$scope.toggleBoolSix = function() { if($scope.expandedSix == true) { $scope.expandedSix = false; } else { $scope.expandedSix = true; } }
$scope.toggleBoolSeven = function() { if($scope.expandedSeven == true) { $scope.expandedSeven = false; } else { $scope.expandedSeven = true; } }
$scope.toggleBoolNew = function() { if($scope.expandedNew == true) { $scope.expandedNew = false; } else { $scope.expandedNew = true; } }



$scope.update = function() {

console.log('updating experience..');
	$scope.dataArray = [];
	// iterate through output and push to main data array.
	$.each($("#sortable").sortable("toArray"), function(index, value) {
		$scope.dataArray.push($scope.getDataByID(value));
	});

	// Check if NEW is active and if so then add it too

	if($scope.addNewActive == true) {
		$scope.dataArray.push($scope.updateNew());
	}

console.log('items array...');
console.log($("#sortable").sortable("toArray"));

// if the data array is empty than just add a blank array. needed for being able to delete all the items
	if($scope.dataArray.length < 1) {
		$scope.dataArray = ['', '', '', '', '', '', '', '', ''];
	}


console.log($scope.dataArray);
		// start of ajax wrapper
		$(".item-preloader").show();
		$(".item-failed-msg").hide();
		$(".item-success-msg").hide();
		$.ajax({
		    data: ({
		    	action : 'og_edit_experience',
		    	params: $scope.dataArray
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
			console.log(msg);
		 		 $(".item-preloader").hide();
					if(msg == 'failed') {
						//show failed msg
						$(".item-failed-msg").show();
					} else {
						//show success msg
						location.reload();
						$(".item-success-msg").show();
					}
		});
		// end of ajax wrapper



}

$scope.validate = function() {

console.log("start validation");

	var allValid = true;


	if( $("#experience-node-one:visible").length > 0 ) {


		if (!$scope.validateOne()) {
			allValid = false;
			  $timeout(function(){
		        $scope.expandedOne =  true;
		    });
		} else {
			$scope.expandedOne   = false;
		}

	}


	if( $("#experience-node-two:visible").length > 0 ) {
		if (!$scope.validateTwo()) {
			allValid = false;
			$scope.expandedTwo   = true;
		} else {
			$scope.expandedTwo   = false;
		}
	}

	if( $("#experience-node-three:visible").length > 0 ) {
		if (!$scope.validateThree()) {
			allValid = false;
			$scope.expandedThree = true;

		} else {
			$scope.expandedThree   = false;
		}
	}


	if( $("#experience-node-four:visible").length > 0 ) {
		if (!$scope.validateFour()) {
			allValid = false;
			$scope.expandedFour = true;

		} else {
			$scope.expandedFour   = false;
		}
	}

	if( $("#experience-node-five:visible").length > 0 ) {
		if (!$scope.validateFive()) {
			allValid = false;
			$scope.expandedFive = true;

		} else {
			$scope.expandedFive   = false;
		}
	}

	if( $("#experience-node-six:visible").length > 0 ) {
		if (!$scope.validateSix()) {
			allValid = false;
			$scope.expandedSix = true;

		} else {
			$scope.expandedSix   = false;
		}
	}


	if( $("#experience-node-seven:visible").length > 0 ) {
		if (!$scope.validateSeven()) {
			allValid = false;
			$scope.expandedSeven = true;

		} else {
			$scope.expandedSeven   = false;
		}
	}

	if( $("#experience-node-new:visible").length > 0 ) {
		console.log('new exp exists.');
		if (!$scope.validateNew()) {
			allValid = false;
			$scope.expandedNew   = true;

		} else {
			$scope.expandedNew   = false;
		}
	}


	if (allValid == true) {
		console.log("all valid. Starting update.");
		$scope.update();
	}



}



	$scope.updateOne = function() {

			// create object
			var data = {};

			data.organisation_name = $scope.organisation_nameOne;
			data.industry          = $scope.industryOne;
			data.company_type      = $scope.company_typeOne;
			data.job_function      = $scope.job_functionOne;
			data.seniority         = $scope.seniorityOne;
			data.year_started      = $scope.year_startedOne;
			data.year_ended        = $scope.year_endedOne;
			data.contract          = $scope.contractOne;
			data.responsibilities  = $scope.responsibilitiesOne;


			return data;
			$("#experience-node-one").find('.unsaved-changes').hide();
			$scope.formOne.$setPristine();


	}

	$scope.validateOne = function() {

		// once validated update
		$scope.validOne = true;

		if ($scope.organisation_nameOne &&
				$scope.industryOne &&
				$scope.company_typeOne &&
				$scope.job_functionOne &&
				$scope.seniorityOne &&
				$scope.year_startedOne &&
				$scope.year_endedOne &&
				$scope.responsibilitiesOne) {

			if(!$scope.is_alpha($scope.organisation_nameOne)) {
				$scope.validOne = false;
				console.log("invalid characters used.");
			} else {
				return true;
			}


		} else {

				$scope.validOne = false;
				console.log("invalid");
		}

	}






	$scope.updateTwo = function() {

			// create object
			var data = {};

			data.organisation_name = $scope.organisation_nameTwo;
			data.industry          = $scope.industryTwo;
			data.company_type      = $scope.company_typeTwo;
			data.job_function      = $scope.job_functionTwo;
			data.seniority         = $scope.seniorityTwo;
			data.year_started      = $scope.year_startedTwo;
			data.year_ended        = $scope.year_endedTwo;
			data.contract          = $scope.contractTwo;
			data.responsibilities  = $scope.responsibilitiesTwo;

			return data;
			$("#experience-node-two").find('.unsaved-changes').hide();
			$scope.formTwo.$setPristine();


	}

	$scope.validateTwo = function() {

		// once validated update
		$scope.validTwo = true;

		if ($scope.organisation_nameTwo &&
				$scope.industryTwo &&
				$scope.company_typeTwo &&
				$scope.job_functionTwo &&
				$scope.seniorityTwo &&
				$scope.year_startedTwo &&
				$scope.year_endedTwo &&
				$scope.responsibilitiesTwo) {

			if(!$scope.is_alpha($scope.organisation_nameTwo)) {
				$scope.validTwo = false;
				console.log("invalid characters used.");
			} else {
				return true;
			}

		} else {

				$scope.validTwo = false;
				console.log("invalid");

		}

	}


	$scope.updateThree = function() {

			// create object
			var data = {};

			data.organisation_name = $scope.organisation_nameThree;
			data.industry          = $scope.industryThree;
			data.company_type      = $scope.company_typeThree;
			data.job_function      = $scope.job_functionThree;
			data.seniority         = $scope.seniorityThree;
			data.year_started      = $scope.year_startedThree;
			data.year_ended        = $scope.year_endedThree;
			data.contract          = $scope.contractThree;
			data.responsibilities  = $scope.responsibilitiesThree;
			return data;
			$("#experience-node-three").find('.unsaved-changes').hide();
			$scope.formThree.$setPristine();


	}

	$scope.validateThree = function() {

		// once validated update
		$scope.validThree = true;

		if ($scope.organisation_nameThree &&
				$scope.industryThree &&
				$scope.company_typeThree &&
				$scope.job_functionThree &&
				$scope.seniorityThree &&
				$scope.year_startedThree &&
				$scope.year_endedThree &&
				$scope.responsibilitiesThree) {

			if(!$scope.is_alpha($scope.organisation_nameThree)) {
				$scope.validThree = false;
				console.log("invalid characters used.");
			} else {
				return true;
			}

		} else {

				$scope.validThree = false;
				console.log("invalid");
		}

	}

	$scope.updateFour = function() {

			// create object
			var data = {};

			data.organisation_name = $scope.organisation_nameFour;
			data.industry          = $scope.industryFour;
			data.company_type      = $scope.company_typeFour;
			data.job_function      = $scope.job_functionFour;
			data.seniority         = $scope.seniorityFour;
			data.year_started      = $scope.year_startedFour;
			data.year_ended        = $scope.year_endedFour;
			data.contract          = $scope.contractFour;
			data.responsibilities  = $scope.responsibilitiesFour;

			return data;
			$("#experience-node-four").find('.unsaved-changes').hide();
			$scope.formFour.$setPristine();



	}

	$scope.validateFour = function() {

		// once validated update
		$scope.validFour = true;

		if ($scope.organisation_nameFour &&
				$scope.industryFour &&
				$scope.company_typeFour &&
				$scope.job_functionFour &&
				$scope.seniorityFour &&
				$scope.year_startedFour &&
				$scope.year_endedFour &&
				$scope.responsibilitiesFour) {

				if(!$scope.is_alpha($scope.organisation_nameFour)) {
					$scope.validFour = false;
					console.log("invalid characters used.");
				} else {
					return true;
				}

		} else {

				$scope.validFour = false;
				console.log("invalid");
		}

	}



	$scope.updateFive = function() {

			// create object
			var data = {};

			data.organisation_name = $scope.organisation_nameFive;
			data.industry          = $scope.industryFive;
			data.company_type      = $scope.company_typeFive;
			data.job_function      = $scope.job_functionFive;
			data.seniority         = $scope.seniorityFive;
			data.year_started      = $scope.year_startedFive;
			data.year_ended        = $scope.year_endedFive;
			data.contract          = $scope.contractFive;
			data.responsibilities  = $scope.responsibilitiesFive;
			return data;
			$("#experience-node-five").find('.unsaved-changes').hide();
			$scope.formFive.$setPristine();

	}

	$scope.validateFive = function() {

		// once validated update
		$scope.validFive = true;

		if ($scope.organisation_nameFive &&
				$scope.industryFive &&
				$scope.company_typeFive &&
				$scope.job_functionFive &&
				$scope.seniorityFive &&
				$scope.year_startedFive &&
				$scope.year_endedFive &&
				$scope.responsibilitiesFive) {

				if(!$scope.is_alpha($scope.organisation_nameFive)) {
					$scope.validFive = false;
					console.log("invalid characters used.");
				} else {
					return true;
				}

		} else {

				$scope.validFive = false;
				console.log("invalid");
		}

	}


	$scope.updateSix = function() {

			// create object
			var data = {};

			data.organisation_name = $scope.organisation_nameSix;
			data.industry          = $scope.industrySix;
			data.company_type      = $scope.company_typeSix;
			data.job_function      = $scope.job_functionSix;
			data.seniority         = $scope.senioritySix;
			data.year_started      = $scope.year_startedSix;
			data.year_ended        = $scope.year_endedSix;
			data.contract          = $scope.contractSix;
			data.responsibilities  = $scope.responsibilitiesSix;
			return data;
			$("#experience-node-six").find('.unsaved-changes').hide();
			$scope.formSix.$setPristine();


	}

	$scope.validateSix = function() {

		// once validated update
		$scope.validSix = true;

		if ($scope.organisation_nameSix &&
				$scope.industrySix &&
				$scope.company_typeSix &&
				$scope.job_functionSix &&
				$scope.senioritySix &&
				$scope.year_startedSix &&
				$scope.year_endedSix &&
				$scope.responsibilitiesSix) {

				if(!$scope.is_alpha($scope.organisation_nameSix)) {
					$scope.validSix = false;
					console.log("invalid characters used.");
				} else {
					return true;
				}

		} else {

				$scope.validSix = false;
				console.log("invalid");
		}

	}

	$scope.updateSeven = function() {

			// create object
			var data = {};

			data.organisation_name = $scope.organisation_nameSeven;
			data.industry          = $scope.industrySeven;
			data.company_type      = $scope.company_typeSeven;
			data.job_function      = $scope.job_functionSeven;
			data.seniority         = $scope.senioritySeven;
			data.year_started      = $scope.year_startedSeven;
			data.year_ended        = $scope.year_endedSeven;
			data.contract          = $scope.contractSeven;
			data.responsibilities  = $scope.responsibilitiesSeven;
			return data;
			$("#experience-node-seven").find('.unsaved-changes').hide();
			$scope.formSeven.$setPristine();


	}

	$scope.validateSeven = function() {

		// once validated update
		$scope.validSeven = true;

		if ($scope.organisation_nameSeven &&
				$scope.industrySeven &&
				$scope.company_typeSeven &&
				$scope.job_functionSeven &&
				$scope.senioritySeven &&
				$scope.year_startedSeven &&
				$scope.year_endedSeven &&
				$scope.responsibilitiesSeven) {

				if(!$scope.is_alpha($scope.organisation_nameSeven)) {
					$scope.validSeven = false;
					console.log("invalid characters used.");
				} else {
					return true;
				}

		} else {

				$scope.validSeven = false;
				console.log("invalid");
		}

	}


	$scope.updateNew = function() {

			// create object
			var data = {};

			data.organisation_name = $scope.organisation_nameNew;
			data.industry          = $scope.industryNew;
			data.company_type      = $scope.company_typeNew;
			data.job_function      = $scope.job_functionNew;
			data.seniority         = $scope.seniorityNew;
			data.year_started      = $scope.year_startedNew;
			data.year_ended        = $scope.year_endedNew;
			data.contract         = $scope.contractNew;
			data.responsibilities  = $scope.responsibilitiesNew;

			return data;

	}

	$scope.validateNew = function() {

		// once validated update
		$scope.validNew = true;

		if ($scope.organisation_nameNew &&
				$scope.industryNew &&
				$scope.company_typeNew &&
				$scope.job_functionNew &&
				$scope.seniorityNew &&
				$scope.year_startedNew &&
				$scope.year_endedNew) {

				if(!$scope.is_alpha($scope.organisation_nameNew)) {
					$scope.validNew = false;
					console.log("invalid characters used.");
				} else {
					console.log('validating new. it is true');
					return true;
				}

		} else {

				$scope.validNew = false;
				console.log("invalid");
		}

	}

});



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Education editor

var ogEducationApp = angular.module('ogEducationApp', ['auto-value']);

ogEducationApp.controller('educationController', function ($scope, $timeout) {


	$scope.is_alpha = function(val) {
			return isOnlyText(val);
	}

	$scope.deleteNode = function(n) {
			if(n === 1) {

				// ajax call

				// on done

				$('#education-node-one').remove();

			} else if (n === 2) {
				// ajax call

				// on done

				$('#education-node-two').remove();

			} else  if (n === 3) {
				// ajax call

				// on done

				$('#education-node-three').remove();

			}
	}

// Main array with data
$scope.dataArray = [];


// Init validity
$scope.validOne      = true;
$scope.validTwo      = true;
$scope.validThree    = true;
$scope.validNew      = true;

//Init state
$scope.expandedOne   = false;
$scope.expandedTwo   = false;
$scope.expandedThree = false;
$scope.expandedNew   = true;

$scope.addNewActive = false;

$scope.getDataByID = function(id) {
	var data = [];
	if(id == 'education-node-one') {
		data = $scope.updateOne();
	}
	if(id == 'education-node-two') {
		data = $scope.updateTwo();
	}
	if(id == 'education-node-three') {
		data = $scope.updateThree();
	}
	if(id == 'education-node-new') {
		data = $scope.updateNew();
	}
	return data;
}

// Toggle state
$scope.toggleBoolOne = function() { if($scope.expandedOne == true) { $scope.expandedOne = false; } else { $scope.expandedOne = true; } }
$scope.toggleBoolTwo = function() { if($scope.expandedTwo == true) { $scope.expandedTwo = false; } else { $scope.expandedTwo = true; } }
$scope.toggleBoolThree = function() { if($scope.expandedThree == true) { $scope.expandedThree = false; } else { $scope.expandedThree = true; } }
$scope.toggleBoolNew = function() { if($scope.expandedNew == true) { $scope.expandedNew = false; } else { $scope.expandedNew = true; } }



$scope.update = function() {

	$scope.dataArray = [];

	console.log('starting update');
	// iterate through output and push to main data array.
	$.each($("#sortable").sortable("toArray"), function(index, value) {
		$scope.dataArray.push($scope.getDataByID(value));
	});
	console.log('sortable value');
	console.log($("#sortable").sortable("toArray"));
	// Check if NEW is active and if so then add it too

	if($scope.addNewActive == true) {
		$scope.dataArray.push($scope.updateNew());
	}

// if the data array is empty than just add a blank array. needed for being able to delete all the items
	if($scope.dataArray.length < 1) {
		$scope.dataArray = ['', '', '', '', ''];
	}

		// start of ajax wrapper
		$(".item-preloader").show();
		$(".item-failed-msg").hide();
		$(".item-success-msg").hide();
		$.ajax({
		    data: ({
		    	action : 'og_edit_education',
		    	params: $scope.dataArray
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
		 		 $(".item-preloader").hide();
					console.log(msg);
					if(msg == 'failed') {
						//show failed msg
						$(".item-failed-msg").show();
					} else {
						//show success msg
						$(".item-success-msg").show();
						location.reload();
						// redirect to dashboard
					}
		});
		// end of ajax wrapper



}

$scope.validate = function() {

	var allValid = true;


	if( $("#education-node-one:visible").length > 0 ) {


		if (!$scope.validateOne()) {
			allValid = false;
			  $timeout(function(){
		        $scope.expandedOne =  true;
		    });
		} else {
			$scope.expandedOne   = false;
		}

	}


	if( $("#education-node-two:visible").length > 0 ) {
		if (!$scope.validateTwo()) {
			allValid = false;
			$scope.expandedTwo   = true;
		} else {
			$scope.expandedTwo   = false;
		}
	}

	if( $("#education-node-three:visible").length > 0 ) {
		if (!$scope.validateThree()) {
			allValid = false;
			$scope.expandedThree = true;

		} else {
			$scope.expandedThree   = false;
		}
	}


	if( $("#education-node-new:visible").length > 0 ) {
		if (!$scope.validateNew()) {
			allValid = false;
			$scope.expandedNew   = true;

		} else {
			$scope.expandedNew   = false;
		}
	}


	if (allValid == true) {
		$scope.update();
	}



}



$scope.updateOne = function() {

		// create object
		var data = {};
		if($scope.universityOne == "Other") {
			data.university    = $scope.university_nameOne;
		} else {
			data.university    = $scope.universityOne;
		}

		if($scope.course_nameOne == "Other") {
			data.course_name   = $scope.your_course_nameOne;
		} else {
			data.course_name   = $scope.course_nameOne;
		}
		data.qualification = $scope.qualificationOne;
		data.grade         = $scope.gradeOne;
		data.year          = $scope.yearOne;

		return data;
		$("#education-node-one").find('.unsaved-changes').hide();
		$scope.formOne.$setPristine();


}

$scope.validateOne = function() {

	$scope.validOne = true;

	if($scope.universityOne &&
	$scope.course_nameOne &&
	$scope.qualificationOne &&
	$scope.gradeOne &&
	$scope.yearOne) {

		if(
			(($scope.universityOne == "Other") && (!$scope.university_nameOne || !$scope.is_alpha($scope.university_nameOne)))
			 ||
			 (($scope.course_nameOne == "Other") && (!$scope.your_course_nameOne || !$scope.is_alpha($scope.your_course_nameOne))) )  {


			$scope.validOne    = false;

			console.log("invalid");

		} else {
				return true;
		}



	} else {
			$scope.validOne = false;
			console.log("invalid");
	}

}


$scope.updateTwo = function() {

		// create object
		var data = {};
		if($scope.universityTwo == "Other") {
			data.university    = $scope.university_nameTwo;
		} else {
			data.university    = $scope.universityTwo;
		}

		if($scope.course_nameTwo == "Other") {
			data.course_name   = $scope.your_course_nameTwo;
		} else {
			data.course_name   = $scope.course_nameTwo;
		}
		data.qualification = $scope.qualificationTwo;
		data.grade        = $scope.gradeTwo;
		data.year          = $scope.yearTwo;


		return data;

		$("#education-node-two").find('.unsaved-changes').hide();
		$scope.formTwo.$setPristine();


}

$scope.validateTwo = function() {

	$scope.validTwo = true;

	if($scope.universityTwo &&
	$scope.course_nameTwo &&
	$scope.qualificationTwo &&
	$scope.gradeTwo &&
	$scope.yearTwo) {

		if(
			(($scope.universityTwo == "Other") && (!$scope.university_nameTwo || !$scope.is_alpha($scope.university_nameTwo)))
			 ||
			 (($scope.course_nameTwo == "Other") && (!$scope.your_course_nameTwo || !$scope.is_alpha($scope.your_course_nameTwo))) )  {

			$scope.validTwo = false;
			console.log("invalid");

		} else {
				return true;
		}



	} else {
			$scope.validTwo = false;
			console.log("invalid");
	}

}




$scope.updateThree = function() {

		// create object
		var data = {};
		if($scope.universityThree == "Other") {
			data.university    = $scope.university_nameThree;
		} else {
			data.university    = $scope.universityThree;
		}

		if($scope.course_nameThree == "Other") {
			data.course_name   = $scope.your_course_nameThree;
		} else {
			data.course_name   = $scope.course_nameThree;
		}
		data.qualification = $scope.qualificationThree;
		data.grade        = $scope.gradeThree;
		data.year         = $scope.yearThree;
		return data;
		$("#education-node-three").find('.unsaved-changes').hide();
		$scope.formThree.$setPristine();


}

$scope.validateThree = function() {

	$scope.validThree = true;

	if($scope.universityThree &&
	$scope.course_nameThree &&
	$scope.qualificationThree &&
	$scope.gradeThree &&
	$scope.yearThree) {

		if(
			(($scope.universityThree == "Other") && (!$scope.university_nameThree || !$scope.is_alpha($scope.university_nameThree)))
			 ||
			 (($scope.course_nameThree == "Other") && (!$scope.your_course_nameThree || !$scope.is_alpha($scope.your_course_nameThree))) )  {

			$scope.validThree = false;
			console.log("invalid");

		} else {
				return true;
		}



	} else {
			$scope.validThree = false;
			console.log("invalid");
	}

}


$scope.updateNew = function() {

		// create object
		var data = {};
		if($scope.universityNew == "Other") {
			data.university    = $scope.university_nameNew;
		} else {
			data.university    = $scope.universityNew;
		}

		if($scope.course_nameNew == "Other") {
			data.course_name   = $scope.your_course_nameNew;
		} else {
			data.course_name   = $scope.course_nameNew;
		}
		data.qualification = $scope.qualificationNew;
		data.grade         = $scope.gradeNew;
		data.year          = $scope.yearNew;
		return data;
		$scope.formnew.$setPristine();



}

$scope.validateNew = function() {

	$scope.validNew = true;

	if($scope.universityNew &&
	$scope.course_nameNew &&
	$scope.qualificationNew &&
	$scope.gradeNew &&
	$scope.yearNew) {

		if(
			(($scope.universityNew == "Other") && (!$scope.university_nameNew || !$scope.is_alpha($scope.university_nameNew)))
			 ||
			 (($scope.course_nameNew == "Other") && (!$scope.your_course_nameNew || !$scope.is_alpha($scope.your_course_nameNew))) )  {

			$scope.validNew = false;
			console.log("invalid");

		} else {
				return true;
		}



	} else {
			$scope.validNew = false;
			console.log("invalid");
	}

}

});

// End of education editor
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//// Skills editor

var ogSkillsApp = angular.module('ogSkillsApp', ['auto-value']);

ogSkillsApp.controller('skillsController', function ($scope, $timeout) {

	$scope.valid = true;

	$scope.is_alpha = function(val) {
		if (val.length > 0) {
			return isOnlyText(val);
		} else {
			return true;
		}
	}

	$scope.update = function() {


		var skillsArr = [];

		if($scope.skill_one.length > 0) {
			skillsArr.push($scope.skill_one);
		}

		if($scope.skill_two.length > 0) {
			skillsArr.push($scope.skill_two);
		}

		if($scope.skill_three.length > 0) {
			skillsArr.push($scope.skill_three);
		}

		console.log(skillsArr);
		// create object
		var data = {};

		if(skillsArr[0] !== undefined) {
			data.skill_one   = skillsArr[0];
		} else {
			data.skill_one   = '';
		}

		if(skillsArr[1] !== undefined) {
			data.skill_two   = skillsArr[1];
		} else {
			data.skill_two   = '';
		}


		if(skillsArr[2] !== undefined) {
			data.skill_three   = skillsArr[2];
		} else {
			data.skill_three   = '';
		}


		// start of ajax wrapper
		$(".item-preloader").show();
		$(".item-failed-msg").hide();
		$(".item-success-msg").hide();
		$.ajax({
		    data: ({
		    	action : 'og_edit_skills',
		    	params: data
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
		 		 $(".item-preloader").hide();
					console.log(msg);
					if(msg == 'failed') {
						//show failed msg
						$(".item-failed-msg").show();
					} else {
						//show success msg
						location.reload();
						$(".item-success-msg").show();

					}
		});
		// end of ajax wrapper

	}


	$scope.validate = function() {



		$scope.valid = true;

		if (     (!$scope.is_alpha($scope.skill_one) && $scope.skill_one.length > 0 )
					|| (!$scope.is_alpha($scope.skill_two) && $scope.skill_two.length > 0 )
					|| (!$scope.is_alpha($scope.skill_three) && $scope.skill_three.length > 0 ) ) {

			$scope.valid = false;

		} else {

			$scope.update();

		}

	}

});






//// Settings editor

var ogSettingsApp = angular.module('ogSettingsApp', ['auto-value']);

ogSettingsApp.controller('settingsController', function ($scope) {

	$scope.valid = true;

	$scope.is_alpha = function(val) {
		if (val.length > 0) {
			return isOnlyText(val);
		} else {
			return true;
		}
	}

	$scope.updateDN = function() {

		if($scope.is_alpha($scope.display_name)) {
			// start of ajax wrapper
			$(".item-preloader").show();
			$(".item-failed-msg").hide();
			$(".item-success-msg").hide();

			var data = {};

			data.display_name = $scope.display_name;
			data.email_frequency = $('#email-frequency').val();

      data.slug = $('#slug').val();

			$.ajax({
			    data: ({
			    	action : 'og_edit_settings',
			    	params: data
			    	}),
				    type: 'POST',
				    async: true,
				    url: ogAjax,
				  	})
			// On done return response
			.done(function( msg ) {
			 		 $(".item-preloader").hide();
						console.log(msg);

            var data = JSON.parse(msg);


						if(data.status == 'failed') {
							//show failed msg
							$(".item-failed-msg").show();
						} else {
							//show success msg
							$(".item-success-msg").show();

              if(data.new_slug) {

                $('#slug').val(data.new_slug);



              }

              window.location.reload();

						}
			});
			// end of ajax wrapper

		}

	}


	$scope.deleteProfile = function() {
					$.ajax({
			    data: ({
			    	action : 'og_delete_profile',
			    	params: null
			    	}),
				    type: 'POST',
				    async: true,
				    url: ogAjax,
				  	})
			// On done return response
			.done(function( msg ) {
			 		 $(".item-preloader").hide();
						console.log(msg);
						if(msg == 'failed') {
							//show failed msg
							$(".item-failed-msg").show();
						} else {
							//show success msg
							$(".item-success-msg").show();
							window.location.href = '/whats-next/';
						}
			});
			// end of ajax wrapper
	}


});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



$(document).ready(function() {


// Init multiselect
if($('.multiselect').length > 0) {
    $('.multiselect').multiselect({maxHeight:200});
}

        $('.multiselect-single').multiselect({
            on: {
                change: function(option, checked) {
                    var values = [];
                    $('.multiselect-single option').each(function() {
                        if ($(this).val() !== option.val()) {
                            values.push($(this).val());
                        }
                    });

                    $('.multiselect-single').multiselect('deselect', values);
                }
            }
        });




// Init multiselect with filter
if($('.multiselect-filter').length > 0) {

		$('.multiselect-filter').selectpicker({
		  style: 'btn-default',
		  size: 8,
		  liveSearch: true,
		  dropupAuto:false,
		  liveSearchPlaceholder:'search',
		});


    //$('.multiselect-filter').multiselect({maxHeight:200, enableFiltering: true, enableCaseInsensitiveFiltering: true,filterPlaceholder: 'Search'});


}

// Init multiselect with three possible options
if($('.multiselect-limit-three').length > 0) {

	$('.multiselect-limit-three').each(function(){

		var thisselect = $(this);

		thisselect.multiselect({
			maxHeight:200,
			numberDisplayed:true,
			onChange: function(option, checked) {

	        // Get selected options.
	        var selectedOptions = thisselect.find('option:selected');

	        if (selectedOptions.length >= 3) {
	            // Disable all other checkboxes.
	            var nonSelectedOptions = thisselect.find('option').filter(function() {
	            	return !$(this).is(':selected');
	            });

	            var dropdown = thisselect.siblings('.multiselect-container');
	            nonSelectedOptions.each(function() {
	            	var input = $('input[value="' + $(this).val() + '"]');
	            	input.prop('disabled', true);
	            	input.parent('li').addClass('disabled');
	            });
	        }
	        else {
	            // Enable all checkboxes.
	            var dropdown = thisselect.siblings('.multiselect-container');
	            thisselect.find('option').each(function() {
	            	var input = $('input[value="' + $(this).val() + '"]');
	            	input.prop('disabled', false);
	            	input.parent('li').addClass('disabled');
	            });
	        }
	    	}
		});
	});

}


if ($('#og-wysiwyg').length > 0) {

  $('#og-wysiwyg').summernote({
		height: 140,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,
		  toolbar: [
		    //[groupname, [button list]]

		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['para', ['ul', 'ol', 'paragraph']],
		  ]

	});
}


});




// job matches load more
// usually we load 10 items at a time and then add 10 more items when load more is clicked

$(document).on('click', '#load-more-button', function(){

	//preloader show
		$('.job-matches .loader').show();
	//increase load limit by 140
var params = {};
params.limit = parseInt($('.counter .loaded').html()) + 10;

	//send ajax request
		$.ajax({
		    data: ({
		    	action : 'og_load_more_jobs',
		    	params: params,
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
				var output = JSON.parse(msg);
				$('.job-matches .list-wrapper .list-group').html(output.data);
		 		$('.job-matches .loader').hide();

 		    if(params.limit >= output.total) {
          $('.counter .loaded').html(output.total);
          $('#load-more-button').hide();
        } else {
          $('.counter .loaded').html(params.limit);
          $('#load-more-button').show();
        }

					console.log(msg);

					if(msg == 'failed') {


					} else {



					}
		});
		// end of ajax wrapper
	// get ajax response

	// preloader hide

	// append html

	// update counters

});



///////////////////////////////////////////////////////////////////
// Register interest

// We need to get job_id, profile_id, array of test_ids and answers.

$('#register-interest').click(function(){

	var allValid = true;

	if($('.error-message').length > 0) {
		$('.error-message').hide();
	}

	var data = {};

	data.job_id = $('#job-match').data('job-id');
	data.profile_id = $('#job-match').data('profile-id');

	data.tests = [];

	if($('.capability-requirements').length > 0) {


		$('.capability-line').each(function() {

			var that = $(this);

			if($(this).find('.multiselect-single').val()) {

				var capabilitySet = [$(this).find('.capability-question').data('test-id'), $(this).find('.capability-question').html(), $(this).find('.multiselect-single').val()];
					data.tests.push(capabilitySet);
			} else {
				$(this).next('.error-message').show();
				allValid = false;
			}

		});

	}


	if(allValid == true) {
		console.log(data);
		//send ajax request
$('.button-wrapper .item-preloader').show();
	//send ajax request
		$.ajax({
		    data: ({
		    	action : 'og_register_interest',
		    	params: data,
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
					console.log(msg);
					$('.button-wrapper .item-preloader').hide();

					$('.interest_registered').modal('show');
					$('.capability-requirements').html('<div class="already-applied">You have already registered interest for this job.</div>');
					$('#register-interest').hide();
					//window.location.href = '/jobs/';
		});
		// end of ajax wrapper


	} else {
		console.log('please correct errors above');
	}


});




// Register interest
///////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////
// load more messages in inbox
$(document).on('click', '#load-more-conversations', function(){

	//preloader show
	$('.inbox-content .loader').show();
	//increase load limit by 140
var params = {};
params.limit = parseInt($('.counter .loaded').html()) + 5;
params.section = section;

	//send ajax request
		$.ajax({
		    data: ({
		    	action : 'og_load_more_conversations',
		    	params: params,
		    	}),
			    type: 'POST',
			    async: true,
			    url: ogAjax,
			  	})
		// On done return response
		.done(function( msg ) {
				var output = JSON.parse(msg);
				$('.inbox-content .list-wrapper .list-group').html(output.data);
		 		$('.inbox-content .loader').hide();

 		    if(params.limit >= output.total) {
          $('.counter .loaded').html(output.total);
          $('#load-more-conversations').hide();
        } else {
          $('.counter .loaded').html(params.limit);
          $('#load-more-conversations').show();
        }

					console.log(msg);

					if(msg == 'failed') {


					} else {



					}
		});


});




///////////////////////////////////////////////////////////////////////////////
// load more posts to blog archive page

$(document).on('click', '.load-more-blog-posts #show-more', function(event) {
  event.preventDefault();

  var spinner = $(this).parent().find('.loader-wrap > #ajax-loader');
  var PostList = $('.blog-archive-list');
  var trigger = $(this).parent();
  spinner.show();

  var params = {};

  params.offset = $('.blog-archive-list > .blog-archive-section').length;

  if($('.load-more-blog-posts #show-more').attr('data-tag-id') != undefined) {
  	params.tag = $('.load-more-blog-posts #show-more').attr('data-tag-id');
  }

  console.log(params);

  $.ajax({
    type: 'POST',
    data: ({
      action : 'load_more_blog_posts',
      params: params,
      }),
    async: true,
    url: ogAjax,
  })

  .done(function(msg) {
      var data = JSON.parse(msg);
      PostList.append(data.html);
      spinner.hide();

      if(data.all == 1) {
        trigger.hide();
      }

  })

});

///////////////////////////////////////////////////////////////////////////////
// load more posts to dashboard page

$(document).on('click', '.load-more-blog-posts #show-more-posts-dashboard', function(event) {
  event.preventDefault();

  var spinner = $(this).parent().find('.loader-wrap > #ajax-loader');
  var PostList = $('.blog-archive-list');
  var trigger = $(this).parent();
  spinner.show();

  var params = {};

  params.offset = $('.blog-archive-list > .blog-archive-section').length;

  console.log(params);

  $.ajax({
    type: 'POST',
    data: ({
      action : 'load_more_blog_posts_dashboard',
      params: params,
      }),
    async: true,
    url: ogAjax,
  })

  .done(function(msg) {
      var data = JSON.parse(msg);
      PostList.append(data.html);
      spinner.hide();

      if(data.all == 1) {
        trigger.hide();
      }

  })

});

//////////////////////////////////////////////////////////////////////////////
// Align Plans


function alignExPlans() {

  var maxHeightTitle = -1;
  var maxHeightDesc = -1;

  $('.ex-plan .plan-title').css('height', 'auto');
  $('.ex-plan .plan-description').css('height', 'auto');

  $('.ex-plan > .plan > .aligner >.plan-title').each(function() {
   maxHeightTitle = maxHeightTitle > $(this).outerHeight() ? maxHeightTitle : $(this).outerHeight();
 });

   $('.ex-plan > .plan > .aligner > .plan-description').each(function() {
    maxHeightDesc = maxHeightDesc > $(this).outerHeight() ? maxHeightDesc : $(this).outerHeight();
  });

 $('.ex-plan .plan-title').each(function() {
   $(this).outerHeight(maxHeightTitle);
 });


 $('.ex-plan .plan-description').each(function() {
   $(this).outerHeight(maxHeightDesc);
 });

 //console.log(maxHeightTitle);
 //console.log(maxHeightDesc);
}

// function alignExPlans() {
//   var maxHeight = -1;

//   $('.ex-plan .aligner').css('height', 'auto');

//   $('.ex-plan > .plan > .aligner').each(function() {
//    maxHeight = maxHeight > $(this).outerHeight() ? maxHeight : $(this).outerHeight();
//  });

//  $('.ex-plan .aligner').each(function() {
//    $(this).outerHeight(maxHeight);
//  });
//  //console.log(maxHeight);
// }

alignExPlans();

$(document).ready(function() {
  alignExPlans();
});

var rtime;
var timeout = false;
var delta = 50;
$(window).resize(function() {
    rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, delta);
    }
});

function resizeend() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
    } else {
        timeout = false;
        alignExPlans();
    }
}



///////////////////////////////////////////////////////////////////////////////
// load more posts to dashboard page

$(document).on('click', '.load-more-execution-plans #show-more-execution-plans-dashboard', function(event) {
  event.preventDefault();

  var spinner = $(this).parent().find('.loader-wrap > #ajax-loader');
  var PostList = $('.execution-plans-wrapper');
  var trigger = $(this).parent();
  spinner.show();

  var params = {};

  params.offset = $('.execution-plans-wrapper > .ex-plan').length;

  console.log(params);

  $.ajax({
    type: 'POST',
    data: ({
      action : 'load_more_execution_plans_dashboard',
      params: params,
      }),
    async: true,
    url: ogAjax,
  })

  .done(function(msg) {
      var data = JSON.parse(msg);
      PostList.append(data.html);
      spinner.hide();

      if(data.all == 1) {
        trigger.hide();
      }


      alignExPlans();

  })

});






})( jQuery );
