<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package OpenGo
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">

			<div class="blue-part"></div>
			<div class="content-part">

				<?php while ( have_posts() ) : the_post(); ?>
					<div class="middle-section">
						<section class="page-general  clearfix">

							<div class="page-title-wrapper">
								<h1><?php the_title(); ?></h1>
							</div>
							<article class="page-content-wrapper">
							 	<?php the_content(); ?>


							</article>

						</section><!-- .page-general-->
					</div>
				<?php endwhile; // End of the loop. ?>


			</div>


		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
