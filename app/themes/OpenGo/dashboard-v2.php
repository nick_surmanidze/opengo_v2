<?php
/**
 * Template Name: Dashboard V2
 * Custom template.
 */
get_header();

// check if user matches the job he registered for
$profile = opengo::get_profile();

if(opengo::completed_current_job($profile)) {

	if(isset($_SESSION['gts']['signup-for-job']) && $_SESSION['gts']['signup-for-job'] > 0) {
		global $api;

		$target_match_id = $_SESSION['gts']['signup-for-job'];
		$match_arr = opengo::job_matches_id_array();

		if(in_array($target_match_id, $match_arr)) {

			// then job matches the user who registered for it..

		} else {

			$job = $api->sendRequest(array(
				'action'             => 'read',
				'controller'         => 'job',
				'mark_read'          => false, // or jobs
				'id'                 => $target_match_id,
				'read_by_profile_id' =>  0 // profile id goes here
			));

			if(strlen($job->job->job_title) > 0) {
				$target_job_title = $job->job->job_title;
			} else {
				$target_job_title = '***';
			}

			@$api->sendRequest(array(
				'action'     => 'create',
				'controller' => 'email',
				'type'       => 'opengo_rejected_notification',
				'name'       => stripslashes(sanitize_text_field($_SESSION['nsauth']['user_display_name'])),
				'email'      => $_SESSION['nsauth']['user_email'],
				'job_title'  => $target_job_title
		  ));
		}
		unset($_SESSION['gts']['signup-for-job']);
	}
}

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">

			<div class="blue-part"></div>
			<div class="content-part">

				<?php while ( have_posts() ) : the_post(); ?>

					<div class="middle-section">
						<section class="page-general page-dashboard-v2">

							<div class="dashboard-widget-area-wrapper">
								<div class="widgets">

									<!-- widget -->
									<div class="widget">
										<div class="title">
											<h4>Career Profile</h4>
										</div>
										<div class="content">
											<a target="_blank" href="<?php get_home_url(); ?><?php echo opengo::get_my_cv_url(); ?>" class="btn btn-success btn btn-block ">Review</a>
											<a href="/edit-personal-details" class="btn btn-success btn btn-block">Edit</a>
											<a href="#"  data-toggle="modal" data-target="#share-profile-modal" class="btn btn-success btn btn-block">Share</a>
											<a href="/career-accelerator" class="btn btn-opengo-red btn btn-block">Upgrade to download</a>
										</div>
									</div>
									<!-- Widget - end -->

									<!-- widget -->
									<div class="widget">
										<div class="title">
											<h4>Opportunities</h4>
										</div>
										<div class="content">
											<p class='text-center'>Automatically scan over 25 million live job vacancies.</p>
											<a href="/career-accelerator" class="btn btn-opengo-red btn btn-block">Upgrade to activate</a>
										</div>
									</div>
									<!-- Widget - end -->

								</div>
							</div>

							<div class="dashboard-main-area-wrapper">
								<div class="content">


									<!-- Execution Plans - start -->
									<div class="title">
										<h1>Execution Plans</h1>
										<a  href="<?php get_home_url(); ?>/the-ultimate-career-shortcut" class="btn btn-danger btn-xs pull-right opengo-btn">Upgrade to access all plans</a>
									</div>

									<section class="section row">

										<div class="execution-plans-wrapper">


											<?php

											$featured_plan_id = get_field('featured_execution_plan', 'option');

											$featured_plan_url = '/career-accelerator';
											$regular_plan_url = '/the-ultimate-career-shortcut';

											if($featured_plan_id) {
											$args = array(
												'post_type' => 'execution_plan',
												'posts_per_page' => 1,
												'offset' => 0,
												'post__in' => array($featured_plan_id)
											);

											$my_query = new WP_Query($args);



											// featured execution plan

												?>

												<?php if ( $my_query->have_posts() ) : ?>

													<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>


													<div class="ex-plan">
														<div class="plan">

															<div class="plan-image" onclick="location.href='<?php echo $featured_plan_url; ?>'" style="background: #01a2be url(<?php echo the_post_thumbnail_url(); ?>) no-repeat center center; background-size: cover; cursor: pointer;">
																<span class="shadow-title"><?php the_title(); ?></span>
															</div>
															<div class="aligner">
																<h3 class="plan-title"><a href="<?php echo $featured_plan_url; ?>"><?php the_title(); ?></a></h3>

																<div class="plan-description"><?php strip_tags(get_field('article_intro')); ?></div>
															</div>

															<div class="plan-footer">
																<a href="<?php echo $featured_plan_url; ?>" class="btn btn-opengo-red btn-block">Learn More</a>
															</div>
														</div>
													</div>

												<?php endwhile; ?>

											<?php endif;




											// Now normal execution plans


											 }


											$args = array(
												'post_type' => 'execution_plan',
												'posts_per_page' => 2,
												'offset' => 0,
												'post__not_in' => array($featured_plan_id)
											);

											$my_query = new WP_Query($args);

											?>

												<?php if ( $my_query->have_posts() ) : ?>

													<?php while ( $my_query->have_posts() ) : $my_query->the_post();

													?>




													<div class="ex-plan">
														<div class="plan">

															<div class="plan-image" onclick="location.href='<?php echo $regular_plan_url; ?>'" style="background: #01a2be url(<?php echo the_post_thumbnail_url(); ?>) no-repeat center center; background-size: cover; cursor: pointer;">
																<span class="shadow-title"><?php the_title(); ?></span>
															</div>
															<div class="aligner">
																<h3 class="plan-title"><a href="<?php echo $regular_plan_url; ?>"><?php the_title(); ?></a></h3>

																<div class="plan-description"><?php strip_tags(get_field('article_intro')); ?></div>
															</div>

															<div class="plan-footer">
																<a href="<?php echo $regular_plan_url; ?>" class="btn btn-opengo-red btn-block">Learn More</a>
															</div>
														</div>
													</div>


												<?php


												endwhile; ?>

											<?php endif;

											/**
											 * Check if current plan output is less than total number
											 */
											$current_output = $my_query->post_count;
											$total_published = wp_count_posts('execution_plan')->publish;
											if (($current_output + 1) < $total_published) {
												$show_more_plan_button = 1;
											} else {
												$show_more_plan_button = 0;
											}
											wp_reset_query();
											?>




										</div>

									</section>


								<?php if($show_more_plan_button == 1): ?>

									<div class="load-more-wrapper load-more-execution-plans">
										<a id="show-more-execution-plans-dashboard" href="#">Show More Plans</a>
										<div class="loader-wrap">
											<img id="ajax-loader" style="display: none;" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" alt="loader">
										</div>
									</div>
									<!-- Execution Plans - END -->

								<?php endif; ?>




									<!-- Blog - start -->
									<div class="title">
										<h1>Blog</h1>

									</div>
									<section class="section row blog-row">



										<?php

										// get featured post

										$featured_id = get_field('featured_post', 'option');
									  if($featured_id > 0) {

											$large_image_url = '';
											$large_image_url = get_field('full_width_image', $featured_id);

											?>
											<!-- Featured Post -->
											<div class="full-width-post featured-post">
												<section class="banner" onclick="location.href='<?php echo get_the_permalink($featured_id); ?>'" style="background: #01a2be url(<?php echo $large_image_url; ?>) no-repeat top center; background-size: cover; cursor: pointer;">
											    <div class="container-fluid">
											      <h2><a href="<?php echo get_the_permalink($featured_id); ?>" title=""><small>Featured</small><span><?php echo get_the_title($featured_id); ?></span></a></h2>
											    </div>
											  </section>
											</div>
											<!-- Featured post - END -->

										<?php } ?>




									<?php
									$args = array(
										'posts_per_page' => 3,
										'offset' => 0,
									);

									$my_query = new WP_Query($args);

									?>



										<div class="blog-archive-list">
										<?php if ( $my_query->have_posts() ) : ?>

											<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

												<!-- post snippet - start -->
												<section class="white-section blog-archive-section">
													<div class="container-fluid blog-archive-container">
															<div class="row blog-post">
																	<div class="post-snippet col-md-12">
																		<div class="post-tags">
																			<a href="<?php the_permalink() ?>"  title="Permanent Link to <?php the_title_attribute(); ?>">
																				<?php
																			 $tag_array = array();
																			 $posttags = get_the_tags();
																			 if ($posttags) {
																				 foreach($posttags as $tag) {
																					 array_push($tag_array, $tag->name);
																				 }
																			 }
																			 $tags_out = implode(', ', $tag_array);
																			 echo $tags_out;
																			 ?>
																			</a>
																		</div>
																		<h3 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 image-wrapper">
																				<a href="<?php the_permalink() ?>">
																				<?php if ( has_post_thumbnail() ) : ?>
																								<?php the_post_thumbnail(); ?>
																				<?php endif; ?>
																				</a>
																			</div>

																			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 excerpt-wrapper">
																				<div class="excerpt"><p><?php echo strip_tags(get_field('article_intro')); ?>  <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">read more...</a></p></div>
																				<div class="author">By Ian Hallett</div>
																			</div>
																		</div>

																	</div>
															 </div>

													</div>
												</section>
												<!-- post snippet - end -->

											<?php endwhile; ?>

										<?php endif;

										/**
										 * Check if current plan output is less than total number
										 */
										$current_output = $my_query->post_count;
										$total_published = wp_count_posts()->publish;
										if ($current_output < $total_published) {
											$show_more_post_button = 1;
										} else {
											$show_more_post_button = 0;
										}
										wp_reset_query();
										?>
										</div>


										<?php if($show_more_post_button == 1): ?>

										<div class="load-more-wrapper load-more-blog-posts">
											<a id="show-more-posts-dashboard" href="#">Show More Posts</a>
											<div class="loader-wrap">
												<img id="ajax-loader" style="display: none;" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" alt="loader">
											</div>
										</div>

										<?php endif; ?>


										<!-- Popular Posts -->
										<div class="popular-posts">
											<h1>Most Popular Posts</h1>


											<?php

											$args = array(
												'posts_per_page' => 2,
												'meta_key' => 'post_view_count',
												'orderby' => 'meta_value_num',
												'order' => 'desc',
											);
											$my_query = new WP_Query($args);
											if( $my_query->have_posts() ) :
											  while ($my_query->have_posts()) : $my_query->the_post();

												$large_image_url = '';
												$large_image_url = get_field('full_width_image');
											 ?>

												<section class="banner" onclick="location.href='<?php the_permalink(); ?>'" style="background: #01a2be url(<?php echo $large_image_url; ?>) no-repeat top center; background-size: cover; cursor: pointer;">
													<div class="container-fluid clearfix">
														<h2><a href="<?php the_permalink(); ?>" title=""><span><?php the_title(); ?></span></a></h2>
													</div>
												</section>
											<?php
												endwhile;
											endif;
											wp_reset_query();
											?>

										</div>
										<!-- Popular Posts - end -->


									</section>



									<!-- Blog - END -->

								</div>
							</div>


						</section><!-- .page-general-->
					</div>
				<?php endwhile; // End of the loop. ?>


			</div>


		</main><!-- #main -->
	</div><!-- #primary -->

  <!-- Modal Begin -->
    <div class="modal fade share-profile-modal" id="share-profile-modal" tabindex="-1" role="dialog" aria-labelledby="share-profile-modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="share-profile-modal">Share profile</h4>
          </div>
          <div class="modal-body clearfix share-profile">
          	<div class="lead-text">
          		Share your online profile via email or social media.
          	</div>
						<div class="button-wrapper">

							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-12">
									<?php

										$share_text = 'Check out my career profile that I created using OpenGo: ' . home_url() . opengo::get_my_cv_url();
										$share_url = home_url() . opengo::get_my_cv_url();


									 ?>
										<a href="mailto:?subject=My career profile&body=<?php echo htmlentities(opengo::generate_email_sharing_body()); ?>" class="btn btn-success btn-lg btn-block"><i class="fa fa-envelope" aria-hidden="true"></i>Share Via Email</a>
									</div>
									<div class="col-xs-12 col-sm-4">
										<a  href="http://www.facebook.com/sharer.php?u=<?php echo $share_url; ?>" target="_blank" class="btn btn-default"><i class="fa fa-facebook" aria-hidden="true"></i>Share via Facebook</a>
									</div>
									<div class="col-xs-12 col-sm-4">
										<a  href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $share_url; ?>" target="_blank" class="btn btn-default"><i class="fa fa-linkedin" aria-hidden="true"></i>Share via Linkedin</a>
									</div>
									<div class="col-xs-12 col-sm-4">
										<a  href="https://twitter.com/intent/tweet?text=<?php echo htmlentities($share_text); ?>" target="_blank" class="btn btn-default"><i class="fa fa-twitter" aria-hidden="true"></i>Share via Twitter</a>
									</div>
								</div>

							</div>

						</div>


          </div>
          <div class="modal-body clearfix share-file-attachment">
          	<div class="lead-text">
          		Share your profile as a PDF or Word file as an email attachment.
          	</div>

						<div class="button-wrapper">

							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-12 col-sm-6">
										<a href="/career-accelerator" class="btn btn-opengo-red">Upgrade to share as PDF<i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
									</div>
									<div class="col-xs-12 col-sm-6">
										<a  href="/career-accelerator" class="btn btn-opengo-red">Upgrade to share as Word<i class="fa fa-file-word-o" aria-hidden="true"></i></a>
									</div>
								</div>

							</div>

						</div>


          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal END -->

<?php get_footer(); ?>
