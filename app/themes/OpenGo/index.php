<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package OpenGo
 */


get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

	<?php /*

	// get featured post

	$featured_id = get_field('featured_post', 'option');
  if($featured_id > 0) {

		$large_image_url = '';
		$large_image_url = get_field('full_width_image', $featured_id);

		?>
		<!-- Featured Post -->
		<div class="full-width-post featured-post">
			<section class="banner" onclick="location.href='<?php echo get_the_permalink($featured_id); ?>'" style="background: #01a2be url(<?php echo $large_image_url; ?>) no-repeat center center; background-size: cover; cursor: pointer;">
		    <div class="container">
		      <h2><a href="<?php echo get_the_permalink($featured_id); ?>" title=""><small>Featured</small><span><?php echo get_the_title($featured_id); ?></span></a></h2>
		    </div>
		  </section>
		</div>
		<!-- Featured post - END -->

	<?php } */ ?>



	<div class="blog-archive-list">
	<?php if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- post snippet - start -->
			<section class="white-section blog-archive-section">
				<div class="container blog-archive-container">
						<div class="row blog-post">
								<div class="post-snippet col-md-12">
									<div class="post-tags">
										<?php
									 $tag_array = array();
									 $posttags = get_the_tags();
									 if ($posttags) {
										 foreach($posttags as $tag) {
										 $tlink = '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>';
										 	array_push($tag_array, $tlink );
										  }
									 }
									 $tags_out = implode(', ', $tag_array);
									 echo $tags_out;
									 ?>
									</div>
									<h3 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 image-wrapper">
											<a href="<?php the_permalink() ?>">
											<?php if ( has_post_thumbnail() ) : ?>
															<?php the_post_thumbnail(); ?>
											<?php endif; ?>
											</a>
										</div>

										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 excerpt-wrapper">
											<div class="excerpt"><p><?php echo strip_tags(get_field('article_intro')); ?>  <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">read more...</a></p></div>
											<div class="author">By Ian Hallett</div>
										</div>
									</div>

								</div>
						 </div>

				</div>
			</section>
			<!-- post snippet - end -->

		<?php endwhile; ?>

	<?php endif; ?>
	</div>


<?php

/**
 * Check if current post output is less than total number
 */
$current_output = $wp_query->post_count;
$total_published = wp_count_posts()->publish;
if ($current_output < $total_published) {
?>

<div class="load-more-wrapper load-more-blog-posts">
	<a id="show-more" href="#">Show More Posts</a>
	<div class="loader-wrap">
		<img id="ajax-loader" style="display: none;" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" alt="loader">
	</div>
</div>

<?php } ?>



	<!-- Popular Posts -->
	<div class="popular-posts">
		<h1>Most Popular Posts</h1>


		<?php

		$args = array(
			'posts_per_page' => 2,
			'meta_key' => 'post_view_count',
			'orderby' => 'meta_value_num',
			'order' => 'desc',
		);
		$my_query = new WP_Query($args);
		if( $my_query->have_posts() ) :
		  while ($my_query->have_posts()) : $my_query->the_post();

			$large_image_url = '';
			$large_image_url = get_field('full_width_image');
		 ?>

			<section class="banner" onclick="location.href='<?php the_permalink(); ?>'" style="background: #01a2be url(<?php echo $large_image_url; ?>) no-repeat center center; background-size: cover; cursor: pointer;">
				<div class="container">
					<h2><a href="<?php the_permalink(); ?>" title=""><span><?php the_title(); ?></span></a></h2>
				</div>
			</section>
		<?php
			endwhile;
		endif;
		wp_reset_query();
		?>

	</div>
	<!-- Popular Posts - end -->



	</main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
