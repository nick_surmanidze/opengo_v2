<?php
/**
 * Template Name: Edit Settings
 * Custom template.
 */
get_header();


// get email frequency from the core

// default
$talent_email_frequency = 'daily';

$apicaller = new ApiCaller(OG_APP_ID, OG_APP_KEY, OG_API_URL);

$email_frequency = $apicaller->sendRequest(array(
    'action'       => 'read',
    'controller'   => 'meta',
    'data_type'    => 'user', // can be job, profile or user
    'data_id'      => opengo::user_logged_in(), // read by profile id -> output all meta as an array
    'meta_key'     => 'talent_email_frequency', // read by meta id
));

if($email_frequency == 'weekly' || $email_frequency == 'none' ) {
	$talent_email_frequency = $email_frequency;
}

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general editor clearfix">

						<div class="page-title-wrapper">
							<h1>Editor</h1>
						</div>
						<article class="page-content-wrapper editor-wrapper">

							<div class="sidebar">
								<?php get_template_part('editor/editor-left-sidebar'); ?>
							</div>

							<div class="editor-content clearfix settings-page" ng-cloak ng-app="ogSettingsApp" ng-controller="settingsController">
										<h2 class="fields-group-title">
											Edit settings
										</h2>
								<div class="editor-flex">

									<div class="hint">
											<div class="bg-info og-info"><?php the_field('hint_settings', 'option'); ?></div>
									</div>

									<div class="fields-wrapper">

										<form class  ="form-horizontal">


										<form class  ="form-horizontal" ng-cloak>



											<div class="form-group">
												<label class="col-sm-4 control-label">Display Name *</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="name" placeholder="Display Name" ng-model="display_name" value="<?php echo stripslashes($_SESSION['nsauth']['user_display_name']); ?>">
												</div>
												<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(display_name) || display_name.length > 40) &amp;&amp; display_name.length > 0">Display Name can contain minimum 2 and maximum 40 characters and should not contain any special symbols.</div>
											</div>

                    <div class="form-group">
											<div class="col-sm-8  col-sm-offset-4 text-left">

												<a target="_blank" href="<?php echo get_home_url(); ?><?php echo opengo::get_my_cv_url(); ?>" class=""><strong><?php echo get_home_url(); ?><?php echo opengo::get_my_cv_url(); ?></strong></a>

											</div>
										</div>


                    <div class="form-group">
											<label class="col-sm-4 control-label">Your Profile URL</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="slug" placeholder="Handle" value="<?php echo $_SESSION['nsauth']['user_profile']->slug; ?>">
											</div>
										</div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email Frequency </label>
                      <div class="col-sm-8">
                        <select class="multiselect-single" style="visibility:hidden;" title="Email Frequency" id='email-frequency'>
                            <option value="daily" <?php if($talent_email_frequency == 'daily') {echo 'selected';}?>>Daily</option>
                            <option value="weekly" <?php if($talent_email_frequency == 'weekly') {echo 'selected';}?>>Weekly</option>
                            <option value="none" <?php if($talent_email_frequency == 'none') {echo 'selected';}?>>No Emails</option>
                        </select>
                      </div>
                    </div>


										<div class="bottom">

											<span id="save-btn" class="btn btn-primary btn-sm save-btn" ng-click="updateDN()" ng-show="display_name.length > 0" ng-cloak><i class="fa fa-check-circle-o"></i> Save</span>

											<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"> Updating..</span>
											<span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
											<span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
										</div>

										</form>

										<hr>
										<div class="delete-profile-wrapper">


											<div class="btn-group delete-profile">
											  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click=""  data-toggle="modal" data-target="#delete-account-modal">
											    <i class="fa fa-trash-o"></i> Delete Profile
											  </button>
											</div>


										</div>
										</form>

									</div>

								</div>

                  <!-- Modal Begin -->
                    <div class="modal fade delete-account-modal" id="delete-account-modal" tabindex="-1" role="dialog" aria-labelledby="delete-account-modal">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="delete-account-modal">Delete my profile</h4>
                          </div>
                          <div class="modal-body clearfix">

                            <div style="display: inline-block;padding: 40px 15px;text-align: center;width: 100%;font-size: 16px;font-weight: 500;">
                              Are you sure? You cannot reverse this action.
                            </div>
                            <div style="display:inline-block; width:100%; text-align: center; padding:15px;">
                              <button class="btn btn-danger" ng-click="deleteProfile()">Delete</button>
                              <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>

                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    <!-- Modal END -->

							</div>




						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
