<?php

$profile = opengo::get_profile();

$complete_label = '<span class="label label-success">complete</span>';
$incomplete_label = '<span class="label label-warning">incomplete</span>';
$percentage = 0;


if(opengo::completed_personal($profile) == true) {
	$completed_personal_label = $complete_label;
	$percentage = $percentage + 15;
} else {
	$completed_personal_label = $incomplete_label;
}


if(opengo::completed_ideal_job($profile) == true) {
	$completed_ideal_job_label = $complete_label;
	$percentage = $percentage + 20;
} else {
	$completed_ideal_job_label = $incomplete_label;
}


if(opengo::completed_current_job($profile) == true) {
	$completed_current_job_label = $complete_label;
	$percentage = $percentage + 15;
} else {
	$completed_current_job_label = $incomplete_label;
}


if(opengo::completed_experience_one($profile) == true) {
	$completed_experience_label = $complete_label;
	$percentage = $percentage + 15;
} else {
	$completed_experience_label = $incomplete_label;
}


if(opengo::completed_education_one($profile) == true) {
	$completed_education_label = $complete_label;
	$percentage = $percentage + 20;
} else {
	$completed_education_label = $incomplete_label;
}

if(opengo::completed_skills($profile) == true) {
	$completed_skills_label = $complete_label;
	$percentage = $percentage + 15;
} else {
	$completed_skills_label = $incomplete_label;
}


$profile_progress_label = "Basic";
if($percentage > 20) {
	$profile_progress_label = "Weak";
}
if($percentage > 40) {
	$profile_progress_label = "Weak";
}
if($percentage > 60) {
	$profile_progress_label = "Average";
}
if($percentage > 80) {
	$profile_progress_label = "Excellent";
}
if($percentage > 90) {
	$profile_progress_label = "Ultimate";
}





?>
<div class="list-group">
  <a href ="<?php get_home_url(); ?>/dashboard" class="list-group-item">
		<i class="fa fa-tachometer"></i>Dashboard
	</a>

	<a href ="<?php get_home_url(); ?>/edit-personal-details" class="list-group-item <?php if_is_page_echo('edit-personal-details', 'active'); ?>">
		<i class="fa fa-user"></i> Personal<?php echo $completed_personal_label; ?>
	</a>

	<a href ="<?php get_home_url(); ?>/edit-ideal-job" class="list-group-item <?php if_is_page_echo('edit-ideal-job', 'active'); ?>">
		<i class="fa fa-magic"></i>Ideal Job<?php echo $completed_ideal_job_label; ?>
	</a>

	<a href ="<?php get_home_url(); ?>/edit-current-job" class="list-group-item <?php if_is_page_echo('edit-current-job', 'active'); ?>">
		<i class="fa fa-briefcase"></i>Current Job<?php echo $completed_current_job_label; ?>
	</a>

	<a href ="<?php get_home_url(); ?>/edit-previous-experience" class="list-group-item <?php if_is_page_echo('edit-previous-experience', 'active'); ?>">
		<i class="fa fa-history"></i>Experience<?php echo $completed_experience_label; ?>
	</a>

	<a href ="<?php get_home_url(); ?>/edit-education" class="list-group-item <?php if_is_page_echo('edit-education', 'active'); ?>">
		<i class="fa fa-graduation-cap"></i>Education<?php echo $completed_education_label; ?>
	</a>

	<a href ="<?php get_home_url(); ?>/edit-skills" class="list-group-item <?php if_is_page_echo('edit-skills', 'active'); ?>">
		<i class="fa fa-cube"></i>Skills<?php echo $completed_skills_label; ?>
	</a>

	 <a href ="<?php get_home_url(); ?>/settings" class="list-group-item <?php if_is_page_echo('settings', 'active'); ?>">
		<i class="fa fa-cog"></i>Settings
	</a>
</div>

<div class="profile-completion-widget">

	<h4>Profile Completion</h4>
	<h6><?php echo $profile_progress_label; ?></h6>
	<div class="progress">
	  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentage; ?>%">
	    <span><?php echo $percentage; ?>%</span>
	  </div>
	</div>
	<div class="bg-info og-info"><?php the_field('hint_profile_completion_widget', 'option'); ?></div>

</div>




