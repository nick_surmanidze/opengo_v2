<?php
/**
 * Template Name: Edit Ideal Job
 * Custom template.
 */
get_header();

$profile = opengo::get_profile();

$ideal = array(
	'function'      => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_function),
	'seniority'     => opengo::check_selected($profile->ideal_job->ideal_job_seniority),
	'industry'      => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_industry),
	'location'      => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_location),
	'company_type'  => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_company_type),
	'contract_type' => opengo::check_selected($profile->ideal_job->ideal_job_contract_type),
	'salary'        => opengo::check_selected($profile->ideal_job->ideal_job_basic_salary),
	'currency'      => opengo::check_selected($profile->ideal_job->ideal_job_basic_salary_currency)
	);
$Public_preview_url = get_home_url() . opengo::get_my_cv_url();
$public_preview_btn = "<a class='public-preview-btn btn btn-default btn-sm' href=" . $Public_preview_url . " target='_blank'><i class='fa fa-file-text-o'></i>Public CV</a>";

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general editor clearfix">

						<div class="page-title-wrapper">
							<h1>Editor</h1>
						</div>
						<article class="page-content-wrapper editor-wrapper">

							<div class="sidebar">
								<?php get_template_part('editor/editor-left-sidebar'); ?>
							</div>

							<!-- NG-APP -->
							<div id="editor-ideal-job" class="editor-content clearfix" ng-app="ogIdealJobApp" ng-controller="idealJobController">
										<h2 class="fields-group-title">
											Edit ideal job
										</h2>
								<div class="editor-flex">

									<div class="hint">
											<div class="bg-info og-info"><?php the_field('hint_ideal_job', 'option'); ?></div>
									</div>

									<div class="fields-wrapper">
										<div class="splash" ng-cloak>

											<div class="preloader">
												<div class="cssload-thecube">
													<div class="cssload-cube cssload-c1"></div>
													<div class="cssload-cube cssload-c2"></div>
													<div class="cssload-cube cssload-c4"></div>
													<div class="cssload-cube cssload-c3"></div>
												</div>
											</div>

								      <h3>LOADING</h3>
								    </div>

										<form class  ="form-horizontal" ng-cloak>


											<div class="form-group">
												<label class="col-sm-4 control-label">Job function</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_job_function">
													<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), $ideal['function'], true, false); ?>
													</select>
												</div>
											</div>


											<div class="form-group">
												<label class="col-sm-4 control-label">Seniority</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="seniority" class="multiselect" style="visibility:hidden;" ng-model="ideal_job_seniority">
													<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), $ideal['seniority']); ?>
													</select>
												</div>
											</div>



											<div class="form-group">
												<label class="col-sm-4 control-label">Industry</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="industry" class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_job_industry">
														<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), $ideal['industry'], true, false, 'Any'); ?>
													</select>
												</div>
											</div>



											<div class="form-group">
												<label class="col-sm-4 control-label">Location</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="location" class="multiselect-limit-three" style="visibility:hidden;"  multiple="multiple" ng-model="ideal_job_location">
													<?php echo list_options_from_textarea(get_field('pdl_ideal_job_location', 'option'), $ideal['location'], true, false, 'Any'); ?>
													</select>
												</div>
											</div>


											<div class="form-group multiselect-wrapper">
												<label class="col-sm-4 control-label">Company type</label>
												<div class="col-sm-8">
													<select id="company_type" class="multiselect-limit-three" style="visibility:hidden;" ng-model="ideal_job_company_type" multiple='multiple'>
													<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), $ideal['company_type'], true, false, 'Any'); ?>
													</select>
												</div>
											</div>



											<div class="form-group">
												<label class="col-sm-4 control-label">Contract type</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="contract_type" class="multiselect" style="visibility:hidden;" ng-model="ideal_job_contract">
													<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), $ideal['contract_type'], false, true, 'Any'); ?>
													</select>
												</div>
											</div>


											<div class="form-group">
												<label class="col-sm-4 control-label">Basic salary</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="basic_salary" class="multiselect" style="visibility:hidden;" ng-model="ideal_job_salary">
													<?php echo list_options_from_textarea(get_field('pdl_salary', 'option'), $ideal['salary']); ?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Currency</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="currency" class="multiselect" style="visibility:hidden;" ng-model="ideal_job_currency">
													<?php echo list_options_from_textarea_currency(get_field('pdl_currency', 'option'), $ideal['currency']); ?>
													</select>
												</div>
											</div>

										</form>

									</div>

								</div>

								<div class="bottom">
									<span id="save-btn" class="btn btn-primary btn-sm save-btn" ng-click="update()" ng-cloak><i class="fa fa-check-circle-o"></i> Update</span>
									<?php  echo $public_preview_btn; ?>
									<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"> Updating..</span>
									<span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
									<span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
								</div>
							</div>

						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
