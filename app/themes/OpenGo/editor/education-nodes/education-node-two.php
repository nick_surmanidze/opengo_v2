	<?php
	global $education_2;
	?>

	<div id="education-node-two"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedTwo]">
	<span class="label label-warning unsaved-changes">unsaved changed</span>
	  <div class="rendered" ng-cloak ng-click="toggleBoolTwo()">

	    <h5 class="list-group-item-heading">

		    <span ng-cloak ng-show="universityTwo &amp;&amp; universityTwo != 'Other'">{{ universityTwo }}</span>

		    <span ng-cloak ng-show="university_nameTwo &amp;&amp; universityTwo == 'Other'">{{ university_nameTwo }}</span>

		    <span class="ng-placeholder" ng-cloak ng-show="!universityTwo">University</span>
	    </h5>


		  <span ng-cloak ng-show="course_nameTwo &amp;&amp; course_nameTwo != 'Other'">{{ course_nameTwo }}</span>
			<span ng-cloak ng-show="your_course_nameTwo &amp;&amp; course_nameTwo == 'Other'">{{ your_course_nameTwo }}</span>

	    <span class="ng-placeholder" ng-cloak ng-show="!course_nameTwo">Course name</span>

	    <span>({{ qualificationTwo }}<span class="ng-placeholder" ng-cloak ng-show="!qualificationTwo">qualification</span>)</span><br>

			<span>Grade Attained: {{ gradeTwo }}<span class="ng-placeholder" ng-cloak ng-show="!gradeTwo">----</span></span><br>
			<small>
				<span>Awarded in {{ yearTwo }}<span class="ng-placeholder" ng-cloak ng-show="!yearTwo">----</span></span>
			</small>

	  </div>

	<div class="editor-form-wrapper"  ng-show="expandedTwo">

	<div class="hint">
		<div class="bg-info og-info"><?php the_field('hint_education', 'option'); ?></div>
	</div>

	<div class="fields-wrapper">

		<form class  ="form-horizontal" name="formTwo">

			<!-- University -->
			<div class="form-group bootstrap-select-wrapper">
				<label class="col-sm-4 control-label">University*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect-filter" style="visibility:hidden;" ng-model="universityTwo">
					<?php echo list_options_from_textarea(get_field('pdl_university', 'option'), $education_2['university'], false, false, $education_2['university']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !universityTwo">This field is required.</div>
			</div>

			<!-- displayed dynamically -->
			<div class="form-group" ng-cloak ng-show="universityTwo =='Other'">
				<label class="col-sm-4 control-label"></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" placeholder="Your university name" ng-model="university_nameTwo"  value="<?php echo  $education_2['university']; ?>">
				</div>

				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(university_nameTwo) || university_nameTwo.length > 100) &amp;&amp; university_nameTwo.length > 0">University name can contain up to 100 characters and should not contain any special symbols.</div>

				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !university_nameTwo">This field is required.</div>
			</div>

			<!-- Course topic -->
			<div class="form-group">
				<label class="col-sm-4 control-label">Course topic*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect" style="visibility:hidden;" ng-model="course_nameTwo">
					<?php echo list_options_from_textarea(get_field('pdl_course_name', 'option'), $education_2['course_name'], false, false, $education_2['course_name']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !course_nameTwo">This field is required.</div>
			</div>

			<!-- displayed dynamically -->
			<div class="form-group" ng-cloak ng-show="course_nameTwo =='Other'">
				<label class="col-sm-4 control-label"></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" placeholder="Your course name" ng-model="your_course_nameTwo"  value="<?php echo  $education_2['course_name']; ?>">
				</div>

				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(your_course_nameTwo) || your_course_nameTwo.length > 100) &amp;&amp; your_course_nameTwo.length > 0">Course name can contain up to 100 characters and should not contain any special symbols.</div>

				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !your_course_nameTwo">This field is required.</div>
			</div>

			<!-- Qualification -->
			<div class="form-group">
				<label class="col-sm-4 control-label">Qualification type*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect" style="visibility:hidden;" ng-model="qualificationTwo">
					<?php echo list_options_from_textarea(get_field('pdl_qualification', 'option'), $education_2['qualification']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !qualificationTwo">This field is required.</div>
			</div>

			<!-- Grade -->
			<div class="form-group">
				<label class="col-sm-4 control-label">Grade attained*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect" style="visibility:hidden;" ng-model="gradeTwo">
					<?php echo list_options_from_textarea(get_field('pdl_grade', 'option'), $education_2['grade']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !gradeTwo">This field is required.</div>
			</div>
			 <!-- Year Awarded -->
			<div class="form-group">
				<label class="col-sm-4 control-label">Year attained*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect" style="visibility:hidden;" ng-model="yearTwo">
					<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $education_2['year']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !yearTwo">This field is required.</div>
			</div>

		</form>


		<div class="bottom">

			<div class="btn-group">
			  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
			    <li><a ng-click="deleteNode(2);">Delete</a></li>
			    <li role="separator" class="divider"></li>
			    <li><a >cancel</a></li>
			  </ul>
			</div>
			<div class="og-footer-error errors-above" ng-cloak ng-show="!validTwo">Please correct errors above</div>
		</div>
		</div>

	</div>

	</div>
