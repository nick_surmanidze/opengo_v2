<?php
global $education_3;
?>


<div id="education-node-three"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedThree]">
<span class="label label-warning unsaved-changes">unsaved changed</span>
  <div class="rendered" ng-cloak ng-click="toggleBoolThree()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="universityThree &amp;&amp; universityThree != 'Other'">{{ universityThree }}</span>

	    <span ng-cloak ng-show="university_nameThree &amp;&amp; universityThree == 'Other'">{{ university_nameThree }}</span>

	    <span class="ng-placeholder" ng-cloak ng-show="!universityThree">University</span>
    </h5>


	  <span ng-cloak ng-show="course_nameThree &amp;&amp; course_nameThree != 'Other'">{{ course_nameThree }}</span>
		<span ng-cloak ng-show="your_course_nameThree &amp;&amp; course_nameThree == 'Other'">{{ your_course_nameThree }}</span>

    <span class="ng-placeholder" ng-cloak ng-show="!course_nameThree">Course name</span>

    <span>({{ qualificationThree }}<span class="ng-placeholder" ng-cloak ng-show="!qualificationThree">qualification</span>)</span><br>

		<span>Grade Attained: {{ gradeThree }}<span class="ng-placeholder" ng-cloak ng-show="!gradeThree">----</span></span><br>
		<small>
			<span>Awarded in {{ yearThree }}<span class="ng-placeholder" ng-cloak ng-show="!yearThree">----</span></span>
		</small>

  </div>

<div class="editor-form-wrapper"  ng-show="expandedThree">

<div class="hint">
	<div class="bg-info og-info"><?php the_field('hint_education', 'option'); ?></div>
</div>

<div class="fields-wrapper">

	<form class  ="form-horizontal" name="formThree">

		<!-- University -->
		<div class="form-group bootstrap-select-wrapper">
			<label class="col-sm-4 control-label">University*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect-filter" style="visibility:hidden;" ng-model="universityThree">
				<?php echo list_options_from_textarea(get_field('pdl_university', 'option'), $education_3['university'], false, false, $education_3['university']); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !universityThree">This field is required.</div>
		</div>

		<!-- displayed dynamically -->
		<div class="form-group" ng-cloak ng-show="universityThree =='Other'">
			<label class="col-sm-4 control-label"></label>
			<div class="col-sm-8">
				<input type="text" class="form-control" placeholder="Your university name" ng-model="university_nameThree"  value="<?php echo  $education_3['university']; ?>">
			</div>

			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(university_nameThree) || university_nameThree.length > 100) &amp;&amp; university_nameThree.length > 0">University name can contain up to 100 characters and should not contain any special symbols.</div>

			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !university_nameThree">This field is required.</div>
		</div>

		<!-- Course topic -->
		<div class="form-group">
			<label class="col-sm-4 control-label">Course topic*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect" style="visibility:hidden;" ng-model="course_nameThree">
				<?php echo list_options_from_textarea(get_field('pdl_course_name', 'option'), $education_3['course_name'], false, false, $education_3['course_name']); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !course_nameThree">This field is required.</div>
		</div>

		<!-- displayed dynamically -->
		<div class="form-group" ng-cloak ng-show="course_nameThree =='Other'">
			<label class="col-sm-4 control-label"></label>
			<div class="col-sm-8">
				<input type="text" class="form-control" placeholder="Your course name" ng-model="your_course_nameThree"  value="<?php echo  $education_3['course_name']; ?>">
			</div>

			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(your_course_nameThree) || your_course_nameThree.length > 100) &amp;&amp; your_course_nameThree.length > 0">Course name can contain up to 100 characters and should not contain any special symbols.</div>

			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !your_course_nameThree">This field is required.</div>
		</div>

		<!-- Qualification -->
		<div class="form-group">
			<label class="col-sm-4 control-label">Qualification type*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect" style="visibility:hidden;" ng-model="qualificationThree">
				<?php echo list_options_from_textarea(get_field('pdl_qualification', 'option'), $education_3['qualification']); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !qualificationThree">This field is required.</div>
		</div>

		<!-- Grade -->
		<div class="form-group">
			<label class="col-sm-4 control-label">Grade attained*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect" style="visibility:hidden;" ng-model="gradeThree">
				<?php echo list_options_from_textarea(get_field('pdl_grade', 'option'), $education_3['grade']); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !gradeThree">This field is required.</div>
		</div>
		 <!-- Year Awarded -->
		<div class="form-group">
			<label class="col-sm-4 control-label">Year attained*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect" style="visibility:hidden;" ng-model="yearThree">
				<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $education_3['year']); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !yearThree">This field is required.</div>
		</div>

	</form>


	<div class="bottom">
		<div class="btn-group">
		  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu">
		    <li><a ng-click="deleteNode(3);">Delete</a></li>
		    <li role="separator" class="divider"></li>
		    <li><a >cancel</a></li>
		  </ul>
		</div>
		<div class="og-footer-error errors-above" ng-cloak ng-show="!validThree">Please correct errors above</div>
	</div>
	</div>

</div>

</div>
