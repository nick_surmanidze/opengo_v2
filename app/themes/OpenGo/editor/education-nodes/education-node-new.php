<div class="add-new-placeholder" ng-show="!addNewActive" ng-cloak>
	<h4> Add New Education Entry </h4>
	<button class="btn btn-success" ng-click="addNewActive = true"><i class="fa fa-check-circle"></i> Add New </button>
</div>

<div id="education-node-new"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedNew]" ng-show="addNewActive">
  <span class="label label-success create-new-label">New</span>
  <button class="btn btn-default cancel-btn" ng-click="addNewActive = false">cancel</button>
  <div class="rendered" ng-cloak ng-click="toggleBoolNew()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="universityNew &amp;&amp; universityNew != 'Other'">{{ universityNew }}</span>

	    <span ng-cloak ng-show="university_nameNew &amp;&amp; universityNew == 'Other'">{{ university_nameNew }}</span>

	    <span class="ng-placeholder" ng-cloak ng-show="!universityNew">University</span>
    </h5>


	  <span ng-cloak ng-show="course_nameNew &amp;&amp; course_nameNew != 'Other'">{{ course_nameNew }}</span>
		<span ng-cloak ng-show="your_course_nameNew &amp;&amp; course_nameNew == 'Other'">{{ your_course_nameNew }}</span>

    <span class="ng-placeholder" ng-cloak ng-show="!course_nameNew">Course name</span>

    <span>({{ qualificationNew }}<span class="ng-placeholder" ng-cloak ng-show="!qualificationNew">qualification</span>)</span><br>

		<span>Grade Attained: {{ gradeNew }}<span class="ng-placeholder" ng-cloak ng-show="!gradeNew">----</span></span><br>
		<small>
			<span>Awarded in {{ yearNew }}<span class="ng-placeholder" ng-cloak ng-show="!yearNew">----</span></span>
		</small>

  </div>

<div class="editor-form-wrapper" ng-show="expandedNew">

<div class="hint">
	<div class="bg-info og-info"><?php the_field('hint_education', 'option'); ?></div>
</div>

<div class="fields-wrapper">

	<form class  ="form-horizontal" name="formnew">

		<!-- University -->
		<div class="form-group bootstrap-select-wrapper">
			<label class="col-sm-4 control-label">University*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect-filter" style="visibility:hidden;" ng-model="universityNew">
				<?php echo list_options_from_textarea(get_field('pdl_university', 'option'), get_current_profile_value("country")); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !universityNew">This field is required.</div>
		</div>

		<!-- displayed dynamically -->
		<div class="form-group" ng-cloak ng-show="universityNew =='Other'">
			<label class="col-sm-4 control-label"></label>
			<div class="col-sm-8">
				<input type="text" class="form-control" placeholder="Your university name" ng-model="university_nameNew">
			</div>

			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(university_nameNew) || university_nameNew.length > 100) &amp;&amp; university_nameNew.length > 0">University name can contain up to 100 characters and should not contain any special symbols.</div>

			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !university_nameNew">This field is required.</div>
		</div>

		<!-- Course topic -->
		<div class="form-group">
			<label class="col-sm-4 control-label">Course topic*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect" style="visibility:hidden;" ng-model="course_nameNew">
				<?php echo list_options_from_textarea(get_field('pdl_course_name', 'option'), get_current_profile_value("country")); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !course_nameNew">This field is required.</div>
		</div>

		<!-- displayed dynamically -->
		<div class="form-group" ng-cloak ng-show="course_nameNew =='Other'">
			<label class="col-sm-4 control-label"></label>
			<div class="col-sm-8">
				<input type="text" class="form-control" placeholder="Your course name" ng-model="your_course_nameNew">
			</div>

			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(your_course_nameNew) || your_course_nameNew.length > 100) &amp;&amp; your_course_nameNew.length > 0">Course name can contain up to 100 characters and should not contain any special symbols.</div>

			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !your_course_nameNew">This field is required.</div>
		</div>

		<!-- Qualification -->
		<div class="form-group">
			<label class="col-sm-4 control-label">Qualification type*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect" style="visibility:hidden;" ng-model="qualificationNew">
				<?php echo list_options_from_textarea(get_field('pdl_qualification', 'option'), get_current_profile_value("country")); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !qualificationNew">This field is required.</div>
		</div>

		<!-- Grade -->
		<div class="form-group">
			<label class="col-sm-4 control-label">Grade attained*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect" style="visibility:hidden;" ng-model="gradeNew">
				<?php echo list_options_from_textarea(get_field('pdl_grade', 'option'), get_current_profile_value("country")); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !gradeNew">This field is required.</div>
		</div>
		 <!-- Year Awarded -->
		<div class="form-group">
			<label class="col-sm-4 control-label">Year attained*</label>
			<div class="col-sm-8 multiselect-wrapper">
				<select class="multiselect" style="visibility:hidden;" ng-model="yearNew">
				<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), get_current_profile_value("country")); ?>
				</select>
			</div>
			<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !yearNew">This field is required.</div>
		</div>

	</form>

	</div>

</div>

</div>
