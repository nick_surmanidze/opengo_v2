<?php
global $education_1;
?>

<div id="education-node-one"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedOne]">
<span class="label label-warning unsaved-changes">unsaved changed</span>
  <div class="rendered" ng-cloak ng-click="toggleBoolOne()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="universityOne &amp;&amp; universityOne != 'Other'">{{ universityOne }}</span>

	    <span ng-cloak ng-show="university_nameOne &amp;&amp; universityOne == 'Other'">{{ university_nameOne }}</span>

	    <span class="ng-placeholder" ng-cloak ng-show="!universityOne">University</span>
    </h5>


	  <span ng-cloak ng-show="course_nameOne &amp;&amp; course_nameOne != 'Other'">{{ course_nameOne }}</span>
		<span ng-cloak ng-show="your_course_nameOne &amp;&amp; course_nameOne == 'Other'">{{ your_course_nameOne }}</span>

    <span class="ng-placeholder" ng-cloak ng-show="!course_nameOne">Course name</span>

    <span>({{ qualificationOne }}<span class="ng-placeholder" ng-cloak ng-show="!qualificationOne">qualification</span>)</span><br>

		<span>Grade Attained: {{ gradeOne }}<span class="ng-placeholder" ng-cloak ng-show="!gradeOne">----</span></span><br>
		<small>
			<span>Awarded in {{ yearOne }}<span class="ng-placeholder" ng-cloak ng-show="!yearOne">----</span></span>
		</small>

  </div>

<div class="editor-form-wrapper" ng-show="expandedOne">

	<div class="hint">
		<div class="bg-info og-info"><?php the_field('hint_education', 'option'); ?></div>
	</div>

	<div class="fields-wrapper">

		<form class="form-horizontal" name="formOne">

			<!-- University -->
			<div class="form-group bootstrap-select-wrapper">
				<label class="col-sm-4 control-label">University*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect-filter" style="visibility:hidden;" ng-model="universityOne">
					<?php echo list_options_from_textarea(get_field('pdl_university', 'option'), $education_1['university'], false, false, $education_1['university']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !universityOne">This field is required.</div>
			</div>

			<!-- displayed dynamically -->
			<div class="form-group" ng-cloak ng-show="universityOne =='Other'">
				<label class="col-sm-4 control-label"></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" placeholder="Your university name" ng-model="university_nameOne" value="<?php echo  $education_1['university']; ?>">
				</div>

				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(university_nameOne) || university_nameOne.length > 100) &amp;&amp; university_nameOne.length > 0">University name can contain up to 100 characters and should not contain any special symbols.</div>

				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !university_nameOne">This field is required.</div>
			</div>

			<!-- Course topic -->
			<div class="form-group">
				<label class="col-sm-4 control-label">Course topic*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect" style="visibility:hidden;" ng-model="course_nameOne">
					<?php echo list_options_from_textarea(get_field('pdl_course_name', 'option'), $education_1['course_name'], false, false, $education_1['course_name']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !course_nameOne">This field is required.</div>
			</div>

			<!-- displayed dynamically -->
			<div class="form-group" ng-cloak ng-show="course_nameOne =='Other'">
				<label class="col-sm-4 control-label"></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" placeholder="Your course name" ng-model="your_course_nameOne"  value="<?php echo  $education_1['course_name']; ?>">
				</div>

				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(your_course_nameOne) || your_course_nameOne.length > 100) &amp;&amp; your_course_nameOne.length > 0">Course name can contain up to 100 characters and should not contain any special symbols.</div>

				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !your_course_nameOne">This field is required.</div>
			</div>

			<!-- Qualification -->
			<div class="form-group">
				<label class="col-sm-4 control-label">Qualification type*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect" style="visibility:hidden;" ng-model="qualificationOne">
					<?php echo list_options_from_textarea(get_field('pdl_qualification', 'option'), $education_1['qualification']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !qualificationOne">This field is required.</div>
			</div>

			<!-- Grade -->
			<div class="form-group">
				<label class="col-sm-4 control-label">Grade attained*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect" style="visibility:hidden;" ng-model="gradeOne">
					<?php echo list_options_from_textarea(get_field('pdl_grade', 'option'), $education_1['grade']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !gradeOne">This field is required.</div>
			</div>
			 <!-- Year Awarded -->
			<div class="form-group">
				<label class="col-sm-4 control-label">Year attained*</label>
				<div class="col-sm-8 multiselect-wrapper">
					<select class="multiselect" style="visibility:hidden;" ng-model="yearOne">
					<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $education_1['year']); ?>
					</select>
				</div>
				<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !yearOne">This field is required.</div>
			</div>

		</form>


		<div class="bottom">

			<div class="btn-group">
			  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
			    <li><a ng-click="deleteNode(1);">Delete</a></li>
			    <li role="separator" class="divider"></li>
			    <li><a >cancel</a></li>
			  </ul>
			</div>

			<div class="og-footer-error errors-above" ng-cloak ng-show="!validOne">Please correct errors above</div>
		</div>
		</div>

</div>




</div>
