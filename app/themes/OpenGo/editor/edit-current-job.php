<?php
/**
 * Template Name: Edit Current Job
 * Custom template.
 */
get_header();

$profile = opengo::get_profile();

$current = array(
	'organisation_name' => opengo::check_selected($profile->current_job->current_job_organisation_name),
	'industry'          => opengo::check_selected($profile->current_job->current_job_industry),
	'company_type'      => opengo::check_selected($profile->current_job->current_job_company_type),
	'job_function'      => opengo::check_selected($profile->current_job->current_job_function),
	'seniority'         => opengo::check_selected($profile->current_job->current_job_seniority),
	'year_started'      => opengo::check_selected($profile->current_job->current_job_year_started),
	'contract'          => opengo::check_selected($profile->current_job->current_job_type_of_contract),
	'notice_period'     => opengo::check_selected($profile->current_job->current_job_notice_period),
	'salary'            => opengo::check_selected($profile->current_job->current_job_basic_salary),
	'currency'          => opengo::check_selected($profile->current_job->current_job_basic_salary_currency),
	'responsibilities'  => opengo::check_selected($profile->current_job->current_job_main_responsibilities)
	);

$Public_preview_url = get_home_url() . opengo::get_my_cv_url();
$public_preview_btn = "<a class='public-preview-btn btn btn-default btn-sm' href=" . $Public_preview_url . " target='_blank'><i class='fa fa-file-text-o'></i>Public CV</a>";

?>
<div id="primary" class="content-area">
	<main id="main" class="site-main clearfix" role="main">
		<div class="blue-part"></div>
		<div class="content-part">
			<div class="middle-section">
				<section class="page-general editor clearfix">

					<div class="page-title-wrapper">
						<h1>Editor</h1>
					</div>
					<article class="page-content-wrapper editor-wrapper">

						<div class="sidebar">
							<?php get_template_part('editor/editor-left-sidebar'); ?>
						</div>

						<div class="editor-content clearfix" ng-app="ogCurrentJobApp" ng-controller="currentJobController">
									<h2 class="fields-group-title">What role are you currently doing?</h2>
							<div class="editor-flex">
								<div class="hint">
										<div class="bg-info og-info"><?php the_field('hint_current_job', 'option'); ?></div>
								</div>
								<div class="fields-wrapper">

										<div class="splash" ng-cloak>

											<div class="preloader">
												<div class="cssload-thecube">
													<div class="cssload-cube cssload-c1"></div>
													<div class="cssload-cube cssload-c2"></div>
													<div class="cssload-cube cssload-c4"></div>
													<div class="cssload-cube cssload-c3"></div>
												</div>
											</div>

								      <h3>LOADING</h3>
								    </div>


									<form class  ="form-horizontal"  ng-cloak>


										<div class="form-group">
											<label class="col-sm-4 control-label">Organisation name*</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_name" value="<?php echo $current['organisation_name']; ?>">
											</div>

											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_name) || organisation_name.length > 100) &amp;&amp; organisation_name.length > 0">Organisation name can contain up to 100 characters and should not contain any special symbols.</div>
											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !organisation_name">This field is required.</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Industry*</label>
											<div class="col-sm-8 multiselect-wrapper">
												<select class="multiselect" style="visibility:hidden;" ng-model="industry">
												<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), $current['industry']); ?>
												</select>
											</div>
											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !industry">This field is required.</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Company type*</label>
											<div class="col-sm-8 multiselect-wrapper">
												<select class="multiselect" style="visibility:hidden;" ng-model="company_type">
												<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), $current['company_type']); ?>
												</select>
											</div>
											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !company_type">This field is required.</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Job function*</label>
											<div class="col-sm-8 multiselect-wrapper">
												<select class="multiselect" style="visibility:hidden;" ng-model="job_function">
												<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), $current['job_function']); ?>
												</select>
											</div>
											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !job_function">This field is required.</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Seniority*</label>
											<div class="col-sm-8 multiselect-wrapper">
												<select class="multiselect" style="visibility:hidden;" ng-model="seniority">
												<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), $current['seniority']); ?>
												</select>
											</div>
											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !seniority">This field is required.</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Year started*</label>
											<div class="col-sm-8 multiselect-wrapper">
												<select class="multiselect" style="visibility:hidden;" ng-model="year_started">
												<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $current['year_started']); ?>
												</select>
											</div>
											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !year_started">This field is required.</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Type of contract</label>
											<div class="col-sm-8 multiselect-wrapper">
												<select class="multiselect" style="visibility:hidden;" ng-model="contract">
												<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), $current['contract']); ?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Notice period</label>
											<div class="col-sm-8 multiselect-wrapper">
												<select class="multiselect" style="visibility:hidden;" ng-model="notice_period">
												<?php echo list_options_from_textarea(get_field('pdl_notice_period', 'option'), $current['notice_period']); ?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Basic salary</label>
											<div class="col-sm-8 multiselect-wrapper">
												<select class="multiselect" style="visibility:hidden;" ng-model="current_salary">
												<?php echo list_options_from_textarea(get_field('pdl_salary', 'option'), $current['salary']); ?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Currency</label>
											<div class="col-sm-8 multiselect-wrapper">
												<select class="multiselect" style="visibility:hidden;" ng-model="current_currency">
												<?php echo list_options_from_textarea_currency(get_field('pdl_currency', 'option'), $current['currency']); ?>
												</select>
											</div>
										</div>


										<div class="form-group wysiwyg">
											<label class="col-sm-12 control-label text-align-left">Responsibilities and achievements</label>
											<div class="col-sm-12">
											<summernote config="summernoteOptions" ng-model="responsibilities" ><?php echo stripslashes($current['responsibilities']); ?></summernote>
											</div>
											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !responsibilities">This field is required.</div>
										</div>

									</form>
								</div>
							</div>
							<div class="bottom">
								<span id="save-btn" class="btn btn-primary btn-sm save-btn" ng-click="validate()" ng-cloak><i class="fa fa-check-circle-o"></i> Save</span>
								<?php  echo $public_preview_btn; ?>
								<div class="og-footer-error errors-above" ng-cloak ng-show="!valid">Please correct errors above</div>
									<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"> Updating..</span>
									<span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
									<span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
							</div>
						</div>

					</article>
				</section><!-- .page-general-->
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
