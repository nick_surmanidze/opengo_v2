<?php
/**
 * Template Name: Edit Skills
 * Custom template.
 */
get_header();


$profile = opengo::get_profile();

$skill = array(
		'one'   => htmlspecialchars(stripslashes(opengo::check_selected($profile->skills->specialist_skill_1))),
		'two'   => htmlspecialchars(stripslashes(opengo::check_selected($profile->skills->specialist_skill_2))),
		'three' => htmlspecialchars(stripslashes(opengo::check_selected($profile->skills->specialist_skill_3)))
	);

$Public_preview_url = get_home_url() . opengo::get_my_cv_url();
$public_preview_btn = "<a class='public-preview-btn btn btn-default btn-sm' href=" . $Public_preview_url . " target='_blank'><i class='fa fa-file-text-o'></i>Public CV</a>";

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general editor clearfix">

						<div class="page-title-wrapper">
							<h1>Editor</h1>
						</div>
						<article class="page-content-wrapper editor-wrapper" >

							<div class="sidebar">
								<?php get_template_part('editor/editor-left-sidebar'); ?>
							</div>

							<div class="editor-content clearfix" ng-app="ogSkillsApp" ng-controller="skillsController">
										<h2 class="fields-group-title">
											Edit skills
										</h2>
								<div class="editor-flex skills">

									<div class="hint">
											<div class="bg-info og-info"><?php the_field('hint_skills', 'option'); ?></div>
									</div>

									<div class="fields-wrapper">

										<form class  ="form-horizontal">


											<div class="form-group">
												<label class="col-sm-4 control-label">Specialist Skill</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" placeholder="Skill" ng-model="skill_one" value="<?php echo stripslashes($skill['one']); ?>">
												</div>

											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(skill_one) || skill_one.length > 100) &amp;&amp; skill_one.length > 0">Specialist skill can contain up to 100 characters and should not contain any special symbols.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Specialist Skill</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" placeholder="Skill" ng-model="skill_two"  value="<?php echo stripslashes($skill['two']); ?>">
												</div>

											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(skill_two) || skill_two.length > 100) &amp;&amp; skill_two.length > 0">Specialist skill can contain up to 100 characters and should not contain any special symbols.</div>

											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Specialist Skill</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" placeholder="Skill" ng-model="skill_three"  value="<?php echo stripslashes($skill['three']); ?>">
												</div>

											<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(skill_three) || skill_three.length > 100) &amp;&amp; skill_three.length > 0">Specialist skill can contain up to 100 characters and should not contain any special symbols.</div>
											</div>

										</form>

									</div>

								</div>

								<div class="bottom">
									<span id="save-btn" class="btn btn-primary btn-sm save-btn" ng-click="validate()"><i class="fa fa-check-circle-o"></i> Save</span>
									<?php  echo $public_preview_btn; ?>
									<div class="og-footer-error errors-above" ng-cloak ng-show="!valid">Please correct errors above</div>
									<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"> Updating..</span>
									<span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
									<span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
								</div>
							</div>

						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
