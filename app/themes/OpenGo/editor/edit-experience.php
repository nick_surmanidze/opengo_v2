<?php
/**
 * Template Name: Edit Experience
 * Custom template.
 */
get_header();

global $profile;

if(!isset($profile)) {
	$profile = opengo::get_profile();
}


$experience_1 = array(
	'organisation_name' => htmlspecialchars(stripslashes(opengo::check_selected($profile->experience_1->previous_exp_organisation_name))),
	'industry'          => opengo::check_selected($profile->experience_1->previous_exp_industry),
	'company_type'      => opengo::check_selected($profile->experience_1->previous_exp_company_type),
	'job_function'      => opengo::check_selected($profile->experience_1->previous_exp_job_function),
	'seniority'         => opengo::check_selected($profile->experience_1->previous_exp_seniority),
	'year_started'      => opengo::check_selected($profile->experience_1->previous_exp_year_started),
	'year_ended'        => opengo::check_selected($profile->experience_1->previous_exp_year_ended),
	'contract'          => opengo::check_selected($profile->experience_1->previous_exp_type_of_contract),
	'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_1->previous_exp_main_responsibilities))
);

$experience_2 = array(
	'organisation_name' => htmlspecialchars(stripslashes(opengo::check_selected($profile->experience_2->previous_exp_organisation_name))),
	'industry'          => opengo::check_selected($profile->experience_2->previous_exp_industry),
	'company_type'      => opengo::check_selected($profile->experience_2->previous_exp_company_type),
	'job_function'      => opengo::check_selected($profile->experience_2->previous_exp_job_function),
	'seniority'         => opengo::check_selected($profile->experience_2->previous_exp_seniority),
	'year_started'      => opengo::check_selected($profile->experience_2->previous_exp_year_started),
	'year_ended'        => opengo::check_selected($profile->experience_2->previous_exp_year_ended),
	'contract'          => opengo::check_selected($profile->experience_2->previous_exp_type_of_contract),
	'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_2->previous_exp_main_responsibilities))
);

$experience_3 = array(
	'organisation_name' => htmlspecialchars(stripslashes(opengo::check_selected($profile->experience_3->previous_exp_organisation_name))),
	'industry'          => opengo::check_selected($profile->experience_3->previous_exp_industry),
	'company_type'      => opengo::check_selected($profile->experience_3->previous_exp_company_type),
	'job_function'      => opengo::check_selected($profile->experience_3->previous_exp_job_function),
	'seniority'         => opengo::check_selected($profile->experience_3->previous_exp_seniority),
	'year_started'      => opengo::check_selected($profile->experience_3->previous_exp_year_started),
	'year_ended'        => opengo::check_selected($profile->experience_3->previous_exp_year_ended),
	'contract'          => opengo::check_selected($profile->experience_3->previous_exp_type_of_contract),
	'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_3->previous_exp_main_responsibilities))
);

$experience_4 = array(
	'organisation_name' => htmlspecialchars(stripslashes(opengo::check_selected($profile->experience_4->previous_exp_organisation_name))),
	'industry'          => opengo::check_selected($profile->experience_4->previous_exp_industry),
	'company_type'      => opengo::check_selected($profile->experience_4->previous_exp_company_type),
	'job_function'      => opengo::check_selected($profile->experience_4->previous_exp_job_function),
	'seniority'         => opengo::check_selected($profile->experience_4->previous_exp_seniority),
	'year_started'      => opengo::check_selected($profile->experience_4->previous_exp_year_started),
	'year_ended'        => opengo::check_selected($profile->experience_4->previous_exp_year_ended),
	'contract'          => opengo::check_selected($profile->experience_4->previous_exp_type_of_contract),
	'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_4->previous_exp_main_responsibilities))
);

$experience_5 = array(
	'organisation_name' => htmlspecialchars(stripslashes(opengo::check_selected($profile->experience_5->previous_exp_organisation_name))),
	'industry'          => opengo::check_selected($profile->experience_5->previous_exp_industry),
	'company_type'      => opengo::check_selected($profile->experience_5->previous_exp_company_type),
	'job_function'      => opengo::check_selected($profile->experience_5->previous_exp_job_function),
	'seniority'         => opengo::check_selected($profile->experience_5->previous_exp_seniority),
	'year_started'      => opengo::check_selected($profile->experience_5->previous_exp_year_started),
	'year_ended'        => opengo::check_selected($profile->experience_5->previous_exp_year_ended),
	'contract'          => opengo::check_selected($profile->experience_5->previous_exp_type_of_contract),
	'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_5->previous_exp_main_responsibilities))
);

$experience_6 = array(
	'organisation_name' => htmlspecialchars(stripslashes(opengo::check_selected($profile->experience_6->previous_exp_organisation_name))),
	'industry'          => opengo::check_selected($profile->experience_6->previous_exp_industry),
	'company_type'      => opengo::check_selected($profile->experience_6->previous_exp_company_type),
	'job_function'      => opengo::check_selected($profile->experience_6->previous_exp_job_function),
	'seniority'         => opengo::check_selected($profile->experience_6->previous_exp_seniority),
	'year_started'      => opengo::check_selected($profile->experience_6->previous_exp_year_started),
	'year_ended'        => opengo::check_selected($profile->experience_6->previous_exp_year_ended),
	'contract'          => opengo::check_selected($profile->experience_6->previous_exp_type_of_contract),
	'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_6->previous_exp_main_responsibilities))
);

$experience_7 = array(
	'organisation_name' => htmlspecialchars(stripslashes(opengo::check_selected($profile->experience_7->previous_exp_organisation_name))),
	'industry'          => opengo::check_selected($profile->experience_7->previous_exp_industry),
	'company_type'      => opengo::check_selected($profile->experience_7->previous_exp_company_type),
	'job_function'      => opengo::check_selected($profile->experience_7->previous_exp_job_function),
	'seniority'         => opengo::check_selected($profile->experience_7->previous_exp_seniority),
	'year_started'      => opengo::check_selected($profile->experience_7->previous_exp_year_started),
	'year_ended'        => opengo::check_selected($profile->experience_7->previous_exp_year_ended),
	'contract'          => opengo::check_selected($profile->experience_7->previous_exp_type_of_contract),
	'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_7->previous_exp_main_responsibilities))
);
$Public_preview_url = get_home_url() . opengo::get_my_cv_url();
$public_preview_btn = "<a class='public-preview-btn btn btn-default btn-sm' href=" . $Public_preview_url . " target='_blank'><i class='fa fa-file-text-o'></i>Public CV</a>";

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general editor clearfix">

						<div class="page-title-wrapper">
							<h1>Editor</h1>
						</div>
						<article class="page-content-wrapper editor-wrapper">

							<div class="sidebar">
								<?php get_template_part('editor/editor-left-sidebar'); ?>
							</div>

							<div class="editor-content clearfix" ng-app="ogExperienceApp" ng-controller="experienceController">
								<h2 class="fields-group-title no-bottom-margin">Add / edit previous experience</h2>

								<div class="editor-flex">

									<div class="editor-list-group-list-wrapper">

										<div class="list-group full-width">

									    <div class="splash" ng-cloak>

												<div class="preloader">
													<div class="cssload-thecube">
														<div class="cssload-cube cssload-c1"></div>
														<div class="cssload-cube cssload-c2"></div>
														<div class="cssload-cube cssload-c4"></div>
														<div class="cssload-cube cssload-c3"></div>
													</div>
												</div>

									      <h3>LOADING</h3>
									    </div>
											<div id="sortable">
										    <?php
												if(opengo::completed_experience_one($profile) ) {
											    get_template_part('/editor/experience-nodes/experience-node-one');
											  }
												if(opengo::completed_experience_two($profile) ) {
										    	get_template_part('/editor/experience-nodes/experience-node-two');
										 		}
										 		if(opengo::completed_experience_three($profile) ) {
										    	get_template_part('/editor/experience-nodes/experience-node-three');
										    }
										    if(opengo::completed_experience_four($profile) ) {
										    	get_template_part('/editor/experience-nodes/experience-node-four');
										    }
										    if(opengo::completed_experience_five($profile) ) {
										    	get_template_part('/editor/experience-nodes/experience-node-five');
										    }
										    if(opengo::completed_experience_six($profile) ) {
										    	get_template_part('/editor/experience-nodes/experience-node-six');
										  	}
										  	if(opengo::completed_experience_seven($profile) ) {
										    	get_template_part('/editor/experience-nodes/experience-node-seven');
										    }

										    ?>
												</div>
											  <?php
												if(!opengo::completed_experience_seven($profile) ) {
											  	get_template_part('/editor/experience-nodes/experience-node-new');
											  }

											   ?>

										</div>
									<div class="bottom" ng-cloak>
									<span id="save-btn" class="btn btn-primary btn-sm save-btn" ng-click="validate()" ng-cloak><i class="fa fa-check-circle-o"></i> Update</span>
									<?php  echo $public_preview_btn; ?>
									<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"> Updating..</span>
									<span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
									<span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
								</div>
									</div>

								</div>
						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
