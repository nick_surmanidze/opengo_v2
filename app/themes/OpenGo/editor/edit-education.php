<?php
/**
 * Template Name: Edit Education
 * Custom template.
 */
get_header();

$profile = opengo::get_profile();

$education_1 = array(
	'university'    => htmlspecialchars(stripslashes(opengo::check_selected($profile->education_1->university))),
	'course_name'   => htmlspecialchars(stripslashes(opengo::check_selected($profile->education_1->course_name))),
	'qualification' => opengo::check_selected($profile->education_1->qualification_type),
	'grade'         => opengo::check_selected($profile->education_1->grade_attained),
	'year'          => opengo::check_selected($profile->education_1->year_attained)
	);

$education_2 = array(
	'university'    => htmlspecialchars(stripslashes(opengo::check_selected($profile->education_2->university))),
	'course_name'   => htmlspecialchars(stripslashes(opengo::check_selected($profile->education_2->course_name))),
	'qualification' => opengo::check_selected($profile->education_2->qualification_type),
	'grade'         => opengo::check_selected($profile->education_2->grade_attained),
	'year'          => opengo::check_selected($profile->education_2->year_attained)
	);

$education_3 = array(
	'university'    => htmlspecialchars(stripslashes(opengo::check_selected($profile->education_3->university))),
	'course_name'   => htmlspecialchars(stripslashes(opengo::check_selected($profile->education_3->course_name))),
	'qualification' => opengo::check_selected($profile->education_3->qualification_type),
	'grade'         => opengo::check_selected($profile->education_3->grade_attained),
	'year'          => opengo::check_selected($profile->education_3->year_attained)
	);

$Public_preview_url = get_home_url() . opengo::get_my_cv_url();
$public_preview_btn = "<a class='public-preview-btn btn btn-default btn-sm' href=" . $Public_preview_url . " target='_blank'><i class='fa fa-file-text-o'></i>Public CV</a>";

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general editor clearfix">

						<div class="page-title-wrapper">
							<h1>Editor</h1>
						</div>
						<article class="page-content-wrapper editor-wrapper">

							<div class="sidebar">
								<?php get_template_part('editor/editor-left-sidebar'); ?>
							</div>

							<div class="editor-content clearfix" ng-app="ogEducationApp" ng-controller="educationController">
										<h2 class="fields-group-title no-bottom-margin">
											Add / edit education
										</h2>
								<div class="editor-flex">

								<div class="editor-list-group-list-wrapper">

									<div class="list-group full-width">

								    <div class="splash" ng-cloak>

											<div class="preloader">
												<div class="cssload-thecube">
													<div class="cssload-cube cssload-c1"></div>
													<div class="cssload-cube cssload-c2"></div>
													<div class="cssload-cube cssload-c4"></div>
													<div class="cssload-cube cssload-c3"></div>
												</div>
											</div>

								      <h3>LOADING</h3>
								    </div>
										<div id="sortable">
											<?php
											if(opengo::completed_education_one($profile) ) {

												get_template_part('/editor/education-nodes/education-node-one');

												}
											?>

											<?php
											if(opengo::completed_education_two($profile) ) {

												get_template_part('/editor/education-nodes/education-node-two');

												}
											?>

											<?php
											if(opengo::completed_education_three($profile) ) {

												get_template_part('/editor/education-nodes/education-node-three');

												}
											?>



										</div>

											<?php
											if (!opengo::completed_education_three($profile) ) {

												get_template_part('/editor/education-nodes/education-node-new');

												}
											?>


									</div>
								</div>

								<div class="bottom" ng-cloak>
									<span id="save-btn" class="btn btn-primary btn-sm save-btn" ng-click="validate()" ng-cloak><i class="fa fa-check-circle-o"></i> Update</span>
									<?php  echo $public_preview_btn; ?>
									<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"> Updating..</span>
									<span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
									<span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
								</div>

								</div>

							</div>

						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
