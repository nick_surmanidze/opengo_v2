<?php
global $experience_3;
?>
<div id="experience-node-three"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedThree]">

<span class="label label-warning unsaved-changes">unsaved changed</span>

  <div class="rendered" ng-cloak ng-click="toggleBoolThree()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="seniorityThree">{{ seniorityThree }}, </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!seniorityThree">Seniority, </span>

	    <span ng-cloak ng-show="job_functionThree">{{ job_functionThree }} </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!job_functionThree">job function </span>

	    <br>

    </h5>

    <span ng-cloak ng-show="organisation_nameThree">{{ organisation_nameThree }}, </span>
    <span class="ng-placeholder" ng-cloak ng-show="!organisation_nameThree">Organisation name, </span>

    <span ng-cloak ng-show="company_typeThree">{{ company_typeThree }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!company_typeThree">company type </span>

    <span ng-cloak> in the </span>

    <span ng-cloak ng-show="industryThree">{{ industryThree }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!industryThree"> industry </span>

   	<span ng-cloak> sector.</span> <br>

		<small>
			<span ng-cloak ng-show="year_startedThree">{{ year_startedThree }} </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_startedThree"> Year started </span>

    	<span ng-cloak> to </span>

    	<span ng-cloak ng-show="year_endedThree" >{{ year_endedThree }}. </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_endedThree"> date. </span> <br>


			<span ng-cloak ng-show="responsibilitiesThree"><pre ng-bind-html="responsibilitiesThree | unsafe"></pre></span>
    	<span class="ng-placeholder" ng-cloak ng-show="!responsibilitiesThree"> Responsibilities and achievements </span>

		</small>

  </div>







	<div class="editor-form-wrapper" ng-show="expandedThree">

		<div class="hint">
				<div class="bg-info og-info"><?php the_field('hint_experience', 'option'); ?></div>
		</div>




		<div class="fields-wrapper">


			<form class  ="form-horizontal" name="formThree">


				<div class="form-group">
					<label class="col-sm-4 control-label">Organisation name*</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_nameThree" value="<?php echo $experience_3['organisation_name']; ?>" >
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_nameThree) || organisation_nameThree.length > 100) &amp;&amp; organisation_nameThree.length > 0">Organization name can contain up to 100 characters.</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !organisation_nameThree">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Industry*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="industryThree">
							<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), $experience_3['industry']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !industryThree">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Company type*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="company_typeThree">
							<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), $experience_3['company_type']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !company_typeThree">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Job function*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="job_functionThree">
							<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), $experience_3['job_function']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !job_functionThree">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Seniority*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="seniorityThree">
							<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), $experience_3['seniority']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !seniorityThree">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year started*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_startedThree">
							<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_3['year_started']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !year_startedThree">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year ended*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_endedThree">
						<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_3['year_ended']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validThree &amp;&amp; !year_endedThree">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Type of contract</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="contractThree">
						<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), $experience_3['contract']); ?>
						</select>
					</div>
				</div>




				<div class="form-group wysiwyg">
					<label class="col-sm-12 control-label  text-align-left">Responsibilities and achievements</label>
					<div class="col-sm-12">
						<summernote config="summernoteOptions" ng-model="responsibilitiesThree"><?php echo $experience_3['responsibilities']; ?></summernote>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(responsibilitiesThree.length > 500) &amp;&amp; responsibilitiesThree.length > 0">This field can contain up to 500 characters.</div>
				</div>
			</form>




			<div class="bottom">
				<div class="btn-group">
				  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a ng-click="deleteNode(3);">Delete</a></li>
				    <li role="separator" class="divider"></li>
				    <li><a >cancel</a></li>
				  </ul>
				</div>


				<div class="og-footer-error errors-above" ng-cloak ng-show="!validThree">Please correct errors above</div>
			</div>

		</div>



	</div>
</div>
