<?php
global $experience_5;
?>
<div id="experience-node-five"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedFive]">

<span class="label label-warning unsaved-changes">unsaved changed</span>

  <div class="rendered" ng-cloak ng-click="toggleBoolFive()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="seniorityFive">{{ seniorityFive }}, </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!seniorityFive">Seniority, </span>

	    <span ng-cloak ng-show="job_functionFive">{{ job_functionFive }} </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!job_functionFive">job function </span>

	    <br>

    </h5>

    <span ng-cloak ng-show="organisation_nameFive">{{ organisation_nameFive }}, </span>
    <span class="ng-placeholder" ng-cloak ng-show="!organisation_nameFive">Organisation name, </span>

    <span ng-cloak ng-show="company_typeFive">{{ company_typeFive }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!company_typeFive">company type </span>

    <span ng-cloak> in the </span>

    <span ng-cloak ng-show="industryFive">{{ industryFive }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!industryFive"> industry </span>

   	<span ng-cloak> sector.</span> <br>

		<small>
			<span ng-cloak ng-show="year_startedFive">{{ year_startedFive }} </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_startedFive"> Year started </span>

    	<span ng-cloak> to </span>

    	<span ng-cloak ng-show="year_endedFive" >{{ year_endedFive }}. </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_endedFive"> date. </span> <br>


			<span ng-cloak ng-show="responsibilitiesFive"><pre ng-bind-html="responsibilitiesFive | unsafe"></pre></span>
    	<span class="ng-placeholder" ng-cloak ng-show="!responsibilitiesFive"> Responsibilities and achievements </span>

		</small>

  </div>







	<div class="editor-form-wrapper" ng-show="expandedFive">

		<div class="hint">
				<div class="bg-info og-info"><?php the_field('hint_experience', 'option'); ?></div>
		</div>




		<div class="fields-wrapper">


			<form class  ="form-horizontal" name="formFive">


				<div class="form-group">
					<label class="col-sm-4 control-label">Organisation name*</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_nameFive" value="<?php echo $experience_5['organisation_name']; ?>" >
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_nameFive) || organisation_nameFive.length > 100) &amp;&amp; organisation_nameFive.length > 0">Organization name can contain up to 100 characters.</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFive &amp;&amp; !organisation_nameFive">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Industry*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="industryFive">
							<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), $experience_5['industry']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFive &amp;&amp; !industryFive">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Company type*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="company_typeFive">
							<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), $experience_5['company_type']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFive &amp;&amp; !company_typeFive">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Job function*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="job_functionFive">
							<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), $experience_5['job_function']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFive &amp;&amp; !job_functionFive">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Seniority*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="seniorityFive">
							<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), $experience_5['seniority']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFive &amp;&amp; !seniorityFive">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year started*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_startedFive">
							<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_5['year_started']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFive &amp;&amp; !year_startedFive">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year ended*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_endedFive">
						<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_5['year_ended']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFive &amp;&amp; !year_endedFive">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Type of contract</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="contractFive">
						<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), $experience_5['contract']); ?>
						</select>
					</div>
				</div>

				<div class="form-group wysiwyg">
					<label class="col-sm-12 control-label  text-align-left">Responsibilities and achievements</label>
					<div class="col-sm-12">
						<summernote config="summernoteOptions" ng-model="responsibilitiesFive"><?php echo $experience_5['responsibilities']; ?></summernote>
					</div>
					<div  class="col-sm-12 og-msg og-error ng-cloak ng-hide" ng-show="(responsibilitiesFive.length > 500) &amp;&amp; responsibilitiesFive.length > 0">This field can contain up to 500 characters.</div>

				</div>


			</form>




			<div class="bottom">
				<div class="btn-group">
				  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a ng-click="deleteNode(5);">Delete</a></li>
				    <li role="separator" class="divider"></li>
				    <li><a >cancel</a></li>
				  </ul>
				</div>


				<div class="og-footer-error errors-above" ng-cloak ng-show="!validFive">Please correct errors above</div>
			</div>

		</div>



	</div>
</div>
