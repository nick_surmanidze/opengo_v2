<?php
global $experience_2;
?>
<div id="experience-node-two"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedTwo]">

<span class="label label-warning unsaved-changes">unsaved changed</span>

  <div class="rendered" ng-cloak ng-click="toggleBoolTwo()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="seniorityTwo">{{ seniorityTwo }}, </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!seniorityTwo">Seniority, </span>

	    <span ng-cloak ng-show="job_functionTwo">{{ job_functionTwo }} </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!job_functionTwo">job function </span>

	    <br>

    </h5>

    <span ng-cloak ng-show="organisation_nameTwo">{{ organisation_nameTwo }}, </span>
    <span class="ng-placeholder" ng-cloak ng-show="!organisation_nameTwo">Organisation name, </span>

    <span ng-cloak ng-show="company_typeTwo">{{ company_typeTwo }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!company_typeTwo">company type </span>

    <span ng-cloak> in the </span>

    <span ng-cloak ng-show="industryTwo">{{ industryTwo }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!industryTwo"> industry </span>

   	<span ng-cloak> sector.</span> <br>

		<small>
			<span ng-cloak ng-show="year_startedTwo">{{ year_startedTwo }} </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_startedTwo"> Year started </span>

    	<span ng-cloak> to </span>

    	<span ng-cloak ng-show="year_endedTwo" >{{ year_endedTwo }}. </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_endedTwo"> date. </span> <br>


			<span ng-cloak ng-show="responsibilitiesTwo"><pre ng-bind-html="responsibilitiesTwo | unsafe"></pre></span>
    	<span class="ng-placeholder" ng-cloak ng-show="!responsibilitiesTwo"> Responsibilities and achievements </span>

		</small>

  </div>







	<div class="editor-form-wrapper" ng-show="expandedTwo">

		<div class="hint">
				<div class="bg-info og-info"><?php the_field('hint_experience', 'option'); ?></div>
		</div>




		<div class="fields-wrapper">


			<form class  ="form-horizontal" name="formTwo">


				<div class="form-group">
					<label class="col-sm-4 control-label">Organisation name*</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_nameTwo" value="<?php echo $experience_2['organisation_name']; ?>">
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_nameTwo) || organisation_nameTwo.length > 100) &amp;&amp; organisation_nameTwo.length > 0">Organization name can contain up to 100 characters.</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !organisation_nameTwo">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Industry*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="industryTwo">
							<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), $experience_2['industry']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !industryTwo">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Company type*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="company_typeTwo">
							<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), $experience_2['company_type']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !company_typeTwo">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Job function*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="job_functionTwo">
							<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), $experience_2['job_function']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !job_functionTwo">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Seniority*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="seniorityTwo">
							<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), $experience_2['seniority']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !seniorityTwo">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year started*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_startedTwo">
							<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_2['year_started']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !year_startedTwo">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year ended*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_endedTwo">
						<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_2['year_ended']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validTwo &amp;&amp; !year_endedTwo">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Type of contract</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="contractTwo">
						<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), $experience_2['contract']); ?>
						</select>
					</div>
				</div>

				<div class="form-group wysiwyg">
					<label class="col-sm-12 control-label  text-align-left">Responsibilities and achievements</label>
					<div class="col-sm-12">
						<summernote config="summernoteOptions" ng-model="responsibilitiesTwo"><?php echo $experience_2['responsibilities']; ?></summernote>
					</div>
					<div  class="col-sm-12 og-msg og-error ng-cloak ng-hide" ng-show="(responsibilitiesTwo.length > 500) &amp;&amp; responsibilitiesTwo.length > 0">This field can contain up to 500 characters.</div>
				</div>


			</form>




			<div class="bottom">
				<div class="btn-group">
				  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a ng-click="deleteNode(2);">Delete</a></li>
				    <li role="separator" class="divider"></li>
				    <li><a >cancel</a></li>
				  </ul>
				</div>


				<div class="og-footer-error errors-above" ng-cloak ng-show="!validTwo">Please correct errors above</div>
			</div>

		</div>



	</div>
</div>
