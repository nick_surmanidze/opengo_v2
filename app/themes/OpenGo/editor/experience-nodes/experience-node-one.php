<?php
global $experience_1;
?>
<div id="experience-node-one"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedOne]">

<span class="label label-warning unsaved-changes">unsaved changed</span>

  <div class="rendered" ng-cloak ng-click="toggleBoolOne()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="seniorityOne">{{ seniorityOne }}, </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!seniorityOne">Seniority, </span>

	    <span ng-cloak ng-show="job_functionOne">{{ job_functionOne }} </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!job_functionOne">job function </span>

	    <br>

    </h5>

    <span ng-cloak ng-show="organisation_nameOne">{{ organisation_nameOne }}, </span>
    <span class="ng-placeholder" ng-cloak ng-show="!organisation_nameOne">Organisation name, </span>

    <span ng-cloak ng-show="company_typeOne">{{ company_typeOne }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!company_typeOne">company type </span>

    <span ng-cloak> in the </span>

    <span ng-cloak ng-show="industryOne">{{ industryOne }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!industryOne"> industry </span>

   	<span ng-cloak> sector.</span> <br>

		<small>
			<span ng-cloak ng-show="year_startedOne">{{ year_startedOne }} </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_startedOne"> Year started </span>

    	<span ng-cloak> to </span>

    	<span ng-cloak ng-show="year_endedOne" >{{ year_endedOne }}. </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_endedOne"> date. </span> <br>


			<span ng-cloak ng-show="responsibilitiesOne"><pre ng-bind-html="responsibilitiesOne | unsafe"></pre></span>
    	<span class="ng-placeholder" ng-cloak ng-show="!responsibilitiesOne"> Responsibilities and achievements </span>

		</small>

  </div>







	<div class="editor-form-wrapper" ng-show="expandedOne">

		<div class="hint">
				<div class="bg-info og-info"><?php the_field('hint_experience', 'option'); ?></div>
		</div>




		<div class="fields-wrapper">


			<form class  ="form-horizontal" name="formOne">


				<div class="form-group">
					<label class="col-sm-4 control-label">Organisation name*</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_nameOne" value="<?php echo $experience_1['organisation_name']; ?>">
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_nameOne) || organisation_nameOne.length > 100) &amp;&amp; organisation_nameOne.length > 0">Organization name can contain up to 100 characters.</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !organisation_nameOne">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Industry*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="industryOne">
							<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), $experience_1['industry']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !industryOne">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Company type*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="company_typeOne">
							<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), $experience_1['company_type']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !company_typeOne">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Job function*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="job_functionOne">
							<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), $experience_1['job_function']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !job_functionOne">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Seniority*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="seniorityOne">
							<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), $experience_1['seniority']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !seniorityOne">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year started*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_startedOne">
							<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_1['year_started']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !year_startedOne">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year ended*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_endedOne">
						<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_1['year_ended']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !year_endedOne">This field is required.</div>


				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Type of contract</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="contractOne">
						<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), $experience_1['contract']); ?>
						</select>
					</div>
				</div>

				<div class="form-group wysiwyg">
					<label class="col-sm-12 control-label  text-align-left">Responsibilities and achievements</label>
					<div class="col-sm-12">
						<summernote config="summernoteOptions" ng-model="responsibilitiesOne"><?php echo $experience_1['responsibilities']; ?></summernote>
					</div>
					<div  class="col-sm-12 og-msg og-error ng-cloak ng-hide" ng-show="(responsibilitiesOne.length > 500) &amp;&amp; responsibilitiesOne.length > 0">This field can contain up to 500 characters.</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validOne &amp;&amp; !responsibilitiesOne">This field is required.</div>

				</div>


			</form>




			<div class="bottom">
				<div class="btn-group">
				  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a ng-click="deleteNode(1);">Delete</a></li>
				    <li role="separator" class="divider"></li>
				    <li><a >cancel</a></li>
				  </ul>
				</div>


				<div class="og-footer-error errors-above" ng-cloak ng-show="!validOne">Please correct errors above</div>
			</div>

		</div>



	</div>
</div>
