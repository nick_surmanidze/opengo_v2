<?php
global $experience_6;
?>
<div id="experience-node-six"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedSix]">

<span class="label label-warning unsaved-changes">unsaved changed</span>

  <div class="rendered" ng-cloak ng-click="toggleBoolSix()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="senioritySix">{{ senioritySix }}, </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!senioritySix">Seniority, </span>

	    <span ng-cloak ng-show="job_functionSix">{{ job_functionSix }} </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!job_functionSix">job function </span>

	    <br>

    </h5>

    <span ng-cloak ng-show="organisation_nameSix">{{ organisation_nameSix }}, </span>
    <span class="ng-placeholder" ng-cloak ng-show="!organisation_nameSix">Organisation name, </span>

    <span ng-cloak ng-show="company_typeSix">{{ company_typeSix }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!company_typeSix">company type </span>

    <span ng-cloak> in the </span>

    <span ng-cloak ng-show="industrySix">{{ industrySix }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!industrySix"> industry </span>

   	<span ng-cloak> sector.</span> <br>

		<small>
			<span ng-cloak ng-show="year_startedSix">{{ year_startedSix }} </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_startedSix"> Year started </span>

    	<span ng-cloak> to </span>

    	<span ng-cloak ng-show="year_endedSix" >{{ year_endedSix }}. </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_endedSix"> date. </span> <br>


			<span ng-cloak ng-show="responsibilitiesSix"><pre ng-bind-html="responsibilitiesSix | unsafe"></pre></span>
    	<span class="ng-placeholder" ng-cloak ng-show="!responsibilitiesSix"> Responsibilities and achievements </span>

		</small>

  </div>







	<div class="editor-form-wrapper" ng-show="expandedSix">

		<div class="hint">
				<div class="bg-info og-info"><?php the_field('hint_experience', 'option'); ?></div>
		</div>




		<div class="fields-wrapper">


			<form class  ="form-horizontal" name="formSix">


				<div class="form-group">
					<label class="col-sm-4 control-label">Organisation name*</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_nameSix" value="<?php echo $experience_6['organisation_name']; ?>" >
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_nameSix) || organisation_nameSix.length > 100) &amp;&amp; organisation_nameSix.length > 0">Organization name can contain up to 100 characters.</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSix &amp;&amp; !organisation_nameSix">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Industry*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="industrySix">
							<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), $experience_6['industry']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSix &amp;&amp; !industrySix">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Company type*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="company_typeSix">
							<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), $experience_6['company_type']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSix &amp;&amp; !company_typeSix">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Job function*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="job_functionSix">
							<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), $experience_6['job_function']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSix &amp;&amp; !job_functionSix">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Seniority*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="senioritySix">
							<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), $experience_6['seniority']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSix &amp;&amp; !senioritySix">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year started*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_startedSix">
							<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_6['year_started']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSix &amp;&amp; !year_startedSix">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year ended*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_endedSix">
						<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_6['year_ended']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSix &amp;&amp; !year_endedSix">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Type of contract</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="contractSix">
						<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), $experience_6['contract']); ?>
						</select>
					</div>
				</div>

				<div class="form-group wysiwyg">
					<label class="col-sm-12 control-label  text-align-left">Responsibilities and achievements</label>
					<div class="col-sm-12">
						<summernote config="summernoteOptions" ng-model="responsibilitiesSix"><?php echo $experience_6['responsibilities']; ?></summernote>
					</div>
					<div  class="col-sm-12 og-msg og-error ng-cloak ng-hide" ng-show="(responsibilitiesSix.length > 500) &amp;&amp; responsibilitiesSix.length > 0">This field can contain up to 500 characters.</div>
				</div>


			</form>




			<div class="bottom">
				<div class="btn-group">
				  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a ng-click="deleteNode(6);">Delete</a></li>
				    <li role="separator" class="divider"></li>
				    <li><a >cancel</a></li>
				  </ul>
				</div>


				<div class="og-footer-error errors-above" ng-cloak ng-show="!validSix">Please correct errors above</div>
			</div>

		</div>



	</div>
</div>
