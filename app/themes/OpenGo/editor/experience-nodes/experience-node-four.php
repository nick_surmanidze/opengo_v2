<?php
global $experience_4;
?>
<div id="experience-node-four"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedFour]">

<span class="label label-warning unsaved-changes">unsaved changed</span>

  <div class="rendered" ng-cloak ng-click="toggleBoolFour()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="seniorityFour">{{ seniorityFour }}, </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!seniorityFour">Seniority, </span>

	    <span ng-cloak ng-show="job_functionFour">{{ job_functionFour }} </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!job_functionFour">job function </span>

	    <br>

    </h5>

    <span ng-cloak ng-show="organisation_nameFour">{{ organisation_nameFour }}, </span>
    <span class="ng-placeholder" ng-cloak ng-show="!organisation_nameFour">Organisation name, </span>

    <span ng-cloak ng-show="company_typeFour">{{ company_typeFour }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!company_typeFour">company type </span>

    <span ng-cloak> in the </span>

    <span ng-cloak ng-show="industryFour">{{ industryFour }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!industryFour"> industry </span>

   	<span ng-cloak> sector.</span> <br>

		<small>
			<span ng-cloak ng-show="year_startedFour">{{ year_startedFour }} </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_startedFour"> Year started </span>

    	<span ng-cloak> to </span>

    	<span ng-cloak ng-show="year_endedFour" >{{ year_endedFour }}. </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_endedFour"> date. </span> <br>


			<span ng-cloak ng-show="responsibilitiesFour"><pre ng-bind-html="responsibilitiesFour | unsafe"></pre></span>
    	<span class="ng-placeholder" ng-cloak ng-show="!responsibilitiesFour"> Responsibilities and achievements </span>

		</small>

  </div>







	<div class="editor-form-wrapper" ng-show="expandedFour">

		<div class="hint">
				<div class="bg-info og-info"><?php the_field('hint_experience', 'option'); ?></div>
		</div>




		<div class="fields-wrapper">


			<form class  ="form-horizontal" name="formFour">


				<div class="form-group">
					<label class="col-sm-4 control-label">Organisation name*</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_nameFour" value="<?php echo $experience_4['organisation_name']; ?>" >
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_nameFour) || organisation_nameFour.length > 100) &amp;&amp; organisation_nameFour.length > 0">Organization name can contain up to 100 characters.</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFour &amp;&amp; !organisation_nameFour">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Industry*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="industryFour">
							<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), $experience_4['industry']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFour &amp;&amp; !industryFour">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Company type*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="company_typeFour">
							<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), $experience_4['company_type']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFour &amp;&amp; !company_typeFour">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Job function*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="job_functionFour">
							<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), $experience_4['job_function']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFour &amp;&amp; !job_functionFour">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Seniority*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="seniorityFour">
							<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), $experience_4['seniority']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFour &amp;&amp; !seniorityFour">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year started*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_startedFour">
							<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_4['year_started']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFour &amp;&amp; !year_startedFour">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year ended*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_endedFour">
						<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_4['year_ended']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validFour &amp;&amp; !year_endedFour">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Type of contract</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="contractFour">
						<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), $experience_4['contract']); ?>
						</select>
					</div>
				</div>

				<div class="form-group wysiwyg">
					<label class="col-sm-12 control-label  text-align-left">Responsibilities and achievements</label>
					<div class="col-sm-12">
						<summernote config="summernoteOptions" ng-model="responsibilitiesFour"><?php echo $experience_4['responsibilities']; ?></summernote>
					</div>
					<div  class="col-sm-12 og-msg og-error ng-cloak ng-hide" ng-show="(responsibilitiesFour.length > 500) &amp;&amp; responsibilitiesFour.length > 0">This field can contain up to 500 characters.</div>
				</div>

			</form>




			<div class="bottom">
				<div class="btn-group">
				  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a ng-click="deleteNode(4);">Delete</a></li>
				    <li role="separator" class="divider"></li>
				    <li><a >cancel</a></li>
				  </ul>
				</div>


				<div class="og-footer-error errors-above" ng-cloak ng-show="!validFour">Please correct errors above</div>
			</div>

		</div>



	</div>
</div>
