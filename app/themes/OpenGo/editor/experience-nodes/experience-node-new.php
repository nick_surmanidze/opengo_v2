<div class="add-new-placeholder" ng-show="!addNewActive" ng-cloak>
	<h4> Add New Experience Entry </h4>
	<button class="btn btn-success" ng-click="addNewActive = true"><i class="fa fa-check-circle"></i> Add New </button>
</div>


<div id="experience-node-new"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedNew]" ng-show="addNewActive">
  <span class="label label-success create-new-label">New</span>
  <button class="btn btn-default cancel-btn" ng-click="addNewActive = false">cancel</button>
  <div class="rendered"  ng-cloak ng-click="toggleBoolNew()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="seniorityNew">{{ seniorityNew }}, </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!seniorityNew">Seniority, </span>

	    <span ng-cloak ng-show="job_functionNew">{{ job_functionNew }} </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!job_functionNew">job function </span>

	    <br>

    </h5>

    <span ng-cloak ng-show="organisation_nameNew">{{ organisation_nameNew }}, </span>
    <span class="ng-placeholder" ng-cloak ng-show="!organisation_nameNew">Organisation name, </span>

    <span ng-cloak ng-show="company_typeNew">{{ company_typeNew }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!company_typeNew">company type </span>

    <span ng-cloak> in the </span>

    <span ng-cloak ng-show="industryNew">{{ industryNew }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!industryNew"> industry </span>

   	<span ng-cloak> sector.</span> <br>

		<small>
			<span ng-cloak ng-show="year_startedNew">{{ year_startedNew }} </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_startedNew"> Year started </span>

    	<span ng-cloak> to </span>

    	<span ng-cloak ng-show="year_endedNew" >{{ year_endedNew }}. </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_endedNew"> date. </span> <br>


			<span ng-cloak ng-show="responsibilitiesNew"><pre ng-bind-html="responsibilitiesNew | unsafe"></pre></span>
    	<span class="ng-placeholder" ng-cloak ng-show="!responsibilitiesNew"> Responsibilities and achievements </span>

		</small>

  </div>







	<div class="editor-form-wrapper"  ng-show="expandedNew">

		<div class="hint">
				<div class="bg-info og-info"><?php the_field('hint_experience', 'option'); ?></div>
		</div>




		<div class="fields-wrapper">


			<form class  ="form-horizontal" name="formNew">


				<div class="form-group">
					<label class="col-sm-4 control-label">Organisation name*</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_nameNew">
					</div>



					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_nameNew) || organisation_nameNew.length > 100) &amp;&amp; organisation_nameNew.length > 0">Organization name can contain up to 100 characters.</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !organisation_nameNew">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Industry*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="industryNew">
							<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), 'nothing'); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !industryNew">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Company type*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="company_typeNew">
							<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), 'nothing'); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !company_typeNew">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Job function*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="job_functionNew">
							<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), 'nothing'); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !job_functionNew">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Seniority*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="seniorityNew">
							<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), 'nothing'); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !seniorityNew">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year started*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_startedNew">
							<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), 'nothing'); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !year_startedNew">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year ended*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_endedNew">
						<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), 'nothing'); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validNew &amp;&amp; !year_endedNew">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Type of contract</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="contractNew">
						<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), 'nothing'); ?>
						</select>
					</div>
				</div>

				<div class="form-group wysiwyg">
					<label class="col-sm-12 control-label  text-align-left">Responsibilities and achievements</label>
					<div class="col-sm-12">
						<summernote config="summernoteOptions" ng-model="responsibilitiesNew"></summernote>
					</div>
					<div  class="col-sm-12 og-msg og-error ng-cloak ng-hide" ng-show="(responsibilitiesNew.length > 500) &amp;&amp; responsibilitiesNew.length > 0">This field can contain up to 500 characters.</div>
				</div>
			</form>
		</div>
	</div>
</div>
