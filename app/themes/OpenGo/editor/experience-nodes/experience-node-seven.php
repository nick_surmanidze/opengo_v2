<?php
global $experience_7;
?>
<div id="experience-node-seven"  class="list-group-item" ng-cloak ng-class="{true: 'expanded', false: 'collapsed'}[expandedSeven]">

<span class="label label-warning unsaved-changes">unsaved changed</span>

  <div class="rendered" ng-cloak ng-click="toggleBoolSeven()">

    <h5 class="list-group-item-heading">

	    <span ng-cloak ng-show="senioritySeven">{{ senioritySeven }}, </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!senioritySeven">Seniority, </span>

	    <span ng-cloak ng-show="job_functionSeven">{{ job_functionSeven }} </span>
	    <span class="ng-placeholder" ng-cloak ng-show="!job_functionSeven">job function </span>

	    <br>

    </h5>

    <span ng-cloak ng-show="organisation_nameSeven">{{ organisation_nameSeven }}, </span>
    <span class="ng-placeholder" ng-cloak ng-show="!organisation_nameSeven">Organisation name, </span>

    <span ng-cloak ng-show="company_typeSeven">{{ company_typeSeven }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!company_typeSeven">company type </span>

    <span ng-cloak> in the </span>

    <span ng-cloak ng-show="industrySeven">{{ industrySeven }} </span>
    <span class="ng-placeholder" ng-cloak ng-show="!industrySeven"> industry </span>

   	<span ng-cloak> sector.</span> <br>

		<small>
			<span ng-cloak ng-show="year_startedSeven">{{ year_startedSeven }} </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_startedSeven"> Year started </span>

    	<span ng-cloak> to </span>

    	<span ng-cloak ng-show="year_endedSeven" >{{ year_endedSeven }}. </span>
    	<span class="ng-placeholder" ng-cloak ng-show="!year_endedSeven"> date. </span> <br>


			<span ng-cloak ng-show="responsibilitiesSeven"><pre ng-bind-html="responsibilitiesSeven | unsafe"></pre></span>
    	<span class="ng-placeholder" ng-cloak ng-show="!responsibilitiesSeven"> Responsibilities and achievements </span>

		</small>

  </div>







	<div class="editor-form-wrapper" ng-show="expandedSeven">

		<div class="hint">
				<div class="bg-info og-info"><?php the_field('hint_experience', 'option'); ?></div>
		</div>




		<div class="fields-wrapper">


			<form class  ="form-horizontal" name="formSeven">


				<div class="form-group">
					<label class="col-sm-4 control-label">Organisation name*</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_nameSeven" value="<?php echo $experience_7['organisation_name']; ?>">
					</div>



					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_nameSeven) || organisation_nameSeven.length > 100) &amp;&amp; organisation_nameSeven.length > 0">Organization name can contain up to 100 characters.</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSeven &amp;&amp; !organisation_nameSeven">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Industry*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="industrySeven">
							<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), $experience_7['industry']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSeven &amp;&amp; !industrySeven">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Company type*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="company_typeSeven">
							<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), $experience_7['company_type']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSeven &amp;&amp; !company_typeSeven">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Job function*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="job_functionSeven">
							<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), $experience_7['job_function']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSeven &amp;&amp; !job_functionSeven">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Seniority*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="senioritySeven">
							<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), $experience_7['seniority']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSeven &amp;&amp; !senioritySeven">This field is required.</div>

				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year started*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_startedSeven">
							<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_7['year_started']); ?>
						</select>
					</div>

					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSeven &amp;&amp; !year_startedSeven">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Year ended*</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="year_endedSeven">
						<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), $experience_7['year_ended']); ?>
						</select>
					</div>
					<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!validSeven &amp;&amp; !year_endedSeven">This field is required.</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label">Type of contract</label>
					<div class="col-sm-8 multiselect-wrapper">
						<select class="multiselect" style="visibility:hidden;" ng-model="contractSeven">
						<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), $experience_7['contract']); ?>
						</select>
					</div>
				</div>

				<div class="form-group wysiwyg">
					<label class="col-sm-12 control-label  text-align-left">Responsibilities and achievements</label>
					<div class="col-sm-12">
						<summernote config="summernoteOptions" ng-model="responsibilitiesSeven"><?php echo $experience_7['responsibilities']; ?></summernote>
					</div>
					<div  class="col-sm-12 og-msg og-error ng-cloak ng-hide" ng-show="(responsibilitiesSeven.length > 500) &amp;&amp; responsibilitiesSeven.length > 0">This field can contain up to 500 characters.</div>
				</div>


			</form>




			<div class="bottom">
				<div class="btn-group">
				  <button type="button" class="btn btn-danger btn-sm delete-btn delete-yn dropdown-toggle  extra-margin" ng-click="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-trash-o"></i> Delete <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a ng-click="deleteNode(7);">Delete</a></li>
				    <li role="separator" class="divider"></li>
				    <li><a >cancel</a></li>
				  </ul>
				</div>


				<div class="og-footer-error errors-above" ng-cloak ng-show="!validSeven">Please correct errors above</div>
			</div>

		</div>



	</div>
</div>
