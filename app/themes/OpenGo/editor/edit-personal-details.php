<?php
/**
 * Template Name: Edit Personal Details
 * Custom template.
 */
get_header();

$profile = opengo::get_profile();

$personal = array(
	'name'    => htmlspecialchars(stripslashes(opengo::check_selected($profile->first_name))),
	'surname' => htmlspecialchars(stripslashes(opengo::check_selected($profile->last_name))),
	'email'   => htmlspecialchars(stripslashes(opengo::check_selected($profile->email_address))),
	'phone'   => opengo::check_selected($profile->phone_number),
	'country' => opengo::check_selected($profile->country)
	// 'summary' => opengo::check_selected($profile->personal->personality)
	);


	global $api;
	$get_avatar = $api->sendRequest(array(
      'action'       => 'read',
      'controller'   => 'meta',
      'data_type'    => 'user',
      'data_id'      => opengo::user_logged_in(),
      'meta_key'     => 'avatar'
  ));
	$defaultAvatar = home_url() . "/app/themes/OpenGo/images/defaultavatar.jpg";
	$avatar_array = unserialize($get_avatar);
	if(isset($avatar_array['url'])) {
		$current_avatar = $avatar_array['url'];
	} else {
		$current_avatar = $defaultAvatar;
	}


$Public_preview_url = get_home_url() . opengo::get_my_cv_url();
$public_preview_btn = "<a class='public-preview-btn btn btn-default btn-sm' href=" . $Public_preview_url . " target='_blank'><i class='fa fa-file-text-o'></i>Public CV</a>";

?>

<script>
var defaultAvatar = "<?php echo $defaultAvatar; ?>";
</script>
	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general editor clearfix">

						<div class="page-title-wrapper">
							<h1>Editor</h1>
						</div>
						<article class="page-content-wrapper editor-wrapper">

							<div class="sidebar">
								<?php get_template_part('editor/editor-left-sidebar'); ?>
							</div>


							<div id="editor-personal-details" class="editor-content clearfix">



								<h2 class="fields-group-title">Edit personal detals</h2>
								<div class="editor-flex">

									<div class="hint">
											<div class="bg-info og-info"><?php the_field('hint_personal', 'option'); ?></div>
									</div>

									<div class="fields-wrapper">

										<div class="splash">

											<div class="preloader">
												<div class="cssload-thecube">
													<div class="cssload-cube cssload-c1"></div>
													<div class="cssload-cube cssload-c2"></div>
													<div class="cssload-cube cssload-c4"></div>
													<div class="cssload-cube cssload-c3"></div>
												</div>
											</div>

								      <h3>LOADING</h3>
								    </div>

										<form class  ="form-horizontal" >



											<div class="form-group image-uploader">

												<div class="col-sm-8 col-sm-offset-4">

												<div class="row">
													<div class="col-sm-7">
														<div id="avatar-dropzone" class="dropzone" >
															<img class="avatar" src="<?php echo $current_avatar; ?>" alt="">
														</div>
													</div>

													<div class="col-sm-5">
														<div id="upload-controls" >
															<button type="button"  id="upload-new-avatar" class="btn btn-success btn btn-block">Upload Image</button>
															<?php
															if($defaultAvatar === $current_avatar) { ?>
																<button type="button" id="dz-remove-file" style="display:none;" class="btn btn-opengo-red btn btn-block">Remove</a>
															<?php } else { ?>
																<button type="button" id="dz-remove-file" class="btn btn-opengo-red btn btn-block">Remove</a>
															<?php } ?>

														</div>

														<div class="info upload-error">
															File is too large.
														</div>

													</div>

												</div>



												</div>

											</div>



											<div class="form-group">
												<label class="col-sm-4 control-label">Name *</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="name" placeholder="Your Name" value="<?php echo $personal['name']; ?>">
												</div>
												<div  class="col-sm-8 og-msg og-error">Name can contain up to 40 characters and should not contain any special symbols.</div>
											</div>


											<div class="form-group">
												<label class="col-sm-4 control-label">Surname *</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="surname" placeholder="Your Surname" value="<?php echo $personal['surname']; ?>">
												</div>

												<div  class="col-sm-8 og-msg og-error">Surname can contain up to 40 characters and should not contain any special symbols.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Email Address *</label>
												<div class="col-sm-8">
													<input type="email" class="form-control" id="email" placeholder="mail@example.com" value="<?php echo $personal['email']; ?>">
												</div>
												<div  class="col-sm-8 og-msg og-error">Please enter a valid email address.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Phone</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="phone" placeholder="+1234567890" ng-model="phone" value="<?php echo $personal['phone']; ?>">
												</div>
												<div  class="col-sm-8 og-msg og-error">Phone number should consist of numbers and + symbol.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Country</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="country" class="multiselect-filter" style="visibility:hidden;" ng-model="country">
													<?php echo list_options_from_textarea(get_field('pdl_country', 'option'), $personal['country']); ?>
													</select>
												</div>
											</div>


											<div class="form-group">
												<h3 class="col-sm-12 control-label text-align-left" style="padding-bottom:20px;">Cover Letter</h3>
												<div class="col-sm-12">
													<p><?php echo opengo::generate_cover_letter('<br/>', $profile, true); ?></p>
												</div>
											</div>

										</form>
									</div>
								</div>
								<div class="bottom">

									<span id="save-btn" class="btn btn-primary btn-sm save-btn" ng-click="log()" ><i class="fa fa-check-circle-o"></i> Save</span>
									<?php  echo $public_preview_btn; ?>
									<div class="og-footer-error errors-required-missing og-hidden">Please fill all required fields</div>
									<div class="og-footer-error errors-above og-hidden">Please correct errors above</div>


									<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"> Updating..</span>

									<span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
									<span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
								</div>
							</div>
						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
