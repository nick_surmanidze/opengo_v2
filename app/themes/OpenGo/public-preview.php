<?php
/**
 * Template Name: Public Preview
 * Custom template.
 */
get_header();

$slug = get_query_var('profile_slug');

// if(isset($_GET['cv']) && strlen($_GET['cv']) > 0 ) {
// 	$id = base64_decode($_GET['cv']);
// }

$profile = opengo::get_profile_by_slug($slug);

	$ideal = array(
		'function'      => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_function),
		'seniority'     => opengo::check_selected($profile->ideal_job->ideal_job_seniority),
		'industry'      => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_industry),
		'location'      => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_location),
		'company_type'  => opengo::check_selected($profile->ideal_job->og_pr_ideal_job_company_type),
		'contract_type' => opengo::check_selected($profile->ideal_job->ideal_job_contract_type),
		'salary'        => opengo::check_selected($profile->ideal_job->ideal_job_basic_salary),
		'currency'      => opengo::check_selected($profile->ideal_job->ideal_job_basic_salary_currency)
		);


		$personal = array(
			'name'    => opengo::check_selected($profile->first_name),
			'surname' => opengo::check_selected($profile->last_name),
			'email'   => opengo::check_selected($profile->email_address),
			'phone'   => opengo::check_selected($profile->phone_number),
			'country' => opengo::check_selected($profile->country),
			'summary' => stripslashes(opengo::check_selected($profile->personal->personality))
		);



	if (opengo::completed_current_job($profile)) {
		$current = array(
			'organisation_name' => opengo::check_selected($profile->current_job->current_job_organisation_name),
			'industry'          => opengo::check_selected($profile->current_job->current_job_industry),
			'company_type'      => opengo::check_selected($profile->current_job->current_job_company_type),
			'job_function'      => opengo::check_selected($profile->current_job->current_job_function),
			'seniority'         => opengo::check_selected($profile->current_job->current_job_seniority),
			'year_started'      => opengo::check_selected($profile->current_job->current_job_year_started),
			'contract'          => opengo::check_selected($profile->current_job->current_job_type_of_contract),
			'notice_period'     => opengo::check_selected($profile->current_job->current_job_notice_period),
			'salary'            => opengo::check_selected($profile->current_job->current_job_basic_salary),
			'currency'          => opengo::check_selected($profile->current_job->current_job_basic_salary_currency),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->current_job->current_job_main_responsibilities))
		);
	}


	if (opengo::completed_experience_one($profile)) {
		$experience_1 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_1->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_1->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_1->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_1->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_1->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_1->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_1->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_1->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_1->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_two($profile)) {
		$experience_2 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_2->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_2->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_2->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_2->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_2->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_2->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_2->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_2->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_2->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_three($profile)) {
		$experience_3 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_3->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_3->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_3->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_3->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_3->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_3->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_3->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_3->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_3->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_four($profile)) {
		$experience_4 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_4->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_4->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_4->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_4->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_4->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_4->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_4->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_4->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_4->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_five($profile)) {
		$experience_5 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_5->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_5->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_5->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_5->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_5->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_5->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_5->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_5->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_5->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_six($profile)) {
		$experience_6 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_6->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_6->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_6->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_6->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_6->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_6->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_6->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_6->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_6->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_experience_seven($profile)) {
		$experience_7 = array(
			'organisation_name' => stripslashes(opengo::check_selected($profile->experience_7->previous_exp_organisation_name)),
			'industry'          => opengo::check_selected($profile->experience_7->previous_exp_industry),
			'company_type'      => opengo::check_selected($profile->experience_7->previous_exp_company_type),
			'job_function'      => opengo::check_selected($profile->experience_7->previous_exp_job_function),
			'seniority'         => opengo::check_selected($profile->experience_7->previous_exp_seniority),
			'year_started'      => opengo::check_selected($profile->experience_7->previous_exp_year_started),
			'year_ended'        => opengo::check_selected($profile->experience_7->previous_exp_year_ended),
			'contract'          => opengo::check_selected($profile->experience_7->previous_exp_type_of_contract),
			'responsibilities'  => stripslashes(opengo::check_selected($profile->experience_7->previous_exp_main_responsibilities))
		);
	}

	if (opengo::completed_education_one($profile)) {
		$education_1 = array(
			'university'    => stripslashes(opengo::check_selected($profile->education_1->university)),
			'course_name'   => stripslashes(opengo::check_selected($profile->education_1->course_name)),
			'qualification' => opengo::check_selected($profile->education_1->qualification_type),
			'grade'         => opengo::check_selected($profile->education_1->grade_attained),
			'year'          => opengo::check_selected($profile->education_1->year_attained)
		);
	}

	if (opengo::completed_education_two($profile)) {
		$education_2 = array(
			'university'    => stripslashes(opengo::check_selected($profile->education_2->university)),
			'course_name'   => stripslashes(opengo::check_selected($profile->education_2->course_name)),
			'qualification' => opengo::check_selected($profile->education_2->qualification_type),
			'grade'         => opengo::check_selected($profile->education_2->grade_attained),
			'year'          => opengo::check_selected($profile->education_2->year_attained)
		);
	}

	if (opengo::completed_education_three($profile)) {
		$education_3 = array(
			'university'    => stripslashes(opengo::check_selected($profile->education_3->university)),
			'course_name'   => stripslashes(opengo::check_selected($profile->education_3->course_name)),
			'qualification' => opengo::check_selected($profile->education_3->qualification_type),
			'grade'         => opengo::check_selected($profile->education_3->grade_attained),
			'year'          => opengo::check_selected($profile->education_3->year_attained)
		);
	}

	if (opengo::completed_skills($profile)) {
		$skill = array(
				'one'   => opengo::check_selected($profile->skills->specialist_skill_1),
				'two'   => opengo::check_selected($profile->skills->specialist_skill_2),
				'three' => opengo::check_selected($profile->skills->specialist_skill_3)
		);
	}



//check if at least one section isset

if(isset($profile->id) && $profile->id > 0) {
?>




	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">

			<div class="blue-part"></div>
			<div class="content-part">
					<div class="middle-section">
						<section class="page-general  clearfix">

							<div id="public-profile">

								<div class="content">


									<section class="section profile row">

										<h3 class="name"><?php echo $personal['name'] . ' ' . $personal['surname']; ?></h3>
										<div class="contacts-wrapper">
										<?php if(isset($personal['email']) && strlen($personal['email']) > 0) { ?>
												<span class="mail"><i class="fa fa-envelope-o"></i><?php echo $personal['email']; ?></span>
										<?php } ?>
										<?php if(isset($personal['phone']) && strlen($personal['phone']) > 0) { ?>
											<span class="phone"><i class="fa fa-phone"></i><?php echo $personal['phone']; ?></span>
										<?php } ?>
										</div>

										<?php if(isset($personal['summary']) && strlen(strip_tags($personal['summary'])) > 0) { ?>
										<div class="subtitle col-md-12">
											<h2>Summary</h2>

										</div>


										<div class="summary col-md-12">
											<?php echo $personal['summary']; ?>
										</div>

										<?php } else { ?>



										<?php } ?>
										<?php if(opengo::completed_current_job($profile) || opengo::completed_experience_one($profile)) { ?>
										<div class="subtitle col-md-12">
											<h2>Career History and Key Achievements</h2>
										</div>
<div class="career timeline-wrapper col-md-12">
										<?php if(opengo::completed_current_job($profile)) { ?>



											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $current['seniority']; ?>, </span>
															    <span><?php echo $current['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $current['organisation_name']; ?>, </span>
															    <span><?php echo $current['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $current['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $current['year_started']; ?> </span><span> to </span>
															    <span>date. </span><br>
																<span><pre><?php echo $current['responsibilities']; ?></pre></span>
															</small>

														</div>
													</div>
												</div>
											</div>

											<?php } ?>

											<?php if(opengo::completed_experience_one($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_1['seniority']; ?>, </span>
															    <span><?php echo $experience_1['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_1['organisation_name']; ?>, </span>
															    <span><?php echo $experience_1['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_1['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_1['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_1['year_ended']; ?>. </span><br>
																<span><pre><?php echo $experience_1['responsibilities']; ?></pre></span>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_experience_two($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_2['seniority']; ?>, </span>
															    <span><?php echo $experience_2['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_2['organisation_name']; ?>, </span>
															    <span><?php echo $experience_2['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_2['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_2['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_2['year_ended']; ?>. </span><br>
																<span><pre><?php echo $experience_2['responsibilities']; ?></pre></span>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_experience_three($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_3['seniority']; ?>, </span>
															    <span><?php echo $experience_3['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_3['organisation_name']; ?>, </span>
															    <span><?php echo $experience_3['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_3['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_3['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_3['year_ended']; ?>. </span><br>
																<span><pre><?php echo $experience_3['responsibilities']; ?></pre></span>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_experience_four($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_4['seniority']; ?>, </span>
															    <span><?php echo $experience_4['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_4['organisation_name']; ?>, </span>
															    <span><?php echo $experience_4['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_4['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_4['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_4['year_ended']; ?>. </span><br>
																<span><pre><?php echo $experience_4['responsibilities']; ?></pre></span>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_experience_five($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_5['seniority']; ?>, </span>
															    <span><?php echo $experience_5['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_5['organisation_name']; ?>, </span>
															    <span><?php echo $experience_5['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_5['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_5['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_5['year_ended']; ?>. </span><br>
																<span><pre><?php echo $experience_5['responsibilities']; ?></pre></span>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>

											<?php if(opengo::completed_experience_six($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_6['seniority']; ?>, </span>
															    <span><?php echo $experience_6['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_6['organisation_name']; ?>, </span>
															    <span><?php echo $experience_6['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_6['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_6['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_6['year_ended']; ?>. </span><br>
																<span><pre><?php echo $experience_6['responsibilities']; ?></pre></span>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>

											<?php if(opengo::completed_experience_seven($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">

															<h5 class="list-group-item-heading">
															    <span><?php echo $experience_7['seniority']; ?>, </span>
															    <span><?php echo $experience_7['job_function']; ?> </span>
															    <br>
															</h5>
															    <span><?php echo $experience_7['organisation_name']; ?>, </span>
															    <span><?php echo $experience_7['company_type']; ?> </span>
															    <span> in the </span>
															    <span><?php echo $experience_7['industry']; ?> </span>
															   	<span> sector.</span> <br>
															<small>
																<span><?php echo $experience_7['year_started']; ?> </span><span> to </span>
															    <span> <?php echo $experience_7['year_ended']; ?>. </span><br>
																<span><pre><?php echo $experience_7['responsibilities']; ?></pre></span>
															</small>

														</div>
													</div>
												</div>
											</div>
											<?php } ?>

										</div>
										<?php } ?>




										<?php if(opengo::completed_education_one($profile) || opengo::completed_skills($profile)) { ?>
										<div class="subtitle col-md-12">
											<h2>Education and Skills</h2>
										</div>


										<div class="education timeline-wrapper col-md-12">

											<?php if(opengo::completed_education_one($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">
															<h5 class="list-group-item-heading">
															    <span><?php echo $education_1['university'] ?></span>
															</h5>
															<span><?php echo $education_1['course_name'] ?></span>
															<span>(<?php echo $education_1['qualification'] ?>)</span><br>
															<span>Grade Attained: <?php echo $education_1['grade'] ?></span><br>
															<small>
															    <span>Awarded in <?php echo $education_1['year'] ?></span>
															</small>
														</div>
													</div>
												</div>
											</div>
											<?php } ?>


											<?php if(opengo::completed_education_two($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">
															<h5 class="list-group-item-heading">
															    <span><?php echo $education_2['university'] ?></span>
															</h5>
															<span><?php echo $education_2['course_name'] ?></span>
															<span>(<?php echo $education_2['qualification'] ?>)</span><br>
															<span>Grade Attained: <?php echo $education_2['grade'] ?></span><br>
															<small>
															    <span>Awarded in <?php echo $education_2['year'] ?></span>
															</small>
														</div>
													</div>
												</div>
											</div>
											<?php } ?>

											<?php if(opengo::completed_education_three($profile)) { ?>
											<div class="item">
												<div class="timeline">
													<div class="circle"></div>
													<div class="line"></div>
												</div>
												<div class="item-content">
													<div class="panel panel-default">
														<div class="panel-body">
															<h5 class="list-group-item-heading">
															    <span><?php echo $education_3['university'] ?></span>
															</h5>
															<span><?php echo $education_3['course_name'] ?></span>
															<span>(<?php echo $education_3['qualification'] ?>)</span><br>
															<span>Grade Attained: <?php echo $education_3['grade'] ?></span><br>
															<small>
															    <span>Awarded in <?php echo $education_3['year'] ?></span>
															</small>
														</div>
													</div>
												</div>
											</div>
											<?php } ?>

										</div>



									<?php if(opengo::completed_skills($profile)) { ?>
										<div class="subtitle col-md-12">
											<h2>Specialist and Technical Skills</h2>
										</div>


										<div class="skills col-md-12">
											<ul class="list-group">
											<?php

											if(isset($skill['one']) && strlen($skill['one']) > 0) {
												echo "<li class='list-group-item'>".$skill['one']."</li>";
											}
											if(isset($skill['two']) && strlen($skill['two']) > 0) {
												echo "<li class='list-group-item'>".$skill['two']."</li>";
											}
											if(isset($skill['three']) && strlen($skill['three']) > 0) {
												echo "<li class='list-group-item'>".$skill['three']."</li>";
											}

											?>

											</ul>
										</div>

 									<?php } ?>
									<?php } ?>




									</section>


								</div>

							</div> <!-- #public-profile-->

						</section><!-- .page-general-->
					</div>

			</div>


		</main><!-- #main -->
	</div><!-- #primary -->
<?php

} else { ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general  clearfix">
						<article class="page-content-wrapper error-wrapper">
							<h1>404</h1>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/404.jpg" alt="404">
							<h3>Seems like the page you've requested does not exist</h3>
						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

}

 get_footer(); ?>
