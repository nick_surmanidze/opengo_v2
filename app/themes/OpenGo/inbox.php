<?php
/**
 * Template Name: Inbox
 * Custom template.
 */
get_header();


$user_id = opengo::user_logged_in();

/**
 * We have three scenarios:
 * inbox  - returns all incoming mail,
 * sent   - all outgoing,
 * Unread - all new
 */

//decide which function to use
$section = 'inbox';
#$output = get_messages($from, $to, $query, $subject, $read, $limit, $offset);

if(isset($_GET['show'])) {

	if($_GET['show'] == 'sent') {
		// we are in 'sent'
		$section = 'sent';

		$output = opengo::get_messages($user_id, 0, '', '', 1, 10, 0);

	} elseif($_GET['show'] == 'unread') {
		// we're in 'unread'
		$section = 'unread';
		$output = opengo::get_messages(0, $user_id, '', '', 0, 10, 0);

	} elseif($_GET['show'] == 'inbox') {
		// we're in 'inbox'
		$section = 'inbox';
		$output = opengo::get_messages(0, $user_id, '', '', 1, 10, 0);

	}

} else {
	// we are in the inbox
  $section = 'inbox';
  $output = opengo::get_messages(0, $user_id, '', '', 1, 10, 0);

}
$total = 0;
if($output->total > 0 ) {
$total = $output->total;
}

?>

<script type="text/javascript"> var section = "<?php echo $section; ?>";</script>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">

			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general inbox clearfix">

						<div class="page-title-wrapper">
							<h1>Inbox</h1>
						</div>

						<article class="page-content-wrapper">

							<div class="inbox-wrapper">
								<div class="inbox-nav">
									<div class="list-group">
									  <a href="<?php get_home_url();?>/inbox?show=inbox" class="list-group-item <?php if(is_current_item('show', 'inbox') || !isset($_GET['show'])) {echo "active"; }?>"><i class="fa fa-inbox"></i>Inbox

										<?php $new = opengo::get_number_of_new_messages();
										if($new > 0) { ?>
											<span class="badge header-badge"><?php echo $new; ?></span>
										<?php } else { ?>
											<span class="badge header-badge grey">0</span>
										<?php } ?>

									  </a>
									  <a href="<?php get_home_url();?>/inbox?show=sent" class="list-group-item <?php is_current_item('show', 'sent'); ?>"><i class="fa fa-paper-plane"></i>Sent</a>
									  <a href="<?php get_home_url();?>/inbox?show=unread" class="list-group-item <?php is_current_item('show', 'unread'); ?>"><i class="fa fa-exclamation"></i>Unread
											<?php
											if($new > 0) { ?>
												<span class="badge header-badge"><?php echo $new; ?></span>
											<?php } else { ?>
												<span class="badge header-badge grey">0</span>
											<?php } ?>
									  </a>
									</div>

								</div>
								<div class="inbox-content">
									<div class="list-wrapper">
									<div class="loader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader-grey.gif'; ?>"></div>
										<div class="list-group">
											<?php
											$number = (array) $output->messages;
											if (count($number) > 0) {
												if($section == 'sent') {
												foreach((array) $output->messages as $message) { ?>
													<a href="<?php get_home_url(); ?>/conversation?from-user=<?php echo $message->from_user_id; ?>&amp;to-user=<?php echo $message->to_user_id; ?>&amp;subject=<?php echo $message->message_subject; ?>" class="list-group-item inbox-item">
												    <h4 class="list-group-item-heading"><span class="direction">TO:</span><?php echo $message->sent_to; ?> / <span class="subject"><?php echo $message->message_subject; ?></span></h4>
												    <p class="list-group-item-text"><?php echo $message->message_body; ?></p>
												  </a>
											 <?php } // endforeach
												} else {
												foreach((array) $output->messages as $message) { ?>
													<a href="<?php get_home_url(); ?>/conversation?from-user=<?php echo $message->from_user_id; ?>&amp;to-user=<?php echo $message->to_user_id; ?>&amp;subject=<?php echo $message->message_subject; ?>" class="list-group-item inbox-item">
												    <h4 class="list-group-item-heading"><span class="direction">FROM:</span><?php echo $message->sent_from; ?> / <span class="subject"><?php echo $message->message_subject; ?></span></h4>
												    <?php if($message->message_read == 0) { ?>
																<span class="label label-danger">New</span>
												    <?php	} ?>
												    <p class="list-group-item-text"><?php echo $message->message_body; ?></p>
												  </a>
											 <?php } // endforeach
											}
											} else { ?>
											<div class="no-messages">There are no messages!</div>
											<? } ?>

										</div>
									</div>
								</div>
							</div>
              <div class="load-more-btn-wrapper">
                <span class='counter'><span class='loaded'><?php echo count((array) $output->messages); ?></span> out of <span class='total'><?php echo $total; ?></span></span>
                <?php if(count((array) $output->messages) < $total) { ?>
                    <button class="btn btn-sm btn-success" id="load-more-conversations">Load More ...</button>
                <?php } ?>
              </div>
						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
