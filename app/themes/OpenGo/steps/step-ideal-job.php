<?php
/**
 * Template Name: Add Ideal Job
 * Custom template.
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general editor clearfix">

						<div class="page-title-wrapper">
							<h1>Registration</h1>
						</div>
						<article class="page-content-wrapper editor-wrapper registration-step">

							<!-- NG-APP -->
							<div id="editor-ideal-job" class="editor-content clearfix" ng-app="ogAddIdealJobApp" ng-controller="addIdealJobController">
										<h2 class="fields-group-title">
											What type of job are you looking for?
										</h2>
								<div class="editor-flex">

									<div class="hint">
											<div class="bg-info og-info"><?php the_field('hint_ideal_job', 'option'); ?></div>
									</div>

									<div class="fields-wrapper">
										<div class="splash" ng-cloak>

											<div class="preloader">
												<div class="cssload-thecube">
													<div class="cssload-cube cssload-c1"></div>
													<div class="cssload-cube cssload-c2"></div>
													<div class="cssload-cube cssload-c4"></div>
													<div class="cssload-cube cssload-c3"></div>
												</div>
											</div>

								      <h3>LOADING</h3>
								    </div>

										<form class  ="form-horizontal" ng-cloak>


											<div class="form-group">
												<label class="col-sm-4 control-label">Job function</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_job_function">
													<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), get_current_profile_value("country"), false, false); ?>
													</select>
												</div>
											</div>


											<div class="form-group">
												<label class="col-sm-4 control-label">Seniority</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="seniority" class="multiselect" style="visibility:hidden;" ng-model="ideal_job_seniority">
													<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
											</div>



											<div class="form-group">
												<label class="col-sm-4 control-label">Industry</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="industry" class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_job_industry">
														<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), get_current_profile_value("country"), false, false, 'Any'); ?>
													</select>
												</div>
											</div>



											<div class="form-group">
												<label class="col-sm-4 control-label">Location</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="location" class="multiselect-limit-three" style="visibility:hidden;"  multiple="multiple" ng-model="ideal_job_location">
													<?php echo list_options_from_textarea(get_field('pdl_ideal_job_location', 'option'), get_current_profile_value("country"), false, false, 'Any'); ?>
													</select>
												</div>
											</div>


											<div class="form-group multiselect-wrapper">
												<label class="col-sm-4 control-label">Company type</label>
												<div class="col-sm-8">
													<select id="company_type" class="multiselect-limit-three" style="visibility:hidden;" multiple="multiple" ng-model="ideal_job_company_type">
													<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), get_current_profile_value("country"), false, false, 'Any'); ?>
													</select>
												</div>
											</div>



											<div class="form-group">
												<label class="col-sm-4 control-label">Contract type</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="contract_type" class="multiselect" style="visibility:hidden;" ng-model="ideal_job_contract">
													<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), get_current_profile_value("country"), false, true, 'Any'); ?>
													</select>
												</div>
											</div>


											<div class="form-group">
												<label class="col-sm-4 control-label">Basic salary</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="basic_salary" class="multiselect" style="visibility:hidden;" ng-model="ideal_job_salary">
													<?php echo list_options_from_textarea(get_field('pdl_salary', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Currency</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="currency" class="multiselect" style="visibility:hidden;" ng-model="ideal_job_currency">
													<?php echo list_options_from_textarea_currency(get_field('pdl_currency', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
											</div>

										</form>

									</div>

								</div>

								<div class="bottom">
									<span id="save-btn" class="btn btn-primary btn-sm save-btn" ng-click="update()" ng-cloak><i class="fa fa-check-circle-o"></i> Save</span>
									<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"> Updating..</span>
									<span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
									<span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
								</div>
							</div>


						</article>
					</section><!-- .page-general-->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>

