<?php
/**
 * Template Name: Add Current Job
 * Custom template.
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix" role="main">
			<div class="blue-part"></div>
			<div class="content-part">
				<div class="middle-section">
					<section class="page-general editor clearfix">

						<div class="page-title-wrapper">
							<h1>Registration</h1>
						</div>
						<article class="page-content-wrapper editor-wrapper registration-step">

							<div class="editor-content clearfix" ng-app="ogAddCurrentJobApp" ng-controller="addCurrentJobController">
										<h2 class="fields-group-title">
											What job are you doing now?
										</h2>
								<div class="editor-flex">
									<div class="hint">
											<div class="bg-info og-info"><?php the_field('hint_current_job', 'option'); ?></div>
									</div>
									<div class="fields-wrapper">

											<div class="splash" ng-cloak>

												<div class="preloader">
													<div class="cssload-thecube">
														<div class="cssload-cube cssload-c1"></div>
														<div class="cssload-cube cssload-c2"></div>
														<div class="cssload-cube cssload-c4"></div>
														<div class="cssload-cube cssload-c3"></div>
													</div>
												</div>

									      <h3>LOADING</h3>
									    </div>


										<form class  ="form-horizontal"  ng-cloak>


											<div class="form-group">
												<label class="col-sm-4 control-label">Organisation name*</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="name" placeholder="Company LTD" ng-model="organisation_name">
												</div>
												<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="(!is_alpha(organisation_name) || organisation_name.length > 100) &amp;&amp; organisation_name.length > 0">Organisation name can contain up to 100 characters and should not contain any special symbols.</div>
												<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !organisation_name">This field is required.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Industry*</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect" style="visibility:hidden;" ng-model="industry">
													<?php echo list_options_from_textarea(get_field('pdl_industry', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
												<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !industry">This field is required.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Company type*</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect" style="visibility:hidden;" ng-model="company_type">
													<?php echo list_options_from_textarea(get_field('pdl_company_type', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
												<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !company_type">This field is required.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Job function*</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect" style="visibility:hidden;" ng-model="job_function">
													<?php echo list_options_from_textarea(get_field('pdl_job_function', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
												<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !job_function">This field is required.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Seniority*</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect" style="visibility:hidden;" ng-model="seniority">
													<?php echo list_options_from_textarea(get_field('pdl_seniority', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
												<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !seniority">This field is required.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Year started*</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect" style="visibility:hidden;" ng-model="year_started">
													<?php echo list_options_from_textarea(get_field('pdl_year', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
												<div  class="col-sm-8 og-msg og-error ng-cloak ng-hide" ng-show="!valid &amp;&amp; !year_started">This field is required.</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Type of contract</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect" style="visibility:hidden;" ng-model="contract">
													<?php echo list_options_from_textarea(get_field('pdl_contract', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Notice period</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect" style="visibility:hidden;" ng-model="notice_period">
													<?php echo list_options_from_textarea(get_field('pdl_notice_period', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Basic salary</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect" style="visibility:hidden;" ng-model="current_salary">
													<?php echo list_options_from_textarea(get_field('pdl_salary', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 control-label">Currency</label>
												<div class="col-sm-8 multiselect-wrapper">
													<select id="job_function" class="multiselect" style="visibility:hidden;" ng-model="current_currency">
													<?php echo list_options_from_textarea_currency(get_field('pdl_currency', 'option'), get_current_profile_value("country")); ?>
													</select>
												</div>
											</div>


										<div class="form-group wysiwyg">
											<label class="col-sm-12 control-label text-align-left">Responsibilities and achievements</label>
											<div class="col-sm-12">
											<summernote config="summernoteOptions" ng-model="responsibilities" ><?php echo stripslashes($current['responsibilities']); ?></summernote>
											</div>
										</div>

										</form>
									</div>
								</div>
								<div class="bottom">
									<span id="save-btn" class="btn btn-primary btn-sm save-btn" ng-click="validate()" ng-cloak><i class="fa fa-check-circle-o"></i> Save</span>
									<div class="og-footer-error errors-above" ng-cloak ng-show="!valid">Please correct errors above</div>
									<span class="item-preloader"><img src="<?php echo get_stylesheet_directory_uri() . '/images/preloader.gif'; ?>"> Updating..</span>
									<span class="item-success-msg"><i class="fa fa-check-circle"></i> Done </span>
									<span class="item-error-msg"><i class="fa fa-times-circle"></i> Failed </span>
								</div>
							</div>

						</article>
					</section><!-- .page-general-->
				</div>
			</div>

<button data-toggle="modal" data-target="#profile-complete-modal">hello</button>


  <!-- Modal Begin -->
    <div class="modal fade profile-complete-modal" id="profile-complete-modal" tabindex="-1" role="dialog" aria-labelledby="profile-complete-modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="profile-complete-modal">Basic profile created!</h4>
          </div>
          <div class="modal-body clearfix share-profile">
          	<div class="lead-text">
          		Great! You have created a basic career profile. We'll now direct you to our editor so you can add additional experience, education details and skills.
          	</div>
						<div class="button-wrapper">

							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-12 text-center main-countdown">
									 You will be redirected in <span class="countdown">5</span> seconds.
									</div>
									<div class="col-xs-12 text-center">
									 <a href="/edit-personal-details" class="btn btn-opengo-red">Complete my career profile</a>
									</div>

								</div>

							</div>

						</div>

          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal END -->




		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
